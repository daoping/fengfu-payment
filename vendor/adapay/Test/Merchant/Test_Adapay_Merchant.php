<?php
# 加载SDK需要的文件
include_once dirname(__FILE__) . '/../../AdapayMerchantSdk/init.php';
# 加载商户的配置文件
include_once dirname(__FILE__) . '/config.php';

# 初始化进件类
$merchant = new \AdaPayMerchant\MerchantUser();
$merchant_params = [
    "request_id"=> 'req_mer_'.date("YmdHis").rand(100000, 999999),
    "usr_phone"=> "1300001".rand(1000,9999),
    "cont_name"=> "风清扬",
    "cont_phone"=> "13100001234",
    "customer_email"=> "cuif".rand(1000, 9999).'@163.com',
    "mer_name"=> "河南通达电缆股份有限公司a",
    "mer_short_name"=> "通达电缆",
    "license_code"=> "91410300X148288455",
    "reg_addr"=> "测试地址zaac",
    "cust_addr"=> "测试地址zaac",
    "cust_tel"=> "13333783701",
    "mer_valid_date"=> "20201010",
    "legal_name"=> "史万福",
    "legal_type"=> "0",
    "legal_idno"=> "321121198606115128",
    "legal_mp"=> "13333333300",
    "legal_id_expires"=> "20300101",
    "card_id_mask"=> "6222021703001692228",
    "bank_code"=> "01020000",
    "card_name"=> "农行银行",
    "bank_acct_type"=> "1",
    "prov_code"=> "1100",
    "area_code"=> "0011",
    "legal_start_cert_id_expires"=> "20300101",
    "mer_start_valid_date"=> "20300101",
    "rsa_public_key"=> "016515646131sdasd1as32d13as2d13asd13",
];

# 发起进件
$merchant->create($merchant_params);
echoExecuteResult($merchant, "商户进件创建");
# 初始化进件类
$merchant = new \AdaPayMerchant\MerchantUser();
# 进件成功后的request_id
# 发起进件
$merchant->query(["request_id"=> "req_mer_20190912013859492871"]);
echoExecuteResult($merchant, "商户进件查询");



# 初始化支付配置类
$merConfig = new \AdaPayMerchant\MerchantConf();

$mer_config_params = [
    "request_id"=> "req_cfg_".date("YmdHis").rand(100000, 999999),
    # 推荐商户的api_key
    "sub_api_key"=> "",
    # 银行渠道号
    "bank_channel_no"=> "",
    # 费率
    "fee_type"=> "1",
    # 商户app_id
    "app_id"=> "app_0c2acc98-7437-4de6-ad4c-7c38a0c782e4",
    # 微信经营类目
    "wx_category"=> "服装类",
    # 支付宝经营类目
    "alipay_category"=> "服装",
    "cls_id"=> "01",
    # 服务商模式
    "model_type"=> "1",
    # 商户种类
    "mer_type"=> "1",
    # 省份code
    "province_code"=> "01",
    # 城市code
    "city_code"=> "11",
    # 县区code
    "district_code"=> "21",
    # 配置信息值
    "add_value_list"=> json_encode(['wx_lite'=> ['appid'=> '13213123123123']])
];

# 发起支付配置
$merConfig->create($mer_config_params);
echoExecuteResult($merConfig, "商户进件配置创建");


$merConfig = new \AdaPayMerchant\MerchantConf();

$mer_config_params = [
    "request_id"=> "req_cfg_".date("YmdHis").rand(100000, 999999),
    # 推荐商户的api_key
    "sub_api_key"=> "api_live_826e0027-68b4-4468-ada8-aa35babdc86e",
    "alipay_request_params"=> json_encode([
        "mer_short_name"=> "尚云科技",
        "mer_phone"=> "18919876721",
        "fee_type"=> "02",
        "card_no"=> "6227000734730752177",
        "card_name"=> "浦发银行",
        "category"=> "2015050700000000",
        "cls_id"=> "5812",
        "mer_name"=> "上海尚云科技服务有限公司",
        "mer_addr"=> "上海市静安区",
        "contact_name"=> "朱先生",
        "contact_phone"=> "15866689123",
        "contact_mobile"=> "15866689123",
        "contact_email"=> "817888900@qq.com",
        "legal_id_no"=> "310555196710215555",
        "mer_license"=> "110108001111111",
        "province_code"=> "310000",
        "city_code"=> "310100",
        "district_code"=> "310115",
    ], JSON_UNESCAPED_UNICODE)
];

# 发起商户进件修改
$merConfig->modify($mer_config_params);
echoExecuteResult($merConfig, "商户进件配置修改");

# 初始化支付配置类
$merConfig = new \AdaPayMerchant\MerchantConf();

# 发起支付配置查询
$merConfig->query(["request_id"=> "req_20190912013859492871"]);
echoExecuteResult($merConfig, "商户进件配置查询");


function  echoExecuteResult($obj, $funcName){
    print_r($funcName."接口调用开始:\n");
    # 对进件结果进行处理
    if ($obj->isError()){
        print_r($funcName."失败结果::".json_encode($obj->result, JSON_UNESCAPED_UNICODE)."\n");
        //失败处理
    } else {
        //成功处理
        print_r($funcName."接口调用成功! \n");
    }
    print_r($funcName."接口调用结束:\n");
}