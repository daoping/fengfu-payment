<?php
# 加载SDK需要的文件
include_once dirname(__FILE__) . '/../../AdapayMerchantSdk/init.php';
# 加载商户的配置文件
include_once dirname(__FILE__) . '/config.php';

# 初始化进件类
$merchantProfile = new \AdaPayMerchant\MerchantProfile();
$file_real_path = realpath('demo.jpg');
$picture_params = [
    'subApiKey'=> 'api_live_fdcbcc99-4fef-4f23-a8f3-e21d7c7e4f7b',
    'file'=> new CURLFile($file_real_path),
    'fileType'=> '04'
];

# 发起进件
$merchantProfile->merProfilePicture($picture_params);
echoExecuteResult($merchantProfile, "代理商上送证照");


$audit_params = [
    'subApiKey'=> 'api_live_fdcbcc99-4fef-4f23-a8f3-e21d7c7e4f7b',
    'socialCreditCodeId'=> '0067363309554112',
    'legalCertIdFrontId'=> '0067363504549376',
    'legalCertIdBackId'=> '0067363574467008',
    'businessAdd'=> 'http://www.baidu.com',
    'storeId'=> '0067361944316352|0067363749735936',
    'accountOpeningPermitId'=> '0067364019530176',
    'shareholderInfoList'=> '',
];

# 发起支付配置
$merchantProfile->merProfileForAudit($audit_params);
echoExecuteResult($merchantProfile, "代理商上送证照提交审核");


$audit_status_params = [
    'subApiKey'=> 'api_live_fdcbcc99-4fef-4f23-a8f3-e21d7c7e4f7b'
];

# 发起支付配置
$merchantProfile->merProfileAuditStatus($audit_status_params);
echoExecuteResult($merchantProfile, "代理商基础信息审核状态查询");

function  echoExecuteResult($obj, $funcName){
    print_r($funcName."接口调用开始:\n");
    # 对进件结果进行处理
    if ($obj->isError()){
        print_r($funcName."失败结果::".json_encode($obj->result, JSON_UNESCAPED_UNICODE)."\n");
        //失败处理
    } else {
        //成功处理
        print_r($funcName."接口调用成功! \n");
    }
    print_r($funcName."接口调用结束:\n");
}