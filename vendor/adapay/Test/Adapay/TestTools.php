<?php
# 加载SDK需要的文件
include_once  dirname(__FILE__). "/../../AdapaySdk/init.php";
# 加载商户的配置文件
include_once  dirname(__FILE__). "/config.php";

# 初始化对账单下载对象类
$tools = new \AdaPaySdk\Tools();
# 对账单下载
$tools->download(["bill_date"=> "20190905"]);
echoExecuteResult($tools, "账单查询");

# 用户标识
# 获取银联云闪付用户标识
$union_params = array(
    # app_id
    'app_id'=> 'app_7d87c043-aae3-4357-9b2c-269349a980d6',
    # 用户授权码
    'user_auth_code'=> '5yRGbi+IRda5khIQoQf1Hw==',
    # App 标识
    'app_up_identifier'=> 'CloudPay',
    # 订单号
    'order_no'=> "_". date("YmdHis").rand(100000, 999999)
);
$tools->unionUserId($union_params);
echoExecuteResult($tools, "获取银联用户标识");



function  echoExecuteResult($obj, $funcName){
    print_r($funcName."接口调用开始:\n");
    # 对进件结果进行处理
    if ($obj->isError()){
        print_r($funcName."失败结果::".json_encode($obj->result, JSON_UNESCAPED_UNICODE)."\n");
        //失败处理
    } else {
        //成功处理
        print_r($funcName."接口调用成功! \n");
    }
    print_r($funcName."接口调用结束:\n\n");
}
