<?php
# 加载SDK需要的文件
include_once  dirname(__FILE__). "/../../AdapaySdk/init.php";
# 加载商户的配置文件
include_once  dirname(__FILE__). "/config.php";


$payment = new \AdaPaySdk\Payment();

# 支付设置
$payment_params = array(
    'app_id'=> 'app_7d87c043-aae3-4357-9b2c-269349a980d6',
    'order_no'=> "PY_". date("YmdHis").rand(100000, 999999),
    'pay_channel'=> 'alipay',
    'time_expire'=> '20200101000000',
    'pay_amt'=> '0.01',
    'goods_title'=> 'subject',
    'goods_desc'=> 'body',
    'description'=> 'description',
    'device_id'=> ['device_id'=>"1111"],
    'expend'=> [
        'buyer_id'=> '1111111',              // 支付宝卖家账号ID
        'buyer_logon_id'=> '22222222222',   // 支付宝卖家账号
        'promotion_detail'=>[              // 优惠信息
            'cont_price'=> '100.00',      // 订单原价格
            'receipt_id'=> '123',        // 商家小票ID
            'goodsDetail'=> [           // 商品信息集合
                ['goods_id'=> "111", "goods_name"=>"商品1", "quantity"=> 1, "price"=> "1.00"],
                ['goods_id'=> "112", "goods_name"=>"商品2", "quantity"=> 1, "price"=> "1.01"]
            ]
        ]
    ]
);

# 发起支付
$payment->create($payment_params);

echoExecuteResult($payment, "支付创建");

#发起支付订单查询
$payment->orderQuery(['payment_id'=> '002112019101615181410030442335571763200']);
echoExecuteResult($payment, "支付查询");

$payment_params = array(
    # 设置支付对象ID
    'payment_id'=> '002112019101517084010030107738472407040',
    # 设置描述
    'reason'=> '关单描述',
    # 设置扩展域
    'expend'=> '{"key": "1233"}'
);

# 发起关单
$payment->orderClose($payment_params);
echoExecuteResult($payment, "支付关闭操作");


# 支付设置
$payment_params = array(
    'payment_id'=> '123123123131231231',
    'order_no'=> date("YmdHis").rand(100000, 999999),
    'confirm_amt'=> '0.01',
    'description'=> '附件说明',
    'div_members'=> '' //分账参数列表 默认是数组List
);

# 发起支付
$payment->createConfirm($payment_params);
echoExecuteResult($payment, "支付确认创建");


$payment_params = array(
    "app_id"=> "1231123123123131231",
    "payment_id"=> "10023123123101",
    "page_index"=> "",
    "page_size"=> "",
    "created_gte"=> "",
    "created_lte"=> ""
);

# 发起支付
$payment->queryConfirmList($payment_params);
echoExecuteResult($payment, "支付确认列表");

# 支付设置
$payment_params = array(
    "payment_confirm_id"=> "100000000000012312344"
);

# 发起支付
$payment->queryConfirm($payment_params);
echoExecuteResult($payment, "支付确认查询");


# 支付设置
$payment_params = array(
    'payment_id'=> '10000000000000001',
    'app_id'=> 'app_7d87c043-aae3-4357-9b2c-269349a980d6',
    'order_no'=> "R".date("YmdHis").rand(100000, 999999),
    'reverse_amt'=> '0.01',
    'notify_url'=> '',
    'reason'=> '订单支金额错误',
    'expand'=> '',
    'device_info'=> '',
);

# 发起支付
$payment->createReverse($payment_params);
echoExecuteResult($payment, "支付撤销创建");

# 支付设置
$payment_params = array(
    "app_id"=> "app_7d87c043-aae3-4357-9b2c-269349a980d6",
    "payment_id"=> "10023123123101",
    "page_index"=> "",
    "page_size"=> "",
    "created_gte"=> "",
    "created_lte"=> ""
);

# 发起支付
$payment->queryReverseList($payment_params);
echoExecuteResult($payment, "支付撤销列表");

# 支付设置
$payment_params = array(
    'reverse_id'=> '1000000000001123333333'
);

# 发起支付
$payment->queryReverse($payment_params);
echoExecuteResult($payment, "支付撤销查询");


#初始化退款对象
$refund = new \AdaPaySdk\Refund();

$refund_params = array(
    # 原交易支付对象ID
    'payment_id'=> '002112019101519194610030140730621550592',
    # 退款订单号
    'refund_order_no'=> '20190919071231283468359213',
    # 退款金额
    'refund_amt'=> '0.01',
    # 退款描述
    'reason'=> '退款描述',
    # 扩展域
    'expend'=> '',
    # 设备静态信息
    'device_info'=> ''
);

# 发起退款
$refund->orderRefund($refund_params);
echoExecuteResult($refund, "退款订单创建");

# refund_id或charge_id二选一
# 发起退款查询
$refund->orderRefundQuery(['payment_id'=> '002112019101519194610030140730621550592']);
echoExecuteResult($refund, "退款订单查询");


function  echoExecuteResult($obj, $funcName){
    print_r($funcName."接口调用开始:\n");
    # 对进件结果进行处理
    if ($obj->isError()){
        print_r($funcName."失败结果::".json_encode($obj->result, JSON_UNESCAPED_UNICODE)."\n");
        //失败处理
    } else {
        //成功处理
        print_r($funcName."接口调用成功! \n");
    }
    print_r($funcName."接口调用结束:\n\n");
}
