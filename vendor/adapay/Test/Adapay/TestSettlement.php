<?php
# 加载SDK需要的文件
include_once  dirname(__FILE__). "/../../AdapaySdk/init.php";
# 加载商户的配置文件
include_once  dirname(__FILE__). "/config.php";

# 初始化结算账户对象类
$account = new \AdaPaySdk\SettleAccount();

$account_params = array(
    'app_id'=> 'app_7d87c043-aae3-4357-9b2c-269349a980d6',
    'member_id'=> 'hf_prod_member_20191013',
    'channel'=> 'bank_account',
    'account_info'=> [
        'card_id' => '622202170300169222',
        'card_name' => '余益兰',
        'cert_id' => '310109200006068391',
        'cert_type' => '00',
        'tel_no' => '18888818881',
        'bank_code' => '03060000',
        'bank_name' => '建hua',
        'bank_acct_type' => 1,
        'prov_code' => '0031',
        'area_code' => '3100',
    ]
);

# 创建结算账户
$account->create_settle($account_params);

echoExecuteResult($account, "结算账户创建");

$account_params = array(
    'app_id'=> 'app_f8b14a77-dc24-433b-864f-98a62209d6c4',
    'member_id'=> 'hf_test_member_id_account5',
    'settle_account_id'=> '0006017543466816'
);

# 查询结算账户
$account->delete_settle($account_params);
echoExecuteResult($account, "结算账户删除");

$account_params = array(
    'app_id'=> 'app_P000002052092068',
    'member_id'=> 'hf_test_member_id_account5',
    'settle_account_id'=> '0006017543466816',
    'begin_date'=> '20190705',
    'end_date'=> '20190806'
);

# 查询结算账户
$account->query_settle_details($account_params);
echoExecuteResult($account, "结算账户明细查询");

$account_params = array(
    'app_id'=> 'app_7d87c043-aae3-4357-9b2c-269349a980d6',
    'member_id'=> 'hf_test_201999999999',
    'settle_account_id'=> '0006124815051328',
    'min_amt'=> '',
    'remained_amt'=> '',
    'channel_remark'=> '123'
);

# 修改结算账户
$account->modify_settle($account_params);
echoExecuteResult($account, "结算账户修改");

$account_params = array(
    'app_id'=> 'app_f8b14a77-dc24-433b-864f-98a62209d6c4',
    'member_id'=> 'hf_test_201999999999',
    'settle_account_id'=> '0006124815051328'
);

# 查询结算账户
$account->query_settle($account_params);
echoExecuteResult($account, "结算账户查询");

$account_params = array(
    'app_id'=> 'app_7d87c043-aae3-4357-9b2c-269349a980d6',
    'member_id'=> 'user_00008',
    'settle_account_id'=> '0035172521665088'
);

# 查询账户余额
$account->query_balance($account_params);
echoExecuteResult($account, "结算账户余额查询");


$account_params = array(
    'order_no'=> "CS_". date("YmdHis").rand(100000, 999999),
    'app_id'=> 'app_7d87c043-aae3-4357-9b2c-269349a980d6',
    'cash_type'=> 'T1',
    'cash_amt'=> '0.02',
    'member_id'=> 'user_00008',
    'notify_url'=> ''
);

# 账户取现
$account->draw_cash($account_params);
echoExecuteResult($account, "结算账户取现");


function  echoExecuteResult($obj, $funcName){
    print_r($funcName."接口调用开始:\n");
    # 对进件结果进行处理
    if ($obj->isError()){
        print_r($funcName."失败结果::".json_encode($obj->result, JSON_UNESCAPED_UNICODE)."\n");
        //失败处理
    } else {
        //成功处理
        print_r($funcName."接口调用成功! \n");
    }
    print_r($funcName."接口调用结束:\n\n");
}


