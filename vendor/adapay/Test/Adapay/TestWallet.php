<?php
# 加载SDK需要的文件
include_once  dirname(__FILE__). "/../../AdapaySdk/init.php";
# 加载商户的配置文件
include_once  dirname(__FILE__). "/config.php";

# 初始化用户对象类
$wallet = new \AdaPaySdk\Wallet();
$wallet_params = array(
    # 应用ID
    'app_id'=> 'app_7d87c043-aae3-4357-9b2c-269349a980d6',
    # 用户ID
    'member_id'=> 'hf_prod_member_20191013',
    # IP
    'ip'=> '192.168.1.152'
);
# 创建
$wallet->walletLogin($wallet_params);
echoExecuteResult($wallet, "钱包登录");

# 查询用户对象
$wallet_params = array(
    # 商户的应用 id
    'app_id'=> 'app_7d87c043-aae3-4357-9b2c-269349a980d6',
    # 用户ID
    'order_no'=> "WL_". date("YmdHis").rand(100000, 999999),
    # 订单总金额（必须大于0）
    'pay_amt'=> '0.10',
    # 3 位 ISO 货币代码，小写字母
    'currency'=> 'cny',
    # 商品标题
    'goods_title'=> '12314',
    # 商品描述信息
    'goods_desc'=> '123122123',
);
$wallet->accountPayment($wallet_params);
echoExecuteResult($wallet, "钱包支付");


# 查询用户对象
$wallet_params = array(
    # 应用ID
    'app_id'=> 'app_7d87c043-aae3-4357-9b2c-269349a980d6',
    # 用户ID
    'member_id'=> 'user_00013',
    # IP
    'order_no'=>  "CK_". date("YmdHis").rand(100000, 999999),
    'pay_amt'=> '0.01',
    'goods_title'=> '收银台测试',
    'goods_desc'=> '收银台测试',
    'div_members'=> [],
    'currency'=> '',
    'time_expire'=> '',
    'description'=> '',
    'notify_url'=> '',
    'callback_url'=> ''
);
$wallet->createCheckout($wallet_params);
echoExecuteResult($wallet, "收银台创建");


function  echoExecuteResult($obj, $funcName){
    print_r($funcName."接口调用开始:\n");
    # 对进件结果进行处理
    if ($obj->isError()){
        print_r($funcName."失败结果::".json_encode($obj->result, JSON_UNESCAPED_UNICODE)."\n");
        //失败处理
    } else {
        //成功处理
        print_r($funcName."接口调用成功! \n");
    }
    print_r($funcName."接口调用结束:\n\n");
}
