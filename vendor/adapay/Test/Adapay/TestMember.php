<?php
# 加载SDK需要的文件
include_once  dirname(__FILE__). "/../../AdapaySdk/init.php";
# 加载商户的配置文件
include_once  dirname(__FILE__). "/config.php";

# 初始化用户对象类
$member = new \AdaPaySdk\Member();
$member_params = array(
    # app_id
    'app_id'=> 'app_7d87c043-aae3-4357-9b2c-269349a980d6',
    # 用户id
    'member_id'=> 'hf_prod_member_20190920',
    # 用户地址
    'location'=> '上海市闵行区汇付',
    # 用户邮箱
    'email'=> '123123@126.com',
    # 性别
    'gender'=> 'MALE',
    # 用户手机号
    'tel_no'=> '18177722312',
    # 用户昵称
    'nickname'=> 'test',
);
# 创建
$member->create($member_params);
echoExecuteResult($member, "支付普通用户创建");

# 查询用户对象
$member->query(['app_id'=> 'app_7d87c043-aae3-4357-9b2c-269349a980d6', 'member_id'=> 'hf_prod_member_20190920']);
echoExecuteResult($member, "支付普通用户查询");

$member_params = array(
    'app_id'=> 'app_7d87c043-aae3-4357-9b2c-269349a980d6'
);


# 查询用户对象
$member->query_list($member_params);
echoExecuteResult($member, "支付普通用户列表");


$corp_member = new \AdaPaySdk\CropMember();

$file_real_path = realpath('123.zip');
$member_params = array(
    # app_id
    'app_id'=> 'app_7d87c043-aae3-4357-9b2c-269349a980d6',
    # 商户用户id
    'member_id'=> 'hf_prod_member_20190920',
    # 订单号
    'order_no'=> date("YmdHis").rand(100000, 999999),
    # 企业名称
    'name'=> '测试企业',
    # 省份
    'prov_code'=> '0031',
    # 地区
    'area_code'=> '3100',
    # 统一社会信用码
    'social_credit_code'=> 'social_credit_code',
    'social_credit_code_expires'=> '20301109',
    # 经营范围
    'business_scope'=> '123123',
    # 法人姓名
    'legal_person'=> 'frname',
    # 法人身份证号码
    'legal_cert_id'=> '1234567890',
    # 法人身份证有效期
    'legal_cert_id_expires'=> '20301010',
    # 法人手机号
    'legal_mp'=> '13333333333',
    # 企业地址
    'address'=> '1234567890',
    # 邮编
    'zip_code'=> '企业地址测试',
    # 企业电话
    'telphone'=> '1234567890',
    # 企业邮箱
    'email'=> '1234567890@126.com',
    # 上传附件
    'attach_file'=> new CURLFile($file_real_path),
    # 银行代码
    'bank_code'=> '1001',
    # 银行账户类型
    'bank_acct_type'=> '1',
);
# 创建企业用户
$corp_member->create($member_params);
echoExecuteResult($corp_member, "支付企业用户创建");

# 查询企业用户
$corp_member->query(['app_id'=> 'app_7d87c043-aae3-4357-9b2c-269349a980d6', 'member_id'=> 'hf_prod_member_20190920']);
echoExecuteResult($corp_member, "支付企业用户查询");


function  echoExecuteResult($obj, $funcName){
    print_r($funcName."接口调用开始:\n");
    # 对进件结果进行处理
    if ($obj->isError()){
        print_r($funcName."失败结果::".json_encode($obj->result, JSON_UNESCAPED_UNICODE)."\n");
        //失败处理
    } else {
        //成功处理
        print_r($funcName."接口调用成功! \n");
    }
    print_r($funcName."接口调用结束:\n\n");
}
