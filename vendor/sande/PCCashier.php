<?php

class PCCashier extends Common
{

    // 参数映射
    public function apiMap()
    {
        return array(
            'orderPay'       => array(
                'method' => 'sandpay.trade.barpay',
                'url'    => '/qr/api/order/pay',
                'custom'   => false
            ),
            'orderCreate'        => array(
                'method' => 'sandpay.trade.precreate',
                'url'    => '/qr/api/order/create',
                'custom'   => false  
            ),

            'orderQuery'        => array(
                'method' => 'sandpay.trade.query',
                'url'    => '/gateway/api/order/query',
                'custom'   => false
                
            ),
            'orderRefund'       => array(
                'method' => 'sandpay.trade.refund',
                'url'    => '/gw/api/order/refund',
                'custom'   => false
            ),
            'orderMcAutoNotice' => array(
                'method' => 'sandpay.trade.notify',
                'url'    => '/gateway/api/order/mcAutoNotice',
                'custom'   => false
            ),
            'clearfileDownload' => array(
                'method' => 'sandpay.trade.download',
                'url'    => '/gateway/api/clearfile/download',
                'custom'   => false
                // 普通商户（即时交易）请求地址：https://cashier.sandpay.com.cn/gateway/api /clearfile/download
                // 供应链商户（担保交易）请求地址；https://cashier.sandpay.com.cn/gw/api/clearfile/download
            ),
            'BackGoodsNotice' => array(
                'method' => '',
                'url'    => '',
                'custom'   => true
            ),

        );
    }
}
