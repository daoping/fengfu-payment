<?php
namespace app\quotient\controller;

use think\Db;
use think\Request;
use think\Controller;
use think\facade\Env;


class UpFiles extends Common
{

    public function upload(){
        // 获取上传文件表单字段名
        $fileKey = array_keys(request()->file());
        // 获取表单上传文件
        $file = request()->file($fileKey['0']);
        // 移动到框架应用根目录/public/uploads/ 目录下

        $info = $file->validate(['ext' => 'jpg,png,gif,jpeg'])->move('uploads');
        if($info){
            $result['code'] = 1;
            $result['info'] = '图片上传成功!';
            $path=str_replace('\\','/',$info->getSaveName());
            $result['url'] = '/uploads/'. $path;
            return $result;
        }else{
            // 上传失败获取错误信息

            $result['code'] =0;
            $result['info'] =  $file->getError();
            $result['url'] = '';
            return $result;
        }
    }
    public function file(){
        $fileKey = array_keys(request()->file());
        // 获取表单上传文件 例如上传了001.jpg
        $file = request()->file($fileKey['0']);
        // 移动到框架应用根目录/public/uploads/ 目录下
        $info = $file->validate(['ext' => 'zip,rar,pdf,swf,ppt,psd,ttf,txt,xls,doc,docx'])->move('uploads');
        if($info){
            $result['code'] = 0;
            $result['info'] = '文件上传成功!';
            $path=str_replace('\\','/',$info->getSaveName());

            $result['url'] = '/uploads/'. $path;
            $result['ext'] = $info->getExtension();
            $result['size'] = byte_format($info->getSize(),2);
            return $result;
        }else{
            // 上传失败获取错误信息
            $result['code'] =1;
            $result['info'] = '文件上传失败!';
            $result['url'] = '';
            return $result;
        }
    }

    /**
     * 微信cert证书上传
     */
    public function fileCert(){
        $fileKey = array_keys(request()->file());
        // 获取表单上传文件 例如上传了001.jpg
        $file = request()->file($fileKey['0']);

        $info = $file->validate(['ext' => 'pem'])->move('certs');

        if($info){
            $result['code'] = 1;
            $result['info'] = '文件上传成功!';
            $path=str_replace('\\','/',$info->getSaveName());
            $result['url'] = '/certs/'.$path ;
            return $result;
        }else{
            // 上传失败获取错误信息
            $result['code'] =0;
            $result['info'] = '文件上传失败!';
            $result['url'] = '';
            return $result;
        }
    }

    /**
     * 上传支付背景图
     */
    public function uploadPayBG()
    {
        $fileKey = array_keys(request()->file());
        $file = request()->file($fileKey['0']);
        $file_path = ROOT_PATH."/public/static/bg/";
        !is_dir($file_path) && mkdir($file_path,0755,true);
        $info = $file->validate(['ext' => 'jpg,png,gif,jpeg'])->move('static/bg','');
        if($info){
            $result['code'] = 1;
            $result['msg'] = '图片上传成功!';
            $path=str_replace('\\','/',$info->getFilename());
            $result['url'] = '/static/bg/'. $path;
            return $result;
        }else{
            $result['code'] =0;
            $result['msg'] =  $file->getError();
            $result['url'] = '';
            return $result;
        }
    }

}