<?php
namespace app\quotient\controller;
use app\common\model\Agent;
use think\Db;
use think\Controller;
class Common extends Controller
{
    protected $mod,$role,$system,$nav,$user,$user_id,$module,$moduleid,$businessRules,$HrefId,$businessRule;
    public function initialize()
    {
        return;
        //判断商家是否登录
        if (!session('quotient_id')) {
            $this->redirect('quotient/login/index');
            return;
        }

        //检测用户是否被锁定
        $user =  Agent::get(session('quotient_id'));

        $this->user_id =  session('quotient_id');
        $this->user =  $user;

        define('MODULE_NAME',strtolower(request()->controller()));
        define('ACTION_NAME',strtolower(request()->action()));
        //权限管理
        //操作权限ID
        $this->HrefId = db('business_auth_rule')->where('href',MODULE_NAME.'/'.ACTION_NAME)->value('id');
        //商家权限
        $rules = db('agent')->alias('b')
            ->join('business_auth_group ag','ag.id = b.group_id','left')
            ->where('b.id',session('quotient_id'))
            ->value('ag.rules');

        $this->businessRules = explode(',',$rules);
        if ($this->HrefId){
            if (!in_array($this->HrefId,$this->businessRules)){
                $this->error('您无此操作权限');
            }
        }

    }
    //空操作
    public function _empty(){
        return $this->error('空操作，返回上次访问页面中...');
    }
    //获取子地区
    public function getchildarea() {
        $pid= input('post.parent_id');

        $area = db('region')->field('id, name')->where('pid',$pid)->select();
        if ($area) {
            $option = '<option value="">---请选择---</option>';
            foreach ($area as $key => $value) {
                $option .= '<option value="'.$value['id'].'">'.$value['name'].'</option>';
            }
            return callback(1,'请求成功','',$option);
        } else {
            return callback(0,'请求失败');
        }
    }
}
