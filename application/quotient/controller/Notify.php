<?php
namespace app\quotient\controller;

use app\common\model\Agent;
use app\common\model\Hcxiafa;
use app\common\service\Hcdaifu;
use think\Controller;

use think\facade\Validate;

class Notify extends Controller
{
    /**
     * 下发异步测试
     * https://huichao.yapin.shop/quotient/notify/LfQvoBhRxDLCVGWLxYxZObuCqnDQHhmf
     */
    public function LfQvoBhRxDLCVGWLxYxZObuCqnDQHhmf(){
        $model = new Hcxiafa();
        $notify_ip = request()->ip();//回调异步ip

        $data =$_POST;
        //$data返回数据
        //array(8) {
        //  ["CardNo"] => string(19) "6217002290009347773"//请求转账账户号
        //  ["BillNo"] => string(17) "GATEWAY0041488604"//一麻袋平台流水号
        //  ["MerNo"] => string(5) "53030"//麻袋商户号
        //  ["Amount"] => string(1) "1"//转账金额
        //  ["SignInfo"] => string(172) "G7Dz6UrA8r3B+gQoQgj4WB0DVNDt0TMR9CRyrC9PsKASb/y7zbGQVDl21Ae5KXE2i9TazaKS2IcCdM7NW/jjboh/bUs0PV8A+W7VHiz1aSNO/gBi/NSXAzd5E3dNd/FuwyzXVltzBI49GuASRoMKL+JbjwxqwddDxNKimpflodw="
        //  ["MerBillNo"] => string(14) "16426417188738"//商户请求流水号
        //  ["Succeed"] => string(2) "00"//00成功  11转账退回
        //  ["Result"] => string(19) "成功[00000000000]"
        //}
        db("test")->insert(["add_time"=>time(),"content"=>"汇潮下发异步支付返回数据".json_encode($data),"ip"=>$notify_ip]);
        $order = $model::where(["transId"=>$data["MerBillNo"],"cardNo"=>$data["CardNo"]])->find();
        if(!$order){
            return AjaxReturn(0,"订单不存在");
        }
        if($order["payment"]){
            return AjaxReturn(0,"订单已打款");
        }
        $daifu = new Hcdaifu();
        $res = $daifu->rsaVerify($data);
        if($res){
            if($data["Succeed"] == "11"){
                //转账退回
                $order_data = [
                    "payment"=>2,
                    "pay_time"=>time(),
                    "notify_ip"=>$notify_ip,
                    "BillNo"=>$data["BillNo"],
                    "notify_amount"=>$data["Amount"],
                ];
                $model->where(["id"=>$order["id"]])->update($order_data);
            }else if($data["Succeed"] == "00"){
                //转账成功
                $order_data = [
                    "payment"=>1,
                    "pay_time"=>time(),
                    "notify_ip"=>$notify_ip,
                    "BillNo"=>$data["BillNo"],
                    "notify_amount"=>$data["Amount"],
                ];
                $model->where(["id"=>$order["id"]])->update($order_data);
            }
        }
    }

    /**
     * 返回数据
     * https://huichao.yapin.shop/quotient/notify/test
     */
    public function test(){
        $json = '{"CardNo":"6217002290009347773","BillNo":"GATEWAY0041488604","MerNo":"53030","Amount":"1","SignInfo":"G7Dz6UrA8r3B+gQoQgj4WB0DVNDt0TMR9CRyrC9PsKASb\/y7zbGQVDl21Ae5KXE2i9TazaKS2IcCdM7NW\/jjboh\/bUs0PV8A+W7VHiz1aSNO\/gBi\/NSXAzd5E3dNd\/FuwyzXVltzBI49GuASRoMKL+JbjwxqwddDxNKimpflodw=","MerBillNo":"16426417188738","Succeed":"00","Result":"\u6210\u529f[00000000000]"}';
        $daifu = new Hcdaifu();
        $arr = json_decode($json, true);
        halt($arr);

    }


}