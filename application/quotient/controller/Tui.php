<?php
namespace app\quotient\controller;



use app\admin\controller\Order;

use app\common\model\StoreAdapayApply;

use app\common\model\UserOrder;

use think\Controller;
use think\facade\Env;
use think\facade\Request;
use think\Model;

include_once  Env::get("root_path"). "vendor/adapay/AdapaySdk/init.php";

class Tui extends Controller
{
    //http://pay11.m5d.co/admin/tui/index
    //退款操作
    public function index()
    {
        $order_id = input("post.id");
        //$order_id = 8555;

        $order = db("ad_order")->where(["id"=>$order_id])->find();
        //halt($order);
        if(!$order){
            return AjaxReturn(0,"不存在的订单");
        }
        if($order["payment"] < 1){
            return AjaxReturn(0,"不是退款订单状态");
        }

        $business_adapay_apply_model = new  StoreAdapayApply();
        $info = $business_adapay_apply_model::get(1);
       // halt($info);
        $arr = [
            "api_key_live"=>$info["live_api_key"],
            "api_key_test"=>$info["test_api_key"],
            "rsa_private_key"=>config("app.adapay.rsa_private_key")
        ];
        $json = json_encode($arr);
        //halt($json);
        \AdaPay\AdaPay::inits($json, "live");
        $refund = new \AdaPaySdk\Refund();
        $refund_params = array(
            'payment_id'=> $order["ada_id"],//原交易支付对象ID
            // 'refund_order_no'=> $order["order_num"],//退款订单号
            'refund_order_no'=> orderNum(),//退款订单号
            'refund_amt'=> $order["money"],//退款金额
            'reason'=> '退款',//退款描述
            'expend'=> '',//扩展域
            'device_info'=> ''//设备静态信息
        );

        // 发起退款
        $refund->create($refund_params);

        //对退款结果进行处理$refund->result 类型为数组
        if ($refund->isError()){
            halt($refund->result);
            //失败处理
            return AjaxReturn(0,"退款失败,请重试");
        } else {
            //halt($refund->result);
            //成功处理
            db("ad_order")->where("id",$order_id)->update(["payment"=>-1]);
            //halt("退款成功");
            return AjaxReturn(1,"退款成功");
        }

    }

    //http://ly.xinmke.com/home/test/tui
    public function tui(){
        //$order_id =235;
        return ;
        $bid = 1;
        $business_adapay_apply_model = new  AdapayApply();
        $info = $business_adapay_apply_model::get(["bid"=>$bid]);
        $arr = [
            "api_key_live"=>$info["live_api_key"],
            "api_key_test"=>$info["test_api_key"],
            "rsa_private_key"=>config("app.adapay.rsa_private_key")
        ];
        $json = json_encode($arr);
        \AdaPay\AdaPay::inits($json, "live");
        $refund = new \AdaPaySdk\Refund();
        $refund_params = array(
            'payment_id'=> "002112021012202330310198397740428595200",//原交易支付对象ID
            'refund_order_no'=> "122539824602678",//退款订单号
            'refund_amt'=> "1979.54",//退款金额
            'reason'=> '退款',//退款描述
            'expend'=> '',//扩展域
            'device_info'=> ''//设备静态信息
        );

        // 发起退款
        $refund->orderRefund($refund_params);

        //对退款结果进行处理$refund->result 类型为数组
        if ($refund->isError()){
            halt($refund->result);
            //失败处理
            return AjaxReturn(0,"退款失败,请重试");
        } else {
            //成功处理
            //UserOrder::where("id",$order_id)->update(["pay_type"=>-1]);
            halt("退款成功");
        }
    }









}