<?php
/**
 * Created by PhpStorm.
 * User: zhao
 * Date: 2019/6/15
 * Time: 14:39
 */
namespace app\quotient\controller;


use AdaPay\AdaPay;
use app\common\model\AdapayBank;
use app\common\model\AdapayMember;
use app\common\model\AdapayPc;
use app\common\model\StoreAdapayApply;
use think\facade\Env;
use think\facade\Request;
include_once  Env::get("root_path"). "vendor/adapay/AdapaySdk/init.php";
require_once Env::get("root_path")."/vendor/adapay/AdapayCore/AdaPay.php";
use AdaPaySdk\Member;

class Members extends Common
{
    public function initialize()
    {
        parent::initialize(); // TODO: Change the autogenerated stub

    }

    /**
     * 非法人列表
     */
    public function noncorporate()
    {
        $AdapayMember = new AdapayMember();
        if(Request::isPost()){
            $page = input("page", 1);
            $page_size = input("limit", 10);
            $aid = 1;


            $where = [];
            $aid != "" && $where[] = ["aid", "eq",$aid];
            $list = $AdapayMember::with("adapayapply")->where($where)
                ->order("id desc")
                ->paginate(["page" => $page, "list_rows" => $page_size])
                ->toArray();
            return ["code"=>0,"data"=>$list["data"],"count"=>$list["total"],"rel"=>1];
        }

        return  view();
    }

    /**
     * 添加非法人信息
     */
    public function noncorporateAdd()
    {
        $business_adapay_apply_model = new  StoreAdapayApply();
        if(Request::isPost()){
            $AdapayMember = new AdapayMember();
            $data = input("post.");
            $rand = rand(1,9999);
            $data['member_id'] = "hf_".$data["cert_id"].$rand;
            $res = $AdapayMember->save($data);

            if($res){
                return ["code"=>1,"msg"=>"添加入网材料成功","url"=>url('noncorporate')];
            }else{
                return AjaxReturn(0,"添加失败,请重试");
            }
        }else{
            $id = 1;
            $info = $business_adapay_apply_model::get($id);
            $regionProArea = $this->regionProArea();//银行省份
            $bankList = $this->bankList();//银行列表
            //$city =$this->regionCity();
            $data = [
                "regionProArea"=>$regionProArea,
                //"city"=>$city,
                'bankList'=>$bankList,
                "info"=>$info,
                "id"=>$id
            ];

            return view("add",$data);
        }
    }

    /**
     * 修改结算信息
     * @return array|\think\response\Json|\think\response\View
     */
    public  function edit(){
        $business_adapay_apply_model = new  StoreAdapayApply();
        $AdapayMember = new AdapayMember();
        if(Request::isPost()){

            $data = input("post.");

            $data['member_id'] = "hf_".$data["cert_id"];
            $res = $AdapayMember->save($data,["id"=>$data["id"]]);

            if($res){
                return ["code"=>1,"msg"=>"非法人入网材料修改成功","url"=>url('noncorporate')];
            }else{
                return AjaxReturn(0,"修改失败,请重试");
            }
        }else{
            $id = input('get.id');
            $info = $AdapayMember::get($id);
            $store = $business_adapay_apply_model::get($info["aid"]);
            $regionProArea = $this->regionProArea();//银行省份
            $bankList = $this->bankList();//银行列表
            $city =$this->regionCity($store['prov_code']);
            $data = [
                "regionProArea"=>$regionProArea,
                "city"=>$city,
                'bankList'=>$bankList,
                "info"=>$info,
            ];

            return view("edit",$data);
        }
    }


    /**
     * 省市联动
     * http://www.xmk.com/admin/adapay/regionProArea?pid=11
     */
    public function regionProArea(){
        $pid = input("pid",0000);
        $adapay_pc_model = new AdapayPc();
        if($pid){
            $data =   $adapay_pc_model::where(["pid"=>$pid])->select();
            return  AjaxReturn(1,"查询成功",$data);
        }
        $data =   $adapay_pc_model::where(["pid"=>$pid])->select();
        return $data;
    }

    /**
     * 查询城市
     */
    public function regionCity($pid){

        $adapay_pc_model = new AdapayPc();
        if($pid){
            $data =   $adapay_pc_model::where(["pid"=>$pid])->select();
            return  $data;
        }

        return [];
    }

    /**
     * 汇付银行列表
     */
    public function bankList(){
        $adpay_bank_model = new AdapayBank();
        $list = $adpay_bank_model::select();
        // halt($list);
        return $list;
    }


    /**
     * 1创建用户对象
     * x.xinmke.com/admin/test/createMember
     */
    public function createMember()
    {
        $id = input("id",0);
        if($id < 1){
            return AjaxReturn(0,"id不能为空");
        }
        $AdapayMember = new AdapayMember();
        $infos = $AdapayMember::get($id);
        if(!$infos){
            return AjaxReturn(0,"创建用户不存在");
        }
        $business_adapay_apply_model = new  StoreAdapayApply();
        $info = $business_adapay_apply_model::get($infos["aid"]);

        $arr = [
            "api_key_live"=>$info["live_api_key"],
            "api_key_test"=>$info["test_api_key"],
            "rsa_private_key"=>config("app.adapay.rsa_private_key")
        ];
        //halt($arr);
        $json = json_encode($arr);

        \AdaPay\AdaPay::inits($json, "live");

        $member = new Member();
        //halt($member);
        $member_params = array(
            "app_id"=> $info["app_id"],
            "member_id"=> $infos["member_id"],# 用户id
            "location"=> $infos["location"],# 用户地址
            "email"=> $infos["email"],# 用户邮箱
            "gender"=> $infos["gender"],// # 性别  MALE：男，FEMALE：女，为空时表示未填写
            "tel_no"=> $infos["tel_no"],# 用户手机号
            "nickname"=>$infos["nickname"]# 用户昵称
        );
        //halt($member_params);
        $member->create($member_params);

        if ($member->isError()){
            //失败处理
            var_dump($member->result);
        } else {
            //array(13) { ["app_id"]=> string(40) "app_79a59464-6c6d-4431-b8ac-829e0428e5c4" ["created_time"]=> string(10) "1601434692" ["disabled"]=> string(1) "N" ["email"]=> string(16) "778251855@qq.com" ["gender"]=> string(4) "MALE" ["identified"]=> string(1) "N" ["location"]=> string(39) "山东省临沂费县胡阳镇新合村" ["member_id"]=> string(21) "hf_371325198805136613" ["nickname"]=> string(9) "刘道平" ["object"]=> string(6) "member" ["tel_no"]=> string(11) "18177722312" ["status"]=> string(9) "succeeded" ["prod_mode"]=> string(4) "true" }
            //成功处理
            //echo "添加成功";
            //var_dump($member->result);
            $this->redirect(url('noncorporate'));

        }
    }

    /**
     * 2查询创建用户
     * x.xinmke.com/admin/test/queryMember
     */
    public function queryMember()
    {
        $id = input("id",0);
        $AdapayMember = new AdapayMember();
        $infos = $AdapayMember::get($id);
        $business_adapay_apply_model = new  StoreAdapayApply();
        $info = $business_adapay_apply_model::get($infos["aid"]);
        //halt($info);
        $arr = [
            "api_key_live"=>$info["live_api_key"],
            "api_key_test"=>$info["test_api_key"],
            "rsa_private_key"=>config("app.adapay.rsa_private_key")
        ];
        $json = json_encode($arr);
        \AdaPay\AdaPay::inits($json, "live");

        $member = new \AdaPaySdk\Member();

# 查询用户对象
        $member->query(["app_id"=> $info["app_id"], "member_id"=> $infos["member_id"]]);

# 对查询用户对象结果进行处理
        if ($member->isError()){
            //失败处理
            echo "处理失败";
            var_dump($member->result);
        } else {
            $AdapayMember::where(["id"=>$id])->update(["status"=>1]);
            $this->redirect(url('noncorporate'));
            //成功处理
            //echo "处理成功";
            //array(15) { ["app_id"]=> string(40) "app_79a59464-6c6d-4431-b8ac-829e0428e5c4" ["cert_type"]=> string(0) "" ["created_time"]=> string(10) "1601434693" ["disabled"]=> string(1) "N" ["email"]=> string(16) "778251855@qq.com" ["gender"]=> string(4) "MALE" ["identified"]=> string(1) "N" ["location"]=> string(39) "山东省临沂费县胡阳镇新合村" ["member_id"]=> string(21) "hf_371325198805136613" ["nickname"]=> string(9) "刘道平" ["object"]=> string(6) "member" ["settle_accounts"]=> array(0) { } ["tel_no"]=> string(11) "18177722312" ["status"]=> string(9) "succeeded" ["prod_mode"]=> string(4) "true" }
            //var_dump($member->result);
        }
    }


    /**
     * 3创建支付用户
     * x.xinmke.com/admin/test/settleaccount
     */
    public function settleaccount()
    {
        $id = input("id",0);
        $AdapayMember = new AdapayMember();
        $infos = $AdapayMember::get($id);
        if(strlen($infos['prov_code'])<4){
            $infos['prov_code'] = "00".$infos['prov_code'];
        }
        if(strlen($infos['bank_code'])<8){
            $infos['bank_code'] = "0".$infos['bank_code'];
        }
        $business_adapay_apply_model = new  StoreAdapayApply();
        $info = $business_adapay_apply_model::get($infos["aid"]);
        //halt($info);
        $arr = [
            "api_key_live"=>$info["live_api_key"],
            "api_key_test"=>$info["test_api_key"],
            "rsa_private_key"=>config("app.adapay.rsa_private_key")
        ];
        $json = json_encode($arr);
        \AdaPay\AdaPay::inits($json, "live");

        $account = new \AdaPaySdk\SettleAccount();
        //halt($account);
        $account_params = array(
            "app_id"=> $info["app_id"],
            "member_id"=> $infos["member_id"],
            "channel"=> "bank_account",
            "account_info"=> [
                "card_id" => $infos["card_id"],//银行卡
                "card_name" => $infos["card_name"],//银行卡对应姓名
                "cert_id" => $infos["cert_id"],//身份证号
                "cert_type" => "00",//证件类型，仅支持：00-身份证，银行账户类型为对私时，必填
                "tel_no" => $infos["tel_no"],//银行卡号
                "bank_code" => $infos["bank_code"],//银行编码
                "bank_acct_type" => "2",//1对公 2对私
                "prov_code" => $infos["prov_code"],
                "area_code" => $infos["area_code"],
            ]
        );
        //halt($account_params);
//        $account_params = json_encode($account_params);
//        $account_params = json_decode($account_params,true);

        //halt($account_params);
        # 创建结算账户
        $account->create($account_params);
        //halt($account);
        # 对创建结算账户结果进行处理
        if ($account->isError()){
            //失败处理
            echo "处理失败";
            var_dump($account->result);
        } else {
            $res = $account->result;
            $AdapayMember::where(["id"=>$id])->update(["settle_account_id"=>$res["id"]]);
            //成功处理
            //echo "添加成功";
            //array(8) { ["object"]=> string(14) "settle_account" ["status"]=> string(9) "succeeded" ["prod_mode"]=> string(4) "true" ["id"]=> string(16) "0139446938050240" ["create_time"]=> string(10) "1601435535" ["app_id"]=> string(40) "app_79a59464-6c6d-4431-b8ac-829e0428e5c4" ["channel"]=> string(12) "bank_account" ["account_info"]=> array(10) { ["card_id"]=> string(19) "6217002290********3" ["card_name"]=> string(9) "刘道平" ["cert_type"]=> string(2) "00" ["cert_id"]=> string(12) "3713****6613" ["tel_no"]=> string(11) "155****0231" ["bank_code"]=> string(8) "01050000" ["bank_name"]=> string(0) "" ["bank_acct_type"]=> string(1) "2" ["prov_code"]=> string(4) "0037" ["area_code"]=> string(4) "3710" } }
            //var_dump($account->result);
            $this->redirect(url('noncorporate'));
        }
    }

    /**
     * 4查询支付对象
     * x.xinmke.com/admin/test/querySettle
     */
    public function querySettle(){
        $id = input("id",0);
        $AdapayMember = new AdapayMember();
        $infos = $AdapayMember::get($id);
        $business_adapay_apply_model = new  StoreAdapayApply();
        $info = $business_adapay_apply_model::get($infos["aid"]);
        //halt($info);
        $arr = [
            "api_key_live"=>$info["live_api_key"],
            "api_key_test"=>$info["test_api_key"],
            "rsa_private_key"=>config("app.adapay.rsa_private_key")
        ];
        $json = json_encode($arr);
        \AdaPay\AdaPay::inits($json, "live");
        $account = new \AdaPaySdk\SettleAccount();
        $account_params = array(
            "app_id"=> $info["app_id"],
            "member_id"=> $infos["member_id"],
            "settle_account_id"=> $infos["settle_account_id"]
        );

        # 查询结算账户
        $account->query($account_params);

        # 对查询结算账户结果进行处理
        if ($account->isError()){
            //失败处理
            echo "处理失败";
            var_dump($account->result);
        } else {
            $AdapayMember::where(["id"=>$id])->update(["status"=>2]);
            //成功处理
            //echo "处理成功";
            //var_dump($account->result);
            $this->redirect(url('noncorporate'));
        }
    }


    /**
     * 修改代理状态
     * @return array
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     */
    public function editState()
    {
        $id = input('post.id');
        $open = input('post.open');
        $model = new AdapayMember();
        if ($model::where('id',$id)->update(['open'=> $open]) !== false) {
            return AjaxReturn(1,"设置成功");
        } else {
            return AjaxReturn(0,"设置失败");
        }
    }

    /**
     * 设置满减停的金额
     */
    public function manjian(){
        $model = new AdapayMember();
        if(Request::isPost()){
            $data = input("post.");
            $res = $model->save($data,["id"=>$data["id"]]);
            if($res){
                return AjaxReturn(1,"设置成功");
            } else {
                return AjaxReturn(0,"设置失败");
            }
        }else{
            $id =input("get.id");
            $info = $model::get($id);
            $this->assign("info",$info);
            return view();
        }
    }


}