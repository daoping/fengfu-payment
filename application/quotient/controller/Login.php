<?php
namespace app\quotient\controller;

use app\common\model\Agent;
use think\Controller;
use think\captcha\Captcha;
use think\facade\Validate;

class Login extends Controller
{
    private $cache_model,$system;
    public function initialize(){
        //session('bid',69);
        //halt(session("bid"));
        if (session('quotient_id')) {
            $this->redirect('quotient/index/index');
        }
        $this->cache_model=array();
        $this->system = cache('System');
        $this->assign('system',$this->system);
        if(empty($this->system)){
            foreach($this->cache_model as $r){
                savecache($r);
            }
        }
    }

    public function index(){
        if(request()->isPost()) {
            $data = input('post.');
            $validate = Validate::make([
                'mobile|手机号'            => 'require|mobile',
                'password|密码'          => 'require'
            ]);
            if (!$validate->check($data)) {
                return AjaxReturn(0,$validate->getError());
            }
            $userModel = new Agent();
            $user = $userModel::get(["mobile"=>$data["mobile"]]);
            if(!$user){
                return  AjaxReturn(0,"用户不存在");
            }
            $password = MD5(config("salt").$data["password"]);
            if($user['password'] != $password){
                return AjaxReturn(0,"账号密码错误");
            }
            if($user["is_lock"]){
                return AjaxReturn(0,"该用户已经被锁定,请联系管理员");
            }
            session('quotient_id',$user['id']);
            //$this->update(['last_time'=>time()],['id'=>$user['id']]);
            return AjaxReturn(1,"登录成功");

        }
        return view('login');
    }

    public function verify(){
        $config =    [
            //验证码字符集
            'codeSet'   => '0123456789',
            // 验证码字体大小
            'fontSize'    =>    25,
            // 验证码位数
            'length'      =>    4,
            // 关闭验证码杂点
            'useNoise'    =>    false,
            'bg'          =>    [255,255,255],
        ];
        $captcha = new Captcha($config);
        return $captcha->entry();
    }


}