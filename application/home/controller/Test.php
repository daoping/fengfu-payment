<?php

namespace app\home\controller;

class Test
{
    public function test(){
        $json1 ='{
    "version":"1.0",
    "charset":"UTF-8",
    "signType":"01",
    "mid":"6888801044802",
    "orderCode":"16567346667995",
    "reqTime":"20220702120426",
    "txnTimeOut":"20220702123426",
    "productId":"00002022",
    "clearCycle":"0",
    "totalAmount":"000000000101",
    "notifyUrl":"http://121.4.89.230:228/sdmore/index/notify",
    "frontUrl":"https://www.baidu.com",
    "clientIp":"121.4.89.230",
    "productName":"测试商品",
    "extend":"{}",
    "beforKey":"MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQC7y8lEgKJ4AVc9GdS7SknaE/b3gnql6pZzaxqviyKubhgfuXa7y+5jPmXzAjIoRtzUCoieHuicBjl6UiSd6y39L51pGmHSkmJk2nsKqNB3HETIpkuMsBZxEgovoKJxHu28DwoU2M/ugfaFtRgNcOrBROCxHWz2/BQLv/9r1H9GzPpRaJ5ja3LlEfyIiBDTIEEiswxxnlNBtZZ3f+Zjmr4Fz8cfwOTPETv3KZt70y5HsDn1o5OMlqMFTGv9YpXdHtV+lX7we0iUoh3N0xIRsweL4MbZOQsmAHZvJGzuTOXfjhaI9lNoy5eeP/MR/KanbxmcjCFxSqTi0VMo6qzMCBmBAgMBAAECggEAXJk4beGrutaWaJdjENKppbFwdkzNIyCP/OtT5jkJ6c55cLMne6QrsJ3k8TUk/D0syBVFlgoRklVzZAA68+8x5wTibh/HGmh5wdcFEpV47TJwLCvrVrzA5BIMGb3wOy0P9knII81gge9tWjKYYJBMAO5wiITE5OgZDk4mqMnH1GWHt+d+D7Nk5y7Rrl47FTryAtqAuG11xxanT1Yyl6jPECi5ODCGcQIXd2bxYdq8QrgJoCu22lQDiebiMVL1o1L2rUWoO712SdMfiTRxc5APAR2ZFkEuljX6lxuZV6YQAdLQG4aWSLfRfM+C0w8mUHtXO5OnszzbaFVGgL2VLVxJ7QKBgQDUvNu5S1+YhOyz/NwI+fahH4Xkmax/BI1VK3pBaXHQ0RGxrpTHdsLAGPhX18Es0Wzjao+T+7DAguhfRHHzh9IbjImESB67269nlxck3wwgzOREwR3o/YYNjO8lUokA2ckRg1LR8ORcJicrBcnaHXbKEhwDgc6KZNx7HgsZoLeMvwKBgQDh/HY47FZAeE9buSs5FoCniDDf21FoFUpM/Wx3AcD/9SuF/7ECZ+abSmlw4pkPMfUAnpJOiEemxyqbPOVUZu3qZxF1CKn6sZHGbXWlJ60JUwMtSkYksKc11V1y3l7N8lSVPVtOVE8Of6+kr+pBDQzm/JRLaqjpyTPHGN7xF9SpvwKBgGO00FacGQXm5vnEvWwHm6wQljR6QfqWK5eLdRVLUmjIVD6u0FRcORLHoT2qhYN39EzyGLEG+eSAdmCQN8goFDWNPLafBqiNYjxu+A3z7D9DSIlg6Z/Vkwquzzby4/teF3r5zctP1/fIefsUD3MG9DHpIvAR2rfKZfveLN2qEd0JAoGBALogz9aOgXPUI7iuSHVanVhfCbI211PcxTeoOx/BqA6pBdH2CPqsTPl2TmYVnG8xfFOoR2S7RhiNTgBSJahgpnA7t9sm6al7e09tdEo05YYagzFYTXGGthHWzIMBuB/0CcppoLxhIEQW0rnX0h8F+qx9OD7teHYlq6m+SFFWiWfNAoGBAM9+bS8rXj7QNVUESMlCwN2nhpMHnwKxtxYU2W8Oj8J5IHZ/OytwlNATedKIV5V5isHIGaNv",
    "afterKey":"ThcOJ1XCV1C8n1q5Uiu8kr0KguGRyfifBF6eH2QCIVoR3ML3TVhcDs11gehyTinRFmpNTSMo7vHGeBnREAvDNT3WsBrNef6kkhBOpzWjaM/MkKIJsqaoRSuhGV2/S92SXmTJP+vshOGcHI+PLUBLWPXvYItY7PpMctlw3CNJaRM=",
    "sign":"SQwin6RE9NXtY1+FwMGme6yF/KIkyhXU9rsAPE6jfw9PbXv7G5dnqJPjy3Xx1Rhkc2hRH4vqNTElqM8zRHTh5xTC+uq+xF9Aw2qINM7/t2n+Qsp67UrEQwiBULKrYNABMQY4dt7DitKOw47IXRsjU6o0S+yZ2M0yqG6m9twdXkB9L7CSckbwohVqEYca4UjRYpBPOzW+K6AM01f+SFikKOQHbIzyTAu0ZMe23GH/m83VClGphTrCd9or9ni1mSqgAeql3HpeYnvg8odxFxRpbu8Uj7KswHzyxmJ6uexp7EwJX9MhUltMdaz9tUGgOtrdRC3BdkJIUcbHcwco16sblA=="
}';
        $json2 = '{
    "version":"1.0",
    "charset":"UTF-8",
    "signType":"01",
    "mid":"6888800045441",
    "orderCode":"711230036709907",
    "reqTime":"20220711150323",
    "txnTimeOut":"20220711153323",
    "productId":1657523003,
    "clearCycle":"0",
    "totalAmount":"000000000101",
    "notifyUrl":"https://mmall.mixienet.com.cn/notify.php",
    "frontUrl":"https://mmall.mixienet.com.cn",
    "clientIp":"172.0.0.1",
    "productName":"测试商品",
    "extend":"{}",
    "beforKey":"MIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQDB3q7Q5LPq/TFHPjTVmp7Y2kXgH1m90wC5qJhzabgwAqfnxDm0o75GCd1TIRwNvQ0rHK+I+Z1y+QNV9JXnINP/Jiw9K46GqGNs6Huvy5xweQ45kvHcNJ3HbdGHD26Flzm4mvAje1+uWRfxPJBqqZiue3FqgmfuXsh7GcUr5fNcR0Rz/PnfyI3Kp35uk2FvKFGsUH/8izB0hkaztqn6A9txXvvtanmGULi7NEmHfnxoB6nPLVRYHlFz48clCLihPHGQwWzCYdQkHduJr/dd0aJfoVrFUCEU3XuaPBa3znAEX6olG8GBv7JfuIsKfMwoZ+HAYIV3ixqHkW++lwebIvGxAgMBAAECggEBAJosku2A0yvvSn8Tk0TDf8uIWcVQaTeyU4NR9lriGh3kHs8Ll7B1/10Hf0EatYqfa5d7aoUNrQ6/xM1YgtWZn73rB4dl2iRwSRFVGPfGfpRbUNqsZzbtiQQN63Dy7rvCdZ6/aHVvKbonFAk+VsLcOJWZiapgJnLEYb6z0b+FUTFyAS9d2IdJOvOGHrG5NS2Z8XD3Za/7VbzG6jQwHYwL6rsG0ySf+nYXTL7G+/YCuwaBNd8wB6Ka49gM2bwWewCypBtxJ4ftKtJoUTag1SGNCzl1pDtQtwtoOyOwbD+cqhvVQ+hHzHuqPqIT9MU9eydquGhF3oJZwg2ur/y9WI42OJkCgYEA2j6DXu0i+spmSrtw2nG/fShk85ORozZiGGBUaGuMPDJf8quET8eVwdVB+uAvW4UhMtDd8hGZm0f03980+h/08j3rivv6J2pyRxfMPaD1L6wiAri6PiZIu6NO6Z6SQIGISA1TPbVtmTTqskgsuFLKXTvFi9JCCTjU9OrUf0QvcM8CgYEA42ixCrVWVXNnnLe0FUFL8+Nk47zG801if6v0RPlzpj7wgX6OUBFNEhSZ8qNs3rn0B5vG+byUOKXD7NGSC/DSN1V7UiAN2fnfltjabGwMl0rmBkLE7mBLufZq84zbgqXi2VFIpMoOwRg+cZK5i2Vv75BQZf3vRAVlwaFs+uNXFX8CgYEArja2saEEikPlMgS8inPAkw0awOFsoWe5GJJU7uHjGO5xbwqBXRoKLqeourYYt6rnHGRkt2ZbQvxGuiq4ZG38Hx9UliacwrgTLzMNA6kn7/0tTRTe/tuatcYgKRc+0iVUf4e7igf4zBLUwqogoKbeCVy8TefydOAcBsIqDMmGQTsCgYEAqwsaZnCAMCNuIiz2ESqcwyMcxkI97dAdruOaBasOdlNOuEeFKOnqaxf9tsd+4oNA7dwYK8zQO09K7zuturd2ldIAwIOFh5buuln+55/ZVI4sNzdPQfBB5OxYAznF/R1iY4xEtFWaBViD1w7B6fvw1mJYZm454ZKht6MAAjCuVmcCgYALY5lJoOv4Ajm87OXeEiI9sEdJXdSYzbPHDeJMyDIsib3C4GIfIzU6Ul6DJ+rPqT7ma0qddPGI",
    "afterKey":"OuMat87CoBXpA5vbLJVGyevI6ITpI771lmNOgMM/m33wKG5Jhv6je0WIGS8EPR2PrnAa+rA66R7liA1Iv748mvwwCbMwqhDl2MEx/HcuGL+HiYJ3L7NvohM65NC1QZb8otgI/3fiuxo9GKrOx5oc6bTgflI+E9poRmsQIbk2694=",
    "sign":"t6QTIFmGMw4ZUVxldl0QN3qGpuK593VMkKAzSVaRdBRDm4OvgMH8pwOWX9iD2J6O2M8+d3dnctQZMlbi8tehFcFwkcP+4VwsGUCfWmCGZ2xwpnMED9PiBL+WKFSQ66D0MJMd49PDH2FgAEdZebzvwc9zuhIym9ehgEdH/kzysE/J4of+RplqaBPIpro2j6G0sdz8pyl1olNa6H/xnmFErYOLfC4wfTEwr/7WuHVLQlrYFds2FCkC5paPagzduVSh1syvRVTcQhl9DjcY04YoVrwxQrGdatp6KEgMiUeswAQvmrnh/Sfa+9CfIo/sc0ALAd3EkuxEhLJZzsRbRY1CpA=="
}';
    }
}