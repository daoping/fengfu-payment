<?php
namespace app\home\controller;
use app\common\model\Agent;
use app\common\model\CzOrder;
use app\common\model\Order;
use app\common\model\PayChannel;
use think\Controller;
use think\Db;
use clt\Lunar;
use think\facade\Env;
use app\common\service\SdCommon;
use app\common\service\Cashier;
use think\facade\Validate;

use think\facade\Request;


class Fivepay extends Controller{



    public $privateKeyPwd = '123456';

    /**
     *h5 同步跳转
     * http://sd1.yapin.shop/home/fivepay/front?order_num=
     */
    public function front(){
        $model = new Order();
        $get = $_GET;
        $order_num = $get["order_num"];
        if(!$order_num){
            return AjaxReturn(0,"订单不存在");
        }
        $info =$model::get(["system_order_num"=>$order_num]);
        if(!$info){
            return AjaxReturn(0,"订单号已存在");
        }
        if($info["payment"] < 1){
            return AjaxReturn(0,"订单未支付");
        }
        $this->assign("info",$info);
        return view();
    }
    /**
     * 有沐杉德收银台
     */
    public function shandeyoumualipay(){
        $model = new Order();
        $id = input("get.id");
        if(!$id){
            return AjaxReturn(0,"参数错误");
        }
        $order = $model::get($id);
        if(!$order){
            return AjaxReturn(0,"订单不存在");
        }
        if($order["payment"] == 1){
            return AjaxReturn(0,"订单已支付");
        }
        $ip = request()->ip();
        $model->where("id",$id)->update(["buy_ip"=>$ip]);//更新付款用户下单ip


        $ip = str_replace(".","_",$ip);


        $PayChannelModel  = new PayChannel();
        $PayChannel = $PayChannelModel::get($order['cid']);
        if(!$PayChannel){
            return AjaxReturn(1,"支付通道不存在");
        }
        if($PayChannel['open'] != 1){
            return AjaxReturn(1,"通道未开启");
        }
        $order_num = $order['system_order_num'];
        $data = [
            'version' => 10,
            'mer_no' => $PayChannel['business'] , //商户号
            'mer_order_no' =>$order_num, //商户唯一订单号
            'create_time' => date('YmdHis'),
            'expire_time' => date('YmdHis', time()+30*60),
            'order_amt' => strval($order['pay_money']), //订单支付金额
            'notify_url' => ym().'/api/notify/youmualipaynotifysande', //订单支付异步通知
            'return_url' => ym()."/home/fivepay/front?order_num=".$order_num, //订单前端页面跳转地址
            'create_ip' => $ip,
            'goods_name' => '购买产品',//产品名称
            'store_id' => '000000',
            'product_code' => '02020002',
            'clear_cycle' => '3',
            'pay_extra' =>"{}", //pay_extra参考语雀文档4.3
            'accsplit_flag' => 'NO',
            'jump_scheme' => '',
            'meta_option' => json_encode([["s" => "Android","n" => "wxDemo","id" => "com.pay.paytypetest","sc" => "com.pay.paytypetest"]]),
            'sign_type' => 'RSA',
            'merch_extend_params'=>"{}"
        ];

        //goods_name , notify_url，return_url，pay_extra，meta_option，extend，merch_extend_params，sign
        $temp = $data;
        unset($temp['goods_name']);
        unset($temp['jump_scheme']);
        unset($temp['expire_time']);
        unset($temp['product_code']);
        unset($temp['clear_cycle']);
        unset($temp['meta_option']);

        //file_put_contents('log.txt', date('Y-m-d H:i:s', time()) . " 签名串:" . $this->getSignContent($temp) . "\r\n", FILE_APPEND);

        // echo $this->getSignContent($temp);exit;
        $sign = $this->sign($this->getSignContent($temp),$data['mer_no']);
        $data['sign'] = $sign;
        $query = http_build_query($data) ;
        //$query = http_build_query($data, null, '&');

        $payurl = "https://sandcash.mixienet.com.cn/pay/h5/alipay?".$query;
        //halt($redirectUrl);
        //return $payurl;
        header('Location:'.$payurl);
        die();
    }



    /**
     * 新版支付
     * http://dld1.yapin.shop/home/fivepay/xinpay
     * https://pay.lnxinmeng.xyz/home/fivepay/xinpay
     */
    public function xinpay(){
        $ip = getIp();
        $ip = str_replace(".","_",$ip);
        //halt($ip);
        //68888TS120015
        $order_num = orderNum();
        $data = [
            'version' => 10,
            'mer_no' =>  '6888801120015', //商户号
            'mer_order_no' =>$order_num, //商户唯一订单号
            'create_time' => date('YmdHis'),
            'expire_time' => date('YmdHis', time()+30*60),
            'order_amt' => '0.5', //订单支付金额
            'notify_url' => ym().'/api/notify/shadealipayyoumu', //订单支付异步通知
            'return_url' => ym()."/home/fivepay/front?order_num=".$order_num, //订单前端页面跳转地址
            'create_ip' => $ip,
            'goods_name' => '测试',//产品名称
            'store_id' => '000000',
            'product_code' => '02020002',
            'clear_cycle' => '3',
            //pay_extra参考语雀文档4.3
            'pay_extra' =>"{}",
            'accsplit_flag' => 'NO',
            'jump_scheme' => '',
            'meta_option' => json_encode([["s" => "Android","n" => "wxDemo","id" => "com.pay.paytypetest","sc" => "com.pay.paytypetest"]]),
            'sign_type' => 'RSA',
            'merch_extend_params'=>"{}"
        ];

        //goods_name , notify_url，return_url，pay_extra，meta_option，extend，merch_extend_params，sign
        $temp = $data;
        unset($temp['goods_name']);
        unset($temp['jump_scheme']);
        unset($temp['expire_time']);
        unset($temp['product_code']);
        unset($temp['clear_cycle']);
        unset($temp['meta_option']);

        //file_put_contents('log.txt', date('Y-m-d H:i:s', time()) . " 签名串:" . $this->getSignContent($temp) . "\r\n", FILE_APPEND);

        // echo $this->getSignContent($temp);exit;
        $sign = $this->sign($this->getSignContent($temp),"6888801120015");
        $data['sign'] = $sign;
        $query = http_build_query($data) ;
        //$query = http_build_query($data, null, '&');

        $payurl = "https://sandcash.mixienet.com.cn/pay/h5/alipay?".$query;
        //halt($redirectUrl);
        //return $payurl;
        header('Location:'.$payurl);
        die();
    }

    /**
     * 有沐app支付宝支付
     * @param $str
     * @return string
     * @throws \Exception
     */
    public function ymapp(){
        $model = new CzOrder();
        $id = input("get.id");
        if(!$id){
            return AjaxReturn(0,"参数错误");
        }
        $order = $model::get($id);
        if(!$order){
            return AjaxReturn(0,"订单不存在");
        }
        if($order["payment"] == 1){
            return AjaxReturn(0,"订单已支付");
        }
        $ip = getIp();
        $ip = str_replace(".","_",$ip);


        $PayChannelModel  = new PayChannel();
        $PayChannel = $PayChannelModel::get($order['cid']);
        if(!$PayChannel){
            return AjaxReturn(1,"支付通道不存在");
        }
        if($PayChannel['open'] != 1){
            return AjaxReturn(1,"通道未开启");
        }
        $order_num = $order['system_order_num'];
        $data = [
            'version' => 10,
            'mer_no' => $PayChannel['business'] , //商户号
            'mer_order_no' =>$order_num, //商户唯一订单号
            'create_time' => date('YmdHis'),
            'expire_time' => date('YmdHis', time()+30*60),
            'order_amt' => strval($order['money']), //订单支付金额
            'notify_url' => ym().'/api/notify/youmuapp', //订单支付异步通知
            'return_url' => ym()."/home/fivepay/front?order_num=".$order_num, //订单前端页面跳转地址
            'create_ip' => $ip,
            'goods_name' => '购买产品',//产品名称
            'store_id' => '000000',
            'product_code' => '02020002',
            'clear_cycle' => '3',
            'pay_extra' =>"{}", //pay_extra参考语雀文档4.3
            'accsplit_flag' => 'NO',
            'jump_scheme' => '',
            'meta_option' => json_encode([["s" => "Android","n" => "wxDemo","id" => "com.pay.paytypetest","sc" => "com.pay.paytypetest"]]),
            'sign_type' => 'RSA',
            'merch_extend_params'=>"{}"
        ];

        //goods_name , notify_url，return_url，pay_extra，meta_option，extend，merch_extend_params，sign
        $temp = $data;
        unset($temp['goods_name']);
        unset($temp['jump_scheme']);
        unset($temp['expire_time']);
        unset($temp['product_code']);
        unset($temp['clear_cycle']);
        unset($temp['meta_option']);

        //file_put_contents('log.txt', date('Y-m-d H:i:s', time()) . " 签名串:" . $this->getSignContent($temp) . "\r\n", FILE_APPEND);

        // echo $this->getSignContent($temp);exit;
        $sign = $this->sign($this->getSignContent($temp),$data['mer_no']);
        $data['sign'] = $sign;
        $query = http_build_query($data) ;
        //$query = http_build_query($data, null, '&');

        $payurl = "https://sandcash.mixienet.com.cn/pay/h5/alipay?".$query;
        //halt($redirectUrl);
        //return $payurl;
        header('Location:'.$payurl);
        die();
    }


   public function sign($str,$privateKeynum) {
        $privateKeyPath =   Env::get("root_path")."extend/sande/".$privateKeynum."/".$privateKeynum.".pfx";
        $file = file_get_contents($privateKeyPath);
        if (!$file) {
            throw new \Exception('loadPk12Cert::file
                    _get_contents');
        }
        if (!openssl_pkcs12_read($file, $cert, $this->privateKeyPwd)) {
            throw new \Exception('loadPk12Cert::openssl_pkcs12_read ERROR');
        }
        $pem = $cert['pkey'];
        openssl_sign($str, $sign, $pem);
        $sign = base64_encode($sign);
        return $sign;
    }

   public function getSignContent($params) {
        ksort($params);

        $stringToBeSigned = "";
        $i = 0;
        foreach ($params as $k => $v) {
            if (false === $this->checkEmpty($v) && "@" != substr($v, 0, 1)) {

                if ($i == 0) {
                    $stringToBeSigned .= "$k" . "=" . "$v";
                } else {
                    $stringToBeSigned .= "&" . "$k" . "=" . "$v";
                }
                $i++;
            }
        }

        unset ($k, $v);
        return $stringToBeSigned;
    }

   public function checkEmpty($value)
    {
        if (!isset($value))
            return true;
        if ($value === null)
            return true;
        if (trim($value) === "")
            return true;

        return false;
    }

    /******************************************图南杉德支付宝***************************************************/

    /**
     * 图南app支付
     * @return void
     *  http://dld1.yapin.shop/home/fivepay/xinmengapp
     */
    public function tunanapp(){
        $model = new CzOrder();
        $id = input("get.id");
        if(!$id){
            return AjaxReturn(0,"参数错误");
        }
        $order = $model::get($id);
        if(!$order){
            return AjaxReturn(0,"订单不存在");
        }
        if($order["payment"] == 1){
            return AjaxReturn(0,"订单已支付");
        }
        $ip = getIp();
        $ip = str_replace(".","_",$ip);


        $PayChannelModel  = new PayChannel();
        $PayChannel = $PayChannelModel::get($order['cid']);
        if(!$PayChannel){
            return AjaxReturn(1,"支付通道不存在");
        }
        if($PayChannel['open'] != 1){
            return AjaxReturn(1,"通道未开启");
        }
        $order_num = $order['system_order_num'];
        $data = [
            'version' => 10,
            'mer_no' => $PayChannel['business'] , //商户号
            'mer_order_no' =>$order_num, //商户唯一订单号
            'create_time' => date('YmdHis'),
            'expire_time' => date('YmdHis', time()+30*60),
            'order_amt' => strval($order['money']), //订单支付金额
            'notify_url' => ym().'/api/notify/tunanappnotify', //订单支付异步通知
            'return_url' => ym()."/home/fivepay/front?order_num=".$order_num, //订单前端页面跳转地址
            'create_ip' => $ip,
            'goods_name' => '购买产品',//产品名称
            'store_id' => '000000',
            'product_code' => '02020002',
            'clear_cycle' => '3',
            'pay_extra' =>"{}", //pay_extra参考语雀文档4.3
            'accsplit_flag' => 'NO',
            'jump_scheme' => '',
            'meta_option' => json_encode([["s" => "Android","n" => "wxDemo","id" => "com.pay.paytypetest","sc" => "com.pay.paytypetest"]]),
            'sign_type' => 'RSA',
            'merch_extend_params'=>"{}"
        ];

        //goods_name , notify_url，return_url，pay_extra，meta_option，extend，merch_extend_params，sign
        $temp = $data;
        unset($temp['goods_name']);
        unset($temp['jump_scheme']);
        unset($temp['expire_time']);
        unset($temp['product_code']);
        unset($temp['clear_cycle']);
        unset($temp['meta_option']);

        //file_put_contents('log.txt', date('Y-m-d H:i:s', time()) . " 签名串:" . $this->getSignContent($temp) . "\r\n", FILE_APPEND);

        // echo $this->getSignContent($temp);exit;
        $sign = $this->sign($this->getSignContent($temp),$data['mer_no']);
        $data['sign'] = $sign;
        $query = http_build_query($data) ;
        //$query = http_build_query($data, null, '&');

        $payurl = "https://sandcash.mixienet.com.cn/pay/h5/alipay?".$query;
        //halt($redirectUrl);
        //return $payurl;
        header('Location:'.$payurl);
        die();
    }

    /**
     * 图南杉德收银台支付
     */
    public function tunanshouyintai(){
        $model = new Order();
        $id = input("get.id");
        if(!$id){
            return AjaxReturn(0,"参数错误");
        }
        $order = $model::get($id);
        if(!$order){
            return AjaxReturn(0,"订单不存在");
        }
        if($order["payment"] == 1){
            return AjaxReturn(0,"订单已支付");
        }
        $ip = request()->ip();
        $model->where("id",$id)->update(["buy_ip"=>$ip]);//更新付款用户下单ip


        $ip = str_replace(".","_",$ip);


        $PayChannelModel  = new PayChannel();
        $PayChannel = $PayChannelModel::get($order['cid']);
        if(!$PayChannel){
            return AjaxReturn(1,"支付通道不存在");
        }
        if($PayChannel['open'] != 1){
            return AjaxReturn(1,"通道未开启");
        }
        $order_num = $order['system_order_num'];
        $data = [
            'version' => 10,
            'mer_no' => $PayChannel['business'] , //商户号
            'mer_order_no' =>$order_num, //商户唯一订单号
            'create_time' => date('YmdHis'),
            'expire_time' => date('YmdHis', time()+30*60),
            'order_amt' => strval($order['pay_money']), //订单支付金额
            'notify_url' => ym().'/api/notify/tunnasytnotify', //订单支付异步通知
            'return_url' => ym()."/home/fivepay/front?order_num=".$order_num, //订单前端页面跳转地址
            'create_ip' => $ip,
            'goods_name' => '购买产品',//产品名称
            'store_id' => '000000',
            'product_code' => '02020002',
            'clear_cycle' => '3',
            'pay_extra' =>"{}", //pay_extra参考语雀文档4.3
            'accsplit_flag' => 'NO',
            'jump_scheme' => '',
            'meta_option' => json_encode([["s" => "Android","n" => "wxDemo","id" => "com.pay.paytypetest","sc" => "com.pay.paytypetest"]]),
            'sign_type' => 'RSA',
            'merch_extend_params'=>"{}"
        ];

        //goods_name , notify_url，return_url，pay_extra，meta_option，extend，merch_extend_params，sign
        $temp = $data;
        unset($temp['goods_name']);
        unset($temp['jump_scheme']);
        unset($temp['expire_time']);
        unset($temp['product_code']);
        unset($temp['clear_cycle']);
        unset($temp['meta_option']);

        //file_put_contents('log.txt', date('Y-m-d H:i:s', time()) . " 签名串:" . $this->getSignContent($temp) . "\r\n", FILE_APPEND);

        // echo $this->getSignContent($temp);exit;
        $sign = $this->sign($this->getSignContent($temp),$data['mer_no']);
        $data['sign'] = $sign;
        $query = http_build_query($data) ;
        //$query = http_build_query($data, null, '&');

        $payurl = "https://sandcash.mixienet.com.cn/pay/h5/alipay?".$query;
        //halt($redirectUrl);
        //return $payurl;
        header('Location:'.$payurl);
        die();
    }


    /***********************************************鑫梦支付宝***************************************************/

    /**
     * 鑫梦app支付
     * @return void
     *  http://dld1.yapin.shop/home/fivepay/xinmengapp
     */
    public function xinmengapp(){
        $model = new CzOrder();
        $id = input("get.id");
        if(!$id){
            return AjaxReturn(0,"参数错误");
        }
        $order = $model::get($id);
        if(!$order){
            return AjaxReturn(0,"订单不存在");
        }
        if($order["payment"] == 1){
            return AjaxReturn(0,"订单已支付");
        }
        $ip = getIp();
        $ip = str_replace(".","_",$ip);


        $PayChannelModel  = new PayChannel();
        $PayChannel = $PayChannelModel::get($order['cid']);
        if(!$PayChannel){
            return AjaxReturn(1,"支付通道不存在");
        }
        if($PayChannel['open'] != 1){
            return AjaxReturn(1,"通道未开启");
        }
        $order_num = $order['system_order_num'];
        $data = [
            'version' => 10,
            'mer_no' => $PayChannel['business'] , //商户号
            'mer_order_no' =>$order_num, //商户唯一订单号
            'create_time' => date('YmdHis'),
            'expire_time' => date('YmdHis', time()+30*60),
            'order_amt' => strval($order['money']), //订单支付金额
            'notify_url' => ym().'/api/notify/xinmengappnotify', //订单支付异步通知
            'return_url' => ym()."/home/fivepay/front?order_num=".$order_num, //订单前端页面跳转地址
            'create_ip' => $ip,
            'goods_name' => '购买产品',//产品名称
            'store_id' => '000000',
            'product_code' => '02020002',
            'clear_cycle' => '3',
            'pay_extra' =>"{}", //pay_extra参考语雀文档4.3
            'accsplit_flag' => 'NO',
            'jump_scheme' => '',
            'meta_option' => json_encode([["s" => "Android","n" => "wxDemo","id" => "com.pay.paytypetest","sc" => "com.pay.paytypetest"]]),
            'sign_type' => 'RSA',
            'merch_extend_params'=>"{}"
        ];

        //goods_name , notify_url，return_url，pay_extra，meta_option，extend，merch_extend_params，sign
        $temp = $data;
        unset($temp['goods_name']);
        unset($temp['jump_scheme']);
        unset($temp['expire_time']);
        unset($temp['product_code']);
        unset($temp['clear_cycle']);
        unset($temp['meta_option']);

        //file_put_contents('log.txt', date('Y-m-d H:i:s', time()) . " 签名串:" . $this->getSignContent($temp) . "\r\n", FILE_APPEND);

        // echo $this->getSignContent($temp);exit;
        $sign = $this->sign($this->getSignContent($temp),$data['mer_no']);
        $data['sign'] = $sign;
        $query = http_build_query($data) ;
        //$query = http_build_query($data, null, '&');

        $payurl = "https://sandcash.mixienet.com.cn/pay/h5/alipay?".$query;
        //halt($redirectUrl);
        //return $payurl;
        header('Location:'.$payurl);
        die();
    }

    /**
     * 鑫梦收银台支付
     */
    public function xinmengshouyintai(){
        $model = new Order();
        $id = input("get.id");
        if(!$id){
            return AjaxReturn(0,"参数错误");
        }
        $order = $model::get($id);
        if(!$order){
            return AjaxReturn(0,"订单不存在");
        }
        if($order["payment"] == 1){
            return AjaxReturn(0,"订单已支付");
        }
        $ip = request()->ip();
        $model->where("id",$id)->update(["buy_ip"=>$ip]);//更新付款用户下单ip


        $ip = str_replace(".","_",$ip);


        $PayChannelModel  = new PayChannel();
        $PayChannel = $PayChannelModel::get($order['cid']);
        if(!$PayChannel){
            return AjaxReturn(1,"支付通道不存在");
        }
        if($PayChannel['open'] != 1){
            return AjaxReturn(1,"通道未开启");
        }
        $order_num = $order['system_order_num'];
        $data = [
            'version' => 10,
            'mer_no' => $PayChannel['business'] , //商户号
            'mer_order_no' =>$order_num, //商户唯一订单号
            'create_time' => date('YmdHis'),
            'expire_time' => date('YmdHis', time()+30*60),
            'order_amt' => strval($order['pay_money']), //订单支付金额
            'notify_url' => ym().'/api/notify/xinmengsytnotify', //订单支付异步通知
            'return_url' => ym()."/home/fivepay/front?order_num=".$order_num, //订单前端页面跳转地址
            'create_ip' => $ip,
            'goods_name' => '购买产品',//产品名称
            'store_id' => '000000',
            'product_code' => '02020002',
            'clear_cycle' => '3',
            'pay_extra' =>"{}", //pay_extra参考语雀文档4.3
            'accsplit_flag' => 'NO',
            'jump_scheme' => '',
            'meta_option' => json_encode([["s" => "Android","n" => "wxDemo","id" => "com.pay.paytypetest","sc" => "com.pay.paytypetest"]]),
            'sign_type' => 'RSA',
            'merch_extend_params'=>"{}"
        ];

        //goods_name , notify_url，return_url，pay_extra，meta_option，extend，merch_extend_params，sign
        $temp = $data;
        unset($temp['goods_name']);
        unset($temp['jump_scheme']);
        unset($temp['expire_time']);
        unset($temp['product_code']);
        unset($temp['clear_cycle']);
        unset($temp['meta_option']);

        //file_put_contents('log.txt', date('Y-m-d H:i:s', time()) . " 签名串:" . $this->getSignContent($temp) . "\r\n", FILE_APPEND);

        // echo $this->getSignContent($temp);exit;
        $sign = $this->sign($this->getSignContent($temp),$data['mer_no']);
        $data['sign'] = $sign;
        $query = http_build_query($data) ;
        //$query = http_build_query($data, null, '&');

        $payurl = "https://sandcash.mixienet.com.cn/pay/h5/alipay?".$query;
        //halt($redirectUrl);
        //return $payurl;
        header('Location:'.$payurl);
        die();
    }

    /***************************************杉德测试环境****************************************************************/
    /**
     * 杉德测试环境
     * https://pay.lnxinmeng.xyz/home/fivepay/tunanshandepaytest
     */
    public function tunanshandepaytest_(){
        $ip = getIp();
        $ip = str_replace(".","_",$ip);
        //halt($ip);
        $mer_no = "6888805120113";
        $order_num = orderNum();
        db("test")->insert(["add_time"=>time(),"content"=>$order_num,"ip"=>"","explain"=>"测试订单"]);
        $data = [
            'version' => 10,
            'mer_no' =>  $mer_no, //商户号
            'mer_order_no' =>$order_num, //商户唯一订单号
            'create_time' => date('YmdHis'),
            'expire_time' => date('YmdHis', time()+30*60),
            'order_amt' => strval(10.00), //订单支付金额
            'notify_url' => ym().'/api/notify/xinmengsytnotify', //订单支付异步通知
            'return_url' => ym()."/home/fivepay/front?order_num=".$order_num, //订单前端页面跳转地址
            'create_ip' => $ip,
            'goods_name' => '购买产品',//产品名称
            'store_id' => '000000',
            'product_code' => '02020002',
            'clear_cycle' => '3',
            'pay_extra' =>"{}", //pay_extra参考语雀文档4.3
            'accsplit_flag' => 'NO',
            'jump_scheme' => '',
            'meta_option' => json_encode([["s" => "Android","n" => "wxDemo","id" => "com.pay.paytypetest","sc" => "com.pay.paytypetest"]]),
            'sign_type' => 'RSA',
            'merch_extend_params'=>"{}"

        ];
        $temp = $data;
        unset($temp['goods_name']);
        unset($temp['jump_scheme']);
        unset($temp['expire_time']);
        unset($temp['product_code']);
        unset($temp['clear_cycle']);
        unset($temp['meta_option']);

        //file_put_contents('log.txt', date('Y-m-d H:i:s', time()) . " 签名串:" . $this->getSignContent($temp) . "\r\n", FILE_APPEND);

        // echo $this->getSignContent($temp);exit;
        $sign = $this->sign($this->getSignContent($temp),$mer_no);
        $data['sign'] = $sign;
        $query = http_build_query($data) ;
        //$query = http_build_query($data, null, '&');

        $payurl = "https://sandcash.mixienet.com.cn/pay/h5/alipay?".$query;
        //halt($redirectUrl);
        header('Location:'.$payurl);
        die();
    }

    /*********************************************达运测试***********************************************************/

    /**
     * 新版支付
     * http://pay1.lnxinmeng.xyz/home/fivepay/sandepay
     * https://pay.lnxinmeng.xyz/home/fivepay/sandepay
     */
    public function sandepay(){
        $ip = getIp();
        $ip = str_replace(".","_",$ip);
        //halt($ip);
        $mer_no = "68888TS123146";
        $order_num = orderNum();
        db("test")->insert(["add_time"=>time(),"content"=>$order_num,"ip"=>"","explain"=>"测试订单"]);
        $data = [
            'version' => 10,
            'mer_no' =>  $mer_no, //商户号
            'mer_order_no' =>$order_num, //商户唯一订单号
            'create_time' => date('YmdHis'),
            'expire_time' => date('YmdHis', time()+30*60),
            'order_amt' => strval(10.00), //订单支付金额
            'notify_url' => ym().'/api/notify/xinmengsytnotify', //订单支付异步通知
            'return_url' => ym()."/home/fivepay/front?order_num=".$order_num, //订单前端页面跳转地址
            'create_ip' => $ip,
            'goods_name' => '购买产品',//产品名称
            'store_id' => '000000',
            'product_code' => '02020002',
            'clear_cycle' => '3',
            'pay_extra' =>"{}", //pay_extra参考语雀文档4.3
            'accsplit_flag' => 'NO',
            'jump_scheme' => '',
            'meta_option' => json_encode([["s" => "Android","n" => "wxDemo","id" => "com.pay.paytypetest","sc" => "com.pay.paytypetest"]]),
            'sign_type' => 'RSA',
            'merch_extend_params'=>"{}"

        ];
        $temp = $data;
        unset($temp['goods_name']);
        unset($temp['jump_scheme']);
        unset($temp['expire_time']);
        unset($temp['product_code']);
        unset($temp['clear_cycle']);
        unset($temp['meta_option']);

        //file_put_contents('log.txt', date('Y-m-d H:i:s', time()) . " 签名串:" . $this->getSignContent($temp) . "\r\n", FILE_APPEND);

        // echo $this->getSignContent($temp);exit;
        $sign = $this->sign($this->getSignContent($temp),$mer_no);
        $data['sign'] = $sign;
        $query = http_build_query($data) ;
        //$query = http_build_query($data, null, '&');

        $payurl = "https://sandcash-uat01.sand.com.cn/pay/h5/alipay?".$query;//测试的url

        //halt($redirectUrl);
        header('Location:'.$payurl);
        die();
    }


}