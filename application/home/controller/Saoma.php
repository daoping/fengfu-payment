<?php
namespace app\home\controller;
use app\common\model\Agent;
use app\common\model\Order;
use think\Controller;
use think\Db;
use clt\Lunar;
use think\facade\Env;
use app\common\service\SdCommon;
use app\common\service\Cashier;
use think\facade\Validate;

use think\facade\Request;


class Saoma extends Controller{

    // http://sd1.yapin.shop/home/fivepay/pay
    // http://sd.yapin.shop/home/saoma/pay
    public function pay(){
        $publicKeyMi = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCjW8kxHWXtgUfQRkMy0418PlvvD8CUslJG4AtzXXbcstU+fxC5bL5FEwhq14hvnDtgN//4enhjSfB/6bJV62asXqq5xr5/zAHlMGW1PY/F7Di7ospTv/rQYPX+x1SxGj4ilWtu8Ljpr6q2ZY2h/WfGUpnCgUnTLK/0hGwGoCEtWQIDAQAB";

// 请求url
        $url = "https://boss.sandpay.com.cn/gateway/sand/alih5";

        $path = Env::get("root_path")."extend/cert/xiangtong.pfx";   // 商户导出私钥(用于签名sign)
        $pwd = '123456';   // 商户导出私钥时填写的密码
        $pri = loadPk12Cert($path,$pwd);
        $search = [
            "-----BEGIN PRIVATE KEY-----",
            "-----END PRIVATE KEY-----",
            "\n",
            "\r",
            "\r\n"
        ];
        $privateKey=str_replace($search,"",$pri);

        $data = array(
            "version" => "1.0",
            "charset" => "UTF-8",
            "signType" => "01",
            "mid" => "6888801044802", // 替换成生产商户号
            "orderCode" => orderNum(), // 商户唯一订单号
            "reqTime" => date("YmdHis", time()),
            "txnTimeOut" => date("YmdHis", time()+30*60),
            "productId" => time(),
            "clearCycle" => "0", //0-T1(默认) 1-T0 2-D0  3-D1
            "totalAmount" => "000000000101",
            "notifyUrl" => "https://mmall.mixienet.com.cn/notify.php",
            "frontUrl" => "https://mmall.mixienet.com.cn",
            "clientIp" => getIp(),
            "productName" => "测试商品",
            "extend" => "{}"
        );

// 对商户支付私钥privateKey后100位加密处理
        $b=strlen($privateKey)-100;
        $beforKey=substr($privateKey,0,$b);
        $afterKey=substr($privateKey,$b);
        $MiKey="";
        $pubKey = "-----BEGIN PUBLIC KEY-----\n" .
            wordwrap($publicKeyMi, 64, "\n", true) .
            "\n-----END PUBLIC KEY-----";
        $pubKey = openssl_pkey_get_public($pubKey);
        $MiKey = openssl_public_encrypt($afterKey,$encrypted,$pubKey) ? base64_encode($encrypted) : null;
        $data['beforKey'] = $beforKey;
        $data['afterKey'] = $MiKey;  //私钥后100位加密的数据
// 使用商户私钥privateKey进行签名
        $sign = sign(getSignContent($data), $privateKey, "RSA");
        $data['sign'] = $sign;
        //halt($data);
        $redirectUrl = $url."?name=".urlencode(json_encode($data));
        //halt($redirectUrl);
        header('Location:'.$redirectUrl);
    }





}