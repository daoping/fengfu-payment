<?php
namespace app\home\controller;
use app\common\model\Agent;
use app\common\model\Order;
use think\Controller;
use think\Db;
use clt\Lunar;
use think\facade\Env;
use app\common\service\SdCommon;
use app\common\service\Cashier;
use think\facade\Validate;

use think\facade\Request;


class Fivepay备份 extends Controller{



    public $privateKeyPwd = '123456';
    // http://sd.yapin.shop/home/fivepay/pay
    // http://sd1.yapin.shop/home/fivepay/pay?id=312

    public function pay(){
        $id = input("get.id");
        if(!$id){
            return AjaxReturn(0,"参数错误");
        }
        $model = new Order();
        $order = $model::get($id);
        if(!$order){
            return AjaxReturn(0,"订单不存在");
        }
        if($order["payment"] > 0){
            return AjaxReturn(0,"订单已支付");
        }



        //请求url
        $url = "https://sandcash.mixienet.com.cn/pay/h5/alipaycode";
        $path = Env::get("root_path")."extend/sande/6888800119692/si.pfx";   // 商户导出私钥(用于签名sign)
        $pwd = '123456';   // 商户导出私钥时填写的密码
        $pri = loadPk12Cert($path,$pwd);
        $search = [
            "-----BEGIN PRIVATE KEY-----",
            "-----END PRIVATE KEY-----",
            "\n",
            "\r",
            "\r\n"
        ];
        $privateKey=str_replace($search,"",$pri);
        $order_num = $order["orderCode"];
        $money = strval($order["money"]*100);
        $pay_money =  str_pad($money,12,"0",STR_PAD_LEFT);
        $data = array(
            "version" => "1.0",
            "charset" => "UTF-8",
            "signType" => "01",
            "mid" => $order['mid'], // 替换成生产商户号
            "orderCode" => $order_num, // 商户唯一订单号
            "reqTime" => date("YmdHis", time()),
            "txnTimeOut" => date("YmdHis", time()+30*60),
            "productId" => "02020002",
            "clearCycle" => "0", //0-T1(默认) 1-T0 2-D0  3-D1
            "totalAmount" => $pay_money,//101 分   12位 不足的自动补0  1分开始
            "notifyUrl" => ym()."/api/notify/sandepaysbcigfixkk",
            "frontUrl" => ym()."/home/fivepay/front?order_num=".$order_num,
            "clientIp" => getIp(),
            "productName" => "会员充值-".$id,
            "extend" => "{}"
        );
        //halt($data);
// 对商户支付私钥privateKey后100位加密处理
        $b=strlen($privateKey)-100;
        $beforKey=substr($privateKey,0,$b);
        $afterKey=substr($privateKey,$b);
       // $publicKeyMi = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCjW8kxHWXtgUfQRkMy0418PlvvD8CUslJG4AtzXXbcstU+fxC5bL5FEwhq14hvnDtgN//4enhjSfB/6bJV62asXqq5xr5/zAHlMGW1PY/F7Di7ospTv/rQYPX+x1SxGj4ilWtu8Ljpr6q2ZY2h/WfGUpnCgUnTLK/0hGwGoCEtWQIDAQAB";
//        $pubKey = "-----BEGIN PUBLIC KEY-----\n" .
//            wordwrap($publicKeyMi, 64, "\n", true) .
//            "\n-----END PUBLIC KEY-----";
        $pubKey = Env::get("root_path")."extend/sande/6888800119692/gong.cer";   // 商户导出私钥(用于签名sign)
        $pubKey = openssl_pkey_get_public($pubKey);
        $MiKey = openssl_public_encrypt($afterKey,$encrypted,$pubKey) ? base64_encode($encrypted) : null;
        $data['beforKey'] = $beforKey;
        $data['afterKey'] = $MiKey;  //私钥后100位加密的数据
// 使用商户私钥privateKey进行签名
        $sign = sign(getSignContent($data), $privateKey, "RSA");
        $data['sign'] = $sign;
        db("test")->insert(["add_time"=>time(),"content"=>$order_num."订单号请求参数$id".json_encode($data)]);
        //halt($data);
        $redirectUrl = $url."?name=".urlencode(json_encode($data));
        //halt($redirectUrl);
        header('Location:'.$redirectUrl);
    }


    /**
     * 支付demo备份
     * @return void
     * @throws \Exception
     */
    public function pay_(){
        $publicKeyMi = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCjW8kxHWXtgUfQRkMy0418PlvvD8CUslJG4AtzXXbcstU+fxC5bL5FEwhq14hvnDtgN//4enhjSfB/6bJV62asXqq5xr5/zAHlMGW1PY/F7Di7ospTv/rQYPX+x1SxGj4ilWtu8Ljpr6q2ZY2h/WfGUpnCgUnTLK/0hGwGoCEtWQIDAQAB";

        //请求url
        $url = "https://boss.sandpay.com.cn/gateway/sand/alih5";
        $path = Env::get("root_path")."extend/cert/luyousiyue.pfx";   // 商户导出私钥(用于签名sign)
        $pwd = '123456';   // 商户导出私钥时填写的密码
        $pri = loadPk12Cert($path,$pwd);
        $search = [
            "-----BEGIN PRIVATE KEY-----",
            "-----END PRIVATE KEY-----",
            "\n",
            "\r",
            "\r\n"
        ];
        $privateKey=str_replace($search,"",$pri);
        $order_num = orderNum();
        $data = array(
            "version" => "1.0",
            "charset" => "UTF-8",
            "signType" => "01",
            "mid" => "6888800045441", // 替换成生产商户号
            "orderCode" => $order_num, // 商户唯一订单号
            "reqTime" => date("YmdHis", time()),
            "txnTimeOut" => date("YmdHis", time()+30*60),
            "productId" => "00002022",
            "clearCycle" => "0", //0-T1(默认) 1-T0 2-D0  3-D1
            "totalAmount" => "000000000011",//101 分   12位 不足的自动补0  1分开始
            "notifyUrl" => ym()."/home/notify/fiveQsL7EhBeZ5zKlZI4AbtrAb5CqLw9f12i",
            "frontUrl" => ym()."/home/fivepay/front?order_num=".$order_num,
            "clientIp" => getIp(),
            "productName" => "购买商品",
            "extend" => "{}"
        );

// 对商户支付私钥privateKey后100位加密处理
        $b=strlen($privateKey)-100;
        $beforKey=substr($privateKey,0,$b);
        $afterKey=substr($privateKey,$b);
        $MiKey="";
        $pubKey = "-----BEGIN PUBLIC KEY-----\n" .
            wordwrap($publicKeyMi, 64, "\n", true) .
            "\n-----END PUBLIC KEY-----";
        $pubKey = openssl_pkey_get_public($pubKey);
        $MiKey = openssl_public_encrypt($afterKey,$encrypted,$pubKey) ? base64_encode($encrypted) : null;
        $data['beforKey'] = $beforKey;
        $data['afterKey'] = $MiKey;  //私钥后100位加密的数据
// 使用商户私钥privateKey进行签名
        $sign = sign(getSignContent($data), $privateKey, "RSA");
        $data['sign'] = $sign;
        db("test")->insert(["add_time"=>time(),"content"=>$order_num."订单号签名是".json_encode($sign)]);
        //halt($data);
        $redirectUrl = $url."?name=".urlencode(json_encode($data));
        //halt($redirectUrl);
        header('Location:'.$redirectUrl);
    }

    /**
     *h5 同步跳转
     * http://sd1.yapin.shop/home/fivepay/front?order_num=
     */
    public function front(){
        $model = new Order();
        $get = $_GET;
        $order_num = $get["order_num"];
        if(!$order_num){
            return AjaxReturn(0,"订单不存在");
        }
        $info =$model::get(["system_order_num"=>$order_num]);
        if(!$info){
            return AjaxReturn(0,"订单号已存在");
        }
        if($info["payment"] < 1){
            return AjaxReturn(0,"订单未支付");
        }
        $this->assign("info",$info);
        return view();
    }



    /**
     * 新版支付
     * http://dld1.yapin.shop/home/fivepay/xinpay
     * https://pay.lnxinmeng.xyz/home/fivepay/xinpay
     */
    public function xinpay(){
        $ip = getIp();
        $ip = str_replace(".","_",$ip);
        //halt($ip);
        //68888TS120015
        $order_num = orderNum();
        $data = [
            'version' => 10,
            'mer_no' =>  '68888TS120015', //商户号
            'mer_order_no' =>$order_num, //商户唯一订单号
            'create_time' => date('YmdHis'),
            'expire_time' => date('YmdHis', time()+30*60),
            'order_amt' => '0.5', //订单支付金额
            'notify_url' => ym().'/api/notify/shadealipayyoumu', //订单支付异步通知
            'return_url' => 'https://www.baidu.com', //订单前端页面跳转地址
            'create_ip' => $ip,
            'goods_name' => '测试',
            'store_id' => '000000',
            'product_code' => '02020002',
            //  ○ 开户账户页面 product_code：00000001
            //  ○ 消费C2B product_code：04010001
            //  ○ 担保消费(C2C)  product_code：04010004
            //  ○ 消费（C2C） product_code：04010003
            'clear_cycle' => '3',
            //pay_extra参考语雀文档4.3
            'pay_extra' => json_encode(["userId"=>"12","nickName"=>"123","accountType"=>"1"]),
            'accsplit_flag' => 'NO',
            'jump_scheme' => '',
            'meta_option' => json_encode([["s" => "Android","n" => "wxDemo","id" => "com.pay.paytypetest","sc" => "com.pay.paytypetest"]]),
            'sign_type' => 'RSA'

        ];
        $temp = $data;
        unset($temp['goods_name']);
        unset($temp['jump_scheme']);
        unset($temp['expire_time']);
        unset($temp['product_code']);
        unset($temp['clear_cycle']);
        unset($temp['meta_option']);

        //file_put_contents('log.txt', date('Y-m-d H:i:s', time()) . " 签名串:" . $this->getSignContent($temp) . "\r\n", FILE_APPEND);

        // echo $this->getSignContent($temp);exit;
        $sign = $this->sign($this->getSignContent($temp));
        $data['sign'] = $sign;
        $query = http_build_query($data) ;
        //$query = http_build_query($data, null, '&');

        $payurl = "https://sandcash-uat01.sand.com.cn/pay/h5/alipay?".$query;
        //halt($redirectUrl);
        header('Location:'.$payurl);
        die();
    }


   public function sign($str) {
        $privateKeyPath =   Env::get("root_path")."extend/sande/test/6888800001732.pfx";
        $file = file_get_contents($privateKeyPath);
        if (!$file) {
            throw new \Exception('loadPk12Cert::file
                    _get_contents');
        }
        if (!openssl_pkcs12_read($file, $cert, $this->privateKeyPwd)) {
            throw new \Exception('loadPk12Cert::openssl_pkcs12_read ERROR');
        }
        $pem = $cert['pkey'];
        openssl_sign($str, $sign, $pem);
        $sign = base64_encode($sign);
        return $sign;
    }

   public function getSignContent($params) {
        ksort($params);

        $stringToBeSigned = "";
        $i = 0;
        foreach ($params as $k => $v) {
            if (false === $this->checkEmpty($v) && "@" != substr($v, 0, 1)) {

                if ($i == 0) {
                    $stringToBeSigned .= "$k" . "=" . "$v";
                } else {
                    $stringToBeSigned .= "&" . "$k" . "=" . "$v";
                }
                $i++;
            }
        }

        unset ($k, $v);
        return $stringToBeSigned;
    }

   public function checkEmpty($value)
    {
        if (!isset($value))
            return true;
        if ($value === null)
            return true;
        if (trim($value) === "")
            return true;

        return false;
    }




}