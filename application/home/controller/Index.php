<?php
namespace app\home\controller;
use app\common\model\Agent;
use app\common\model\Order;
use think\Controller;
use think\Db;
use clt\Lunar;
use think\facade\Env;
use app\common\service\SdCommon;
use app\common\service\Cashier;
use think\facade\Validate;

class Index extends Controller{
    //http://sd1.yapin.shop/home/index/index
    //测试补0金额
    public function index()
    {
//        $money = 0.11;
//        $str=strval($money*100);
//
//        echo "原字符串：".$str."<br><br>";
//
//
//
//        echo "补位后：".str_pad($str,12,"0")."<br><br>";
//
//        echo "补位后：".str_pad($str,12,"0",STR_PAD_BOTH)."<br><br>";
//
//        echo "补位后：".str_pad($str,12,"0",STR_PAD_LEFT)."<br><br>";
    }

    /**
     * 接口下单
     * https://dld.yapin.shop/home/index/pay
     */
    public function pay(){
        $model = new Order();
        $data = input("post.");
        $validate = Validate::make([
            'token|商家token'               => 'require',
            'order_num|订单号'              => 'require|length:1,100',
            'notify_url|异步通知地址'        => 'require|url',
            'money|支付金额'                => 'require',
           // "type|支付类型"                 =>'require|number',
        ]);

        if (!$validate->check($data)) {
            return AjaxReturn(0,$validate->getError());
        }
        if(!$data["order_num"]){
            $data["order_num"] = orderNum();
        }
        if($data["money"] < 0.01){
            return AjaxReturn(0,"订单金额不正确");
        }
        $agent = Agent::get(["key"=>$data["token"]]);
        if(!$agent){
            return AjaxReturn(0,'代理不存在');
        }
        if($agent["is_lock"]){
            return AjaxReturn(0,"代理暂停锁定");
        }

        $info =$model::get(["system_order_num"=>$data["order_num"]]);
        if($info){
            return AjaxReturn(0,"订单号已存在");
        }
        $order_num = orderNum();
        $order_data = [
            "money"=>$data["money"],
            "order_num"=>$data["order_num"],
            "system_order_num"=>$order_num,
            //"mid"=>"6888800119692",
            "notify_url"=>$data["notify_url"],
            "agent_id"=>$agent["id"],
            "create_time"=>time(),
            "type"=>1
        ];
        $order_id = $model->insertGetId($order_data);
        if(!$order_id){
            return AjaxReturn(0,"下单失败,请重试");
        }

        $url = ym()."/home/fivepay/pay?id=".$order_id;
        return AjaxReturn(1,"创建成功",["url"=>$url]);

    }


    /**
     * 废弃接口支付宝
     */
    public  function alipay($order_id){
        $model = new Order();
        $order =$model::get($order_id);
        $money = $order["money"]; //支付金额
        $money =number_format($money, 2, '.', '');//转换成字符串金额
        $array = [
            "version"=>"10",//当前接口版本号
            "mer_no"=>"6888802039772",//商户编号
            "mer_key"=>"1gOVX5HB0zmrxqkH8qaI3Hb8e69h1XS2SUTR+WUYWMQ7SLJ0FWbIXIRzpSFfzryS2eT5zgof0KI=",//手机APK工具 生成的key1
            "mer_order_no"=>$order["orderCode"],//商户订单号（最小长度12位）
            "create_time"=>date("YmdHis",time()),//订单创建时间 yyyyMMddHHmmss
            "order_amt"=>$money,//订单金额（单位:元，1分=0.01元）
            "notify_url"=>"ym()./api/Callback/szO3X0eiuCHpC1WA",//异步回调地址
            "return_url" => "ym()./api/returnback/index",//支付后返回的商户显示页面  可以为空
            "create_ip"=>$this->ip(getIp()),
            "store_id"=>"000000",//门店号 没有就填默认值000000
            //"pay_extra"=>"",//签名注意字符串顺序,客户端传的顺序和服务端签名保持一致，支付宝H5可以为空。例：pay_extra=
            "accsplit_flag"=>"NO",//分账标识 NO无分账，YES有分账
            "sign_type"=>"MD5",//MD5
        ];

        $sign = $this->sign($array);
        $array["sign"] = $sign;//签名

        $array["pay_extra"] = "";//签名注意字符串顺序,客户端传的顺序和服务端签名保持一致，支付宝H5可以为空。例：pay_extra=
        $array["activity_no"] = "";
        $array["expire_time"] = date("YmdHis",time()+1000);//订单失效时间 yyyyMMddHHmmss（不参加签名）
        $array["goods_name"] = "alipay";//商品名称（不参加签名）
        $array["product_code"] = "02020002";//支付产品,多个以英文逗号分隔，具体产品见产品编码文档（不参加签名）
        $array["clear_cycle"] = "0";//清算模式（不参加签名）0-T1(默认);1-T0;2-D0;3-D1
        $array["jump_scheme"] = "";//跳转scheme（不参与签名）
        $array["meta_option"] = '[{"s":"Android","n":"wxDemo","id":"com.pay.paytypetest","sc":"com.pay.paytypetest"}]';//"meta_option=[{"s":"Android","n":"wxDemo","id":"com.pay.paytypetest","sc":"com.pay.paytypetest"}]"
        $url = "https://sandcash.mixienet.com.cn/h5/?";
        $str = http_build_query($array, null, '&');
        $url = $url.$str."#/alipayh5";
        return AjaxReturn(1,"ok",["url"=>$url]);
    }

    /**
     * 废弃接口微信
     */
    public  function wxpay($order_id){
        $model = new Order();
        $order =$model::get($order_id);
        $money = $order["money"]; //支付金额
        $money =number_format($money, 2, '.', '');
        //$gh_static_url = "https://cloud1-5g0j4ycx4a76a196-1308043767.tcloudbaseapp.com/jump_mp.html?sign=81607ebafdb39ddb2d4cd7353f81a41e&t=1635818636";
        $gh_static_url = "https://cloud1-5g0j4ycx4a76a196-1308043767.tcloudbaseapp.com/jump_mp.html";
        $pay_extra = [
            "wx_app_id"=>"wxb4b91d43116b8764",
            "gh_ori_id"=>"gh_ae61f1850c73",
            "path_url"=>"pages/zf/index?",
            "miniProgramType"=>"0"
        ];
        $array = [
            "version"       =>"10",//当前接口版本号
            "mer_no"        =>"6888802039772",//商户编号
            "mer_key"       =>"1gOVX5HB0zmrxqkH8qaI3Hb8e69h1XS2SUTR+WUYWMQ7SLJ0FWbIXIRzpSFfzryS2eT5zgof0KI=",//手机APK工具 生成的key1
            "mer_order_no"  =>$order["orderCode"],//商户订单号（最小长度12位）
            "create_time"   =>date("YmdHis",time()),//订单创建时间 yyyyMMddHHmmss
            "order_amt"     =>$money,//订单金额（单位:元，1分=0.01元）
            "notify_url"    =>"ym()./api/Callback/szO3X0eiuCHpC1WA",//异步回调地址
            "return_url"    => json_encode($pay_extra),//支付后返回的商户显示页面  可以为空
            "create_ip"     =>$this->ip(getIp()),
            "store_id"      =>"000000",//门店号 没有就填默认值000000
            //"pay_extra"     =>$pay_extra,//签名注意字符串顺序,客户端传的顺序和服务端签名保持一致，支付宝H5可以为空。例：pay_extra=
            "accsplit_flag" =>"NO",//分账标识 NO无分账，YES有分账
            "sign_type"     =>"MD5",//MD5
            "gh_static_url" =>$gh_static_url
        ];

        $sign = $this->sign($array);
        $array["sign"] = $sign;//签名
        //$array["pay_extra"] = json_encode($pay_extra);
        $array["activity_no"] = "";
        $array["expire_time"] = date("YmdHis",time()+1000);//订单失效时间 yyyyMMddHHmmss（不参加签名）
        $array["goods_name"] = "wxpay";//商品名称（不参加签名）
        $array["product_code"] = "02010006";//支付产品,多个以英文逗号分隔，具体产品见产品编码文档（不参加签名）
        $array["clear_cycle"] = "0";//清算模式（不参加签名）0-T1(默认);1-T0;2-D0;3-D1
        $array["jump_scheme"] = "";//跳转scheme（不参与签名）
        $array["meta_option"] = '[{"s":"Android","n":"wxDemo","id":"com.pay.paytypetest","sc":"com.pay.paytypetest"}]';
        $url = "https://sandcash.mixienet.com.cn/h5/?";
        unset($array["gh_static_url"]);
        $str = http_build_query($array, null, '&');
        $str = $str."&gh_static_url=".$gh_static_url;
        //$str = $str."&pay_extra=".json_encode($pay_extra);
        $url = $url.$str."#/applet";

        return AjaxReturn(1,"ok",["url"=>$url]);
    }




    /**
     * 废弃签名
     */
    public function sign($arr){
        ksort($arr);
        //halt($arr);
        $str =  self::formatQueryParaMap($arr);
        $key = "48KqYynRgZx9u5w21F8U/vhyO+JhXBKjifGLuMOdM86dGFcZ0kdmswRFraoYQeHgZdV5pzlegBeGf31L1KQWpzDCNNCUMks6H6AA+mIVc3gNat5xSGKBNerXRN2MRQGlUJsV5iFv2/5ixuz+UKFTiw==";//用户md5key
        $signStr = strtoupper(md5($str . "&key=" . $key));
        return $signStr;
    }
    //4.4 签名流程
    /**
     * 废弃
     * 数组转换成&链接方式
     * @param $paraMap
     * @param false $urlEncode
     * @return false|string
     */
    protected static function formatQueryParaMap($paraMap, $urlEncode = false)
    {
        $buff = "";
        ksort($paraMap);
        foreach ($paraMap as $k => $v) {
            if (null != $v && "null" != $v) {
                if ($urlEncode) {
                    $v = urlencode($v);
                }
                $buff .= $k . "=" . $v . "&";
            }
        }
        $reqPar = '';
        if (strlen($buff) > 0) {
            $reqPar = substr($buff, 0, strlen($buff) - 1);
        }
        return $reqPar;
    }

    /**
     * 废弃
     * ip转换格式
     * @param $ip
     * @return string
     */
    public function ip($ip){
        $ip = explode(".",$ip);
        $ip = implode('_',$ip);
        return $ip;
    }




    /***************************************************测试支付************************************************/
    /**
     * 废弃
     * 收银台支付宝h5 测试
     * http://www.shandepay.com/home/index/alipayFive
     *  ym()./home/index/alipayFive
     */
    public function alipayFive(){
        $money = rand(1,5)/100; //支付金额
        $money =number_format($money, 2, '.', '');
        $array = [
            "version"=>"10",//当前接口版本号
            "mer_no"=>"6888802039772",//商户编号
            "mer_key"=>"1gOVX5HB0zmrxqkH8qaI3Hb8e69h1XS2SUTR+WUYWMQ7SLJ0FWbIXIRzpSFfzryS2eT5zgof0KI=",//手机APK工具 生成的key1
            "mer_order_no"=>orderNum(),//商户订单号（最小长度12位）
            "create_time"=>date("YmdHis",time()),//订单创建时间 yyyyMMddHHmmss
            "order_amt"=>$money,//订单金额（单位:元，1分=0.01元）
            "notify_url"=>ym()."/api/Callback/szO3X0eiuCHpC1WA",//异步回调地址
            "return_url" => "ym()./api/returnback/index",//支付后返回的商户显示页面  可以为空
            "create_ip"=>$this->ip(getIp()),
            "store_id"=>"000000",//门店号 没有就填默认值000000
            //"pay_extra"=>"",//签名注意字符串顺序,客户端传的顺序和服务端签名保持一致，支付宝H5可以为空。例：pay_extra=
            "accsplit_flag"=>"NO",//分账标识 NO无分账，YES有分账
            "sign_type"=>"MD5",//MD5
        ];

        $sign = $this->sign($array);
        $array["sign"] = $sign;//签名

        $array["pay_extra"] = "";//签名注意字符串顺序,客户端传的顺序和服务端签名保持一致，支付宝H5可以为空。例：pay_extra=
        $array["activity_no"] = "";
        $array["expire_time"] = date("YmdHis",time()+1000);//订单失效时间 yyyyMMddHHmmss（不参加签名）
        $array["goods_name"] = "alipay";//商品名称（不参加签名）
        $array["product_code"] = "02020002";//支付产品,多个以英文逗号分隔，具体产品见产品编码文档（不参加签名）
        $array["clear_cycle"] = "0";//清算模式（不参加签名）0-T1(默认);1-T0;2-D0;3-D1
        $array["jump_scheme"] = "";//跳转scheme（不参与签名）
        $array["meta_option"] = '[{"s":"Android","n":"wxDemo","id":"com.pay.paytypetest","sc":"com.pay.paytypetest"}]';//"meta_option=[{"s":"Android","n":"wxDemo","id":"com.pay.paytypetest","sc":"com.pay.paytypetest"}]"
        $url = "https://sandcash.mixienet.com.cn/h5/?";
        $str = http_build_query($array, null, '&amp;');
        $url = $url.$str."#/alipayh5";
        //halt($url);
        echo $url;
    }
    /**
     * 废弃
     * 微信支付
     *  http://www.shandepay.com/home/index/wxzpay
     */
    public function wxzpay(){
        $money = rand(1,5)/100; //支付金额
        $money =number_format($money, 2, '.', '');
        //$gh_static_url = "https://cloud1-5g0j4ycx4a76a196-1308043767.tcloudbaseapp.com/jump_mp.html?sign=81607ebafdb39ddb2d4cd7353f81a41e&t=1635818636";
        $gh_static_url = "https://cloud1-5g0j4ycx4a76a196-1308043767.tcloudbaseapp.com/jump_mp.html";
        $pay_extra = [
            "wx_app_id"=>"wxb4b91d43116b8764",
            "gh_ori_id"=>"gh_ae61f1850c73",
            "path_url"=>"pages/zf/index?",
            "miniProgramType"=>"0"
        ];
        $array = [
            "version"       =>"10",//当前接口版本号
            "mer_no"        =>"6888802039772",//商户编号
            "mer_key"       =>"1gOVX5HB0zmrxqkH8qaI3Hb8e69h1XS2SUTR+WUYWMQ7SLJ0FWbIXIRzpSFfzryS2eT5zgof0KI=",//手机APK工具 生成的key1
            "mer_order_no"  =>orderNum(),//商户订单号（最小长度12位）
            "create_time"   =>date("YmdHis",time()),//订单创建时间 yyyyMMddHHmmss
            "order_amt"     =>$money,//订单金额（单位:元，1分=0.01元）
            "notify_url"    =>"ym()./api/Callback/szO3X0eiuCHpC1WA",//异步回调地址
            "return_url"    => json_encode($pay_extra),//支付后返回的商户显示页面  可以为空
            "create_ip"     =>$this->ip(getIp()),
            "store_id"      =>"000000",//门店号 没有就填默认值000000
            //"pay_extra"     =>$pay_extra,//签名注意字符串顺序,客户端传的顺序和服务端签名保持一致，支付宝H5可以为空。例：pay_extra=
            "accsplit_flag" =>"NO",//分账标识 NO无分账，YES有分账
            "sign_type"     =>"MD5",//MD5
            "gh_static_url" =>$gh_static_url
        ];

        $sign = $this->sign($array);
        $array["sign"] = $sign;//签名
        //$array["pay_extra"] = json_encode($pay_extra);
        $array["activity_no"] = "";
        $array["expire_time"] = date("YmdHis",time()+1000);//订单失效时间 yyyyMMddHHmmss（不参加签名）
        $array["goods_name"] = "wxpay";//商品名称（不参加签名）
        $array["product_code"] = "02010006";//支付产品,多个以英文逗号分隔，具体产品见产品编码文档（不参加签名）
        $array["clear_cycle"] = "0";//清算模式（不参加签名）0-T1(默认);1-T0;2-D0;3-D1
        $array["jump_scheme"] = "";//跳转scheme（不参与签名）
        $array["meta_option"] = '[{"s":"Android","n":"wxDemo","id":"com.pay.paytypetest","sc":"com.pay.paytypetest"}]';
        $url = "https://sandcash.mixienet.com.cn/h5/?";
        unset($array["gh_static_url"]);
        $str = http_build_query($array, null, '&amp;');
        $str = $str."&gh_static_url=".$gh_static_url;
        //$str = $str."&pay_extra=".json_encode($pay_extra);
        $url = $url.$str."#/applet";
        //halt($url);
        echo $url;
    }




}