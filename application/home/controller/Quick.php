<?php
namespace app\home\controller;
use app\common\model\Agent;
use app\common\model\CzOrder;
use app\common\model\Order;
use app\common\model\PayChannel;
use think\Controller;
use think\Db;
use clt\Lunar;
use think\facade\Env;
use app\common\service\SdCommon;
use app\common\service\Cashier;
use think\facade\Validate;

use think\facade\Request;


class Quick extends Controller{



    public $privateKeyPwd = '123456';



    /**
     * 鑫梦快捷app支付
     * @return void
     *  http://dld1.yapin.shop/home/Quick/xinmengfastapp
     */
    public function xinmengfastapp(){
        $model = new CzOrder();
        $id = input("get.id");
        if(!$id){
            return AjaxReturn(0,"参数错误");
        }
        $order = $model::get($id);
        if(!$order){
            return AjaxReturn(0,"订单不存在");
        }
        if($order["payment"] == 1){
            return AjaxReturn(0,"订单已支付");
        }
        $ip = getIp();
        $model->where("id",$id)->update(["buy_ip"=>$ip]);//更新付款用户下单ip
        $ip = str_replace(".","_",$ip);


        $PayChannelModel  = new PayChannel();
        $PayChannel = $PayChannelModel::get($order['cid']);
        if(!$PayChannel){
            return AjaxReturn(1,"支付通道不存在");
        }
        if($PayChannel['open'] != 1){
            return AjaxReturn(1,"通道未开启");
        }
        $order_num = $order['system_order_num'];
        $data = [
            'version' => 10,
            'mer_no' => $PayChannel['business'] , //商户号
            'mer_order_no' =>$order_num, //商户唯一订单号
            'create_time' => date('YmdHis'),
            'expire_time' => date('YmdHis', time()+30*60),
            'order_amt' =>  strval($order['money']), //订单支付金额
            //'notify_url' => ym().'/api/notify/xinmengsytnotify', //订单支付异步通知
            'notify_url' => ym().'/api/notify/xinmengfastapp', //订单支付异步通知
            'return_url' => ym()."/home/fivepay/front?order_num=".$order_num, //订单前端页面跳转地址
            'create_ip' => $ip,
            'goods_name' => '购买产品',//产品名称
            'store_id' => '000000',
            'product_code' => '05030001',
            'clear_cycle' => '3',
            'pay_extra' =>json_encode(["userId"=>orderNum()]), //pay_extra参考语雀文档4.3
            'accsplit_flag' => 'NO',
            'jump_scheme' => '',
            'meta_option' => json_encode([["s" => "Android","n" => "wxDemo","id" => "com.pay.paytypetest","sc" => "com.pay.paytypetest"]]),
            'sign_type' => 'RSA',
            //'merch_extend_params'=>'{"mchReceiveRemark":"S0"}',

        ];

        //goods_name , notify_url，return_url，pay_extra，meta_option，extend，merch_extend_params，sign
        $temp = $data;
        unset($temp['goods_name']);
        unset($temp['jump_scheme']);
        unset($temp['expire_time']);
        unset($temp['product_code']);
        unset($temp['clear_cycle']);
        unset($temp['meta_option']);

        //file_put_contents('log.txt', date('Y-m-d H:i:s', time()) . " 签名串:" . $this->getSignContent($temp) . "\r\n", FILE_APPEND);

        // echo $this->getSignContent($temp);exit;
        $sign = $this->sign($this->getSignContent($temp),$data['mer_no']);
        $data['sign'] = $sign;
        $query = http_build_query($data) ;
        //$query = http_build_query($data, null, '&');

        $payurl = "https://sandcash.mixienet.com.cn/pay/h5/fastpayment?".$query;
        //halt($redirectUrl);
        //return $payurl;
        header('Location:'.$payurl);
        die();
    }

    /**
     * 达运快捷app支付
     * @return void
     *  http://dld1.yapin.shop/home/Quick/xinmengfastapp
     */
    public function dayunfastapp(){
        $model = new CzOrder();
        $id = input("get.id");
        if(!$id){
            return AjaxReturn(0,"参数错误");
        }
        $order = $model::get($id);
        if(!$order){
            return AjaxReturn(0,"订单不存在");
        }
        if($order["payment"] == 1){
            return AjaxReturn(0,"订单已支付");
        }
        $ip = getIp();
        $model->where("id",$id)->update(["buy_ip"=>$ip]);//更新付款用户下单ip
        $ip = str_replace(".","_",$ip);


        $PayChannelModel  = new PayChannel();
        $PayChannel = $PayChannelModel::get($order['cid']);
        if(!$PayChannel){
            return AjaxReturn(1,"支付通道不存在");
        }
        if($PayChannel['open'] != 1){
            return AjaxReturn(1,"通道未开启");
        }
        $order_num = $order['system_order_num'];
        $data = [
            'version' => 10,
            'mer_no' => $PayChannel['business'] , //商户号
            'mer_order_no' =>$order_num, //商户唯一订单号
            'create_time' => date('YmdHis'),
            'expire_time' => date('YmdHis', time()+30*60),
            'order_amt' =>  strval($order['money']), //订单支付金额
            //'notify_url' => ym().'/api/notify/xinmengsytnotify', //订单支付异步通知
            'notify_url' => ym().'/api/notify/xinmengfastapp', //订单支付异步通知
            'return_url' => ym()."/home/fivepay/front?order_num=".$order_num, //订单前端页面跳转地址
            'create_ip' => $ip,
            'goods_name' => '购买产品',//产品名称
            'store_id' => '000000',
            'product_code' => '05030001',
            'clear_cycle' => '3',
            'pay_extra' =>json_encode(["userId"=>orderNum()]), //pay_extra参考语雀文档4.3
            'accsplit_flag' => 'NO',
            'jump_scheme' => '',
            'meta_option' => json_encode([["s" => "Android","n" => "wxDemo","id" => "com.pay.paytypetest","sc" => "com.pay.paytypetest"]]),
            'sign_type' => 'RSA',
            //'merch_extend_params'=>'{"mchReceiveRemark":"S0"}',

        ];

        //goods_name , notify_url，return_url，pay_extra，meta_option，extend，merch_extend_params，sign
        $temp = $data;
        unset($temp['goods_name']);
        unset($temp['jump_scheme']);
        unset($temp['expire_time']);
        unset($temp['product_code']);
        unset($temp['clear_cycle']);
        unset($temp['meta_option']);

        //file_put_contents('log.txt', date('Y-m-d H:i:s', time()) . " 签名串:" . $this->getSignContent($temp) . "\r\n", FILE_APPEND);

        // echo $this->getSignContent($temp);exit;
        $sign = $this->sign($this->getSignContent($temp),$data['mer_no']);
        $data['sign'] = $sign;
        $query = http_build_query($data) ;
        //$query = http_build_query($data, null, '&');

        $payurl = "https://sandcash.mixienet.com.cn/pay/h5/fastpayment?".$query;
        //halt($redirectUrl);
        //return $payurl;
        header('Location:'.$payurl);
        die();
    }


    /**
     * 达运快捷收银台
     * @return void
     *  http://dld1.yapin.shop/home/Quick/pay
     */
    public function pay(){
        $model = new Order();
        $id = input("get.id");
        if(!$id){
            return AjaxReturn(0,"参数错误");
        }
        $order = $model::get($id);
        if(!$order){
            return AjaxReturn(0,"订单不存在");
        }
        if($order["payment"] == 1){
            return AjaxReturn(0,"订单已支付");
        }
        $ip = getIp();
        $model->where("id",$id)->update(["buy_ip"=>$ip]);//更新付款用户下单ip
        $ip = str_replace(".","_",$ip);


        $PayChannelModel  = new PayChannel();
        $PayChannel = $PayChannelModel::get($order['cid']);
        if(!$PayChannel){
            return AjaxReturn(1,"支付通道不存在");
        }
        if($PayChannel['open'] != 1){
            return AjaxReturn(1,"通道未开启");
        }
        $order_num = $order['system_order_num'];
        $data = [
            'version' => 10,
            'mer_no' => $PayChannel['business'] , //商户号
            'mer_order_no' =>$order_num, //商户唯一订单号
            'create_time' => date('YmdHis'),
            'expire_time' => date('YmdHis', time()+30*60),
            'order_amt' =>  strval($order['money']), //订单支付金额
            //'notify_url' => ym().'/api/notify/xinmengsytnotify', //订单支付异步通知
            'notify_url' => ym().'/api/notify/quicknotify', //订单支付异步通知
            'return_url' => ym()."/home/fivepay/front?order_num=".$order_num, //订单前端页面跳转地址
            'create_ip' => $ip,
            'goods_name' => '购买产品',//产品名称
            'store_id' => '000000',
            'product_code' => '05030001',
            'clear_cycle' => '3',
            'pay_extra' =>json_encode(["userId"=>orderNum()]), //pay_extra参考语雀文档4.3
            'accsplit_flag' => 'NO',
            'jump_scheme' => '',
            'meta_option' => json_encode([["s" => "Android","n" => "wxDemo","id" => "com.pay.paytypetest","sc" => "com.pay.paytypetest"]]),
            'sign_type' => 'RSA',
            //'merch_extend_params'=>'{"mchReceiveRemark":"S0"}',

        ];

        //goods_name , notify_url，return_url，pay_extra，meta_option，extend，merch_extend_params，sign
        $temp = $data;
        unset($temp['goods_name']);
        unset($temp['jump_scheme']);
        unset($temp['expire_time']);
        unset($temp['product_code']);
        unset($temp['clear_cycle']);
        unset($temp['meta_option']);

        //file_put_contents('log.txt', date('Y-m-d H:i:s', time()) . " 签名串:" . $this->getSignContent($temp) . "\r\n", FILE_APPEND);

        // echo $this->getSignContent($temp);exit;
        $sign = $this->sign($this->getSignContent($temp),$data['mer_no']);
        $data['sign'] = $sign;
        $query = http_build_query($data) ;
        //$query = http_build_query($data, null, '&');

        $payurl = "https://sandcash.mixienet.com.cn/pay/h5/fastpayment?".$query;
        //halt($redirectUrl);
        //return $payurl;
        header('Location:'.$payurl);
        die();
    }




    public function sign($str,$privateKeynum) {
        $privateKeyPath =   Env::get("root_path")."extend/sande/".$privateKeynum."/".$privateKeynum.".pfx";
        $file = file_get_contents($privateKeyPath);
        if (!$file) {
            throw new \Exception('loadPk12Cert::file
                    _get_contents');
        }
        if (!openssl_pkcs12_read($file, $cert, $this->privateKeyPwd)) {
            throw new \Exception('loadPk12Cert::openssl_pkcs12_read ERROR');
        }
        $pem = $cert['pkey'];
        openssl_sign($str, $sign, $pem);
        $sign = base64_encode($sign);
        return $sign;
    }

    public function getSignContent($params) {
        ksort($params);

        $stringToBeSigned = "";
        $i = 0;
        foreach ($params as $k => $v) {
            if (false === $this->checkEmpty($v) && "@" != substr($v, 0, 1)) {

                if ($i == 0) {
                    $stringToBeSigned .= "$k" . "=" . "$v";
                } else {
                    $stringToBeSigned .= "&" . "$k" . "=" . "$v";
                }
                $i++;
            }
        }

        unset ($k, $v);
        return $stringToBeSigned;
    }

    public function checkEmpty($value)
    {
        if (!isset($value))
            return true;
        if ($value === null)
            return true;
        if (trim($value) === "")
            return true;

        return false;
    }

}