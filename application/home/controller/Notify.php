<?php
namespace app\home\controller;
use app\common\model\Agent;
use app\common\model\Order;
use app\common\service\Finance;
use think\Controller;
use think\Db;
use clt\Lunar;
use think\facade\Env;
use app\common\service\SdCommon;
use app\common\service\Cashier;
use think\facade\Validate;

use think\facade\Request;


class Notify extends Controller{

    /**
     * h5 异步回调
     * http://sd1.yapin.shop/home/notify/fiveQsL7EhBeZ5zKlZI4AbtrAb5CqLw9f12i
     */
    public function fiveQsL7EhBeZ5zKlZI4AbtrAb5CqLw9f12i(){
        $post = $_POST;
        db("test")->insert(["add_time"=>time(),"content"=>"h5异步参数".json_encode($post)]);
//        die();
//        $json = '{"extend":"","charset":"UTF-8","data":"{\"head\":{\"version\":\"1.0\",\"respTime\":\"20220713091912\",\"respCode\":\"000000\",\"respMsg\":\"\u6210\u529f\"},\"body\":{\"mid\":\"6888800045441\",\"orderCode\":\"713751418896960\",\"tradeNo\":\"713751418896960\",\"clearDate\":\"20220713\",\"totalAmount\":\"000000000011\",\"orderStatus\":\"1\",\"payTime\":\"20220713091912\",\"settleAmount\":\"000000000011\",\"buyerPayAmount\":\"000000000011\",\"discAmount\":\"000000000000\",\"txnCompleteTime\":\"20220713091911\",\"payOrderCode\":\"20220713001388900000000000035225\",\"accLogonNo\":\"778***@qq.com\",\"accNo\":\"208820******7671\",\"midFee\":\"000000000000\",\"extraFee\":\"000000000000\",\"specialFee\":\"000000000000\",\"plMidFee\":\"000000000000\",\"bankserial\":\"232022071322001447671445741637\",\"externalProductCode\":\"00002022\",\"cardNo\":\"208820******7671\",\"creditFlag\":\"\",\"bid\":\"\",\"benefitAmount\":\"000000000000\",\"remittanceCode\":\"\",\"extend\":\"{}\"}}","sign":"hLZaTPWyCq8IIPQcgf3tqbA7wnOCLfGtKxKJk2xT61Yimo1W+nMqpN5gObK6d0gc\/eUncAKeznLkoWcouypidJ1vtEybtzFfpTo8AcAjFJmdSpzafJufIM2pjRcIvdsWfiv4G5sAOYW5r87uYLreyxn9CvFzoWTMmIaJHeZJ+4cc4guKy9O0os6I3U0aDnYvKJKIF2LXXvJAZk939WXpYaoIGghsQdNXeeHKuat+xM526k9D3umGmQqezYS2kAmvx24T7yYjEQKpGCPPGzGf4XNO8pq6\/IJvuTQoxzGoM+1wvpL0y8EMjjYKe3NzmWEATs3qIjFkyK9RrcGVQV4J6A==","signType":"01"}';
//        $post = json_decode($json,true);
        $notify_data = json_decode($post["data"],true);
        $sign = $post["sign"];
//        dump($post);
//        halt($notify_data);
        if($notify_data["head"]["respCode"] != "000000" ){
            return AjaxReturn(0,"订单异步返回状态异常");
        }
        $order_num = $notify_data["body"]["orderCode"];//订单号
        if(!$notify_data["body"]["payOrderCode"]){
            return AjaxReturn(0,"异步参数错误");
        }
        if(!$notify_data["body"]["accLogonNo"]){
            return AjaxReturn(0,"异步参数错误");
        }

        if($notify_data["body"]["orderStatus"] != 1){
            return AjaxReturn(0,"订单支付失败");
        }
        $datas = stripslashes($post['data']); //支付数据 json转换成数组
        $client = new  SdCommon();
        $verifyFlag = $client->verify($datas, $sign);
       // halt($verifyFlag);
        if(!$verifyFlag){
            return "验签失败";
        }
        //halt("验签成功.继续执行回调逻辑");
        $model = new Order();
        $order = $model::get(["orderCode"=>$order_num]);
        if(!$order){
            return AjaxReturn(0,"订单不存在");
        }
        if($order["payment"] == 1){
            return AjaxReturn(0,"订单已支付");
        }
        $order_data = [
            "pay_time"=>time(),
            "payment"=>1,
            "accLogonNo"=>$notify_data["body"]["accLogonNo"],
            "payOrderCode"=>$notify_data["body"]["payOrderCode"],
            "bankserial"=>$notify_data["body"]["bankserial"],
            "totalAmount"=>$notify_data["body"]["totalAmount"],
        ];
        $model::where("id",$order['id'])->update($order_data);
        //todo异步回调给其他平台  system_order_num
        $agent = Agent::get($order["agent_id"]);
        if ($agent) {
            Agent::where("id", $agent["id"])->setInc("money", $order["money"]);//统计代理总金额
            Agent::where("id", $agent["id"])->setInc("total_money", $order["money"]);//统计代余额
            $agentFianceModel = new Finance();
            $agentFianceModel->agentDay($order['id']);//代理日财务
            //异步回调给平台
            $notify_data = [
                "order_num" => $order["system_order_num"],
                "money" => $order["money"],
                "token" => $agent["key"],
            ];
            if ($order["notify_url"]) {
                //异步回调给其他平台
                $this->send_post($order["notify_url"], $notify_data);
                $model::where('id', $order['id'])->update(["return_status" => 1, "return_time" => time()]);//修改订单状态
            }
        }
        return "respCode=000000";//支付成功后给杉德返回数据
    }

    //发送post 数据
    public function  send_post($url, $post_data) {
        $postdata = http_build_query($post_data);
        $options = array(
            'http' => array(
                'method' => 'POST',
                'header' => 'Content-type:application/x-www-form-urlencoded',
                'content' => $postdata,
                'timeout' => 15 * 60 // 超时时间（单位:s）
            )
        );
        $context = stream_context_create($options);
        $result = file_get_contents($url, false, $context);
        return $result;
    }








}