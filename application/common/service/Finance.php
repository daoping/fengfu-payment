<?php
namespace app\common\service;

use app\common\model\AgentFinance;
use app\common\model\AgentFinanceDf;
use app\common\model\AkOrder;
use app\common\model\FfOrder;
use app\common\model\FwsChannel;
use app\common\model\HkOrder;
use app\common\model\LaoxieOrder;
use app\common\model\MoneyFinance;
use app\common\model\Order;
use app\common\model\TlOrder;
use app\common\model\TongdaoFinance;
use app\common\model\TongdaoFinanceDf;
use app\common\model\XsOrder;
use app\common\model\XsXiafa;
use app\common\model\ZhifutongOrder;
use app\common\model\ZshDayFinance;
use app\common\model\ZshWarning;


class Finance
{

    /**
     * 代理增加日财务
     */
    public function agentDay($order_id)
    {
        $model = new AgentFinance();//商家日财务
        $orderModel = new Order();//订单表
        $order = $orderModel::get($order_id);

        if(!$order){
            return AjaxReturn(0,"订单不存在");
        }
        if(!$order["payment"]) {
            return AjaxReturn(0, "订单未付款");
        }
        $time = time();
        $year = date("Y",$time);
        $month = intval(date("m",$time));
        $day = intval(date("d",$time));
        $info = $model::get(["year"=>$year,"month"=>$month,"day"=>$day,"agent_id"=>$order["agent_id"]]);
        if(!$info){
            $data = [
                "year"=>$year,
                "month"=>$month,
                "day"=>$day,
                "agent_id"=>$order["agent_id"],
                "money"=>$order["money"],
                "pay_money"=>$order["pay_money"]
            ];
            $model->save($data);
        }else{
            $model::where("id",$info["id"])->setInc("money",$order["money"]);
            $model::where("id",$info["id"])->setInc("pay_money",$order["pay_money"]);
        }
    }


    /**
     * 当日下单单笔金额统计
     */
    public function jinexia($order_id){
        $model = new MoneyFinance();
        $orderModel = new Order();
        $order = $orderModel::get($order_id);

        if(!$order){
            return AjaxReturn(0,"订单不存在");
        }
        if($order["payment"]) {
            return AjaxReturn(0, "订单已经支付");
        }
        $time = time();
        $year = date("Y",$time);
        $month = intval(date("m",$time));
        $day = intval(date("d",$time));
        $info = $model::get(["year"=>$year,"month"=>$month,"day"=>$day,"jine"=>$order["money"]]);
        if(!$info){
            $data = [
                "year"=>$year,
                "month"=>$month,
                "day"=>$day,
                "jine"=>$order["money"],
                "money"=>$order["money"],
                //"pay_money"=>$order["pay_money"],
                "total_num"=>1
            ];
            $model->save($data);
        }else{
            $model::where("id",$info["id"])->setInc("money",$order["money"]);
            //$model::where("id",$info["id"])->setInc("pay_money",$order["pay_money"]);
            $model::where("id",$info["id"])->setInc("total_num");
        }
    }

    /**
     * 当日付款单笔金额统计
     */
    public function jinedaypay($order_id){
        $model = new MoneyFinance();
        $orderModel = new Order();
        $order = $orderModel::get($order_id);

        if(!$order){
            return AjaxReturn(0,"订单不存在");
        }
        if($order["payment"] < 1) {
            return AjaxReturn(0, "订单未付款");
        }
        $time = time();
        $year = date("Y",$time);
        $month = intval(date("m",$time));
        $day = intval(date("d",$time));
        $info = $model::get(["year"=>$year,"month"=>$month,"day"=>$day,"jine"=>$order["money"]]);
        if(!$info){
            return AjaxReturn(0, "未下单");
        }else{
            $model::where("id",$info["id"])->setInc("pay_num");//支付笔数
            $model::where("id",$info["id"])->setInc("pay_money",$order["pay_money"]);//支付金额

        }
    }

    /**
     * 子商户日财务下单
     */
    public function zshdaybuy($order_id){
        $model = new ZshDayFinance();
        $orderModel = new Order();
        $order = $orderModel::get($order_id);

        if(!$order){
            return AjaxReturn(0,"订单不存在");
        }
        if($order["payment"]) {
            return AjaxReturn(0, "订单已经支付");
        }
        $time = time();
        $year = date("Y",$time);
        $month = intval(date("m",$time));
        $day = intval(date("d",$time));
        $info = $model::get(["year"=>$year,"month"=>$month,"day"=>$day,"bid"=>$order["bid"]]);
        if(!$info){
            $data = [
                "year"=>$year,
                "month"=>$month,
                "day"=>$day,
                "bid"=>$order["bid"],
                "money"=>$order["money"],
                "total_num"=>1,
                "billMerchantId"=>$order['billMerchantId']
            ];
            $model->save($data);
        }else{
            $model::where("id",$info["id"])->setInc("money",$order["money"]);
            $model::where("id",$info["id"])->setInc("total_num");
        }
    }

    /**
     * 子商户日财务支付
     */
    public function zshdaypay($order_id){
        $model = new ZshDayFinance();
        $orderModel = new Order();
        $order = $orderModel::get($order_id);

        if(!$order){
            return AjaxReturn(0,"订单不存在");
        }
        if($order["payment"] < 1) {
            return AjaxReturn(0, "订单已经支付");
        }
        $time = time();
        $year = date("Y",$time);
        $month = intval(date("m",$time));
        $day = intval(date("d",$time));
        $info = $model::get(["year"=>$year,"month"=>$month,"day"=>$day,"bid"=>$order["bid"]]);
        if(!$info){
            return AjaxReturn(0, "未下单");
        }else{
            $model::where("id",$info["id"])->setInc("pay_num");//支付笔数
            $model::where("id",$info["id"])->setInc("pay_money",$order["pay_money"]);//支付金额
        }
    }

    /**
     * 子商户总下单统计
     */
    public function zshbuy($order_id){
        $model = new FwsChannel();
        $orderModel = new Order();
        $order = $orderModel::get($order_id);

        if(!$order){
            return AjaxReturn(0,"订单不存在");
        }
        if($order["payment"]) {
            return AjaxReturn(0, "订单已经支付");
        }
        $model::where(["id"=>$order['bid']])->setInc("money",$order["money"]);//拉单总金额增加
        $model::where(["id"=>$order['bid']])->setInc("total_num");//总订单数量增加

    }

    /**
     * 子商户总支付统计
     */
    public function zshpay($order_id){
        $model = new FwsChannel();
        $orderModel = new Order();
        $order = $orderModel::get($order_id);
        if(!$order){
            return AjaxReturn(0,"订单不存在");
        }
        if(!$order["payment"]) {
            return AjaxReturn(0, "订单未支付");
        }
        $model::where(["id"=>$order['bid']])->setInc("pay_money",$order["pay_money"]);//支付总金额增加
        $model::where(["id"=>$order['bid']])->setInc("pay_num");//支付订单量增加

    }

    /**
     * 子商户预警
     * @param $bid
     * @return bool
     */
    public function orderyj($bid){
        $model = new Order();
        //连续20单没有支付关闭子商户
        $order_list = $model::where(["bid"=>$bid])->order("id desc")->whereTime('create_time', 'today')->limit(30)->field("id,payment")->select();
        $payments = 0;
        foreach ($order_list as $k=>$v){
            $payments +=$v['payment'];

        }
        //halt($payments);
        if($payments < 1){
            $FwsChannelModel = new FwsChannel();
            $FwsChannelModel::where(["id"=>$bid])->update(["open"=>0]);
            $FwsChannelInfo = $FwsChannelModel::where(["id"=>$bid])->find();//查询子商户
            $zsh_arr = [
                "bid"=>$bid,
                "billMerchantId"=>$FwsChannelInfo['billMerchantId'],
                "state"=>0
            ];
            $zsh_wa_model = new ZshWarning();
            $zsh_wa_model->save($zsh_arr);
            return false;
        }else{
            return true;
        }
    }

    /**
     * 丰付增加日财务
     */
    public function agentDayff($order_id)
    {
        $model = new AgentFinance();//商家日财务
        $orderModel = new FfOrder();//订单表
        $order = $orderModel::get($order_id);

        if(!$order){
            return AjaxReturn(0,"订单不存在");
        }
        if(!$order["payment"]) {
            return AjaxReturn(0, "订单未付款");
        }
        $time = time();
        $year = date("Y",$time);
        $month = intval(date("m",$time));
        $day = intval(date("d",$time));
        $info = $model::get(["year"=>$year,"month"=>$month,"day"=>$day,"agent_id"=>$order["agent_id"]]);
        if(!$info){
            $data = [
                "year"=>$year,
                "month"=>$month,
                "day"=>$day,
                "agent_id"=>$order["agent_id"],
                "money"=>$order["money"],
                "pay_money"=>$order["pay_money"]
            ];
            $model->save($data);
        }else{
            $model::where("id",$info["id"])->setInc("money",$order["money"]);
            $model::where("id",$info["id"])->setInc("pay_money",$order["pay_money"]);
        }
    }


    /**
     * 通联增加日财务
     */
    public function agentDaytl($order_id)
    {
        $model = new AgentFinance();//商家日财务
        $orderModel = new TlOrder();//订单表
        $order = $orderModel::get($order_id);

        if(!$order){
            return AjaxReturn(0,"订单不存在");
        }
        if(!$order["payment"]) {
            return AjaxReturn(0, "订单未付款");
        }
        $time = time();
        $year = date("Y",$time);
        $month = intval(date("m",$time));
        $day = intval(date("d",$time));
        $info = $model::get(["year"=>$year,"month"=>$month,"day"=>$day,"agent_id"=>$order["agent_id"],"type"=>"tl"]);
        if(!$info){
            $data = [
                "year"=>$year,
                "month"=>$month,
                "day"=>$day,
                "agent_id"=>$order["agent_id"],
                "money"=>$order["money"],
                "pay_money"=>$order["pay_money"],
                "type"=>"tl"
            ];
            $model->save($data);
        }else{
            $model::where("id",$info["id"])->setInc("money",$order["money"]);
            $model::where("id",$info["id"])->setInc("pay_money",$order["pay_money"]);
        }
    }


    /**
     * 通联通道日财务
     */
    public function payDaytongdao($order_id)
    {
        $model = new TongdaoFinance();//商家日财务
        $orderModel = new TlOrder();//订单表
        $order = $orderModel::get($order_id);

        if(!$order){
            return AjaxReturn(0,"订单不存在");
        }
        if(!$order["payment"]) {
            return AjaxReturn(0, "订单未付款");
        }
        $time = time();
        $year = date("Y",$time);
        $month = intval(date("m",$time));
        $day = intval(date("d",$time));
        $info = $model::get(["year"=>$year,"month"=>$month,"day"=>$day,"bid"=>$order["bid"],"type"=>"tl"]);
        if(!$info){
            $data = [
                "year"=>$year,
                "month"=>$month,
                "day"=>$day,
                "bid"=>$order["bid"],
                "money"=>$order["money"],
                "pay_money"=>$order["pay_money"],
                "type"=>"tl"
            ];
            $model->save($data);
        }else{
            $model::where("id",$info["id"])->setInc("money",$order["money"]);
            $model::where("id",$info["id"])->setInc("pay_money",$order["pay_money"]);
        }
    }



    /**
     * 同行支付增加日财务
     */
    public function agentDayak($order_id)
    {
        $model = new AgentFinance();//商家日财务
        $orderModel = new AkOrder();//订单表
        $order = $orderModel::get($order_id);

        if(!$order){
            return AjaxReturn(0,"订单不存在");
        }
        if(!$order["payment"]) {
            return AjaxReturn(0, "订单未付款");
        }
        $time = time();
        $year = date("Y",$time);
        $month = intval(date("m",$time));
        $day = intval(date("d",$time));
        $info = $model::get(["year"=>$year,"month"=>$month,"day"=>$day,"agent_id"=>$order["agent_id"]]);
        if(!$info){
            $data = [
                "year"=>$year,
                "month"=>$month,
                "day"=>$day,
                "agent_id"=>$order["agent_id"],
                "money"=>$order["money"],
                "pay_money"=>$order["pay_money"]
            ];
            $model->save($data);
        }else{
            $model::where("id",$info["id"])->setInc("money",$order["money"]);
            $model::where("id",$info["id"])->setInc("pay_money",$order["pay_money"]);
        }
    }


    /**
     * 当日下单单笔金额统计
     */
    public function jinexiatl($order_id){
        $model = new MoneyFinance();
        $orderModel = new TlOrder();
        $order = $orderModel::get($order_id);

        if(!$order){
            return AjaxReturn(0,"订单不存在");
        }
        if($order["payment"]) {
            return AjaxReturn(0, "订单已经支付");
        }
        $time = time();
        $year = date("Y",$time);
        $month = intval(date("m",$time));
        $day = intval(date("d",$time));
        $info = $model::get(["year"=>$year,"month"=>$month,"day"=>$day,"jine"=>$order["money"]]);
        if(!$info){
            $data = [
                "year"=>$year,
                "month"=>$month,
                "day"=>$day,
                "jine"=>$order["money"],
                "money"=>$order["money"],
                //"pay_money"=>$order["pay_money"],
                "total_num"=>1
            ];
            $model->save($data);
        }else{
            $model::where("id",$info["id"])->setInc("money",$order["money"]);
            //$model::where("id",$info["id"])->setInc("pay_money",$order["pay_money"]);
            $model::where("id",$info["id"])->setInc("total_num");
        }
    }
    /**
     * 当日下单单笔金额统计
     */
    public function jinexiaxs($order_id){
        $model = new MoneyFinance();
        $orderModel = new XsOrder();
        $order = $orderModel::get($order_id);

        if(!$order){
            return AjaxReturn(0,"订单不存在");
        }
        if($order["payment"]) {
            return AjaxReturn(0, "订单已经支付");
        }
        $time = time();
        $year = date("Y",$time);
        $month = intval(date("m",$time));
        $day = intval(date("d",$time));
        $info = $model::get(["year"=>$year,"month"=>$month,"day"=>$day,"jine"=>$order["money"]]);
        if(!$info){
            $data = [
                "year"=>$year,
                "month"=>$month,
                "day"=>$day,
                "jine"=>$order["money"],
                "money"=>$order["money"],
                //"pay_money"=>$order["pay_money"],
                "total_num"=>1
            ];
            $model->save($data);
        }else{
            $model::where("id",$info["id"])->setInc("money",$order["money"]);
            //$model::where("id",$info["id"])->setInc("pay_money",$order["pay_money"]);
            $model::where("id",$info["id"])->setInc("total_num");
        }
    }

    /**
     * 当日付款单笔金额统计
     */
    public function jinedaypaytl($order_id){
        $model = new MoneyFinance();
        $orderModel = new TlOrder();
        $order = $orderModel::get($order_id);

        if(!$order){
            return AjaxReturn(0,"订单不存在");
        }
        if($order["payment"] < 1) {
            return AjaxReturn(0, "订单未付款");
        }
        $time = time();
        $year = date("Y",$time);
        $month = intval(date("m",$time));
        $day = intval(date("d",$time));
        $info = $model::get(["year"=>$year,"month"=>$month,"day"=>$day,"jine"=>$order["money"]]);
        if(!$info){
            return AjaxReturn(0, "未下单");
        }else{
            $model::where("id",$info["id"])->setInc("pay_num");//支付笔数
            $model::where("id",$info["id"])->setInc("pay_money",$order["pay_money"]);//支付金额

        }
    }


    /**
     * 新生增加日财务
     */
    public function agentDayxs($order_id)
    {
        $model = new AgentFinance();//商家日财务
        $orderModel = new XsOrder();//订单表
        $order = $orderModel::get($order_id);

        if(!$order){
            return AjaxReturn(0,"订单不存在");
        }
        if(!$order["payment"]) {
            return AjaxReturn(0, "订单未付款");
        }
        $time = time();
        $year = date("Y",$time);
        $month = intval(date("m",$time));
        $day = intval(date("d",$time));
        $info = $model::get(["year"=>$year,"month"=>$month,"day"=>$day,"agent_id"=>$order["agent_id"],"type"=>"xs"]);
        if(!$info){
            $data = [
                "year"=>$year,
                "month"=>$month,
                "day"=>$day,
                "agent_id"=>$order["agent_id"],
                "money"=>$order["money"],
                "pay_money"=>$order["pay_money"],
                "type"=>"xs"
            ];
            $model->save($data);
        }else{
            $model::where("id",$info["id"])->setInc("money",$order["money"]);
            $model::where("id",$info["id"])->setInc("pay_money",$order["pay_money"]);
        }
    }


    /**
     * 通联通道日财务
     */
    public function payDaytongdaoxs($order_id)
    {
        $model = new TongdaoFinance();//商家日财务
        $orderModel = new XsOrder();//订单表
        $order = $orderModel::get($order_id);

        if(!$order){
            return AjaxReturn(0,"订单不存在");
        }
        if(!$order["payment"]) {
            return AjaxReturn(0, "订单未付款");
        }
        $time = time();
        $year = date("Y",$time);
        $month = intval(date("m",$time));
        $day = intval(date("d",$time));
        $info = $model::get(["year"=>$year,"month"=>$month,"day"=>$day,"bid"=>$order["bid"],"type"=>"xs"]);
        if(!$info){
            $data = [
                "year"=>$year,
                "month"=>$month,
                "day"=>$day,
                "bid"=>$order["bid"],
                "money"=>$order["money"],
                "pay_money"=>$order["pay_money"],
                "type"=>"xs"
            ];
            $model->save($data);
        }else{
            $model::where("id",$info["id"])->setInc("money",$order["money"]);
            $model::where("id",$info["id"])->setInc("pay_money",$order["pay_money"]);
        }
    }

    /**************************************新生代付财务***********************************************/

    /**
     * 新生代付增加日财务
     */
    public function agentDayxsdf($order_id)
    {
        $model = new AgentFinanceDf();//商家日财务
        $orderModel = new XsXiafa();//订单表
        $order = $orderModel::get($order_id);

        if(!$order){
            return AjaxReturn(0,"订单不存在");
        }
        if(!$order["payment"]) {
            return AjaxReturn(0, "订单未付款");
        }
        $time = time();
        $year = date("Y",$time);
        $month = intval(date("m",$time));
        $day = intval(date("d",$time));
        $info = $model::get(["year"=>$year,"month"=>$month,"day"=>$day,"agent_id"=>$order["agent_id"]]);
        $money =  bcadd($order['sum'],1);
        if(!$info){
            $data = [
                "year"=>$year,
                "month"=>$month,
                "day"=>$day,
                "agent_id"=>$order["agent_id"],
                "money"=>$money,
                "df_money"=>$order["sum"],
                "bi"=>1
            ];
            $model->save($data);
        }else{
            $model::where("id",$info["id"])->setInc("money",$money);
            $model::where("id",$info["id"])->setInc("df_money",$order["sum"]);
            $model::where("id",$info["id"])->setInc("bi");
        }
    }

    public function payDaytongdaoxsdf($order_id)
    {
        $model = new TongdaoFinanceDf();//商家日财务
        $orderModel = new XsXiafa();//订单表
        $order = $orderModel::get($order_id);

        if(!$order){
            return AjaxReturn(0,"订单不存在");
        }
        if($order["payment"] != 1) {
            return AjaxReturn(0, "订单未付款");
        }
        $time = time();
        $year = date("Y",$time);
        $month = intval(date("m",$time));
        $day = intval(date("d",$time));
        $info = $model::get(["year"=>$year,"month"=>$month,"day"=>$day,"bid"=>$order["bid"]]);
        $money =  bcadd($order['sum'],1);
        if(!$info){

            $data = [
                "year"=>$year,
                "month"=>$month,
                "day"=>$day,
                "bid"=>$order["bid"],
                "money"=>$money,
                "df_money"=>$order["sum"],
                "bi"=>1
            ];
            $model->save($data);
        }else{

            $model::where("id",$info["id"])->setInc("money",$money);
            $model::where("id",$info["id"])->setInc("df_money",$order["sum"]);
            $model::where("id",$info["id"])->setInc("bi");
        }
    }

    /***********************************海科支付财务统计************************************************/

    /**
     * 商户日财务统计
     */
    public function agentDayhk($order_id){
        $model = new AgentFinance();//商家日财务
        $orderModel = new HkOrder();//订单表
        $order = $orderModel::get($order_id);

        if(!$order){
            return AjaxReturn(0,"订单不存在");
        }
        if(!$order["payment"]) {
            return AjaxReturn(0, "订单未付款");
        }
        $time = time();
        $year = date("Y",$time);
        $month = intval(date("m",$time));
        $day = intval(date("d",$time));
        $info = $model::get(["year"=>$year,"month"=>$month,"day"=>$day,"agent_id"=>$order["agent_id"],"type"=>"hk"]);
        if(!$info){
            $data = [
                "year"=>$year,
                "month"=>$month,
                "day"=>$day,
                "agent_id"=>$order["agent_id"],
                "money"=>$order["money"],
                "pay_money"=>$order["pay_money"],
                "type"=>"hk"
            ];
            $model->save($data);
        }else{
            $model::where("id",$info["id"])->setInc("money",$order["money"]);
            $model::where("id",$info["id"])->setInc("pay_money",$order["pay_money"]);
        }
    }


    /**
     *通道日财务统计
     */
    public function payDaytongdaoxshk($order_id){
        $model = new TongdaoFinance();//商家日财务
        $orderModel = new HkOrder();//订单表
        $order = $orderModel::get($order_id);

        if(!$order){
            return AjaxReturn(0,"订单不存在");
        }
        if(!$order["payment"]) {
            return AjaxReturn(0, "订单未付款");
        }
        $time = time();
        $year = date("Y",$time);
        $month = intval(date("m",$time));
        $day = intval(date("d",$time));
        $info = $model::get(["year"=>$year,"month"=>$month,"day"=>$day,"bid"=>$order["bid"],"type"=>"hk"]);
        if(!$info){
            $data = [
                "year"=>$year,
                "month"=>$month,
                "day"=>$day,
                "bid"=>$order["bid"],
                "money"=>$order["money"],
                "pay_money"=>$order["pay_money"],
                "type"=>"hk"
            ];
            $model->save($data);
        }else{
            $model::where("id",$info["id"])->setInc("money",$order["money"]);
            $model::where("id",$info["id"])->setInc("pay_money",$order["pay_money"]);
        }
    }


    /**************************************老谢支付通*******************************************************/

    /**
     * 商户日财务统计
     */
    public function agentDaylx($order_id){
        $model = new AgentFinance();//商家日财务
        $orderModel = new LaoxieOrder();//订单表
        $order = $orderModel::get($order_id);

        if(!$order){
            return AjaxReturn(0,"订单不存在");
        }
        if(!$order["payment"]) {
            return AjaxReturn(0, "订单未付款");
        }
        $time = time();
        $year = date("Y",$time);
        $month = intval(date("m",$time));
        $day = intval(date("d",$time));
        $info = $model::get(["year"=>$year,"month"=>$month,"day"=>$day,"agent_id"=>$order["agent_id"],"type"=>"laoxie"]);
        if(!$info){
            $data = [
                "year"=>$year,
                "month"=>$month,
                "day"=>$day,
                "agent_id"=>$order["agent_id"],
                "money"=>$order["money"],
                "pay_money"=>$order["pay_money"],
                "type"=>"laoxie"
            ];
            $model->save($data);
        }else{
            $model::where("id",$info["id"])->setInc("money",$order["money"]);
            $model::where("id",$info["id"])->setInc("pay_money",$order["pay_money"]);
        }
    }


    /**
     *通道日财务统计
     */
    public function payDaytongdaoxslx($order_id){
        $model = new TongdaoFinance();//商家日财务
        $orderModel = new LaoxieOrder();//订单表
        $order = $orderModel::get($order_id);

        if(!$order){
            return AjaxReturn(0,"订单不存在");
        }
        if(!$order["payment"]) {
            return AjaxReturn(0, "订单未付款");
        }
        $time = time();
        $year = date("Y",$time);
        $month = intval(date("m",$time));
        $day = intval(date("d",$time));
        $info = $model::get(["year"=>$year,"month"=>$month,"day"=>$day,"bid"=>$order["bid"],"type"=>"laoxie"]);
        if(!$info){
            $data = [
                "year"=>$year,
                "month"=>$month,
                "day"=>$day,
                "bid"=>$order["bid"],
                "money"=>$order["money"],
                "pay_money"=>$order["pay_money"],
                "type"=>"laoxie"
            ];
            $model->save($data);
        }else{
            $model::where("id",$info["id"])->setInc("money",$order["money"]);
            $model::where("id",$info["id"])->setInc("pay_money",$order["pay_money"]);
        }
    }

    /************************************************支付通*********************************************************/
    /**
     * 商户日财务统计
     */
    public function agentDayzft($order_id){
        $str = 'zhifutong';
        $model = new AgentFinance();//商家日财务
        $orderModel = new ZhifutongOrder();//订单表
        $order = $orderModel::get($order_id);

        if(!$order){
            return AjaxReturn(0,"订单不存在");
        }
        if(!$order["payment"]) {
            return AjaxReturn(0, "订单未付款");
        }
        $time = time();
        $year = date("Y",$time);
        $month = intval(date("m",$time));
        $day = intval(date("d",$time));
        $info = $model::get(["year"=>$year,"month"=>$month,"day"=>$day,"agent_id"=>$order["agent_id"],"type"=>$str]);
        if(!$info){
            $data = [
                "year"=>$year,
                "month"=>$month,
                "day"=>$day,
                "agent_id"=>$order["agent_id"],
                "money"=>$order["money"],
                "pay_money"=>$order["pay_money"],
                "type"=>$str
            ];
            $model->save($data);
        }else{
            $model::where("id",$info["id"])->setInc("money",$order["money"]);
            $model::where("id",$info["id"])->setInc("pay_money",$order["pay_money"]);
        }
    }


    /**
     *通道日财务统计
     */
    public function payDaytongdaoxszft($order_id){
        $str = 'zhifutong';
        $model = new TongdaoFinance();//商家日财务
        $orderModel = new ZhifutongOrder();//订单表
        $order = $orderModel::get($order_id);

        if(!$order){
            return AjaxReturn(0,"订单不存在");
        }
        if(!$order["payment"]) {
            return AjaxReturn(0, "订单未付款");
        }
        $time = time();
        $year = date("Y",$time);
        $month = intval(date("m",$time));
        $day = intval(date("d",$time));
        $info = $model::get(["year"=>$year,"month"=>$month,"day"=>$day,"bid"=>$order["bid"],"type"=>$str]);
        if(!$info){
            $data = [
                "year"=>$year,
                "month"=>$month,
                "day"=>$day,
                "bid"=>$order["bid"],
                "money"=>$order["money"],
                "pay_money"=>$order["pay_money"],
                "type"=>$str
            ];
            $model->save($data);
        }else{
            $model::where("id",$info["id"])->setInc("money",$order["money"]);
            $model::where("id",$info["id"])->setInc("pay_money",$order["pay_money"]);
        }
    }

}
