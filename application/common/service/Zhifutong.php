<?php

namespace app\common\service;

class Zhifutong
{
    private $mchNo = "123445";
//版本号
    private $version = "1.0";

//秘钥
    private $key = "";
//请求地址
    private $payurl='{{url}}/hwgPay/api/unifiedOrder';
//异步回调地址
    private $notifyUrl='https:www.baidu.com';
//同步回调地址
    private $returnUrl='https:www.baidu.com';
//过期时间
    private $expiredTime='180';

    //数据加密
    public function response_encrypt()
    {
        $params = [
            'mchNo'=>$this->mchNo,//商户号
            'wayCode'=>'ALIPAY_H5',//ALIPAY_H5：支付宝Wap支付，ALIPAY_SDK：支付宝app支付,ALIPAY_QR：支付宝当面付
            'amount' => 1, // 价格:单位分
            'clientIp' => request()->ip(),//用户真实ip
            'subject' => '购买商品',
            'mchOrderNo' => 'test-'.time(),//订单号
            'body' => '9',//订单商品类型1游戏币2游戏账号3游戏什盲盒7二手者侈品8二手手表9基他
            'channelExtra'=>'',
            'version' => $this->version,//版本号
            'notifyUrl' => $this->notifyUrl,  // 异步通知地址
            'returnUrl' => $this->returnUrl,//同步回调地址
            'reqTime' => date('Y-m-d H:i:s'),
            'expiredTime'=>$this->expiredTime,//订单失效时间,单位秒,默认2小时.订单在(创建时间+失效时间)后失效
        ];
        // 生成签名
        $params['sign'] = $this->makeSign($params);
        $result = $this->post($this->payurl, $params);
        $prepay = json_decode($result);
        //{
        //    "code": 0,
        //    "data": {
        //        "mchOrderNo": "2024041953950",
        //        "orderState": "0001",
        //        "payData": "https://opena%2F%2FCJLDo7fLo6LnzKxq5vX3b8pD78Dsboecxg%3D%3D&sign_type=RSA2&timestamp=2024-04-19+08%3A02%3A08&version=1.0",
        //        "payDataType": "payurl",
        //        "payOrderId": "20240419100183875"
        //    },
        //    "msg": "SUCCESS",
        //    "sign": "39e549cb13938b16da15c495b09f5"
        //}
        // 请求失败
        if ($prepay['code'] == '0' && $prepay['msg']=='SUCCESS') {
            //处理业务逻辑 返回支付链接

        }else{
            throwError($prepay['msg'], null, ['errorCode' => $prepay['respCode'], 'date' => date('YmdHis'), '$params' => $params]);
        }

    }
    /**
     * 模拟POST请求 [第二种方式, 用于兼容微信api]
     * @param $url
     * @param array $data
     * @return mixed
     * @throws \cores\exception\BaseException
     */
    protected function post($url, array $data = [])
    {
        $header = [
            'Content-Type: application/json;charset=UTF-8',
        ];
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_POST, TRUE);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
        $result = curl_exec($curl);
        if ($result === false) {
            throwError(curl_error($curl));
        }
        curl_close($curl);
        return $result;
    }
    /**
     * 生成签名
     * @param array $values
     * @return string 本函数不覆盖sign成员变量，如要设置签名需要调用SetSign方法赋值
     */
    private function makeSign(array $values)
    {
        //签名步骤一：按字典序排序参数
        ksort($values,SORT_STRING);

        $string = $this->toUrlParams($values);
        //签名步骤二：在string后加入KEY
        $string = $string . $this->key;
        //throwError($string);

        //签名步骤三：MD5加密
        $string = md5($string);
        //签名步骤四：所有字符转为大写
        return $string;
    }
    /**
     * 格式化参数格式化成url参数
     * @param array $values
     * @return string
     */
    private function toUrlParams(array $values)
    {
        $buff = '';
        foreach ($values as $k => $v) {
            if ($k != 'sign' && $v != '' && !is_array($v)) {
                $buff .= $k . '=' . $v . '&';
            }
        }
        return trim($buff, '&');
    }
//异步回调参数
//{"respDesc":"0001",
//"amount":"1.00",
//"payOrderId":"20240419161058020",
//"mchOrderNo":"202401525257",
//"wayCode":"ALIPAY_H5",
//"sign":"4356ea0101ca19df59347c54d03e",
//"reqTime":"2024-04-19 16:18:27",
//"respCode":"0000",
//"nonceStr":"1464570610519438"}
    private function notify()
    {
        $data=input();
        if($data['respCode']=='0000'){
            //支付处理回调
        }
    }
}