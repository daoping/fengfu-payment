<?php
namespace app\common\service;



class Cashier extends SdCommon
{
    protected $productId = '00000006';

    // 参数映射
    protected function apiMap()
    {
        return array(
            'orderPay'          => array(
                'method' => 'sandpay.trade.barpay',
                'url'    => '/qr/api/order/pay',
            ),
            'orderCreate'       => array(
                'method' => 'sandpay.trade.precreate',
                'url'    => '/qr/api/order/create',
            ),
            'orderQuery'        => array(
                'method' => 'sandpay.trade.query',
                'url'    => '/qr/api/order/query',
            ),
            'orderRefund'       => array(
                'method' => 'sandpay.trade.refund',
                'url'    => '/gw/api/order/refund',
            ),
            'orderMcAutoNotice' => array(
                'method' => 'sandpay.trade.notify',
                'url'    => '/gateway/api/order/mcAutoNotice',
            ),
            'clearfileDownload' => array(
                'method' => 'sandpay.trade.download',
                'url'    => '/qr/api/clearfile/download',
            ),
        );
    }
}
