<?php
namespace app\common\model;


use think\Model;

class XsAccount extends Model
{
    public $autoWriteTimestamp = true;

    /**
     * 关联通道表
     * @return \think\model\relation\HasOne
     */
    public function payday()
    {
        return $this->hasOne('XinshengChannel','id','bid')->bind("channel_name");
    }
}
