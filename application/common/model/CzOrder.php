<?php
namespace app\common\model;


use think\Model;

class CzOrder extends Model
{
    public $autoWriteTimestamp = true;

    /**
     * 关联通道表
     * @return \think\model\relation\HasOne
     */
    public function payday()
    {
        return $this->hasOne('PayChannel','id','cid')->bind("channel_name");
    }

}
