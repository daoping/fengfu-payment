<?php
namespace app\common\model;

use think\Model;

class AgentFinanceDf extends Model
{
    public $autoWriteTimestamp = true;
    /**
     * 关联通道表
     * @return \think\model\relation\HasOne
     */
    public function payday()
    {
        return $this->hasOne('Agent','id','agent_id')->bind("mobile");
    }
}