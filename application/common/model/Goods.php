<?php
namespace app\common\model;

use think\Model;

class Goods extends Model
{
    public $autoWriteTimestamp = true;

    /**
     * 关联通道表
     * @return \think\model\relation\HasOne
     */
    public function tongdao()
    {
        return $this->hasOne('ZhifutongChannel','id','bid')->bind("channel_name");
    }
}