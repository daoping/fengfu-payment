<?php
namespace app\common\model;

use think\Db;
use think\Model;

class Provider extends Model
{
    public $autoWriteTimestamp = true;

    //protected $updateTime = false;
    /**
     * 关联商户表
     * @return \think\model\relation\HasOne
     */
    public function channel()
    {
        return $this->hasOne('PayChannel','id','cid')->bind("channel_name");
    }

}
