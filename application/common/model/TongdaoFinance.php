<?php
namespace app\common\model;

use think\Model;

class TongdaoFinance extends Model
{
    public $autoWriteTimestamp = true;
    /**
     * 关联通道表
     * @return \think\model\relation\HasOne
     */
    public function payday()
    {
        return $this->hasOne('TonglianChannel','id','bid')->bind("channel_name");
    }
}