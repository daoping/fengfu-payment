<?php
/**
 * Created by PhpStorm.
 * User: zhao
 * Date: 2019/4/27
 * Time: 15:16
 */
namespace app\common\model;

use think\Model;

class Order extends Model{
    //自动时间戳
    public $autoWriteTimestamp = true;

    /**
     * 关联商家表
     * @return \think\model\relation\HasOne
     */
    public function agent()
    {
        return $this->hasOne('agent','id','agent_id')->bind("mobile");
    }
    /**
     * 关联通道表
     * @return \think\model\relation\HasOne
     */
    public function payday()
    {
        return $this->hasOne('PayChannel','id','pay_channel_id')->bind("channel_name");
    }

}