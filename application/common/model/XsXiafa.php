<?php
namespace app\common\model;


use think\Model;

class XsXiafa extends Model
{
    public $autoWriteTimestamp = true;

    /**
     * 关联商家表
     * @return \think\model\relation\HasOne
     */
    public function paychannel()
    {
        return $this->hasOne('XinshengChannel','id','bid')->bind("channel_name");
    }

}
