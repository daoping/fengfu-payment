<?php
namespace app\common\model;

use think\Db;
use think\Model;
//use app\common\validate\Partner as PartnerValidate;

class PayChannelFinance extends Model
{
    public $autoWriteTimestamp = true;

    //protected $updateTime = false;

    /**
     * 关联商家表
     * @return \think\model\relation\HasOne
     */
    public function paychannel()
    {
        return $this->hasOne('PayChannel','id','cid')->bind("channel_name");
    }

}
