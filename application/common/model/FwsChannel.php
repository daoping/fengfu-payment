<?php
namespace app\common\model;

use think\Db;
use think\Model;

class FwsChannel extends Model
{
    public $autoWriteTimestamp = true;

    //protected $updateTime = false;
    /**
     * 关联商户表
     * @return \think\model\relation\HasOne
     */
    public function channel()
    {
        return $this->hasOne('PayChannel','id','cid')->bind("channel_name");
    }

    /**
     * 关联商户表
     * @return \think\model\relation\HasOne
     */
    public function qds()
    {
        return $this->hasOne('Provider','id','qid')->bind(["qname"=>"name"]);
    }

}
