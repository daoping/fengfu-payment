<?php
namespace app\common\validate;

use think\Validate;

class Agent extends Validate
{

    protected $rule = [
        "id|带库ID"       => ["require","number"],
        "bank_id|银行卡ID"       => ["require","number"],
        "bank_num|银行卡号"       => ["require","number"],
        "is_lock|开关状态"       => ["require"],
        "mobile|手机号"       => ["require","mobile"],
        "name|姓名"       => ["require","chs"],//chs 只能是汉字
        "pwd|密码"       => ["require","length:6,50"],
        "rate|利率"       => ["require"],
        "username|用户名"       => ["require"]
    ];

    protected $scene = [
        "add"   => ["address","bank_id","bank_num","card_num","is_lock","mobile","name","pwd","rate","username"],
        "edit"   => ["id","address","bank_id","bank_num","card_num","is_lock","mobile","name","rate","username"]
    ];


}
