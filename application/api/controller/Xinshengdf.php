<?php

namespace app\api\controller;

use app\common\model\Agent;
use app\common\model\Order;
use app\common\model\PayChannel;
use app\common\model\SdXiafa;
use app\common\model\XinshengChannel;
use app\common\model\XsXiafa;
use JytPay\Client_ONEPAY\JytJsonClient;
use RSAUtils;
use think\Controller;
use think\Db;
use think\facade\Env;
use think\facade\Request;
use think\facade\Validate;


include_once  Env::get("root_path"). "extend/utils/ExpUtils.php";
include_once  Env::get("root_path"). "extend/utils/RSAUtils.php";
class Xinshengdf extends Controller
{



    /**
     *  方哥 11000010741
     * 荣光 11000010694
     *  5.11 商户账户余额查询接口
     *
     * 本地 http://pay.cxlaimeng.cn/api/Xinshengdf/queryBalance
     * 线上  https://pay.cxlaimeng.com/api/Xinshengdf/queryBalance
     */
    public function queryBalance()
    {
        $data = input("post.");
        $validate = Validate::make([
            'agent_id|代理ID'                => 'require',
            'sign|签名'                => 'require|length:32',
        ]);

        if (!$validate->check($data)) {
            return AjaxReturn(0,$validate->getError());
        }
        $agent = Agent::get($data["agent_id"]);
        //halt($agent);
        if(!$agent){
            return AjaxReturn(0,'商户不存在');
        }
        if($agent["is_lock"]){
            return AjaxReturn(0,"商户暂停锁定");
        }
        $arr = [
            'agent_id'              => $data['agent_id'],
        ];
        $sign=createSign($arr,$agent["key"]);
        //halt($sign);
        if($sign != $data['sign']){
            return AjaxReturn(0,"签名错误");
        }
        $arr =[
            "balance"=>$agent['money'],//可用余额
            "accountStatus"=>"open",//账户状态
            //"frozenBalance"=>"0.00",
        ];


        return AjaxReturn(1,"查询成功",$arr);

    }

    public function queryBalancesign()
    {
        $data = input("post.");
        $validate = Validate::make([
            'agent_id|代理ID'                => 'require',
            //'sign|签名'                => 'require|length:32',
        ]);

        if (!$validate->check($data)) {
            return AjaxReturn(0,$validate->getError());
        }
        $agent = Agent::get($data["agent_id"]);
        //halt($agent);
        if(!$agent){
            return AjaxReturn(0,'商户不存在');
        }
        if($agent["is_lock"]){
            return AjaxReturn(0,"商户暂停锁定");
        }
        $arr = [
            'agent_id'              => $data['agent_id'],
        ];
        $sign=createSign($arr,$agent["key"]);
        echo $sign;


    }



    /**
     *  5.4 付款到银行
     * 建设银行 6217000010197280913 孙正
     * https://pay.cxlaimeng.com/api/Xsdaifu/singlePay
     */
    public function singlePay()
    {
        $data = input("post.");
        //"accNo" =>$info['bankAccount'],  //银行账户
        //                "accName" =>$info['accountName'],//姓名
        $validate = Validate::make([
            'agent_id|代理ID'                => 'require',
            'bankAccount|银行卡号'                => 'require',
            'accountName|姓名'                => 'require',
            'sum|打款金额'                => 'require',
            'df_order|订单号'                => 'require',
            'notify_url|异步地址'                => 'require',
            'bid|通道id'                => 'require',
            'sign|签名'                => 'require|length:32',
        ]);

        if (!$validate->check($data)) {
            return AjaxReturn(0,$validate->getError());
        }
        //反查
//        $url = $data['withdrawQueryUrl'];
//        $parms = [
//            "merchantId"=>$data['agent_id'],
//            "money"=>$data['sum'],
//            "orderNo"=>$data['df_order'],
//            "token"=>$data['callToken'],
//            "target"=>$data['bankAccount'],
//            "ownerName"=>$data['accountName'],
//        ];
//        $fancha_res = request_post($url,$parms);
//        //halt($fancha_res);
//        $fancha_res = json_decode($fancha_res,true);
//        if ($fancha_res['status'] < 1){
//            return AjaxReturn(0,$fancha_res['msg']);
//        }


//        $mm =  Db::name('config')->where('id',91)->find();
//        if($data['sum'] > $mm['value']){
//            return AjaxReturn(0,"最大单笔提现金额".$mm['value']);
//        }
        if($data['sum'] < 0){
            return AjaxReturn(0,"金额不对");
        }
        $model = new XsXiafa();
        $info = $model::where(["df_order"=>$data['df_order']])->find();
        if($info){
            return AjaxReturn(0,'代付订单已存在');
        }
        $agent_model = new Agent();
        $agent = $agent_model::get($data["agent_id"]);
        if(!$agent){
            return AjaxReturn(0,'商户不存在');
        }
        if($agent["is_lock"]){
            return AjaxReturn(0,"商户暂停锁定");
        }

        //判断首信金额
        if($agent["money"] < 0){
            return AjaxReturn(0,"商户授信金额不足");
        }

        if($data['sum'] > $agent['money']){
            return AjaxReturn(0,"商户授信金额不足");
        }
        $pay_channel_model = new XinshengChannel();
        $pay_channel = $pay_channel_model::where(['daifu'=>1,"id"=>$data["bid"]])->find();
        //halt($pay_channel);
        if(!$pay_channel){
            return AjaxReturn(0,"代付通道没开启");
        }
        $arr = [
            'agent_id'              => $data['agent_id'],
            'bankAccount'              => $data['bankAccount'],
            'accountName'              => $data['accountName'],
            'sum'                       => $data['sum'],
            'df_order'              => $data['df_order'],
            'notify_url'              => $data['notify_url'],
            'bid'              => $data['bid'],

        ];

        $sign=createSign($arr,$agent["key"]);
        //halt($sign);
        if($sign != $data['sign']){
            return AjaxReturn(0,"签名错误");
        }
        $arr["order_num"] = orderNum();//下发订单号
        $arr["create_time"] = time();//
        $arr["ip"] = request()->ip();
        $arr["bid"] = $pay_channel['id'];
        $res = $model->insert($arr);
        if(!$res){
            return AjaxReturn(0,"创建订单失败");
        }

        $info = $model::where(["df_order"=>$data['df_order']])->find();
        if(!$info){
            return AjaxReturn(0,"打款订单不存在");
        }
        if($info["payment"] > 0){
            return AjaxReturn(0,"不是打款状态");
        }
        //halt(1);//后面执行代付信息
        $merId = $pay_channel['merId'];
        $url = "http://47.121.219.250:39500/v2/single/pay/singlePay";
        $payarr = [
            "merId"=>$merId,
            "merOrderId"=>$arr["order_num"],
            "payeeName"=>$arr['accountName'],//孙正
            "payeeAccount"=>$arr['bankAccount'],//6217000010197280913
            "yibuUrl"=>ym()."/api/xsnotify/daifunotify",
            "submitTime"=>date("YmdHis"),//存数据库
            "tranAmt"=>strval($info['sum']),
        ];
        $key = "93485057aae5504753a6b391e4aad5f9";
        $sign=createSign($payarr,$key);
        $payarr['sign']=$sign;
        //dump($url);
        //dump($arr);
        $res = request_post_jsons($url,$payarr);
        //dump($res);
        $res_arr = json_decode($res,true);
        //halt($res_arr);
        if($res_arr['code'] == "200"){
            $model::where(["df_order"=>$data['df_order']])->update(["submitTime"=>$payarr['submitTime']]);
            if($res_arr['data']['resultCode'] == "0000"){
                return AjaxReturn(1,"已打款,状态异步为准");
            }elseif ($res_arr['data']['resultCode'] == "4444"){
                $model::where(["df_order"=>$data['df_order']])->update(["payment"=>3,"dealRemark"=>"错误代码".$res_arr['data']['resultCode'].",错误信息".$res_arr['data']['errorMsg']]);
                return AjaxReturn(0,"错误代码".$res_arr['data']['resultCode'].",错误信息".$res_arr['data']['errorMsg']);
            }else{
                return AjaxReturn(0,"错误代码".$res_arr['data']['resultCode'].",错误信息".$res_arr['data']['errorMsg']);
            }
        }else{
            return AjaxReturn(0,"打款失败,请重试");
        }
        //同步返回
        //正常
        //{"msg":"操作成功","code":200,"data":{"charset":"1","hnapayOrderId":"2024111481888543","resultCode":"0000","errorCode":"","version":"2.1","signValue":"Jtqua2gi1xcnWYNF9mWV8CGFo7luzQbR9mNQ9L3hUejR7hmjb4TSFhO7mAUg5wXRGbo8QbocHTAa7C4wkBwWAHQoXwLoxaOtIJbhRpweYCuwm+dG8rLHKJ5nkmRjHXVbng0nYz2TG9aXGGJa2KBL+hsqIN8RMMt8JVbWYPDaSZ8=","errorMsg":"","signType":"1","merId":"11000010694","tranCode":"SGP01","merAttach":"https://pay.cxlaimeng.com/api/xsnotify/daifunotify","merOrderId":"jlB14998100430971"}}
        //失败1
        //{"msg":"操作成功","code":200,"data":{"charset":"1","hnapayOrderId":"","resultCode":"4444","errorCode":"A0000819","version":"2.1","signValue":"OUV18tlcotAzSw6Yd8URoPIe2Uh2jKal+hSNN4zCC51Utvrksr6fdJIgoekkFo33BhAwf4m1+kQlvxvLHHPPIFsASqw6DDkVLCr4fSBadcJkiqPhltT0BjYQdXtZpNpquvUm6QKNHJt6wJDwIG/P7cI88U0QmvOMj0dS4tB99Ls=","errorMsg":"余额不足","signType":"1","merId":"11000010694","tranCode":"SGP01","merAttach":"https://pay.cxlaimeng.com/api/xsnotify/daifunotify","merOrderId":"jlB15001444855877"}}
        //打款失败 但是显示成功的
        //{"msg":"操作成功","code":200,"data":{"charset":"1","hnapayOrderId":"2024111581887499","resultCode":"0000","errorCode":"","version":"2.1","signValue":"e6l5xO+YJGoJprEKSx+NCQvzy7RH9FCrWc1mSFp6o8zCEpE/+aoi+eqoiPiB+B8gTLe9KzVakWnjnjIgkkCV9Bgm4MRa8ruPELm/pXBmwhaqF980WaCaT1wYR/09RHEIUuPUt1Ky5h+CVlsTf2tr/epweKqt3ZtkGGAeoKrscyo=","errorMsg":"","signType":"1","merId":"11000010741","tranCode":"SGP01","merAttach":"https://pay.cxlaimeng.com/api/xsnotify/daifunotify","merOrderId":"jlB15003161210557"}}
    }
    public function singlePaysign()
    {
        $data = input("post.");
        //"accNo" =>$info['bankAccount'],  //银行账户
        //                "accName" =>$info['accountName'],//姓名
        $validate = Validate::make([
            'agent_id|代理ID'                => 'require',
            'bankAccount|银行卡号'                => 'require',
            'accountName|姓名'                => 'require',
            'sum|打款金额'                => 'require',
            'df_order|订单号'                => 'require',
            'notify_url|异步地址'                => 'require',
            'bid|通道id'                => 'require',

        ]);

        if (!$validate->check($data)) {
            return AjaxReturn(0,$validate->getError());
        }
//        $mm =  Db::name('config')->where('id',91)->find();
//        if($data['sum'] > $mm['value']){
//            return AjaxReturn(0,"最大单笔提现金额".$mm['value']);
//        }
//        if($data['sum'] < 0){
//            return AjaxReturn(0,"金额不对");
//        }
        $model = new XsXiafa();
        $info = $model::where(["df_order"=>$data['df_order']])->find();
        if($info){
            return AjaxReturn(0,'代付订单已存在');
        }
        $agent_model = new Agent();
        $agent = $agent_model::get($data["agent_id"]);
        if(!$agent){
            return AjaxReturn(0,'商户不存在');
        }
        if($agent["is_lock"]){
            return AjaxReturn(0,"商户暂停锁定");
        }

        //判断首信金额
        if($agent["money"] < 0){
            return AjaxReturn(0,"商户授信金额不足");
        }

        if($data['sum'] > $agent['money']){
            return AjaxReturn(0,"商户授信金额不足");
        }
        $pay_channel_model = new XinshengChannel();
        $pay_channel = $pay_channel_model::where(['daifu'=>1,"id"=>$data["bid"]])->find();
        //halt($pay_channel);
        if(!$pay_channel){
            return AjaxReturn(0,"代付通道没开启");
        }
        $arr = [
            'agent_id'              => $data['agent_id'],
            'bankAccount'              => $data['bankAccount'],
            'accountName'              => $data['accountName'],
            'sum'                       => $data['sum'],
            'df_order'              => $data['df_order'],
            'notify_url'              => $data['notify_url'],
            'bid'              => $data['bid'],

        ];

        $sign=createSign($arr,$agent["key"]);
        echo $sign;

    }

    /**
     *  5.7 查询接口-代付
     * https://pay.cxlaimeng.com/api/Xsdaifu/singlePayQuery?order_num=jlB15012830926224
     */
    public function singlePayQuery()
    {
        $data = input("post.");
        //"accNo" =>$info['bankAccount'],  //银行账户
        //                "accName" =>$info['accountName'],//姓名
        $validate = Validate::make([
            'agent_id|代理ID'                => 'require',
            'df_order|代付订单号'                => 'require',
            'sign|签名'                => 'require|length:32',

        ]);

        if (!$validate->check($data)) {
            return AjaxReturn(0,$validate->getError());
        }
//        $mm =  Db::name('config')->where('id',91)->find();
//        if($data['sum'] > $mm['value']){
//            return AjaxReturn(0,"最大单笔提现金额".$mm['value']);
//        }
//        if($data['sum'] < 0){
//            return AjaxReturn(0,"金额不对");
//        }
        $model = new XsXiafa();
        $info = $model::where(["df_order"=>$data['df_order']])->find();
        if(!$info){
            return AjaxReturn(0,'代付订单不存在');
        }
        if($info['payment'] == 1){
            $df_arr = [
                "df_order" => $info["df_order"],
                "money" => $info["sum"],
                "agent_id" => $info["agent_id"],
                "order_num" => $info["order_num"],
                "sum"=>math_add($info["sum"],1),
            ];
            return AjaxReturn(1,"打款成功",$df_arr);
        }
        $arr = [
            'agent_id'              => $data['agent_id'],
            'df_order'              => $data['df_order'],
        ];
        $agent_model = new Agent();
        $agent = $agent_model::get($data["agent_id"]);
        if(!$agent){
            return AjaxReturn(0,'商户不存在');
        }
        $sign=createSign($arr,$agent["key"]);
        //halt($sign);
        if($sign != $data['sign']){
            return AjaxReturn(0,"签名错误");
        }
        $pay_channel_model = new XinshengChannel();
        $pay_channel = $pay_channel_model::where(['daifu'=>1,"id"=>$info["bid"]])->find();
        //halt($pay_channel);
        if(!$pay_channel){
            return AjaxReturn(0,"通道不存在");
        }
        $merOrderId = $info["order_num"];
        $merId = $pay_channel['merId'];
        $url = "http://47.121.219.250:39500/v2/single/pay/singlePayQuery";
        $time = strtotime($info['create_time']);
        $arr = [
            "merId"=>$merId,
            "merOrderId"=>$merOrderId,
            "submitTime"=>date("Ymd",$time),
        ];
        $key = "93485057aae5504753a6b391e4aad5f9";
        $sign=createSign($arr,$key);
        $arr['sign']=$sign;
        $res = request_post_jsons($url,$arr);
        $res_arr = json_decode($res,true);
        //halt($res_arr);
        //{"msg":"操作成功","code":200,"data":{"orderFailedMsg":"","charset":"1","orderFailedCode":"","hnapayOrderId":"","resultCode":"0000","orderStatus":"3","errorCode":"","remark":"","version":"2.0","signValue":"b8saAB4VddeAz8YTzEj7gkunmz7b0qq5mDelxbRgVhAfY1BEmnYxr+TLdPWIQXrayh4tuvnwMbJaz+8/2HBtj4GPKywGeNQe265G+PFEn8R9QG4yQKVRr27mQsXupzxN6I3K4rw5apOOwbl1TJr8FjJYSUDxpDL2Qn1vvv3GCr0=","errorMsg":"","submitTime":"20241114","successTime":"","tranAmt":"","signType":"1","merId":"11000010741","tranCode":"SGP02","merAttach":"","merOrderId":"jlB14998100430971"}}
        //0：已创建
        //1：交易成功
        //2：交易失败
        //3：交易不存在
        if($res_arr['code'] == "200"){
            if($res_arr['data']['resultCode'] == "0000"){
                if($res_arr['data']['orderStatus'] == "1"){
                    $df_arr = [
                        "df_order" => $info["df_order"],
                        "money" => $info["sum"],
                        "agent_id" => $info["agent_id"],
                        "order_num" => $info["order_num"],
                        "sum"=>math_add($info["sum"],1),
                    ];
                    return AjaxReturn(1,"打款成功",$df_arr);
                }else if($res_arr['data']['orderStatus'] == "2"){
                    $df_arr = [
                        "df_order" => $info["df_order"],
                        "money" => $info["sum"],
                        "agent_id" => $info["agent_id"],
                        "order_num" => $info["order_num"],
                        "sum"=>math_add($info["sum"],1),
                    ];
                    return AjaxReturn(0,"交易失败",$df_arr);
                }else if($res_arr['data']['orderStatus'] == "3"){

                    return AjaxReturn(0,"交易不存在");
                }else {
                    return AjaxReturn(2,"已创建,处理中");
                }
            }else{
              return AjaxReturn(0,"打款失败:错误代码".$res_arr['data']['resultCode']);
            }
        }else{
            return AjaxReturn(0,"支付公司查询失败");
        }
    }


    /**
     *  接口反查
     * https://pay.cxlaimeng.com/api/Xinshengdf/fancha
     * http://pay.cxlaimeng.cn/api/Xinshengdf/fancha
     */
    public function fancha()
    {
        $url = "http://test-wallet-api-gateway.fbnma.com/third/thirdwithdraw/withdraw/verify";
        $parms = [
            "merchantId"=>"12134",
            "money"=>1.00,
            "orderNo"=>"cocomi0TCNY241118095244JW3WT",
            "token"=>md5(orderNum()),
            "target"=>"6217000010197280913",
            "ownerName"=>"孙正"
        ];
        $fancha_res = request_post($url,$parms);
        //halt($fancha_res);
        $fancha_res = json_decode($fancha_res,true);
        if ($fancha_res['status'] < 1){
            return AjaxReturn(0,$fancha_res['msg']);
        }
    }

    /**
     *  5.7 查询接口-代付
     * https://pay.cxlaimeng.com/api/Xinshengdf/singlePayQuery?order_num=jlB15012830926224
     */
    public function singlePayQuerysign()
    {
        $data = input("post.");
        //"accNo" =>$info['bankAccount'],  //银行账户
        //                "accName" =>$info['accountName'],//姓名
        $validate = Validate::make([
            'agent_id|代理ID'                => 'require',
            'df_order|代付订单号'                => 'require',
        ]);

        if (!$validate->check($data)) {
            return AjaxReturn(0,$validate->getError());
        }

        $model = new XsXiafa();
        $info = $model::where(["df_order"=>$data['df_order']])->find();
        if(!$info){
            return AjaxReturn(0,'代付订单不存在');
        }
        $arr = [
            'agent_id'              => $data['agent_id'],
            'df_order'              => $data['df_order'],
        ];
        $agent_model = new Agent();
        $agent = $agent_model::get($data["agent_id"]);
        if(!$agent){
            return AjaxReturn(0,'商户不存在');
        }
        $sign=createSign($arr,$agent["key"]);
       echo $sign;
    }

    /**
     * 付款凭证下载
     * https://pay.cxlaimeng.com/api/Xinshengdf/payCertificate?order_num=jlB15415907012937
     */
    public function payCertificate()
    {
        $order_num = $_GET['order_num'];
        $model = new XsXiafa();
        $info = $model::where(["order_num"=>$order_num])->find();
        if(!$info){
            return AjaxReturn(0,'代付订单不存在');
        }
        if($info['billDownloadUrl']){
            return AjaxReturn(0,'已经下载');
        }

        $agent_model = new Agent();
        $agent = $agent_model::get($info["agent_id"]);
        if(!$agent){
            return AjaxReturn(0,'商户不存在');
        }

        $pay_channel_model = new XinshengChannel();
        $pay_channel = $pay_channel_model::where(['daifu'=>1,"id"=>$info["bid"]])->find();
        //halt($pay_channel);
        if(!$pay_channel){
            return AjaxReturn(0,"通道不存在");
        }
        $merOrderId = $info["order_num"];
        $merId = $pay_channel['merId'];

        $url = "http://47.121.219.250:39500/v2/single/pay/payCertificate";
        $arr = [
            "merId"=>$merId,
            "merOrderId"=>orderNum(),
            "orgMerOrderId"=>$merOrderId,
            "orgSubmitTime"=>$info['submitTime'],
        ];
        $key = "93485057aae5504753a6b391e4aad5f9";
        $sign=createSign($arr,$key);
        $arr['sign']=$sign;
        $res = request_post_jsons($url,$arr);
        //dump($res);

        //string(206887) "{"msg":"操作成功","code":200,"data":{"charset":"1","payCertificate":"","hnapayOrderId":"2024111581893821","resultCode":"0000","errorCode":"","version":"2.0","signValue":"dDqzuWa2fQ/ipv6CH9cuuy+yUQK6FZDRgpGHZDmRQrSe+5GL/aCLt0cG6EqxiL1WtO440yBBJjYkhiFZgw8oKBlA+k4VHgHOtMMg7uUjF0a6a15qEtFAtif8dLSSoe+qBulsDnLyu2YGP6axvvuvhmkzMGKsOV50eheXiPzB6Hg=","errorMsg":"","signType":"1","merId":"11000010741","tranCode":"SGP03","merAttach":"","merOrderId":"jlB15012830926224"}}"
        $data = json_decode($res,true);
        //dump($data);
        //echo $data['data']['payCertificate'];
//        $res = $this->saveBase64($data['data']['payCertificate']);//http://pay.cxlaimeng.cn./uploads/images/673629ad91ba7.png
//        //halt($res);
//        if($res){
//            //$res 就是保存到数据库的地址 直接存数据库就可以
//        }
        if($data['data']['resultCode'] == "0000" || $data['data']['payCertificate']){
            $path = $this->saveBase64($data['data']['payCertificate']);//http://pay.cxlaimeng.cn./uploads/images/673629ad91ba7.png
            //halt($res);
            if($path){
                //$res 就是保存到数据库的地址 直接存数据库就可以
                $model::where(["order_num"=>$order_num])->update(['billDownloadUrl'=>$path]);
                return AjaxReturn(1,"下载成功",['url'=>$path]);
            }else{
                return AjaxReturn(0,"下载失败");
            }
        }
    }



    public function saveBase64($base64String)
    {
        // 从请求中获取 base64 字符串


        // 检查 base64 字符串是否存在
        if (!$base64String) {
            return json(['status' => 'error', 'message' => 'Base64 string is missing']);
        }

        // 去除 base64 字符串中的前缀和空格（如果有）
        $base64String = str_replace(['data:image/png;base64,', 'data:image/jpeg;base64,', ' '], ['', '', '+'], $base64String);

        // 解码 base64 字符串
        $imageData = base64_decode($base64String);

        // 检查解码是否成功
        if ($imageData === false) {
            return json(['status' => 'error', 'message' => 'Base64 decoding failed']);
        }
        $path_time = date("Ymd",time());
        // 设置保存路径和文件名
        $savePath = './uploads/images/'.$path_time.'/';
        $fileName = uniqid() . '.png'; // 使用 uniqid 生成唯一文件名，扩展名根据图片类型设置

        // 确保保存路径存在
        if (!is_dir($savePath)) {
            mkdir($savePath, 0777, true);
        }

        // 构建完整的文件路径
        $filePath = $savePath . $fileName;

        // 将解码后的图像数据写入文件
        $result = file_put_contents($filePath, $imageData);

        // 检查文件写入是否成功
        if ($result === false) {
            return 0;
        }

        // 返回成功响应和文件路径
        return  ym().$filePath;
    }




}