<?php

namespace app\api\controller;

use app\common\model\Agent;
use app\common\model\AkOrder;
use app\common\model\CzOrder;
use app\common\model\FengfuChannel;
use app\common\model\FfOrder;
use app\common\model\FfXiafa;
use app\common\model\Order;

use app\common\model\PayChannel;
use app\common\model\Xiafa;
use app\common\service\Finance;
use think\Controller;
use think\facade\Env;
use JytPay\Client_ONEPAY\JytJsonClient;

include_once  Env::get("root_path"). "extend/ldpay/umfPayService/UmfService.class.php";
include_once  Env::get("root_path"). "extend/JytPay/JytJsonClient.php";
include_once  Env::get("root_path"). "extend/Jytfy/JytJsonClient.php";
class Notify extends Controller
{

    /**
     * 金运通支付异步回调
     * https://pay.2021621.com/api/notify/jytpaypaynotify  线上
     * http://pay1.2021621.com/api/notify/jytpaypaynotify  本地
     */
    public function jytpaypaynotify(){
        $notify_ip = request()->ip();//回调异步ip
//        //上线前开启
        $json_data = $_POST;
        db("test")->insert(["add_time"=>time(),"content"=>json_encode($json_data),"ip"=>$notify_ip,"explain"=>"道常金运通支付异步回调"]);
//        die();
//        $json = '';
//        $json_data = json_decode($json,true);

        $pay_channel_model = new PayChannel();
        //此处改造轮训通道
        $pay_channel = $pay_channel_model::where(["merchant_id"=>$json_data["merchant_id"]])->find();
        if(!$pay_channel){
            return AjaxReturn(0,"支付错误,商户异常");
        }
        $config = [
            "url"=>"https://onepay.jytpay.com/onePayService/onePay.do",
            "merchant_id"=>$pay_channel['merchant_id'],
            "cer_path"=>ROOT_PATH.$pay_channel['cer_path'],
            "pfx_path"=>ROOT_PATH.$pay_channel['pfx_path'],
            "pfx_password" => "password"
        ];
        $config = (object)$config;//数组转化成对象
        $client = new JytJsonClient();
        $client->init($config);
        // 2. 解析回调请求参数
//        $data['merchant_id']= $_POST['merchant_id'];
//        $data['msg_enc']= $_POST['msg_enc'];
//        $data['key_enc']= $_POST['key_enc'];
//        $data['sign']= $_POST['sign'];
//        $data['mer_order_id']= $_POST['mer_order_id'];

        // 3. 处理回调
       $res = $client->parserRes($json_data);
       $pay_data = json_decode($res,true);
       //halt($pay_data);
       $pay_data_array = $pay_data["body"];
       if($pay_data_array["state"] == "11"){
           $model = new Order();
           $order = $model::where(["system_order_num"=>$pay_data_array["merOrderId"]])->find();
           //halt($order);
           if(!$order){
               return AjaxReturn(0,'订单不存在');
           }
           if($order["payment"]){
               return AjaxReturn(0,'订单已支付');
           }
           //  `jytOrderId` varchar(100) DEFAULT NULL COMMENT '金运通平台订单号',
           //  `openid` varchar(100) DEFAULT NULL COMMENT '客户表示 用户标识，微信交易代表openId，支付宝交易代表buyerId',
           //  `outTransactionId` varchar(100) DEFAULT NULL COMMENT '渠道支付订单号  对应支付宝微信交易记录账单详情中的交易号',
           //  `payChannel` varchar(255) DEFAULT NULL COMMENT '//支付渠道  00：weixin 01：alipay 02：QQ 05：银联二维码',
           //  `transactionId` varchar(255) DEFAULT NULL COMMENT '渠道流水号  可用于退款和查询',
           $orderData = [
               "notify_ip"=>$notify_ip,//异步回调支付ip
               "payment" => 1,
               "pay_time" => time(),
               "jytOrderId"=>$pay_data_array['jytOrderId'],
               "openid"=>$pay_data_array['openid'],
               "outTransactionId"=>$pay_data_array['outTransactionId'],
               "payChannel"=>$pay_data_array['payChannel'],
               "transactionId"=>$pay_data_array['transactionId'],
           ];
           $model::where(["id" => $order['id']])->update($orderData);//更改订单信息
           //支付时间统计
           $agentFianceModel = new Finance();
           $agentFianceModel->agentDay($order['id']);//平台日财务
           $agent =  Agent::get($order["agent_id"]);
           $finance = new  Finance();
           $finance->jinedaypay($order['id']);//当日单笔金额支付统计
           if($order["bid"]){
               $finance->zshdaypay($order['id']);//当日子商户财务统计
               $finance->zshpay($order['id']);//子商总户财务统计
           }
           //支付回调
           $notify_data = [
               "order_num" => $order["order_num"],
               "money" => $order["money"],
               "agent_id" => $agent["id"],
           ];
           $sign=createSign($notify_data,$agent["key"]);
           $notify_data['sign'] = $sign;
           if ($order["notify_url"]) {
               //异步回调给其他平台
               request_post($order["notify_url"], $notify_data);
               $order::where('id', $order['id'])->update(["return_status" => 1, "return_time" => time()]);//修改订单状态
           }
           echo "ok";
           die();
       }else{
           return AjaxReturn(0,"支付失败");
       }

       //array(2) {
        //  ["body"] => array(14) {
        //    ["channelMerchantId"] => string(16) "2088510179289990"//商户号
        //    ["feeAmt"] => int(0)
        //    ["jytOrderId"] => string(24) "OP2307271331333709241004" //平台订单号
        //    ["merOrderId"] => string(25) "P202307271331271865361316" //商户订单号
        //    ["merchantRespCode"] => string(8) "S0000000"
        //    ["merchantRespDesc"] => string(12) "交易成功"
        //    ["openid"] => string(16) "2088202392947671" //客户表示 用户标识，微信交易代表openId，支付宝交易代表buyerId
        //    ["outTransactionId"] => string(30) "712023072722001447671439366688" //渠道支付订单号  对应支付宝微信交易记录账单详情中的交易号
        //    ["payChannel"] => string(2) "01" //支付渠道  00：weixin 01：alipay 02：QQ 05：银联二维码
        //    ["state"] => string(2) "11" // 交易状态 11：交易成功 13：交易失败
        //    ["timeEnd"] => string(14) "20230727133203" //支付完成时间
        //    ["totalAmt"] => float(0.01) //支付金额
        //    ["tradeType"] => string(9) "ONEPAY_AS" //交易类型
        //    ["transactionId"] => string(26) "20230727133133001000131281" //渠道流水号  可用于退款和查询
        //  }
        //  ["head"] => array(8) {
        //    ["merchantId"] => string(12) "290071040001"
        //    ["respCode"] => string(8) "S0000000"
        //    ["respDesc"] => string(12) "交易成功"
        //    ["tranCode"] => string(6) "OP3001"
        //    ["tranFlowid"] => string(24) "OP2307271331333709241004"
        //    ["tranTime"] => string(14) "20230727133204"
        //    ["tranType"] => string(2) "01"
        //    ["version"] => string(5) "2.0.0"
        //  }
        //}
    }

    /**
     * 金运通代付异步回调
     * https://pay.2021621.com/api/notify/jytpaydfnotify
     * http://pay1.2021621.com/api/notify/jytpaydfnotify
     */
    public function jytpaydfnotify(){
        $model = new Xiafa();
        $notify_ip = request()->ip();//回调异步ip
//        //上线前开启
        $data = $_POST;
        db("test")->insert(["add_time"=>time(),"content"=>json_encode($data),"ip"=>$notify_ip,"explain"=>"道常金运通代付异步回调"]);
//        $json = '{"msg_enc":"204d8a9cf5a114ed716fb5a9917f88d199845f851d632f184034b11c271b1be418310269078724614b8603b28195894e4c8f54cccb23320562c49a7f58f1cf7b66593f86e023c87fd15828e5c0d04d69927030f4b0f6241361641ba37ac2350f26f7580f2f43962c9ec0822bc233658d03f6501865beef6769a7988bdffa04908b5e6e66833eaec65933db5a5e9c25a68f3f26bd19e0e6836c44b6c5ecd8979e54ee52aafdc5833b53ca0034fd7ca9a8b6e191455d16774fca84a74f159678566643292abd67b85b27b65ba95c7319da3e0ffc4f1bf35cb32c44f28f8242e0fd68cb158a6cd0098dc89e8876e2ced33a9235ff3a0afac14dd233e3bc843cc888775c0efc0752f261c2b5b9f17b49dfc1fc175873d788c774d4dd1abb82995dceeb8057547c9bf2b03f39dc0046875206bba8ca435b6369a59a8c98a93c381d94564b58581ef4bf8d6ec7fc355e5f5d76665819bb5276e9d21ac257f4b92c6cb32c26403c3a31a87a975a33800c1eea3a942476c6ca01ebc23a6992c0a1e8b0cf","key_enc":"a7c2e13b65f0d64880108171234984ce06977e9a104a5708d3303727873484f641bc768e83709939df9dc8cb589f3ed99fa2b3e0a9115a50196fe0834816573487dd2f5764075a5d564dab40e3ed0fcb2f5af28cd42e79f9e4d81c77c6061bc37406fa7b3812dc554cd9836b73bb826591992feba7c2009f78487d317158c999ea65d07c9b97cd2c5c4e16ce9ec99672742e87b0d776b57ea465a9cfad984724087efda38f9740d17cdb669f57908f32ae4f577b9929bf97d658553e33fbc83ccb08bc643f9135c2bbb0a25dbcbafe256589720ccaf254c5004094d3f622d17cc87292b4425221811a0ff569dff65a8248de49d6a13e5aa29777da1d6386ccab","sign":"35de7b737ea9cde10a341b891de17e58de96ac58608aab7bc3e28abbcb0f562aa87ab2552a23a8515340f69228db70e2688cb6bc36ec2f315d314e8a1bd19584877312c33811f1e24d4eef45faf31b2b005e20d3a9b1603e24461c0f127a19aecb3cb628cf63560f8c38e0301d60f603d8e156a5c5d2c612a2119ba263b2da09f98755630811f4b75dd9bcfc46bc47f83f8c9d5078213720b1989ff524b20b7fb4738c0486e437e090ff6653ab5e0536fee73ebb7d5bc12dcb1d191f2f734607de78a8a09031609449143ecb2c56cf14e6193d8cfbeed267750c4a7664d42c5ec93fa454061c8650215d066e7ee1bea3ace9a40b85068e6d8fbfdde485d8b502","mer_order_id":"731848969795666","merchant_id":"473071040006"}';
//       $data = json_decode($json,true);
        //halt($data);
        $client = new \JytPay\DaiFu\JytJsonClient;
        $client->init();
        // 2. 解析回调请求参数
//        $data['merchant_id']= $_POST['merchant_id'];
//        $data['msg_enc']= $_POST['msg_enc'];
//        $data['key_enc']= $_POST['key_enc'];
//        $data['sign']= $_POST['sign'];
//        $data['mer_order_id']= $_POST['mer_order_id'];

        // 3. 处理回调
        $res = $client->parserRes($data);
        $pay_data = json_decode($res,true);
        //dump($pay_data);
        $pay_data_array = $pay_data["body"];
        $info = $model::where(["tranFlowid"=>$pay_data_array["oriTranFlowid"]])->find();
        //dump($info);
        if(!$info){
            return AjaxReturn(0,"代付订单不存在");
        }

        if($pay_data_array['tranState'] == "03"){
            //dump(03);
            $model::where(["tranFlowid"=>$pay_data_array["oriTranFlowid"]])->update(["payment"=>3,"pay_time"=>time(),"stu"=>$pay_data_array["tranRespDesc"]]);
        }
        if($pay_data_array['tranState'] == "01"){
            //dump(01);
            $model::where(["tranFlowid"=>$pay_data_array["oriTranFlowid"]])->update(["payment"=>1,"pay_time"=>time(),"stu"=>$pay_data_array["tranRespDesc"]]);
        }
        echo "ok";
        //错误
        //array(2) {
        //  ["body"] => array(7) {
        //    ["accountName"] => string(7) "李*号"
        //    ["accountNo"] => string(14) "620060****2227"
        //    ["oriTranFlowid"] => string(31) "4730710400062023072715534992588"
        //    ["tranAmt"] => float(5)
        //    ["tranRespCode"] => string(8) "EA000004"
        //    ["tranRespDesc"] => string(18) "账户余额不足"
        //    ["tranState"] => string(2) "03"
        //  }
        //  ["head"] => array(9) {
        //    ["merchantId"] => string(12) "473071040006"
        //    ["respCode"] => string(0) ""
        //    ["respDesc"] => string(0) ""
        //    ["tranCode"] => string(6) "TC3002"
        //    ["tranDate"] => string(8) "20230727"
        //    ["tranFlowid"] => string(31) "4730710400062023072715534992588"
        //    ["tranTime"] => string(6) "155349"
        //    ["tranType"] => string(2) "01"
        //    ["version"] => string(5) "1.0.0"
        //  }
        //}

        //正常
        //array(2) {
        //  ["body"] => array(7) {
        //    ["accountName"] => string(7) "刘*平"
        //    ["accountNo"] => string(14) "621700****7773"
        //    ["oriTranFlowid"] => string(32) "47307104000620230730143201956897" //商户代付原交易流水号
        //    ["tranAmt"] => float(5)
        //    ["tranRespCode"] => string(8) "S0000000"
        //    ["tranRespDesc"] => string(12) "交易成功"
        //    ["tranState"] => string(2) "01"//01 代付成功 03 交易失败
        //  }
        //  ["head"] => array(9) {
        //    ["merchantId"] => string(12) "473071040006"
        //    ["respCode"] => string(0) ""
        //    ["respDesc"] => string(0) ""
        //    ["tranCode"] => string(6) "TC3002"
        //    ["tranDate"] => string(8) "20230730"
        //    ["tranFlowid"] => string(32) "47307104000620230730143201956897"
        //    ["tranTime"] => string(6) "143201"
        //    ["tranType"] => string(2) "01"
        //    ["version"] => string(5) "1.0.0"
        //  }
        //}


    }

    /**
     * 金运通账户支付异步回调
     * https://pay.2021621.com/api/notify/jytpayzhpaynotify
     */
    public function jytpayzhpaynotify(){
        $notify_ip = request()->ip();//回调异步ip
        //上线前开启
        $json_data = $_POST;
        db("test")->insert(["add_time"=>time(),"content"=>json_encode($json_data),"ip"=>$notify_ip,"explain"=>"道常金运通账户支付异步回调"]);
    }


    /**
     * 接受回调响应处理
     * @param $res_array
     */
   public function accept_call_back($res_array)
    {
        global $client;

        // 1. 解密验签
        $res_message = $client->parserRes($res_array);

        // 2. 响应报文明文解析（参考文档的判断）
        echo $res_message;
    }

    /**
     * 丰付异步支付
     */
    public function sumapaynotify(){
        $notify_ip = request()->ip();//回调异步ip
        //上线前开启
        $data = $_POST;
        db("test")->insert(["add_time"=>time(),"content"=>json_encode($data),"ip"=>$notify_ip,"explain"=>"丰付异步支付"]);
        if($data['status'] != "2"){
            return AjaxReturn("订单未支付");
        }
        $model = new FfOrder();
        $order = $model::where(["system_order_num"=>$data["requestId"]])->find();
        //halt($order);
        if(!$order){
            return AjaxReturn(0,'订单不存在');
        }
        if($order["payment"]){
            return AjaxReturn(0,'订单已支付');
        }
        $pay_channel_model = new FengfuChannel();
        $pay_channel = $pay_channel_model::where(["id"=>$order["bid"]])->find();
        if (!$pay_channel){
            return AjaxReturn(0,"通道不存在");
        }
        //requestId 商户系统请求流水号
        //description 透传信息
        //payId 交易流水号
        //fiscalDate 会计日期
        //resultSignature 数字签名
        //payType 支付类型
        //bankCode 银行代码 wechatpay
        //totalPrice 订单金额
        //tradeAmount 订单实际支付金额
        //tradeFee 手续费
        //status 交易状态 2：成功
        //requestId+payId+fiscalDate+description+totalPrice+tradeAmount+tradeFee拼接
        //在一起进行签名
        $sbOld = "";
        $sbOld = $sbOld . $data['requestId'];
        $sbOld = $sbOld . $data['payId'];
        $sbOld = $sbOld . $data['fiscalDate'];
        $sbOld = $sbOld . $data['description'];
        $sbOld = $sbOld . $data['totalPrice'];
        $sbOld = $sbOld . $data['tradeAmount'];
        $sbOld = $sbOld . $data['tradeFee'];

        $merKey =$pay_channel["key"];//密钥
        $signatrue = HmacMd5($sbOld, $merKey);//数字签名
        if($signatrue == $data['resultSignature']){
            //halt("签名正确");
            //  `jytOrderId` varchar(100) DEFAULT NULL COMMENT '金运通平台订单号',
            //  `openid` varchar(100) DEFAULT NULL COMMENT '客户表示 用户标识，微信交易代表openId，支付宝交易代表buyerId',
            //  `outTransactionId` varchar(100) DEFAULT NULL COMMENT '渠道支付订单号  对应支付宝微信交易记录账单详情中的交易号',
            //  `payChannel` varchar(255) DEFAULT NULL COMMENT '//支付渠道  00：weixin 01：alipay 02：QQ 05：银联二维码',
            //  `transactionId` varchar(255) DEFAULT NULL COMMENT '渠道流水号  可用于退款和查询',

            $orderData = [
                "notify_ip"=>$notify_ip,//异步回调支付ip
                "payment" => 1,
                "pay_time" => time(),
                "payId"=>$data['payId'],
                "openId"=>$data['openId'],
                "bankCode"=>$data['bankCode'],
                "tradeFee"=>$data['tradeFee'],
                "description"=>$data["description"]
            ];
            $model::where(["id" => $order['id']])->update($orderData);//更改订单信息
            //支付时间统计
            $agentFianceModel = new Finance();
            $agentFianceModel->agentDayff($order['id']);//平台日财务
            $agent =  Agent::get($order["agent_id"]);


            //支付回调
            $notify_data = [
                "order_num" => $order["order_num"],
                "money" => $order["money"],
                "agent_id" => $agent["id"],
            ];
            $sign=createSign($notify_data,$agent["key"]);
            $notify_data['sign'] = $sign;
            if ($order["notify_url"]) {
                //异步回调给其他平台
                request_post($order["notify_url"], $notify_data);
                $order::where('id', $order['id'])->update(["return_status" => 1, "return_time" => time()]);//修改订单状态
            }

            echo "success";
            die();
        }
    }

    /**
     * 丰付异步支付测试
     * https://pay.dinglianshop.cn/api/notify/ffftest
     * http://p.dinglianshop.cn/api/notify/ffftest
     */
    public function ffftest(){
        $notify_ip = request()->ip();//回调异步ip
       $json = '{"requestId":"430842794830438","description":"430867670482245","payId":"240430608677523484","fiscalDate":"20240430","payType":"20","bankCode":"wechatpay","totalPrice":"9.98","tradeAmount":"9.98","tradeFee":"0.08","status":"2","endTime":"2024-04-30 22:19:51","couponFee":"0.00","feeType":"CNY","openId":"oi7qz6wtetcWXO_aAs2UGygbMRSM","resultSignature":"4e2e44b4fa532f474358426c2b2a04e9"}';
       $data = json_decode($json,true);
       if($data['status'] != "2"){
            return AjaxReturn("订单未支付");
       }
        $model = new FfOrder();
        $order = $model::where(["system_order_num"=>$data["requestId"]])->find();
        //halt($order);
        if(!$order){
            return AjaxReturn(0,'订单不存在');
        }
        if($order["payment"]){
            return AjaxReturn(0,'订单已支付');
        }
        $pay_channel_model = new FengfuChannel();
        $pay_channel = $pay_channel_model::where(["id"=>$order["bid"]])->find();
        if (!$pay_channel){
            return AjaxReturn(0,"通道不存在");
        }
       //requestId 商户系统请求流水号
        //description 透传信息
        //payId 交易流水号
        //fiscalDate 会计日期
        //resultSignature 数字签名
        //payType 支付类型
        //bankCode 银行代码 wechatpay
        //totalPrice 订单金额
        //tradeAmount 订单实际支付金额
        //tradeFee 手续费
        //status 交易状态 2：成功
        //requestId+payId+fiscalDate+description+totalPrice+tradeAmount+tradeFee拼接
        //在一起进行签名
        $sbOld = "";
        $sbOld = $sbOld . $data['requestId'];
        $sbOld = $sbOld . $data['payId'];
        $sbOld = $sbOld . $data['fiscalDate'];
        $sbOld = $sbOld . $data['description'];
        $sbOld = $sbOld . $data['totalPrice'];
        $sbOld = $sbOld . $data['tradeAmount'];
        $sbOld = $sbOld . $data['tradeFee'];

        $merKey =$pay_channel["key"];//密钥
        $signatrue = HmacMd5($sbOld, $merKey);//数字签名
        if($signatrue == $data['resultSignature']){
            //halt("签名正确");
            //  `jytOrderId` varchar(100) DEFAULT NULL COMMENT '金运通平台订单号',
            //  `openid` varchar(100) DEFAULT NULL COMMENT '客户表示 用户标识，微信交易代表openId，支付宝交易代表buyerId',
            //  `outTransactionId` varchar(100) DEFAULT NULL COMMENT '渠道支付订单号  对应支付宝微信交易记录账单详情中的交易号',
            //  `payChannel` varchar(255) DEFAULT NULL COMMENT '//支付渠道  00：weixin 01：alipay 02：QQ 05：银联二维码',
            //  `transactionId` varchar(255) DEFAULT NULL COMMENT '渠道流水号  可用于退款和查询',

            $orderData = [
                "notify_ip"=>$notify_ip,//异步回调支付ip
                "payment" => 1,
                "pay_time" => time(),
                "payId"=>$data['payId'],
                "openId"=>$data['openId'],
                "bankCode"=>$data['bankCode'],
                "tradeFee"=>$data['tradeFee'],
                "description"=>$data["description"]
            ];
            $model::where(["id" => $order['id']])->update($orderData);//更改订单信息
            //支付时间统计
            $agentFianceModel = new Finance();
            $agentFianceModel->agentDayff($order['id']);//平台日财务
            $agent =  Agent::get($order["agent_id"]);


            //支付回调
            $notify_data = [
                "order_num" => $order["order_num"],
                "money" => $order["money"],
                "agent_id" => $agent["id"],
            ];
            $sign=createSign($notify_data,$agent["key"]);
            $notify_data['sign'] = $sign;
            if ($order["notify_url"]) {
                //异步回调给其他平台
                request_post($order["notify_url"], $notify_data);
                $order::where('id', $order['id'])->update(["return_status" => 1, "return_time" => time()]);//修改订单状态
            }

            echo "success";
            die();
        }
    }

    /**
     * 丰付代付异步回调
     */
    public function sumapaydaifunotify(){
        $notify_ip = request()->ip();//回调异步ip
        //上线前开启
        $data =file_get_contents('php://input');
        db("test")->insert(["add_time"=>time(),"content"=>json_encode($data),"ip"=>$notify_ip,"explain"=>"丰付代付异步通知参数"]);
        parse_str($data, $output);
        $arr =$output;
        $pay_channel_model = new FengfuChannel();
        $pay_channel = $pay_channel_model::where(["id"=>11])->find();
        if (!$pay_channel){
            return AjaxReturn(0,"通道不存在");
        }

        //requestId+result+sum
        $sbOld = "";
        $sbOld = $sbOld . $arr['requestId'];
        $sbOld = $sbOld . $arr['result'];
        $sbOld = $sbOld . $arr['sum'];


        $merKey =$pay_channel["key"];//密钥
        $signatrue = HmacMd5($sbOld, $merKey);//数字签名
        if($signatrue == $arr['signature']){
            if ($arr["result"] != "00000"){
                return "不是付款状态";
            }
//            array(16) {
//                ["requestId"] => string(15) "502294801641027"
//                ["result"] => string(5) "00000"
//                ["sum"] => string(4) "1.29"
//                ["totalSum"] => string(0) ""
//                ["balance"] => string(0) ""
//                ["canUsedBalance"] => string(0) ""
//                ["frozenBalance"] => string(0) ""
//                ["unsettledBalance"] => string(0) ""
//                ["payType"] => string(0) ""
//                ["bankAccount"] => string(16) "6226********8070"
//                ["bankName"] => string(0) ""
//                ["name"] => string(3) "��*"
//                ["requestTime"] => string(14) "20240502135816"
//                ["dealTime"] => string(14) "20240502135820"
//                ["noticeType"] => string(1) "1"
//                ["signature"] => string(32) "1688b32cc9252c7c462b50f06a33183b"
            $model = new FfXiafa();
            $info = $model::where(["order_num"=>$arr["requestId"]])->find();
            if(!$info){
                return "订单不存在";
            }
            if($info["payment"] == 1){
                return "订单已经付款";
            }
            $model::where(["order_num"=>$arr["requestId"]])->update(["payment"=>1,"pay_time"=>time(),"notify_ip"=>$notify_ip]);
            echo "success";
            die();
        }else{
            echo "error";
            die();
        }
        //丰付系统付款处理完成后，
        //会调用该地址异步通知商
        //户，当商户返回 success
        //（大小写不敏感）时，支付
        //系统认为异步通知发送成
        //功。返回参数：
        //请求流水号：requestId
        //请求结果：result(00000
        //付款成功)
        //付款金额：sum
        //银行账户：bankAccount（收
        //款账户为银行账户时为必
        //输项，为丰付账户时为选
        //输）
        //收款人姓名：name
        //请求时间：requestTime
        //处理时间：dealTime
        //通知类型：noticeType
        //失败原因：failReason
        //数字签名：signature
        //签名规则：
        //requestId+result+sum
    }

    /**
     * 测试代付
     * https://pay.dinglianshop.cn/api/notify/ccdaifu
     * http://p.dinglianshop.cn/api/notify/ccdaifu
     */
    public function ccdaifu(){
        $notify_ip = request()->ip();//回调异步ip
        $json ='"requestId=502294801641027&result=00000&sum=1.29&totalSum=&balance=&canUsedBalance=&frozenBalance=&unsettledBalance=&payType=&bankAccount=6226********8070&bankName=&name=%C1%F5*&requestTime=20240502135816&dealTime=20240502135820&noticeType=1&signature=1688b32cc9252c7c462b50f06a33183b"';
        $data = json_decode($json,true);
        //halt($data);
        parse_str($data, $output);
        $arr =$output;
        $pay_channel_model = new FengfuChannel();
        $pay_channel = $pay_channel_model::where(["id"=>11])->find();
        if (!$pay_channel){
            return AjaxReturn(0,"通道不存在");
        }

        //requestId+result+sum
        $sbOld = "";
        $sbOld = $sbOld . $arr['requestId'];
        $sbOld = $sbOld . $arr['result'];
        $sbOld = $sbOld . $arr['sum'];


        $merKey =$pay_channel["key"];//密钥
        $signatrue = HmacMd5($sbOld, $merKey);//数字签名
        if($signatrue == $arr['signature']){
            if ($arr["result"] != "00000"){
                return "不是付款状态";
            }
//            array(16) {
//                ["requestId"] => string(15) "502294801641027"
//                ["result"] => string(5) "00000"
//                ["sum"] => string(4) "1.29"
//                ["totalSum"] => string(0) ""
//                ["balance"] => string(0) ""
//                ["canUsedBalance"] => string(0) ""
//                ["frozenBalance"] => string(0) ""
//                ["unsettledBalance"] => string(0) ""
//                ["payType"] => string(0) ""
//                ["bankAccount"] => string(16) "6226********8070"
//                ["bankName"] => string(0) ""
//                ["name"] => string(3) "��*"
//                ["requestTime"] => string(14) "20240502135816"
//                ["dealTime"] => string(14) "20240502135820"
//                ["noticeType"] => string(1) "1"
//                ["signature"] => string(32) "1688b32cc9252c7c462b50f06a33183b"
            $model = new FfXiafa();
            $info = $model::where(["order_num"=>$arr["requestId"]])->find();
            if(!$info){
                return "订单不存在";
            }
            if($info["payment"] == 1){
                return "订单已经付款";
            }
            $model::where(["order_num"=>$arr["requestId"]])->update(["payment"=>1,"pay_time"=>time(),"notify_ip"=>$notify_ip]);
            echo "success";
            die();
        }else{
            echo "error";
            die();
        }
    }

    /**
     * 丰付退款
     */
    public function fengfutuikuan(){
        $notify_ip = request()->ip();//回调异步ip
        //上线前开启
        $data =$_POST;
        db("test")->insert(["add_time"=>time(),"content"=>json_encode($data),"ip"=>$notify_ip,"explain"=>"丰付退款异步通知参数"]);
        echo "success";
        die();
    }

    /**
     *支付宝异步回调
     * https://pay.dinglianshop.cn/api/notify/alipaynotify
     */

    public function alipaynotify()
    {
        echo "success";
        die();
    }

    public function zhifutongpay()
    {
        $notify_ip = request()->ip();//回调异步ip
        //上线前开启
        $data =$_POST;
        db("test")->insert(["add_time"=>time(),"content"=>json_encode($data),"ip"=>$notify_ip,"explain"=>"鼎联直付通异步"]);

        $sign = $data['sign'];
        unset($data['sign']);
        $datasign = $this->makeSign($data);
        if($sign == $datasign){
            //异步参数处理
            $model = new AkOrder();
            $order = $model::where(["system_order_num"=>$data['mchOrderNo']])->find();
            if(!$order){
                return AjaxReturn(0,'订单不存在');
            }
            if($order["payment"]){
                return AjaxReturn(0,'订单已支付');
            }


            //修改订单信息
            $orderData = [
                "notify_ip"=>$notify_ip,//异步回调支付ip
                "payment" => 1,  //支付
                "pay_time" => time(),
                "trade_no" => $data['payOrderId'],//第三方商户订单号
                "nonceStr" => $data['nonceStr']
            ];
            //第三方商户订单号 third_order_no
            //平台订单号  transaction_id
            $model::where(["id" => $order['id']])->update($orderData);//更改订单信息
            //支付时间统计
            $agentFianceModel = new Finance();
            $agentFianceModel->agentDayak($order['id']);//商户日财务
            $agent =  Agent::get($order["agent_id"]);
            //支付回调
            $notify_data = [
                "order_num" => $order["order_num"],
                "money" => $order["money"],
                "agent_id" => $agent["id"],
            ];
            $sign=createSign($notify_data,$agent["key"]);
            $notify_data['sign'] = $sign;
            if ($order["notify_url"]) {
                //异步回调给其他平台
                $this->send_post($order["notify_url"],$notify_data);
                AkOrder::where('id',$order['id'])->update(["return_status"=>1,"return_time"=>time()]);//修改订单状态
            }
            return "success";
            exit();

            echo "success";
            die();
        }else{
            return AjaxReturn(0,"签名错误");
        }


    }

    public function  send_post($url, $post_data) {
        $postdata = http_build_query($post_data);
        $options = array(
            'http' => array(
                'method' => 'POST',
                'header' => 'Content-type:application/x-www-form-urlencoded',
                'content' => $postdata,
                'timeout' => 15 * 60 // 超时时间（单位:s）
            )
        );
        $context = stream_context_create($options);
        $result = file_get_contents($url, false, $context);
        return $result;
    }

    /**
     * http://p.dinglianshop.cn/api/notify/zhifutongpaytest
     */
    public function zhifutongpaytest()
    {

        $json = '{"respDesc":"\u6210\u529f","amount":"1.02","payOrderId":"20240905192549746114835784","mchOrderNo":"jl905355494638004","wayCode":"ALIPAY_H5","sign":"a917e1cec47a1efbbbff837a0836b030","reqTime":"2024-09-05 19:25:49","respCode":"0000","nonceStr":"13485362929143497"}';
        $data = json_decode($json,true);
        dump($data);
        $sign = $data['sign'];
        unset($data['sign']);
        $datasign = $this->makeSign($data);
        if($sign == $datasign){

        }else{
            return AjaxReturn(0,"签名错误");
        }
    }
    /**
     * 生成签名
     * @param array $values
     * @return string 本函数不覆盖sign成员变量，如要设置签名需要调用SetSign方法赋值
     */
    private function makeSign(array $values)
    {
        //签名步骤一：按字典序排序参数
        ksort($values,SORT_STRING);

        $string = $this->toUrlParams($values);
        //签名步骤二：在string后加入KEY
        $string = $string .'cefff6e1a56c4dc38ee083777edf7edf';
        //throwError($string);

        //签名步骤三：MD5加密
        $string = md5($string);
        //签名步骤四：所有字符转为大写
        return $string;
    }

    /**
     * 格式化参数格式化成url参数
     * @param array $values
     * @return string
     */
    private function toUrlParams(array $values)
    {
        $buff = '';
        foreach ($values as $k => $v) {
            if ($k != 'sign' && $v != '' && !is_array($v)) {
                $buff .= $k . '=' . $v . '&';
            }
        }
        return trim($buff, '&');
    }

    /**
     * 大拿支付宝异步回调
     */
    public function alipaypaymentnotify(){
        $notify_ip = request()->ip();//回调异步ip
        //上线前开启
        $data =$_POST;
        db("test")->insert(["add_time"=>time(),"content"=>json_encode($data),"ip"=>$notify_ip,"explain"=>"大拿支付宝异步回调"]);


        //$sign = $data['sign'];
        //        unset($data['sign']);
        //        $datasign = $this->makeSign($data);
        //        if($sign == $datasign){
        //            //异步参数处理
        //            $model = new AkOrder();
        //            $order = $model::where(["system_order_num"=>$data['mchOrderNo']])->find();
        //            if(!$order){
        //                return AjaxReturn(0,'订单不存在');
        //            }
        //            if($order["payment"]){
        //                return AjaxReturn(0,'订单已支付');
        //            }
        //
        //
        //            //修改订单信息
        //            $orderData = [
        //                "notify_ip"=>$notify_ip,//异步回调支付ip
        //                "payment" => 1,  //支付
        //                "pay_time" => time(),
        //                "trade_no" => $data['payOrderId'],//第三方商户订单号
        //                "nonceStr" => $data['nonceStr']
        //            ];
        //            //第三方商户订单号 third_order_no
        //            //平台订单号  transaction_id
        //            $model::where(["id" => $order['id']])->update($orderData);//更改订单信息
        //            //支付时间统计
        //            $agentFianceModel = new Finance();
        //            $agentFianceModel->agentDayak($order['id']);//商户日财务
        //            $agent =  Agent::get($order["agent_id"]);
        //            //支付回调
        //            $notify_data = [
        //                "order_num" => $order["order_num"],
        //                "money" => $order["money"],
        //                "agent_id" => $agent["id"],
        //            ];
        //            $sign=createSign($notify_data,$agent["key"]);
        //            $notify_data['sign'] = $sign;
        //            if ($order["notify_url"]) {
        //                //异步回调给其他平台
        //                $this->send_post($order["notify_url"],$notify_data);
        //                AkOrder::where('id',$order['id'])->update(["return_status"=>1,"return_time"=>time()]);//修改订单状态
        //            }
        //            return "success";
        //            exit();
        //
        //            echo "success";
        //            die();
        //        }else{
        //            return AjaxReturn(0,"签名错误");
        //        }
    }


}