<?php

namespace app\api\controller;

use app\common\model\CzOrder;
use app\common\model\Order;
use app\common\service\SdCommon;
use think\Controller;
use think\facade\Env;
include_once  Env::get("root_path"). "extend/ldpay/umfPayService/UmfService.class.php";
class Pay extends Controller
{
    /**
     * 支付宝h5异步回调
     * http://ldys1.hnyouke888.cn:7888/api/pay/index
     */
    public function index(){

        $model = new Order();
        $id = input("get.id");
        $order = $model::where("id",$id)->find();
        if(!$order){
            return AjaxReturn(0,"订单不存在");
        }
        $order_time = strtotime($order['create_time']) + 1200;
        if($order_time < time()){
            return AjaxReturn(0,"订单已过期");
        }
        if($order["payment"] > 0){
            return AjaxReturn(0,"订单已支付");
        }
        $money  = strval($order["money"]*100);
        $params = array(
            "mer_id"=>"53941",
            "ret_url"=>ym()."/api/pay/returl",
            "notify_url"=>ym()."/api/notify/lfbD77KI0IRLIe3k8v62HCQGoEOowiR5deKOTQ8i038Fwj5295VaAOGxM",//异步地址
            "goods_id"=>rand(100,999999),
            "goods_inf"=>"充值".$id.'-'.time(),
            "order_id"=>$order["system_order_num"],
            "mer_date"=>date ( "Ymd" ),
            "amount"=>$money,
            "user_ip"=>request()->ip(),
            "scancode_type"=>"ALIPAY",
            "expand"=>"",
            "mer_flag"=>"KMER",
            "expire_time"=>"",
            "consumer_id"=>MD5(orderNum()),
            "mer_service_id"=>""
        );
        $path =  Env::get("root_path")."extend/cert/53941_.key.pem";
        $service = new \UmfService("53941",$path);
        $res = $service->H5AutoConnectMap($params);
        header("Location: {$res}");
        //确保重定向后，后续代码不会被执行
        exit;
    }


    /**
     * 土豆支付
     * @return void
     */
    public function tdpay(){
        $model = new CzOrder();
        //判断订单状态 跳转到支付宝
        $id = input("get.id");
        if(!$id){
            return AjaxReturn(0,"参数错误");
        }
        $order = $model::get($id);
        if(!$order){
            return AjaxReturn(0,"订单不存在");
        }
        if($order["payment"] == 1){
            return AjaxReturn(0,"订单已支付");
        }
        $money  = strval($order["money"]*100);
        //halt($money);
        $params = array(
            "mer_id"=>"53941",
            "ret_url"=>ym()."/api/pay/returl",
            "notify_url"=>ym()."/api/pay/notify",
            "goods_id"=>rand(100,999999),
            "goods_inf"=>"充值",
            "order_id"=>orderNum(),
            "mer_date"=>date ( "Ymd" ),
            "amount"=>$money,
            "user_ip"=>getIp(),
            "scancode_type"=>"ALIPAY",
            "expand"=>"",
            "mer_flag"=>"KMER",
            "expire_time"=>"",
            "consumer_id"=>MD5(orderNum()),
            "mer_service_id"=>""
        );
        $path =  Env::get("root_path")."extend/cert/53941_.key.pem";

        $service = new \UmfService("53941",$path);

        $res = $service->H5AutoConnectMap($params);
        //halt($res);
        header("Location: {$res}");
        exit;

    }




    //支付宝扫码支付
    //http://ldys1.hnyouke888.cn:7888/api/pay/saoma
//    public function saoma(){
//
//        $path =  Env::get("root_path")."extend/cert/53941_.key.pem";
//        $params = array(
//            "mer_id"=>"53941",
//            "ret_url"=>ym()."/api/pay/returl",
//            "notify_url"=>ym()."/api/pay/notify",
//            "goods_id"=>rand(100000,999999),
//            "goods_inf"=>"会员充值",
//            "order_id"=>orderNum(),
//            "mer_date"=>date ( "Ymd" ),
//            "amount"=>"999",
//            "scancode_type"=>"ALIPAY",
//            "mer_priv"=>"",
//            "expand"=>"",
//            "user_ip"=>"122.6.137.242",
//            "expire_time"=>"",
//            "mer_flag"=>"KMER",
//            "consumer_id"=>MD5(orderNum()),
//            "mer_service_id"=>"20221208155101749"
//        );
//        $service = new \UmfService("53941",$path);
//        $res = $service->activeNewScanPaymentMap($params);
//        //dump($res);
//        echo base64_decode($res["bank_payurl"]);
//    }


    /**
     * 扫码支付
     * https://ldys.lndayun.xyz//api/pay/smpay?id=25212
     * http://ldys1.yapin.shop/api/pay/smpay?id=25212
     */
    public function  smpay(){
        $model = new Order();
        $id = input("get.id");
        $order = $model::where("id",$id)->find();
        if(!$order){
            return AjaxReturn(0,"订单不存在");
        }
        $order_time = strtotime($order['create_time']) + 1200;
        if($order_time < time()){
            return AjaxReturn(0,"订单已过期");
        }
        if($order["payment"] > 0){
            return AjaxReturn(0,"订单已支付");
        }


        $ip = request()->ip();

        $model->where("id",$id)->update(["buy_ip"=>$ip]);//更新付款用户下单ip
        $money  = strval($order["money"]*100);

        $params = array(
            "mer_id"=>"53941",
            "ret_url"=>ym()."/api/pay/returl",
            "notify_url"=>ym()."/api/notify/lfbD77KI0IRLIe3k8v62HCQGoEOowiR5deKOTQ8i038Fwj5295VaAOGxM",
            "goods_id"=>rand(100000,999999),
            "goods_inf"=>"充值".$id.'-'.time(),
            "order_id"=>$order["system_order_num"],
            "mer_date"=>date ( "Ymd" ),
            "amount"=>$money,
            "scancode_type"=>"ALIPAY",
            "mer_priv"=>"",
            "expand"=>"",
            "user_ip"=>$ip,
            "expire_time"=>"",
            "mer_flag"=>"KMER",
            "consumer_id"=>MD5(orderNum()),
            //"mer_service_id"=>"20221208155101749"
        );
        $path =  Env::get("root_path")."extend/cert/53941_.key.pem";
        $service = new \UmfService("53941",$path);
        $res = $service->activeNewScanPaymentMap($params);



        if($res["bank_payurl"]){
            $url =  base64_decode($res["bank_payurl"]);
            header("Location: {$url}");
            //确保重定向后，后续代码不会被执行
            exit;
        }else{
            echo $res["ret_msg"];
        }
        //array(9) {
        //  ["mer_id"] => string(5) "53941"
        //  ["token"] => NULL
        //  ["order_id"] => string(17) "LD303352905494622"
        //  ["mer_date"] => string(8) "20230303"
        //  ["trade_state"] => string(10) "TRADE_FAIL"
        //  ["trade_no"] => string(16) "3303031721416355"
        //  ["ret_code"] => string(8) "00253055"
        //  ["ret_msg"] => string(50) "某商户产品单笔限额超限,PSP173119b3af0be"
        //  ["bank_payurl"] => string(0) ""
        //}

    }

    /**
     *
     * 微信支付
     * https://dld.yapin.shop/api/pay/wechat
     */
    public function wechat(){
        //php代码示例

        $params = array(
            "mer_id"=>"53941",
            "ret_url"=>ym()."/api/pay/returl",
            "notify_url"=>ym()."/api/notify/lfbD77KI0IRLIe3k8v62HCQGoEOowiR5deKOTQ8i038Fwj5295VaAOGxM",
            "goods_id"=>"",
            "goods_inf"=>"商品描述",
            "order_id"=>rand(0,999999999),
            "mer_date"=>date ( "Ymd" ),
            "amount"=>"1",
            "scancode_type"=>"WECHAT",
            "mer_priv"=>"",
            "expand"=>"",
            "user_ip"=>"",
            "expire_time"=>"",
        );
        $path =  Env::get("root_path")."extend/cert/53941_.key.pem";
        $service = new \UmfService("53941",$path);
        $res = $service->activeNewScanPaymentMap($params);
        //var_dump($res);

        if($res["bank_payurl"]){
            $url =  base64_decode($res["bank_payurl"]);
            header("Location: {$url}");
            //确保重定向后，后续代码不会被执行
            exit;
        }else{
            echo $res["ret_msg"];
        }
    }



}