<?php

namespace app\api\controller;

use app\common\model\Order;
use app\common\model\PayChannel;
use JytPay\Client_ONEPAY\JytJsonClient;
use think\Controller;
use think\facade\Env;
use think\facade\Request;
include_once  Env::get("root_path"). "extend/JytPay/JytJsonClient.php";
class Jytpay extends Controller
{
    /**
     * app微信支付
     */
    public function apppaywechatindex(){
        $model = new Order();
        $id = input("get.id");
        $order = $model::where("id",$id)->find();
        if(!$order){
            return AjaxReturn(0,"订单不存在");
        }
//        $order_time = strtotime($order['create_time']) + 1200;
//        if($order_time < time()){
//            return AjaxReturn(0,"订单已过期");
//        }
        if($order["payment"] > 0){
            return AjaxReturn(0,"订单已支付");
        }
        $ip = request()->ip();
        //$ip = "113.122.236.71";

        //增加用户付款的ip信息
        db("order_ips")->insert(["order_id"=>$id,"ip"=>$ip,"create_time"=>time()]);
        $model->where("id",$id)->update(["buy_ip"=>$ip]);//更新付款用户下单ip
        return view("",["order"=>$order]);
    }
    /**
     * app支付宝支付首页
     */
    public function apppayalipayindex(){
        $model = new Order();
        $id = input("get.id");
        $order = $model::where("id",$id)->find();
        if(!$order){
            return AjaxReturn(0,"订单不存在");
        }
        if($order["payment"] > 0){
            return AjaxReturn(0,"订单已支付");
        }
        $ip = request()->ip();


        //增加用户付款的ip信息
        db("order_ips")->insert(["order_id"=>$id,"ip"=>$ip,"create_time"=>time()]);
        $model->where("id",$id)->update(["buy_ip"=>$ip]);//更新付款用户下单ip
        return view("",["order"=>$order]);
    }
    /**
     * app支付宝支付拉单
     */
    public function apppayalipay(){
        if(Request::isPost()){
            $model = new Order();
            $id = input("post.id");
            //halt($id);
            $order = $model::where("id",$id)->find();
            if(!$order){
                return AjaxReturn(0,"订单不存在");
            }
//            $order_time = strtotime($order['create_time']) + 1200;
//            if($order_time < time()){
//                return AjaxReturn(0,"订单已过期");
//            }
            if($order["payment"] > 0){
                return AjaxReturn(0,"订单已支付");
            }

            $ip = request()->ip();
            //增加用户付款的ip信息
            db("order_ips")->insert(["order_id"=>$id,"ip"=>$ip,"create_time"=>time()]);
            $model->where("id",$id)->update(["buy_ip"=>$ip]);//更新付款用户下单ip
            //查询可用通道
            $pay_channel_model = new PayChannel();
            //此处改造轮训通道
            $pay_channel = $pay_channel_model::where(["open"=>1,"weixin"=>1,"id"=>$order["pay_channel_id"]])->find();
            if(!$pay_channel){
                return AjaxReturn(0,"支付错误,商户异常");
            }

            $config = [
                "url"=>"https://onepay.jytpay.com/onePayService/onePay.do",
                "merchant_id"=>$pay_channel['merchant_id'],
                "cer_path"=>ROOT_PATH.$pay_channel['cer_path'],
                "pfx_path"=>ROOT_PATH.$pay_channel['pfx_path'],
                "pfx_password" => "password"
            ];
            $config = (object)$config;//数组转化成对象
            //halt($config);
            $client = new JytJsonClient;
            //dump(111);
            $client->init($config);
            $data['head']['version']='2.0.0';
            $data['head']['tranType']='01';
            $data['head']['merchantId']= $client->config->merchant_id;
            $data['head']['tranTime']=date('YmdHis',time());
            $data['head']['tranFlowid']= $client->config->merchant_id . date('YmdHis',time()) . substr(rand(),4);
            $data['head']['tranCode']= 'OP1001';
            $data['head']['respCode']='';
            $data['head']['respCesc']='';
            // 报文体
            $data['body']['payChannel'] = "01";//00微信 01支付宝
            $data['body']['payMode'] = "00";//00：主动扫码 02：JS支付  06：小程序 05：H5支付  07：云闪付控件支付
            $data['body']['orderId'] = $order['system_order_num'];
            $data['body']['totalAmt'] = $order['pay_money'];
            $data['body']['subject'] = "商城购物";//todo 上线后更换
            $data['body']['notifyUrl'] = ym().'/api/notify/jytpaypaynotify';
            $data['body']['body'] = "商城购物";//todo 上线后更换
            $data['body']['spbillCreatIp'] = $ip;

            //$data['body']['billMerchantId'] = $order['billMerchantId'];//todo 子商户
            // if (!empty($_POST['splitFlag']) && !empty($_POST['splitAmt'])) {
            $data['body']['splitAmt'] ="";
            $data['body']['splitFlag'] = "";
            //}
            //halt(222);
            $res = $client->sendReq($data);
            //halt($res);
            $data = json_decode($res,true);
            //halt($data);
            //array(2) {
            //  ["head"] => array(8) {
            //    ["merchantId"] => string(12) "473071040007"
            //    ["respCode"] => string(8) "S0000000"
            //    ["respDesc"] => string(18) "交易受理成功"
            //    ["tranCode"] => string(6) "OP1001"
            //    ["tranFlowid"] => string(32) "47307104000720230911133530572483"
            //    ["tranTime"] => string(14) "20230911133530"
            //    ["tranType"] => string(2) "02"
            //    ["version"] => string(5) "2.0.0"
            //  }
            //  ["body"] => array(6) {
            //    ["codeImgUrl"] => string(105) "https://onepay.jytpay.com/onePayService/getcode/qr.do?uuid=https://qr.alipay.com/bax040578h8xkpvpusxs2556"
            //    ["codeUrl"] => string(46) "https://qr.alipay.com/bax040578h8xkpvpusxs2556"
            //    ["jytOrderId"] => string(24) "OP2309111335316820835921"
            //    ["orderNo"] => string(15) "911104776088594"
            //    ["tranState"] => string(2) "12"
            //    ["urlScheme"] => string(46) "https://qr.alipay.com/bax040578h8xkpvpusxs2556"
            //  }
            //}
            //halt($data);
            if (!$data['body']){
                return AjaxReturn(0,"支付错误,请稍后");
            }
            if($data['body']['tranState'] == "13"){
                return AjaxReturn(0,$data['head']['respDesc']);
            }
            if($data['body']['tranState'] ){
                $model::where(["id"=>$order["id"]])->update(["codeUrl"=>$data['body']['codeUrl']]);
                return AjaxReturn(1,"交易受理成功",["url"=>$data['body']['codeUrl']]);
            }
        }
    }

    /**
     * app微信支付拉单
     */
    public function appwechatapay(){
        if(Request::isPost()){
            $model = new Order();
            $id = input("post.id");
            //halt($id);
            $order = $model::where("id",$id)->find();
            if(!$order){
                return AjaxReturn(0,"订单不存在");
            }
//            $order_time = strtotime($order['create_time']) + 1200;
//            if($order_time < time()){
//                return AjaxReturn(0,"订单已过期");
//            }
            if($order["payment"] > 0){
                return AjaxReturn(0,"订单已支付");
            }
            if($order["type"] !=2){
                return AjaxReturn(0,"订单不是微信支付");
            }
            $ip = request()->ip();
            //增加用户付款的ip信息
            db("order_ips")->insert(["order_id"=>$id,"ip"=>$ip,"create_time"=>time()]);
            $model->where("id",$id)->update(["buy_ip"=>$ip]);//更新付款用户下单ip
            //查询可用通道
            $pay_channel_model = new PayChannel();
            //此处改造轮训通道
            $pay_channel = $pay_channel_model::where(["open"=>1,"weixin"=>1,"id"=>$order["pay_channel_id"]])->find();
            if(!$pay_channel){
                return AjaxReturn(0,"支付错误,商户异常");
            }

            $config = [
                "url"=>"https://onepay.jytpay.com/onePayService/onePay.do",
                "merchant_id"=>$pay_channel['merchant_id'],
                "cer_path"=>ROOT_PATH.$pay_channel['cer_path'],
                "pfx_path"=>ROOT_PATH.$pay_channel['pfx_path'],
                "pfx_password" => "password"
            ];
            $config = (object)$config;//数组转化成对象
            //halt($config);
            $client = new JytJsonClient;
            //dump(111);
            $client->init($config);
            $data['head']['version']='2.0.0';
            $data['head']['tranType']='01';
            $data['head']['merchantId']= $client->config->merchant_id;
            $data['head']['tranTime']=date('YmdHis',time());
            $data['head']['tranFlowid']= $client->config->merchant_id . date('YmdHis',time()) . substr(rand(),4);
            $data['head']['tranCode']= 'OP1001';
            $data['head']['respCode']='';
            $data['head']['respCesc']='';
            // 报文体
            $data['body']['payChannel'] = "00";//00微信 01支付宝
            $data['body']['payMode'] = "00";//00：主动扫码 02：JS支付  06：小程序 05：H5支付  07：云闪付控件支付
            $data['body']['orderId'] = $order['system_order_num'];
            $data['body']['totalAmt'] = $order['pay_money'];
            $data['body']['subject'] = "商城购物";//todo 上线后更换
            $data['body']['notifyUrl'] = ym().'/api/notify/jytpaypaynotify';
            $data['body']['body'] = "商城购物";//todo 上线后更换
            $data['body']['spbillCreatIp'] = $ip;

            $data['body']['billMerchantId'] = $order['billMerchantId'];//todo 子商户
            // if (!empty($_POST['splitFlag']) && !empty($_POST['splitAmt'])) {
            $data['body']['splitAmt'] ="";
            $data['body']['splitFlag'] = "";
            //}
            //halt(222);
            $res = $client->sendReq($data);
            //halt($res);
            $data = json_decode($res,true);
            //halt($data);
            //array(2) {
            //  ["head"] => array(8) {
            //    ["merchantId"] => string(12) "473071040007"
            //    ["respCode"] => string(8) "S0000000"
            //    ["respDesc"] => string(12) "处理成功"
            //    ["tranCode"] => string(6) "OP1001"
            //    ["tranFlowid"] => string(30) "473071040007202309111015077233"
            //    ["tranTime"] => string(14) "20230911101507"
            //    ["tranType"] => string(2) "02"
            //    ["version"] => string(5) "2.0.0"
            //  }
            //  ["body"] => array(5) {
            //    ["codeImgUrl"] => string(145) "https://onepay.jytpay.com/onePayService/getcode/qr.do?uuid=https://onepay.jytpay.com/onePayService/wxAsPay.do?tranFlowId=OP2309111015083903989096"
            //    ["codeUrl"] => string(86) "https://onepay.jytpay.com/onePayService/wxAsPay.do?tranFlowId=OP2309111015083903989096"
            //    ["jytOrderId"] => string(24) "OP2309111015083903989096"
            //    ["orderNo"] => string(15) "911972398401262"
            //    ["tranState"] => string(2) "10"
            //  }
            //}
            if (!$data['body']){
                return AjaxReturn(0,"支付错误,请稍后");
            }
            if($data['body']['tranState'] == "10"){
                $model::where(["id"=>$order["id"]])->update(["codeUrl"=>$data['body']['codeUrl']]);
                return AjaxReturn(1,$data['head']['respDesc'],["url"=>ym().'/api/Jytpay/wechatpayhtml?id='.$order['id']]);
            }else{
                return AjaxReturn(0,"支付错误".$data['head']['respDesc']);
            }
        }
    }

    /**
     * 微信扫码支付页面
     */
    public function wechatpayhtml(){
        $model = new Order();
        $id = input("get.id");
        //halt($id);
        $order = $model::where("id",$id)->find();
        if(!$order){
            return AjaxReturn(0,"订单不存在");
        }
        if($order["payment"] > 0){
            return AjaxReturn(0,"订单已支付");
        }
        $this->assign("url",$order['codeUrl']);
        $this->assign("orderCode",$order['system_order_num']);
        $this->assign("orderMoney",$order['pay_money']);

        return view();
    }

    /**
     * 查询订单
     */
    public function orderdetails(){
        $order_num = input("get.order_num");
        $model = new Order();
        $order = $model::where(["system_order_num"=>$order_num])->find();
        //halt($order);
        if(!$order){
            return AjaxReturn(0,"订单不存在");
        }
        if($order["payment"] == 1){
            return AjaxReturn(1,"付款成功");
        }
    }

    /**
     * 支付状态
     * https://pay.hnyouke888.cn/api/sandesm/payment.html?order_num=ym815649473909234
     * http://pay1.lnxinmeng.xyz/api/sandesm/payment.html?order_num=ym815649473909234
     */
    public function payment(){
        $order_num = input("get.order_num");
        $model = new Order();
        $order = $model::where(["system_order_num"=>$order_num])->find();
        if(!$order){
            return AjaxReturn(0,"订单不存在");
        }
//        if($order["payment"] == 1){
//            return AjaxReturn(1,"付款成功");
//        }
        $this->assign("order",$order);
        return view();
    }

    /**
     * 废弃
     * http://pay1.2021621.com/api/jytpay/index 本地
     * http://pay1.2021621.com:7888/api/jytpay/index 本地
     * https://pay.2021621.com/api/jytpay/index 线上
     */
    public function index_(){

       if(Request::isPost()){

           //$post = $_POST;
           //halt($post);
           //array(11) {
           //  ["op1001_test"] => string(42) "点击【扫码支付】按钮发起测试"
           //  ["payChannel"] => string(2) "01"
           //  ["payMode"] => string(2) "00"
           //  ["orderId"] => string(24) "P20230726165106355535607"
           //  ["totalAmt"] => string(4) "0.21"
           //  ["subject"] => string(28) "Apple/苹果 iPhone 7 耳套"
           //  ["notifyUrl"] => string(73) "http://192.168.50.12:8085/onepay-notifyService/notify/map/WXASUnion106.do"
           //  ["body"] => string(28) "Apple/苹果 iPhone 7 耳套"
           //  ["spbillCreatIp"] => string(9) "127.0.0.1"
           //  ["splitAmt"] => string(0) ""
           //  ["splitFlag"] => string(0) ""
           //}
           $client = new JytJsonClient;
           //dump(111);
           $client->init();
            //dump(233);
// 点击提交按钮后才执行
           if (!empty($_POST['op1001_test'])) {
               // 报文头信息
               $data['head']['version']='2.0.0';
               $data['head']['tranType']='01';
               $data['head']['merchantId']= $client->config->merchant_id;
               $data['head']['tranTime']=date('YmdHis',time());
               $data['head']['tranFlowid']= $client->config->merchant_id . date('YmdHis',time()) . substr(rand(),4);
               $data['head']['tranCode']= 'OP1001';
               $data['head']['respCode']='';
               $data['head']['respCesc']='';
               // 报文体
               $data['body']['payChannel'] = $_POST['payChannel'];
               $data['body']['payMode'] = $_POST['payMode'];
               $data['body']['orderId'] = $_POST['orderId'];
               $data['body']['totalAmt'] = $_POST['totalAmt'];
               $data['body']['subject'] = $_POST['subject'];
               $data['body']['notifyUrl'] = $_POST['notifyUrl'];
               $data['body']['body'] = $_POST['body'];
               $data['body']['spbillCreatIp'] = $_POST['spbillCreatIp'];
               if (!empty($_POST['splitFlag']) && !empty($_POST['splitAmt'])) {
                   $data['body']['splitAmt'] = $_POST['splitAmt'];
                   $data['body']['splitFlag'] = $_POST['splitFlag'];
               }
               //halt(222);
               $res = $client->sendReq($data);
               //halt(json_decode($res,true));
               //{
               //	"head": {
               //		"merchantId": "290071040001",
               //		"respCode": "S0000000",
               //		"respDesc": "交易受理成功",
               //		"tranCode": "OP1001",
               //		"tranFlowid": "29007104000120230726183953374548",
               //		"tranTime": "20230726183953",
               //		"tranType": "02",
               //		"version": "2.0.0"
               //	},
               //	"body": {
               //		"codeImgUrl": "https://onepay.jytpay.com/onePayService/getcode/qr.do?uuid=https://qr.alipay.com/bax07209wgsxwwko8n0q55f8",
               //		"codeUrl": "https://qr.alipay.com/bax07209wgsxwwko8n0q55f8",
               //		"jytOrderId": "OP2307261839548196641000",
               //		"orderNo": "P20230726182137162985239",
               //		"tranState": "12",
               //		"urlScheme": "https://qr.alipay.com/bax07209wgsxwwko8n0q55f8"
               //	}
               //}
               echo $res;
           }else{
               halt("请点击支付按钮");
           }
       }else{
           $ip = getIp();
           return view("",["ip"=>$ip]);
       }
    }

    /**
     * 扫码页面
     */
    public function codeImg(){
        $codeImgUrl = input("get.codeImgUrl");
        return view("codeImg",['codeImgUrl'=>$codeImgUrl]);
    }

    /**
     * 金运通支付
     * http://pay1.2021621.com/api/jytpay/pay?id=1
     */
    public function pay(){
        $model = new Order();
        $id = input("get.id");
        $order = $model::where("id",$id)->find();
        if(!$order){
            return AjaxReturn(0,"订单不存在");
        }
//        $order_time = strtotime($order['create_time']) + 1200;
//        if($order_time < time()){
//            return AjaxReturn(0,"订单已过期");
//        }
        if($order["payment"] > 0){
            return AjaxReturn(0,"订单已支付");
        }
        $ip = request()->ip();
        //$ip = "113.122.236.71";

        //增加用户付款的ip信息
        db("order_ips")->insert(["order_id"=>$id,"ip"=>$ip,"create_time"=>time()]);
        $model->where("id",$id)->update(["buy_ip"=>$ip]);//更新付款用户下单ip
        return view("",["order"=>$order]);
    }

    /**
     * 金运通微信支付
     */
    public function wechatpay(){
        if(Request::isPost()){
            $model = new Order();
            $id = input("post.id");
            //halt($id);
            $order = $model::where("id",$id)->find();
            if(!$order){
                return AjaxReturn(0,"订单不存在");
            }
//            $order_time = strtotime($order['create_time']) + 1200;
//            if($order_time < time()){
//                return AjaxReturn(0,"订单已过期");
//            }
            if($order["payment"] > 0){
                return AjaxReturn(0,"订单已支付");
            }
            if($order["type"] !=2){
                return AjaxReturn(0,"订单不是微信支付");
            }
            $ip = request()->ip();
            //增加用户付款的ip信息
            db("order_ips")->insert(["order_id"=>$id,"ip"=>$ip,"create_time"=>time()]);
            $model->where("id",$id)->update(["buy_ip"=>$ip]);//更新付款用户下单ip
            //查询可用通道
            $pay_channel_model = new PayChannel();
            //此处改造轮训通道
            $pay_channel = $pay_channel_model::where(["open"=>1,"weixin"=>1,"id"=>$order["pay_channel_id"]])->find();
            if(!$pay_channel){
                return AjaxReturn(0,"支付错误,商户异常");
            }

            $config = [
                "url"=>"https://onepay.jytpay.com/onePayService/onePay.do",
                "merchant_id"=>$pay_channel['merchant_id'],
                "cer_path"=>ROOT_PATH.$pay_channel['cer_path'],
                "pfx_path"=>ROOT_PATH.$pay_channel['pfx_path'],
                "pfx_password" => "password"
            ];
            $config = (object)$config;//数组转化成对象
            //halt($config);
            $client = new JytJsonClient;
            //dump(111);
            $client->init($config);
            $data['head']['version']='2.0.0';
            $data['head']['tranType']='01';
            $data['head']['merchantId']= $client->config->merchant_id;
            $data['head']['tranTime']=date('YmdHis',time());
            $data['head']['tranFlowid']= $client->config->merchant_id . date('YmdHis',time()) . substr(rand(),4);
            $data['head']['tranCode']= 'OP1001';
            $data['head']['respCode']='';
            $data['head']['respCesc']='';
            // 报文体
            $data['body']['payChannel'] = "00";//00微信 01支付宝
            $data['body']['payMode'] = "05";//00：主动扫码 02：JS支付  06：小程序 05：H5支付  07：云闪付控件支付
            $data['body']['orderId'] = $order['system_order_num'];
            $data['body']['totalAmt'] = $order['pay_money'];
            $data['body']['subject'] = "商城购物";//todo 上线后更换
            $data['body']['notifyUrl'] = ym().'/api/notify/jytpaypaynotify';
            $data['body']['body'] = "商城购物";//todo 上线后更换
            $data['body']['spbillCreatIp'] = $ip;
            //todo 子商户
            $data['body']['billMerchantId'] = $order['billMerchantId'];
           // if (!empty($_POST['splitFlag']) && !empty($_POST['splitAmt'])) {
                $data['body']['splitAmt'] ="";
                $data['body']['splitFlag'] = "";
            //}
            //halt(222);
            $res = $client->sendReq($data);
            $data = json_decode($res,true);
            if (!$data['body']){
                return AjaxReturn(0,"支付错误,请稍后");
            }
            if($data['body']['tranState'] == "10"){
                return AjaxReturn(1,"交易受理成功",["url"=>$data['body']['urlScheme']]);
            }
        }


    }


    /**
     * 金运通微信支付
     */
    public function wechatpay_(){
        if(Request::isPost()){
            $model = new Order();
            $id = input("post.id");
            //halt($id);
            $order = $model::where("id",$id)->find();
            if(!$order){
                return AjaxReturn(0,"订单不存在");
            }
//            $order_time = strtotime($order['create_time']) + 1200;
//            if($order_time < time()){
//                return AjaxReturn(0,"订单已过期");
//            }
            if($order["payment"] > 0){
                return AjaxReturn(0,"订单已支付");
            }
            if($order["type"] !=2){
                return AjaxReturn(0,"订单不是微信支付");
            }
            $ip = request()->ip();
            //增加用户付款的ip信息
            db("order_ips")->insert(["order_id"=>$id,"ip"=>$ip,"create_time"=>time()]);
            $model->where("id",$id)->update(["buy_ip"=>$ip]);//更新付款用户下单ip
            //查询可用通道
            $pay_channel_model = new PayChannel();
            //此处改造轮训通道
            $pay_channel = $pay_channel_model::where(["open"=>1,"weixin"=>1])->find();

            $config = [
                "url"=>"https://onepay.jytpay.com/onePayService/onePay.do",
                "merchant_id"=>$pay_channel['merchant_id'],
                "cer_path"=>ROOT_PATH.$pay_channel['cer_path'],
                "pfx_path"=>ROOT_PATH.$pay_channel['pfx_path'],
                "pfx_password" => "password"
            ];
            $config = (object)$config;
            //halt($pay_channel);
            $client = new JytJsonClient;
            //dump(111);
            $client->init($config);
            $data['head']['version']='2.0.0';
            $data['head']['tranType']='01';
            $data['head']['merchantId']= $client->config->merchant_id;
            $data['head']['tranTime']=date('YmdHis',time());
            $data['head']['tranFlowid']= $client->config->merchant_id . date('YmdHis',time()) . substr(rand(),4);
            $data['head']['tranCode']= 'OP1001';
            $data['head']['respCode']='';
            $data['head']['respCesc']='';
            // 报文体
            $data['body']['payChannel'] = "00";
            $data['body']['payMode'] = "05";
            $data['body']['orderId'] = $order['system_order_num'];
            $data['body']['totalAmt'] = $order['pay_money'];
            $data['body']['subject'] = "商城购物";//todo 上线后更换
            $data['body']['notifyUrl'] = ym().'/api/notify/jytpaypaynotify';
            $data['body']['body'] = "商城购物";//todo 上线后更换
            $data['body']['spbillCreatIp'] = $ip;
            // if (!empty($_POST['splitFlag']) && !empty($_POST['splitAmt'])) {
            $data['body']['splitAmt'] ="";
            $data['body']['splitFlag'] = "";
            //}
            //halt(222);
            $res = $client->sendReq($data);
            $data = json_decode($res,true);
            if (!$data['body']){
                return AjaxReturn(0,"支付错误,请稍后");
            }
            if($data['body']['tranState'] == "10"){
                return AjaxReturn(1,"成功",["url"=>$data['body']['urlScheme']]);
            }
        }


    }


    /**
     * 银联扫码支付
     * http://jytpay1.xiaochenpay.com/api/jytpay/yinlianpay?id=19809
     * https://japy.xiaochenpay.com/api/jytpay/yinlianpay?id=19809
     */
    public function yinlianpay(){
        //if(Request::isPost()){
            $model = new Order();
            $id = input("get.id");
            //halt($id);
            $order = $model::where("id",$id)->find();
            if(!$order){
                return AjaxReturn(0,"订单不存在");
            }
            if($order["payment"]){
                return AjaxReturn(0,"订单已支付");
            }
//            $order_time = strtotime($order['create_time']) + 1200;
//            if($order_time < time()){
//                return AjaxReturn(0,"订单已过期");
//            }
        //return view("yinlian",["orderCode"=>$order['system_order_num'],"money"=>$order['money'],"url"=>"https://onepay.jytpay.com/onePayService/getcode/qr.do?uuid=https://qr.95516.com/00010000/01554155520211065920456467017055"]);
            if($order["payment"] > 0){
                return AjaxReturn(0,"订单已支付");
            }
            if($order["type"] !=3){
                return AjaxReturn(0,"订单不是银联扫码支付");
            }
            $ip = request()->ip();
            //增加用户付款的ip信息
            db("order_ips")->insert(["order_id"=>$id,"ip"=>$ip,"create_time"=>time()]);
            $model->where("id",$id)->update(["buy_ip"=>$ip]);//更新付款用户下单ip
            if($order['codeUrl']){
                //return view("yinlian",["order"=>$order,"url"=>$data['body']['codeImgUrl']]);
                //$model::where(["id"=>$order["id"]])->update(["codeUrl"=>$data['body']['codeImgUrl']]);
                return view("yinlian",["orderCode"=>$order['system_order_num'],"money"=>$order['money'],"url"=>$order['codeUrl']]);
            }


            //查询可用通道
            $pay_channel_model = new PayChannel();
            //此处改造轮训通道
            $pay_channel = $pay_channel_model::where(["id"=>$order['pay_channel_id'],"kuaijie"=>1])->find();
            if(!$pay_channel){
                return AjaxReturn(0,"暂无银联支付通道");
            }
            $config = [
                "url"=>"https://onepay.jytpay.com/onePayService/onePay.do",
                "merchant_id"=>$pay_channel['merchant_id'],
                "cer_path"=>ROOT_PATH.$pay_channel['cer_path'],
                "pfx_path"=>ROOT_PATH.$pay_channel['pfx_path'],
                "pfx_password" => "password"
            ];
            //halt($config);
            $config = (object)$config;
            //halt($pay_channel);
            $client = new JytJsonClient;
            //dump(111);
            $client->init($config);
            $data['head']['version']='2.0.0';
            $data['head']['tranType']='01';
            $data['head']['merchantId']= $client->config->merchant_id;
            $data['head']['tranTime']=date('YmdHis',time());
            $data['head']['tranFlowid']= $client->config->merchant_id . date('YmdHis',time()) . substr(rand(),4);
            $data['head']['tranCode']= 'OP1001';
            $data['head']['respCode']='';
            $data['head']['respCesc']='';
            // 报文体
            $data['body']['payChannel'] = "05";// 支付渠道代码  00：weixin 01：alipay 02：QQ 05：银联二维码
            $data['body']['payMode'] = "00";//支付模式代码 00：主动扫码
            $data['body']['orderId'] = $order['system_order_num'];
            $data['body']['totalAmt'] = $order['pay_money'];
            $data['body']['subject'] = "商城购物";//todo 上线后更换
            $data['body']['notifyUrl'] = ym().'/api/notify/jytpaypaynotify';
            $data['body']['body'] = "商城购物";//todo 上线后更换
            $data['body']['spbillCreatIp'] = $ip;
            // if (!empty($_POST['splitFlag']) && !empty($_POST['splitAmt'])) {
            $data['body']['splitAmt'] ="";
            $data['body']['splitFlag'] = "";
            //}
            //halt(222);
            $res = $client->sendReq($data);
            $data = json_decode($res,true);
            //halt($data);
            //array(2) {
        //  ["head"] => array(8) {
        //    ["merchantId"] => string(12) "473071040007"
        //    ["respCode"] => string(8) "S0000000"
        //    ["respDesc"] => string(18) "交易受理成功"
        //    ["tranCode"] => string(6) "OP1001"
        //    ["tranFlowid"] => string(30) "473071040007202402211626438920"
        //    ["tranTime"] => string(14) "20240221162643"
        //    ["tranType"] => string(2) "02"
        //    ["version"] => string(5) "2.0.0"
        //  }
        //  ["body"] => array(6) {
        //    ["codeImgUrl"] => string(121) "https://onepay.jytpay.com/onePayService/getcode/qr.do?uuid=https://qr.95516.com/00010000/01554155520211065920456467017055"
        //    ["codeUrl"] => string(62) "https://qr.95516.com/00010000/01554155520211065920456467017055"
        //    ["jytOrderId"] => string(24) "OP2402211626448762409338"
        //    ["orderNo"] => string(15) "221039881079569"
        //    ["tranState"] => string(2) "12"
        //    ["urlScheme"] => string(62) "https://qr.95516.com/00010000/01554155520211065920456467017055"
        //  }
        //}
            if (!$data['body']){
                return AjaxReturn(0,"支付错误,".$data["head"]["respDesc"]);
            }
            if($data['body']['tranState'] == "12"){
                //return view("yinlian",["order"=>$order,"url"=>$data['body']['codeImgUrl']]);
                $model::where(["id"=>$order["id"]])->update(["codeUrl"=>$data['body']['codeImgUrl']]);
                return view("yinlian",["orderCode"=>$order['system_order_num'],"money"=>$order['money'],"url"=>$data['body']['codeImgUrl']]);
            }
        //}


    }


    /**
     * app微信支付
     */
//    public function apppaywechatindex(){
//        $model = new Order();
//        $id = input("get.id");
//        $order = $model::where("id",$id)->find();
//        if(!$order){
//            return AjaxReturn(0,"订单不存在");
//        }
////        $order_time = strtotime($order['create_time']) + 1200;
////        if($order_time < time()){
////            return AjaxReturn(0,"订单已过期");
////        }
//        if($order["payment"] > 0){
//            return AjaxReturn(0,"订单已支付");
//        }
//        $ip = request()->ip();
//        //$ip = "113.122.236.71";
//
//        //增加用户付款的ip信息
//        db("order_ips")->insert(["order_id"=>$id,"ip"=>$ip,"create_time"=>time()]);
//        $model->where("id",$id)->update(["buy_ip"=>$ip]);//更新付款用户下单ip
//        return view("",["order"=>$order]);
//    }








}