<?php

namespace app\api\controller;


use app\common\model\AkOrder;
use app\common\model\CzOrder;
use app\common\model\Order;
use app\common\model\PayChannel;
use think\Controller;
use think\facade\Env;

/**
 * 支付宝支付白名单
 */
class Alipaybai extends Controller
{

    /**
     * 白名单支付
     *  http://dld1.yapin.shop/api/alipaybai/pay?id=27
     */
    public function pay(){
        $model = new Order();
        $id = input("get.id");
        if(!$id){
            return AjaxReturn(0,"参数错误");
        }
        $order = $model::get($id);
        if(!$order){
            return AjaxReturn(0,"订单不存在");
        }
        if($order["payment"] == 1){
            return AjaxReturn(0,"订单已支付");
        }
        $ip = request()->ip();
        $model->where("id",$id)->update(["buy_ip"=>$ip]);//更新付款用户下单ip

        $PayChannelModel  = new PayChannel();
        $PayChannel = $PayChannelModel::get($order['cid']);
        if(!$PayChannel){
            return AjaxReturn(1,"支付通道不存在");
        }
        if($PayChannel['open'] != 1){
            return AjaxReturn(1,"通道未开启");
        }
        $keypri ="MIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQDOKogdSZIjZ6eL+YQMp40vydaVMxLIRB41uO9Z5eKtY6YExoi+FCpxkbCeLruf0Xt5SFKKG1bG+qGrpchSQHvIh8/TBD6N22ScD0f+PC4Z/L9JhvgLR/CN3fW8y8Cb9cyz+bBbazmyS3/abCcP7dZaV4rOs4q5pBGYvU9XYFvhVAZU4kE4ZVDXmWkALvUEETID7HIUsTv4A4DD7sTUfT+GeowbvSovYa5cJ4RSuAIYJ7Y/VKe+JU466Hr5HjJ3sehJXWCKJuJJyB68qnLRqO8VYG9TNbRUlhnlWkK8TvRfd5gHs3wGB3/0vJbP1X++xvtvxAfNIDODvnNxj8dyQJUjAgMBAAECggEBAKpLa391o+bm2fi/NBoX1IGi4UMAmwUdby7FAcBqKWE7rIjJTW5kzeJdi1w+EfEjYjB+Ut+NUZKuuBhGqj920EiGFl6hZOJcVLCr1rXkE9iXc5JSkFURKVyl+TPnwcORt3L6TnhVC6WAw60yJNn9hU++fAPdPju1kGtwBDeSepKWDjO4KEeUsM27Q707zBzlqtXFQUnyG4fAmsQmkQluqkSR1K89n5tkQ5u/ZiwMIYda37eepCO/HiZAob/63gquMEhP55uFiOsKrVQUzQsqE33NFV59A2jak+5v0dcvrhGGGtmo72n7088CVnmqj1hgCI0tEwg+TO/Y6LI1Y6HEsMECgYEA/AYwK0/ZeU0zCVE9GM8NE8OR0TzIgVX2kzvo2HMNL7h7DP5NidTdfusqPOsJvFFzyOJ2KIRLLPJwupKSD3EXjZUgd2ItnB5EbW1GgayIvTUo40mcSb724kMyuruscpueeKWhWeJAkWwbmL1qA/+gj9dp+b1Fj1ASyXU/SfjGuhECgYEA0Wskxf+2u3LGAZe8q+IBEIL09/SZHS075NX6rn8xrZinhYqW6TQvAt2ejECi+1DQ97n2b7QcHnoeX5N16Q9NoFm5T/eqj/yuJduMTa6v70ES4dogsdxUgTiuCrKvIwmP9CH5k0WrMUGKsWLpf0UlCPiA8f5oPb4Ub/IqYnKPh/MCgYA2NN6HclVEUeZ4SpDplR8q8RWb+4bkyqiOYoRiidx8NyHAWbIzwyUg91POZn9hkeNlgdAIRuwkbsDwYDYqPBjkyv6Arw1AVwJAxxAzM/j1OUniGSMUeY6AfBOdNmCRBge/y2A5BQD+RlJpN9Rlp8XRKnQQ0zTy7jYcPquuLWQRAQKBgQCzi2TPzNliBJj2rJ050F6RRXW5UKAlf66mFz8BdFOnPgYCXDveXLshfdh87r3NMhC3E1zRkF27U3/O+aJR2qj3HXXftbsqD71O/9hekbKNMgF6WYVGeFyLHYk56tB0/bHJ4YcUWNrZNBQw2VjPoPyLoGMNFs1QgjMLd1gXZ5jfOQKBgQDcwXza0kX/iA0Qhv4d5lcTUov52nkYVKEc4eodKilcF68vAmAZyEDejY/vpGEoKjKuYrlAnOnAfIzZnnovItslxreK6mZhoC036nmjqQ+Gl6NqIJb8afaIMeYZ8eunS7cEX6yjat1oMaYc0kFVgW0J+L2cyBH3L5b0XAuKBg4Wpw==";//商户私钥

        //$keypub = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAx+mAdmRjWYaESCbdcF/6yk59iSaFsKmypUvvQ+miLxJexWvcmen67/RfJgcGnCemmw6DTSwh0pLqJyh7a64UGoRuZSxyqRMZFt+JKeuLDl760euGUDwtVI+Uvsyyhi+UMUZKvN00kBBUDHBwPsoVD0OkfBZIHlkcvpJZe+IF2llktwjzkHJY/2/QiCCyR0VYa+p3olkyyDI7hKMVQlvpeasaSJyN42mTSXLMPHfzIUF/VMSPBVh1CBX3XqmCFlsUyfHmLOjbr7o7HSpIcTN7U9eL6937LOm5stPstTIyimL/bv3r2LRujX+6s3BSRIglLp1oJN99Ej1js6sjiY7WbQIDAQAB";//平台公钥
        $keypub = "-----BEGIN PUBLIC KEY-----\n"."
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAx+mAdmRjWYaESCbdcF/6
yk59iSaFsKmypUvvQ+miLxJexWvcmen67/RfJgcGnCemmw6DTSwh0pLqJyh7a64U
GoRuZSxyqRMZFt+JKeuLDl760euGUDwtVI+Uvsyyhi+UMUZKvN00kBBUDHBwPsoV
D0OkfBZIHlkcvpJZe+IF2llktwjzkHJY/2/QiCCyR0VYa+p3olkyyDI7hKMVQlvp
easaSJyN42mTSXLMPHfzIUF/VMSPBVh1CBX3XqmCFlsUyfHmLOjbr7o7HSpIcTN7
U9eL6937LOm5stPstTIyimL/bv3r2LRujX+6s3BSRIglLp1oJN99Ej1js6sjiY7W
bQIDAQAB\n".
            "-----END PUBLIC KEY-----";//平台公钥
        $goods_name = db("goods")->where("money",$order['pay_money'])->value("title");
        //halt($goods_name);
        if(!$goods_name){
            $goods_name = "商城购物";
        }
        $params = [
            "service"=>"App.Order.Pay",
            "timestamp"=>date('Y-m-d H:i:s'),
            "nonce"=>time(),
            "member_id"=>"320302516",
            "mem_order"=>$order['order_num'],
            "bank_code"=>"953",
            "notifyurl"=>ym()."/api/notify/alipaybainotify",
            "callbackurl"=>ym()."/api/alipaybai/paycallback",
            "amount"=>strval($order['pay_money']),
            "productname"=>$goods_name,
            "attach"=>""
        ];
        $apiKey = "MNq5D1ri1xPI4NA5NPmM5rEaEn5xM5rm";//商户中心的APIKEY

        ksort($params);

        $signString = "";
        foreach($params as $key => $val){
            $signString .= $val;
        }
        $signString .= $apiKey;

        $params['sign'] = strtoupper(md5($signString));

        //halt($params['sign']);
        //到这里签名完成
        //对notifyurl进行rsa加密
        $params['notifyurl'] = sslEn($params['notifyurl'],$keypri);//$key是商户私钥
        //dump($params);
        //最后POST提交  $params
        $url = "http://shopapi.wqkeji.cn";
        $re = httpPost($url,$params);//提交返回
//        dump("返回参数:");
        //dump($re);
        //判断是否为数组
        $re = is_array($re)?$re:json_decode($re,true);
        //dump("返回参数解析");
        //dump($re);
        if($re['ret'] == 500){
            return AjaxReturn(0,$re['msg']);
        }
        //对data进行平台公钥解密
        $data = sslDe($re['data'],$keypub);
        $data = json_decode($data,true);


        $pay_url = $data['pay_url'];
        header('Location:'.$pay_url);
        die();

    }

    /**
     * 图南app支付
     */
    public function apppay(){
        $model = new CzOrder();
        $id = input("get.id");
        if(!$id){
            return AjaxReturn(0,"参数错误");
        }
        $order = $model::get($id);
        if(!$order){
            return AjaxReturn(0,"订单不存在");
        }
        if($order["payment"] == 1){
            return AjaxReturn(0,"订单已支付");
        }
        $ip = getIp();

        $PayChannelModel  = new PayChannel();
        $PayChannel = $PayChannelModel::get($order['cid']);
        if(!$PayChannel){
            return AjaxReturn(1,"支付通道不存在");
        }
        if($PayChannel['open'] != 1){
            return AjaxReturn(1,"通道未开启");
        }
        //halt("支付");
        $keypri ="MIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQDOKogdSZIjZ6eL+YQMp40vydaVMxLIRB41uO9Z5eKtY6YExoi+FCpxkbCeLruf0Xt5SFKKG1bG+qGrpchSQHvIh8/TBD6N22ScD0f+PC4Z/L9JhvgLR/CN3fW8y8Cb9cyz+bBbazmyS3/abCcP7dZaV4rOs4q5pBGYvU9XYFvhVAZU4kE4ZVDXmWkALvUEETID7HIUsTv4A4DD7sTUfT+GeowbvSovYa5cJ4RSuAIYJ7Y/VKe+JU466Hr5HjJ3sehJXWCKJuJJyB68qnLRqO8VYG9TNbRUlhnlWkK8TvRfd5gHs3wGB3/0vJbP1X++xvtvxAfNIDODvnNxj8dyQJUjAgMBAAECggEBAKpLa391o+bm2fi/NBoX1IGi4UMAmwUdby7FAcBqKWE7rIjJTW5kzeJdi1w+EfEjYjB+Ut+NUZKuuBhGqj920EiGFl6hZOJcVLCr1rXkE9iXc5JSkFURKVyl+TPnwcORt3L6TnhVC6WAw60yJNn9hU++fAPdPju1kGtwBDeSepKWDjO4KEeUsM27Q707zBzlqtXFQUnyG4fAmsQmkQluqkSR1K89n5tkQ5u/ZiwMIYda37eepCO/HiZAob/63gquMEhP55uFiOsKrVQUzQsqE33NFV59A2jak+5v0dcvrhGGGtmo72n7088CVnmqj1hgCI0tEwg+TO/Y6LI1Y6HEsMECgYEA/AYwK0/ZeU0zCVE9GM8NE8OR0TzIgVX2kzvo2HMNL7h7DP5NidTdfusqPOsJvFFzyOJ2KIRLLPJwupKSD3EXjZUgd2ItnB5EbW1GgayIvTUo40mcSb724kMyuruscpueeKWhWeJAkWwbmL1qA/+gj9dp+b1Fj1ASyXU/SfjGuhECgYEA0Wskxf+2u3LGAZe8q+IBEIL09/SZHS075NX6rn8xrZinhYqW6TQvAt2ejECi+1DQ97n2b7QcHnoeX5N16Q9NoFm5T/eqj/yuJduMTa6v70ES4dogsdxUgTiuCrKvIwmP9CH5k0WrMUGKsWLpf0UlCPiA8f5oPb4Ub/IqYnKPh/MCgYA2NN6HclVEUeZ4SpDplR8q8RWb+4bkyqiOYoRiidx8NyHAWbIzwyUg91POZn9hkeNlgdAIRuwkbsDwYDYqPBjkyv6Arw1AVwJAxxAzM/j1OUniGSMUeY6AfBOdNmCRBge/y2A5BQD+RlJpN9Rlp8XRKnQQ0zTy7jYcPquuLWQRAQKBgQCzi2TPzNliBJj2rJ050F6RRXW5UKAlf66mFz8BdFOnPgYCXDveXLshfdh87r3NMhC3E1zRkF27U3/O+aJR2qj3HXXftbsqD71O/9hekbKNMgF6WYVGeFyLHYk56tB0/bHJ4YcUWNrZNBQw2VjPoPyLoGMNFs1QgjMLd1gXZ5jfOQKBgQDcwXza0kX/iA0Qhv4d5lcTUov52nkYVKEc4eodKilcF68vAmAZyEDejY/vpGEoKjKuYrlAnOnAfIzZnnovItslxreK6mZhoC036nmjqQ+Gl6NqIJb8afaIMeYZ8eunS7cEX6yjat1oMaYc0kFVgW0J+L2cyBH3L5b0XAuKBg4Wpw==";//商户私钥

        //$keypub = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAx+mAdmRjWYaESCbdcF/6yk59iSaFsKmypUvvQ+miLxJexWvcmen67/RfJgcGnCemmw6DTSwh0pLqJyh7a64UGoRuZSxyqRMZFt+JKeuLDl760euGUDwtVI+Uvsyyhi+UMUZKvN00kBBUDHBwPsoVD0OkfBZIHlkcvpJZe+IF2llktwjzkHJY/2/QiCCyR0VYa+p3olkyyDI7hKMVQlvpeasaSJyN42mTSXLMPHfzIUF/VMSPBVh1CBX3XqmCFlsUyfHmLOjbr7o7HSpIcTN7U9eL6937LOm5stPstTIyimL/bv3r2LRujX+6s3BSRIglLp1oJN99Ej1js6sjiY7WbQIDAQAB";//平台公钥
        $keypub = "-----BEGIN PUBLIC KEY-----\n"."
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAx+mAdmRjWYaESCbdcF/6
yk59iSaFsKmypUvvQ+miLxJexWvcmen67/RfJgcGnCemmw6DTSwh0pLqJyh7a64U
GoRuZSxyqRMZFt+JKeuLDl760euGUDwtVI+Uvsyyhi+UMUZKvN00kBBUDHBwPsoV
D0OkfBZIHlkcvpJZe+IF2llktwjzkHJY/2/QiCCyR0VYa+p3olkyyDI7hKMVQlvp
easaSJyN42mTSXLMPHfzIUF/VMSPBVh1CBX3XqmCFlsUyfHmLOjbr7o7HSpIcTN7
U9eL6937LOm5stPstTIyimL/bv3r2LRujX+6s3BSRIglLp1oJN99Ej1js6sjiY7W
bQIDAQAB\n".
            "-----END PUBLIC KEY-----";//平台公钥
        $params = [
            "service"=>"App.Order.Pay",
            "timestamp"=>date('Y-m-d H:i:s'),
            "nonce"=>time(),
            "member_id"=>"320302516",
            "mem_order"=>$order['order_num'],
            "bank_code"=>"953",
            "notifyurl"=>ym()."/api/notify/alipaybainotifyappweb",
            "callbackurl"=>ym()."/api/alipaybai/paycallback",
            "amount"=>strval($order['money']),
            "productname"=>"商城购物",
            "attach"=>""
        ];
        $apiKey = "MNq5D1ri1xPI4NA5NPmM5rEaEn5xM5rm";//商户中心的APIKEY

        ksort($params);

        $signString = "";
        foreach($params as $key => $val){
            $signString .= $val;
        }
        $signString .= $apiKey;

        $params['sign'] = strtoupper(md5($signString));

        //halt($params['sign']);
        //到这里签名完成
        //对notifyurl进行rsa加密
        $params['notifyurl'] = sslEn($params['notifyurl'],$keypri);//$key是商户私钥
        //dump($params);
        //最后POST提交  $params
        $url = "http://shopapi.wqkeji.cn";
        $re = httpPost($url,$params);//提交返回
//        dump("返回参数:");
        //dump($re);
        //判断是否为数组
        $re = is_array($re)?$re:json_decode($re,true);
        //dump("返回参数解析");
        //dump($re);
        if($re['ret'] == 500){
            return AjaxReturn(0,$re['msg']);
        }
        //对data进行平台公钥解密
        $data = sslDe($re['data'],$keypub);
        $data = json_decode($data,true);


        $pay_url = $data['pay_url'];
        header('Location:'.$pay_url);
        die();
    }

    /**
     * 支付宝白名单支付
     * http://dld1.yapin.shop:7888/api/alipaybai/paytest?money=50
     * https://pay.lnxinmeng.xyz/api/alipaybai/paytest?money=50
     */
    public function paytest(){
        $money = input("get.money");
        if(!$money){
            return AjaxReturn(0,"订单金额不能为空");
        }
        $keypri ="MIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQDOKogdSZIjZ6eL+YQMp40vydaVMxLIRB41uO9Z5eKtY6YExoi+FCpxkbCeLruf0Xt5SFKKG1bG+qGrpchSQHvIh8/TBD6N22ScD0f+PC4Z/L9JhvgLR/CN3fW8y8Cb9cyz+bBbazmyS3/abCcP7dZaV4rOs4q5pBGYvU9XYFvhVAZU4kE4ZVDXmWkALvUEETID7HIUsTv4A4DD7sTUfT+GeowbvSovYa5cJ4RSuAIYJ7Y/VKe+JU466Hr5HjJ3sehJXWCKJuJJyB68qnLRqO8VYG9TNbRUlhnlWkK8TvRfd5gHs3wGB3/0vJbP1X++xvtvxAfNIDODvnNxj8dyQJUjAgMBAAECggEBAKpLa391o+bm2fi/NBoX1IGi4UMAmwUdby7FAcBqKWE7rIjJTW5kzeJdi1w+EfEjYjB+Ut+NUZKuuBhGqj920EiGFl6hZOJcVLCr1rXkE9iXc5JSkFURKVyl+TPnwcORt3L6TnhVC6WAw60yJNn9hU++fAPdPju1kGtwBDeSepKWDjO4KEeUsM27Q707zBzlqtXFQUnyG4fAmsQmkQluqkSR1K89n5tkQ5u/ZiwMIYda37eepCO/HiZAob/63gquMEhP55uFiOsKrVQUzQsqE33NFV59A2jak+5v0dcvrhGGGtmo72n7088CVnmqj1hgCI0tEwg+TO/Y6LI1Y6HEsMECgYEA/AYwK0/ZeU0zCVE9GM8NE8OR0TzIgVX2kzvo2HMNL7h7DP5NidTdfusqPOsJvFFzyOJ2KIRLLPJwupKSD3EXjZUgd2ItnB5EbW1GgayIvTUo40mcSb724kMyuruscpueeKWhWeJAkWwbmL1qA/+gj9dp+b1Fj1ASyXU/SfjGuhECgYEA0Wskxf+2u3LGAZe8q+IBEIL09/SZHS075NX6rn8xrZinhYqW6TQvAt2ejECi+1DQ97n2b7QcHnoeX5N16Q9NoFm5T/eqj/yuJduMTa6v70ES4dogsdxUgTiuCrKvIwmP9CH5k0WrMUGKsWLpf0UlCPiA8f5oPb4Ub/IqYnKPh/MCgYA2NN6HclVEUeZ4SpDplR8q8RWb+4bkyqiOYoRiidx8NyHAWbIzwyUg91POZn9hkeNlgdAIRuwkbsDwYDYqPBjkyv6Arw1AVwJAxxAzM/j1OUniGSMUeY6AfBOdNmCRBge/y2A5BQD+RlJpN9Rlp8XRKnQQ0zTy7jYcPquuLWQRAQKBgQCzi2TPzNliBJj2rJ050F6RRXW5UKAlf66mFz8BdFOnPgYCXDveXLshfdh87r3NMhC3E1zRkF27U3/O+aJR2qj3HXXftbsqD71O/9hekbKNMgF6WYVGeFyLHYk56tB0/bHJ4YcUWNrZNBQw2VjPoPyLoGMNFs1QgjMLd1gXZ5jfOQKBgQDcwXza0kX/iA0Qhv4d5lcTUov52nkYVKEc4eodKilcF68vAmAZyEDejY/vpGEoKjKuYrlAnOnAfIzZnnovItslxreK6mZhoC036nmjqQ+Gl6NqIJb8afaIMeYZ8eunS7cEX6yjat1oMaYc0kFVgW0J+L2cyBH3L5b0XAuKBg4Wpw==";//商户私钥

        //$keypub = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAx+mAdmRjWYaESCbdcF/6yk59iSaFsKmypUvvQ+miLxJexWvcmen67/RfJgcGnCemmw6DTSwh0pLqJyh7a64UGoRuZSxyqRMZFt+JKeuLDl760euGUDwtVI+Uvsyyhi+UMUZKvN00kBBUDHBwPsoVD0OkfBZIHlkcvpJZe+IF2llktwjzkHJY/2/QiCCyR0VYa+p3olkyyDI7hKMVQlvpeasaSJyN42mTSXLMPHfzIUF/VMSPBVh1CBX3XqmCFlsUyfHmLOjbr7o7HSpIcTN7U9eL6937LOm5stPstTIyimL/bv3r2LRujX+6s3BSRIglLp1oJN99Ej1js6sjiY7WbQIDAQAB";//平台公钥
        $keypub = "-----BEGIN PUBLIC KEY-----\n"."
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAx+mAdmRjWYaESCbdcF/6
yk59iSaFsKmypUvvQ+miLxJexWvcmen67/RfJgcGnCemmw6DTSwh0pLqJyh7a64U
GoRuZSxyqRMZFt+JKeuLDl760euGUDwtVI+Uvsyyhi+UMUZKvN00kBBUDHBwPsoV
D0OkfBZIHlkcvpJZe+IF2llktwjzkHJY/2/QiCCyR0VYa+p3olkyyDI7hKMVQlvp
easaSJyN42mTSXLMPHfzIUF/VMSPBVh1CBX3XqmCFlsUyfHmLOjbr7o7HSpIcTN7
U9eL6937LOm5stPstTIyimL/bv3r2LRujX+6s3BSRIglLp1oJN99Ej1js6sjiY7W
bQIDAQAB\n".
"-----END PUBLIC KEY-----";//平台公钥
        $params = [
            "service"=>"App.Order.Pay",
            "timestamp"=>date('Y-m-d H:i:s'),
            "nonce"=>time(),
            "member_id"=>"320302516",
            "mem_order"=>orderNum(),
            "bank_code"=>"953",
            "notifyurl"=>ym()."/api/notify/alipaybainotify",
            "callbackurl"=>ym()."/api/alipaybai/paycallback",
            "amount"=>strval($money),
            "productname"=>"会员充值",
            "attach"=>""
        ];
        $apiKey = "MNq5D1ri1xPI4NA5NPmM5rEaEn5xM5rm";//商户中心的APIKEY

        ksort($params);

        $signString = "";
        foreach($params as $key => $val){
            $signString .= $val;
        }
        $signString .= $apiKey;

        $params['sign'] = strtoupper(md5($signString));

        //halt($params['sign']);
        //到这里签名完成
        //对notifyurl进行rsa加密
        $params['notifyurl'] = sslEn($params['notifyurl'],$keypri);//$key是商户私钥
        //dump($params);
        //最后POST提交  $params
        //$url = "https://apis.wqkeji.cn";
        $url = "http://shopapi.wqkeji.cn";
        $re = httpPost($url,$params);//提交返回
//        dump("返回参数:");
//        dump($re);
        //判断是否为数组
        $re = is_array($re)?$re:json_decode($re,true);
        //dump("返回参数解析");
        //dump($re);
        //对data进行平台公钥解密
        $data = sslDe($re['data'],$keypub);
        $data = json_decode($data,true);
        //dump($data);
        //array(9) {
        //  ["memberId"] => int(320302516)
        //  ["orderid"] => string(41) "320302516ORBB21F89EEA9EF76220230327150159"
        //  ["out_trade_id"] => string(15) "327005192928343"
        //  ["applydate"] => string(19) "2023-03-27 15:01:59"
        //  ["amount"] => int(5)
        //  ["attach"] => string(0) ""
        //  ["productname"] => string(2) "50"
        //  ["pay_url"] => string(103) "https://shopapi.wqkeji.cn/jump.php?memberid=320292516&orderid=320302516ORBB21F89EEA9EF76220230327150159"
        //  ["sign"] => string(32) "B2A45B297F99297C2A10AB9EFB5C6096"
        //}

        $pay_url = $data['pay_url'];
        header('Location:'.$pay_url);
        die();
    }

    /**
     * 支付同步跳转
     */
    public function paycallback(){
        echo "支付同步跳转";
    }

    /**
     * 大拿支付宝
     * 本地 http://pay.cxlaimeng.cn/api/Alipaybai/alipaypayment?id=
     */
    public function alipaypayment(){
        $model = new AkOrder();
        $id = input("get.id");
        //halt($id);
        $order = $model::where("id",$id)->find();
        if(!$order){
            return AjaxReturn(0,"订单不存在");
        }
//            $order_time = strtotime($order['create_time']) + 1200;
//            if($order_time < time()){
//                return AjaxReturn(0,"订单已过期");
//            }
        if($order["payment"] > 0){
            return AjaxReturn(0,"订单已支付");
        }

        $ip = request()->ip();
        //增加用户付款的ip信息
        db("order_ips")->insert(["order_id"=>$id,"ip"=>$ip,"create_time"=>time()]);
        $model->where("id",$id)->update(["buy_ip"=>$ip]);//更新付款用户下单ip
        $url = "http://39.104.62.14:7788/pay/payment";
        $arr = [
            "pay_member_id"=>"100074",
            "pay_order_id"=>$order['system_order_num'],
            "pay_applydate"=>date('Y-m-d H:i:s'),
            "pay_bankcode"=>"100002",
            "pay_notifyurl"=>ym()."/api/notify/alipaypaymentnotify",
            "pay_callbackurl"=>ym()."/api/alipay/paycallback",
            "pay_amount"=>$order['pay_money']
        ];
        $key = "CMJShYRjaZm3gJDjIJbL2qX4s2erUsxk";
        $arr['pay_sign'] = $this->generateSign($arr,$key);
        $arr['pay_productname'] = "会员充值";
        //dump($arr);
//        $res = request_post_json($url,json_encode($arr),$arr['pay_sign']);
//        halt($res);
        $jsonData = json_encode($arr);
        $res = $this->post_json($url,$jsonData);
        $res = json_decode($res,true);
        dump($res);
        if($res['status'] == 1){
            $pay_url = $res['data'];
            header('Location:'.$pay_url);
            exit();
        }else{
            return AjaxReturn(0,$res['msg']);
        }
        //echo $res;
        //{
        //    "status": 1,
        //    "msg": "订单创建成功",
        //    "data": "https://openapi.alipay.com/gateway.do?alipay_root_cert_sn=687b59193f3f462dd5336e5abf83c5d8_02941eef3187dddf3d3b83462e1dfcf6&alipay_sdk=alipay-sdk-java-4.39.134.ALL&app_cert_sn=8ce00f90d6ceefbcbf82645442e13b6a&app_id=2021004199665920&biz_content=%7B%22body%22%3A%22%E5%95%86%E5%9F%8E%E4%BA%A7%E5%93%81%22%2C%22extend_params%22%3A%7B%22sys_service_provider_id%22%3A%222088941764581160%22%7D%2C%22out_trade_no%22%3A%22P1867079708377694209%22%2C%22product_code%22%3A%22QUICK_WAP_PAY%22%2C%22settle_info%22%3A%7B%22settle_detail_infos%22%3A%5B%7B%22amount%22%3A%221.00%22%2C%22trans_in_type%22%3A%22defaultSettle%22%7D%5D%7D%2C%22sub_merchant%22%3A%7B%22merchant_id%22%3A%222088360816703701%22%7D%2C%22subject%22%3A%22%E5%95%86%E5%9F%8E%E4%BA%A7%E5%93%81%22%2C%22time_expire%22%3A%222024-12-12+13%3A36%3A05%22%2C%22total_amount%22%3A%221.00%22%7D&charset=UTF-8&format=json&method=alipay.trade.wap.pay&notify_url=https%3A%2F%2Fpay.huanyuehongmin.top%2Fapi%2Fpay%2Fnotify%2Fzftpay&return_url=https%3A%2F%2Fpay.huanyuehongmin.top%2Fapi%2Fpay%2Freturn%2Fzftpay&sign=e%2BynGDd3eNBcMPAnh9JeBxP1DJui5g%2F4ib9WdLpwYrhQaHRFPtKdmKRDYqmQ2FL2lRr0%2BfhFN8goGH5K%2BhERFnRd%2FnF7fiIaxuW5TGFce6Iqcw2PVpn4dW54s%2B563%2FgMGE%2BbT165pvRip84m0l32Zt6TPgeVws7ZnkUwogopUZiuWU3F4nQpFsGeKGOUrEjDk8KCbzt%2FT2f54O0p9NBk5KYIlCF97cXPYG3ux6I%2BbfuN6Zono1rWbq7lgkskKnciNry1CTQIIWTzJ18gullLnN81riBA%2BsNTQN5k5a3TEnAyc1hnQKQmEavYQ%2FtL%2FPRnJOJ5KnJlBqICw4S4ao7%2FgA%3D%3D&sign_type=RSA2&timestamp=2024-12-12+13%3A31%3A05&version=1.0",
        //    "msgid": "ctd7a6epumug1i2u3es0"
        //}
    }


    /**
     * 大拿支付宝
     * 本地 http://pay.cxlaimeng.cn/api/Alipaybai/alipaypayment
     */
    public function alipaypayment_(){
        $url = "http://39.104.62.14:7788/pay/payment";
        $arr = [
            "pay_member_id"=>"100074",
            "pay_order_id"=>orderNum(),
            "pay_applydate"=>date('Y-m-d H:i:s'),
            "pay_bankcode"=>"100002",
            "pay_notifyurl"=>ym()."/api/notify/alipaypaymentnotify",
            "pay_callbackurl"=>ym()."/api/alipay/paycallback",
            "pay_amount"=>"1.00"
        ];
        $key = "CMJShYRjaZm3gJDjIJbL2qX4s2erUsxk";
        $arr['pay_sign'] = $this->generateSign($arr,$key);
        $arr['pay_productname'] = "会员充值";
        //dump($arr);
//        $res = request_post_json($url,json_encode($arr),$arr['pay_sign']);
//        halt($res);
        $jsonData = json_encode($arr);
        $res = $this->post_json($url,$jsonData);
        echo $res;
    }


    /**
     * 生成签名
     */
   public function generateSign($params, $key) {
        // 1. 将集合M内非空参数值的参数按照参数名ASCII码从小到大排序（字典序）
        ksort($params);
        // 2. 拼接成字符串
       $stringA = '';
        foreach ($params as $k => $v) {
            if ($v != '' && $k != 'pay_sign') {
                $stringA .= $v;
            }
        }
        //dump($stringA);
        // 3. 在stringA最后拼接上key得到stringSignTemp字符串
        $stringSignTemp = $stringA . $key;
        //dump($stringSignTemp);
        // 4. 对stringSignTemp进行MD5运算，再将得到的字符串所有字符转换为大写
        $signValue = strtoupper(md5($stringSignTemp));

        return $signValue;
    }

    /**
     * 发送json请求支付
     */
    public function post_json($url,$jsonData)
    {
        // 初始化cURL会话
        $ch = curl_init();
        // 设置cURL选项
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
            'Content-Length: ' . strlen($jsonData)
        ]);
        // 执行cURL请求
            $response = curl_exec($ch);
        // 检查是否有错误发生
            if (curl_errno($ch)) {
                echo 'Error:' . curl_error($ch);
            }
        // 关闭cURL会话
            curl_close($ch);
        // 输出响应结果
            return $response;

    }
}