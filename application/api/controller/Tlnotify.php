<?php

namespace app\api\controller;

use app\common\model\Agent;
use app\common\model\CzOrder;
use app\common\model\FengfuChannel;
use app\common\model\FfOrder;
use app\common\model\FfXiafa;
use app\common\model\Order;

use app\common\model\PayChannel;
use app\common\model\TlOrder;
use app\common\model\Xiafa;
use app\common\service\Finance;
use think\Controller;
use think\facade\Env;
use JytPay\Client_ONEPAY\JytJsonClient;


class Tlnotify extends Controller
{
    /**
     * 通联支付异步
     * http://p.dinglianshop.cn/api/Tlnotify/tongliannotify   本地
     * https://pay.dinglianshop.cn/api/Tlnotify/tongliannotify   线上
     */
    public function tongliannotify(){
        $notify_ip = request()->ip();//回调异步ip
        //上线前开启
        $data = $_POST;
        db("test")->insert(["add_time"=>time(),"content"=>json_encode($data),"ip"=>$notify_ip,"explain"=>"通联异步支付返回参数"]);
        $params = array();
        foreach($data as $key=>$val) {//动态遍历获取所有收到的参数,此步非常关键,因为收银宝以后可能会加字段,动态获取可以兼容由于收银宝加字段而引起的签名异常
            $params[$key] = $val;
        }
        if($data['trxstatus'] != "0000"){
            return "订单未支付";
        }
        //如果参数为空,则不进行处理
        if(count($params)<1){
            echo "error";
            exit();
        }
        //验签成功
        if(self::ValidSign($params)){
            $model = new TlOrder();
            $order = $model::where(["system_order_num"=>$data["outtrxid"]])->find();
            //halt($order);
            if(!$order){
                return AjaxReturn(0,'订单不存在');
            }
            if($order["payment"]){
                return AjaxReturn(0,'订单已支付');
            }
            $orderData = [
                "notify_ip"=>$notify_ip,//异步回调支付ip
                "payment" => 1,
                "pay_time" => time(),
                "trxid"=>$data['trxid'],
                "chnltrxid"=>$data['chnltrxid'],
                "accttype"=>$data['accttype'],
                "chnlid"=>$data['chnlid'],

            ];
            $model::where(["id" => $order['id']])->update($orderData);//更改订单信息
            //支付时间统计
            $agentFianceModel = new Finance();
            $agentFianceModel->agentDaytl($order['id']);//平台日财务
            $agentFianceModel->payDaytongdao($order['id']);//通道日财务
            $agent =  Agent::get($order["agent_id"]);


            //支付回调
            $notify_data = [
                "order_num" => $order["order_num"],
                "money" => $order["money"],
                "agent_id" => $agent["id"],
            ];
            $sign=createSign($notify_data,$agent["key"]);
            $notify_data['sign'] = $sign;
            if ($order["notify_url"]) {
                //异步回调给其他平台
                request_post($order["notify_url"], $notify_data);
                $order::where('id', $order['id'])->update(["return_status" => 1, "return_time" => time()]);//修改订单状态
            }

            //此处进行业务逻辑处理
            echo "success"; exit();
        }
        else{
            echo "erro";
            exit();
        }
    }

    /**
     * 通联支付异步
     * http://p.dinglianshop.cn/api/Tlnotify/tongliannotifyyinlian   本地
     * https://pay.dinglianshop.cn/api/Tlnotify/tongliannotify   线上
     */
    public function tongliannotifyyinlian(){
        $notify_ip = request()->ip();//回调异步ip
        //上线前开启
        $data = $_POST;
        db("test")->insert(["add_time"=>time(),"content"=>json_encode($data),"ip"=>$notify_ip,"explain"=>"通联云闪付异步支付返回参数"]);
//        $json = '{"acct":"6226227906988070","accttype":"00","appid":"00318060","bankcode":"03050000","cusid":"660303073752HVR","cusorderid":"jl813319327984767","fee":"1","initamt":"200","outtrxid":"jl813319327984767","paytime":"20240813145240","sign":"elfFJcA9coVoj7Lzcse3ZnKCMX71c9V5Pomso1V0rWUuz6lnS\/qapjGL6ImFTzUv79fpNQQPXqydIuA+cTeL06E7R41eP9TsXxX76LTyPr2JpZUqdPl5dyeUs24gVLAuCL00wXH14JoX4fO7OFS4fe15QItm4jtjv0FDu09XlJ8=","signtype":"RSA","termauthno":"20240813145213","termrefnum":"000499921179510813145239","termtraceno":"0","trxamt":"200","trxcode":"VSP551","trxdate":"20240813","trxid":"240813111815894272","trxreserved":"ceshi","trxstatus":"0000"}';
//
//        $data = json_decode($json,true);
        //dump($data);
        $params = array();
        foreach($data as $key=>$val) {//动态遍历获取所有收到的参数,此步非常关键,因为收银宝以后可能会加字段,动态获取可以兼容由于收银宝加字段而引起的签名异常
            $params[$key] = $val;
        }
        if($data['trxstatus'] != "0000"){
            return "订单未支付";
        }
        //如果参数为空,则不进行处理
        if(count($params)<1){
            echo "error";
            exit();
        }
        //halt(self::ValidSignyl($params));
        //验签成功
        if(self::ValidSignyl($params)){
            $model = new TlOrder();
            //dump($data["outtrxid"]);
            $order = $model::where("system_order_num",$data["outtrxid"])->find();
            //halt($order);
            if(!$order){
                return AjaxReturn(0,'订单不存在');
            }
            if($order["payment"]){
                return AjaxReturn(0,'订单已支付');
            }
            $orderData = [
                "notify_ip"=>$notify_ip,//异步回调支付ip
                "payment" => 1,
                "pay_time" => time(),
                "trxid"=>$data['trxid'],
                "chnltrxid"=>$data['chnltrxid'],
                "accttype"=>$data['accttype'],
                "chnlid"=>$data['chnlid'],

            ];
            $model::where(["id" => $order['id']])->update($orderData);//更改订单信息
            //支付时间统计
            $agentFianceModel = new Finance();
            $agentFianceModel->agentDaytl($order['id']);//平台日财务
            $agentFianceModel->payDaytongdao($order['id']);//通道日财务
            $agentFianceModel->jinedaypaytl($order['id']);//金额日财务
            $agent =  Agent::get($order["agent_id"]);


            //支付回调
            $notify_data = [
                "order_num" => $order["order_num"],
                "money" => $order["money"],
                "agent_id" => $agent["id"],
            ];
            $sign=createSign($notify_data,$agent["key"]);
            $notify_data['sign'] = $sign;
            if ($order["notify_url"]) {
                //异步回调给其他平台
                request_post($order["notify_url"], $notify_data);
                $order::where('id', $order['id'])->update(["return_status" => 1, "return_time" => time()]);//修改订单状态
            }

            //此处进行业务逻辑处理
            echo "success"; exit();
        }
        else{
            echo "erro";
            exit();
        }
    }

    /**
     * 校验签名
     * @param array 参数
     * @param unknown_type appkey
     */


    public static function ValidSignyl(array $array){
        $sign =$array['sign'];
        unset($array['sign']);
        ksort($array);
        $bufSignSrc = self::ToUrlParams($array);
        $public_key='MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCm9OV6zH5DYH/ZnAVYHscEELdCNfNTHGuBv1nYYEY9FrOzE0/4kLl9f7Y9dkWHlc2ocDwbrFSm0Vqz0q2rJPxXUYBCQl5yW3jzuKSXif7q1yOwkFVtJXvuhf5WRy+1X5FOFoMvS7538No0RpnLzmNi3ktmiqmhpcY/1pmt20FHQQIDAQAB';
        $public_key = chunk_split($public_key , 64, "\n");
        $key = "-----BEGIN PUBLIC KEY-----\n$public_key-----END PUBLIC KEY-----\n";
        $result= openssl_verify($bufSignSrc,base64_decode($sign), $key );
        return $result;
    }

    //


    /**
     * 通联支付异步
     * http://p.dinglianshop.cn/api/Tlnotify/tongliannotifytest   本地
     * https://pay.dinglianshop.cn/api/Tlnotify/tongliannotifytest   线上
     */
    public function tongliannotifytest(){
        $notify_ip = request()->ip();//回调异步ip
        $json = '{"acct":"opn0buOguLolD40PdcTOrlE8PM5U","accttype":"99","appid":"00318060","bankcode":"OTHERS","chnlid":"205299480","chnltrxid":"4200002434202408069694319305","cmid":"675007539","cusid":"660303073752HVR","cusorderid":"jl806169678300960","fee":"0","initamt":"2","outtrxid":"jl806169678300960","paytime":"20240806120300","sign":"B2AyXSYoSTkab+9UGUCDCZU3xpvcyJrug\/wdRiPpGF97s7rutqbzF3SlLsb4uLJJiBgvPJxgFhbm6Abyn9R307iaWQZwQzjL72eCFjfP2DdKsSu7lo+QgJDEcWjkzOv9a9Ob3CjS1dVejBG1mfv84St9jYm6kHT9FwdxGUwJl1Q=","signtype":"RSA","termauthno":"OTHERS","termrefnum":"4200002434202408069694319305","termtraceno":"0","trxamt":"2","trxcode":"VSP501","trxdate":"20240806","trxid":"240806115214976625","trxreserved":"made in syb","trxstatus":"0000"}';
        //上线前开启
        $data = json_decode($json,true);
        $params = array();
        foreach($data as $key=>$val) {//动态遍历获取所有收到的参数,此步非常关键,因为收银宝以后可能会加字段,动态获取可以兼容由于收银宝加字段而引起的签名异常
            $params[$key] = $val;
        }
        dump($params);
        if(count($params)<1){//如果参数为空,则不进行处理
            echo "error";
            exit();
        }
        if(self::ValidSign($params)){//验签成功
            //此处进行业务逻辑处理
            echo "success"; die();
        }
        else{
            echo "erro"; die();
        }
    }



    /***********************************************通联支付参数***************************************************/

    /**
     * 将参数数组签名
     */

    //RSA签名
    public static function Sign(array $array){
        ksort($array);
        $bufSignSrc = self::ToUrlParams($array);
        $private_key='MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCx5JyAR+wbKQ2+5WhSTDDSk73oIevbGaMiRHtMz/xA5iqnIMbhp7B0fo+8Uo/GYWkuXJWZ2PcFJpRWVo5y7wTaivmlh3PHdj2yGZ7L5bEs7pG55hPpuCQo7debpeBdL3H/Rs3wHqpUGitKy8d6CoNTrFZ1hx9tW/atx00ciGuj76RnvzIhZrSQnMxgF2yD520C68FwW0U/jsMu4ucp6d504dDqgbk0r5d0bKpSAVCDNLpX40ooz1jHowygjZQU65NZA1HCtEofrVO6GUfK6WkxgYyPe8i6jkLFjrVcoBsfQpaDBXsgoztcAOFdYtWCxw1qI3AMw3p/2qnlt7qEOuwtAgMBAAECggEAEvJKSM9gMjRZJm/AgKNkv8jEfSi/ugItAcVRbIjqUO8ys8Il7HqzrHSeUmxoq7RMQ4fQ1yXoiT/mpJtraIpUdgI/PIYEqsXJJGLeKtE34ZU3KBl9HXNjRoiuYgF0t/gJqCqeXGins3VmDv1NLY5ZFlxQiQvvKPKWf0Ouzs8ox7jctpaVA2QHE2IiL33PZl/N69FkdnjPhS7qeM/DTjiw/lrmoP4/DfWA45B535ZS+CGHVMOqoTCGe29EynPwL4ZhJw/avgfKnmFE/71CXiVGs5HKh6SN33ZQAKiv+25hH1eVn/AqBBUTJVpbjuat4gIW38Jz4NrhiZazCJDlQnNbcQKBgQDkK269d3nzQo1hk7N5NFU+RIdrd/RZE4O4lYY3nHQG8rFovbOmZq/7pm2o+8x5lj05WUfRdvfA7I5ZChToMsepjM8O1AeP5V09nUv4YEVGRs41I54TtGnQ8VYmDKSjcBC4BqznFo8u/bKNDRAvdob16MrxoBzuS/VxdwTTHw2wDwKBgQDHl0wRtltHtUvNUxDPNvQrrUCtHjSHmyf0dOW0LKKdmFhr2LYDGYfwj5iy/ng642mo9f0NbAqolORH1+AAZ7HAhU8K0moZgZ2Rv2o1CBGMErugbYNtk//JvjCdPrsww0R3E4h6pn/CC/MLphApne3nnJWJTlQDBDUm2tKsAPRkAwKBgQDT29AzdKBzUzRbif13aTRNYOwsyXDE1O3VkmDReh/x457/FkWdGHQHQf1IjqTJsMqgWVnqEIDzTbLO0iGfiKcDs47+wblTzzDIaSmFMj6ghlYp2SyKN1aZf7zyD4M/jmq3tNsOp0/D3iTP+3Uv3Oprov/tYzH5weXguRZtwcO/2QKBgGeHGRQO7OYTKhCEx7FUJe8J3QRvKHw2hNEOWZ/Nj0QDo8m9DRwqgBLNyTz/NcoF7+aOQgZWRmkPYiHJ3g0XvGHKRCeu4Q3954eULDj5yHBJvz0F0Shjnkg8+OZ3hY1TnA3P+fj0qEw0+orORo/vtVlwJTilgnpWEmPnGWPS0vGVAoGAGbMrmvdmDL+XPe6ff8ye9mDCl5k96/Ddc3+P5FWS4sL0CNcmif+gOvSTK+yxuJoJKlWxssZjoRqueqRFDanR6ccOxdbYzPoHl38WN1ldlTBCeW2oItg4U0+Y4on0UdM8+vRBja8YcWOXX5+ZpQHCgknLMAop5rVG5ucG9W1mfT0=';
        $private_key = chunk_split($private_key , 64, "\n");
        $key = "-----BEGIN RSA PRIVATE KEY-----\n".wordwrap($private_key)."-----END RSA PRIVATE KEY-----";
        //   echo $key;
        if(openssl_sign($bufSignSrc, $signature, $key )){
            //		echo 'sign success';
        }else{
            echo 'sign fail';
        }
        $sign = base64_encode($signature);//加密后的内容通常含有特殊字符，需要编码转换下，在网络间通过url传输时要注意base64编码是否是url安全的
//echo $sign;
// echo $signature,"\n";
        return $sign;

    }


    public static function ToUrlParams(array $array)
    {
        $buff = "";
        foreach ($array as $k => $v)
        {
            if($v != "" && !is_array($v)){
                $buff .= $k . "=" . $v . "&";
            }
        }

        $buff = trim($buff, "&");
        return $buff;
    }

    /**
     * 校验签名
     * @param array 参数
     * @param unknown_type appkey
     */


    public static function ValidSign(array $array){
        $sign =$array['sign'];
        unset($array['sign']);
        ksort($array);
        $bufSignSrc = self::ToUrlParams($array);
        //dump($bufSignSrc);
        $public_key='MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCm9OV6zH5DYH/ZnAVYHscEELdCNfNTHGuBv1nYYEY9FrOzE0/4kLl9f7Y9dkWHlc2ocDwbrFSm0Vqz0q2rJPxXUYBCQl5yW3jzuKSXif7q1yOwkFVtJXvuhf5WRy+1X5FOFoMvS7538No0RpnLzmNi3ktmiqmhpcY/1pmt20FHQQIDAQAB';
        $public_key = chunk_split($public_key , 64, "\n");
        $key = "-----BEGIN PUBLIC KEY-----\n$public_key-----END PUBLIC KEY-----\n";
        $result= openssl_verify($bufSignSrc,base64_decode($sign), $key );
        //dump($result);
        return $result;
    }

    /**
     * 测试
     * http://p.dinglianshop.cn/api/Tlnotify/test
     */
//    public function test(){
//        $id = 19;
//        $agentFianceModel = new Finance();
//
//        $agentFianceModel->payDaytongdao($id);//通道日财务
//    }

        /**
         * 微信支付异步
         * 本地地址:  http://pay.cxlaimeng.cn/api/Tlnotify/tongliannotifywechat
         *   网络地址:  https://p.cxlaimeng.com/api/Tlnotify/tongliannotifywechat
         */
        public function tongliannotifywechat(){
            $notify_ip = request()->ip();//回调异步ip
//            //上线前开启
            $data = $_POST;
            db("test")->insert(["add_time"=>time(),"content"=>json_encode($data),"ip"=>$notify_ip,"explain"=>"通联云闪付异步支付返回参数"]);
//        $json = '{"ifCode":"allinpay","payOrderId":"P1870362071456059394","mchOrderNo":"jlC21639501483822","sign":"ADFF4845EBDD9568C41CBF3ACC71C597","channelOrderNo":"4200002562202412218150809283","reqTime":"1734764061540","body":"\u5546\u54c1\u8d2d\u4e70","createdAt":"1734764042000","channelFeeAmount":"0","appId":"67247a83e4b072ece9d793b5","successTime":"1734764062000","signType":"MD5","currency":"CNY","state":"2","mchNo":"E249974058305","amount":"79","mchName":"\u957f\u6c99\u5e02\u83b1\u6aac\u79d1\u6280\u6709\u9650\u516c\u53f8","wayCode":"SYB_B2C_WECHAT","expiredTime":"2024-12-21 15:54:02","clientIp":"27.197.175.66","channelUser":"oCYH14pgK7sxUmEGRJUVaNLk9mcM"}';
//
//        $data = json_decode($json,true);
//        dump($data);
        $key = "h4fi361gqce4qu4vsx7cego8p7ndrx3dqwy3an12y3ek580qbuhnw0k1eantjxts4rlu3r0teyac1slnt1cp0jsgyb639dh01wx6o68j2tks6yww2zijkp2pqr83j30e";
        $data_sign = $data['sign'];
        unset($data['sign']);
            $sign =  createSign($data,$key);
            if($data_sign  == $sign){
                //array(21) {
                //  ["ifCode"] => string(8) "allinpay"
                //  ["payOrderId"] => string(20) "P1870362071456059394"
                //  ["mchOrderNo"] => string(17) "jlC21639501483822"
                //  ["sign"] => string(32) "ADFF4845EBDD9568C41CBF3ACC71C597"
                //  ["channelOrderNo"] => string(28) "4200002562202412218150809283"
                //  ["reqTime"] => string(13) "1734764061540"
                //  ["body"] => string(12) "商品购买"
                //  ["createdAt"] => string(13) "1734764042000"
                //  ["channelFeeAmount"] => string(1) "0"
                //  ["appId"] => string(24) "67247a83e4b072ece9d793b5"
                //  ["successTime"] => string(13) "1734764062000"
                //  ["signType"] => string(3) "MD5"
                //  ["currency"] => string(3) "CNY"
                //  ["state"] => string(1) "2"
                //  ["mchNo"] => string(13) "E249974058305"
                //  ["amount"] => string(2) "79"
                //  ["mchName"] => string(33) "长沙市莱檬科技有限公司"
                //  ["wayCode"] => string(14) "SYB_B2C_WECHAT"
                //  ["expiredTime"] => string(19) "2024-12-21 15:54:02"
                //  ["clientIp"] => string(13) "27.197.175.66"
                //  ["channelUser"] => string(28) "oCYH14pgK7sxUmEGRJUVaNLk9mcM"
                //}
                $model = new TlOrder();
                //dump($data["outtrxid"]);
                $order = $model::where("system_order_num",$data["mchOrderNo"])->find();
                //halt($order);
                if(!$order){
                    return AjaxReturn(0,'订单不存在');
                }
                if($order["payment"]){
                    return AjaxReturn(0,'订单已支付');
                }
                $orderData = [
                    "notify_ip"=>$notify_ip,//异步回调支付ip
                    "payment" => 1,
                    "pay_time" => time(),
                    "trxid"=>$data['payOrderId'],
                    "chnltrxid"=>$data['channelOrderNo'],
//                    "accttype"=>$data['accttype'],
//                    "chnlid"=>$data['chnlid'],

                ];
                $model::where(["id" => $order['id']])->update($orderData);//更改订单信息
                //支付时间统计
                $agentFianceModel = new Finance();
                $agentFianceModel->agentDaytl($order['id']);//平台日财务
                $agentFianceModel->payDaytongdao($order['id']);//通道日财务
                $agentFianceModel->jinedaypaytl($order['id']);//金额日财务
                $agent =  Agent::get($order["agent_id"]);


                //支付回调
                $notify_data = [
                    "order_num" => $order["order_num"],
                    "money" => $order["money"],
                    "agent_id" => $agent["id"],
                ];
                $sign=createSign($notify_data,$agent["key"]);
                $notify_data['sign'] = $sign;
                if ($order["notify_url"]) {
                    //异步回调给其他平台
                    request_post($order["notify_url"], $notify_data);
                    $order::where('id', $order['id'])->update(["return_status" => 1, "return_time" => time()]);//修改订单状态
                }

                //此处进行业务逻辑处理
                echo "success"; exit();
            }else{
                echo 'sign fail';
            }

        }



}