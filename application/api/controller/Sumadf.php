<?php

namespace app\api\controller;

use app\common\model\FengfuChannel;
use app\common\model\FfAccount;
use app\common\model\FfOrder;
use app\common\model\FfXiafa;
use think\facade\Validate;
use think\Controller;
use app\common\model\Agent;
use think\facade\Env;
//include_once  Env::get("root_path"). "extend/ldpay/umfPayService/UmfService.class.php";
//include_once  Env::get("root_path"). "vendor/adapay/AdapaySdk/init.php";
class Sumadf extends Controller
{

    /**
     * 公户打款
     * http://p.dinglianshop.cn/api/Sumadf/gkdakuan
     * https://pay.dinglianshop.cn/api/Sumadf/gkdakuan
     */
    public function gkdakuan(){
        $pay_channel_model = new FengfuChannel();
        $pay_channel = $pay_channel_model::where(["id"=>11])->find();
        if (!$pay_channel){
            return AjaxReturn(0,"tongdaobucunzai");
        }
        $url = "https://www.sumapay.com/main/singleTransfer_toTransfer";
        $arr = [
            "requestId"=>orderNum(),//请求流水号
            "merchantCode"=>$pay_channel["merchant_id"],//商户编号
            "transferType"=>"2",//付款类型  0：付款到预存账户 1：付款到个人银行账户 2：付款到企业银行账户
            "transToMerCode"=>"",
            "transToMerName"=>"",
            "sum"=>"3.01",//付款金额,
            "unionBankNum"=>"323331000001",//银行联行号当付款类型为 2 付款到企 业银行账户时，不可为空
            "openBankName"=>"浙江网商银行",//开户网点 当付款类型为 2 付款到企 业银行账户时，不可为空
            "openBankProvince"=>"浙江省",//开户省 收款方银行开户省当付款类型为 2 付款到企 业银行账户时，不可为空
            "openBankCity"=>"杭州市",//开户城市 收款方银行开户市 当付款类型为 2 付款到企 业银行账户时，不可为空
            "accountName"=> "四川宇鑫网络技术有限公司",//账户开户名 收款方账户开户名当付款类型为 1 或 2 时，该字段必输
            "bankCode"=>"myb",//银行编码 收款方银行编码当付款类型为 2 时，该字段 必输，参考《银行编码表》
            "bankAccount"=>"8888888981855255",//银行账户收款方银行账户当付款类型为 1 或 2 时，该字段必输
            "reason"=>"推广发放",//付款原因
            "noticeUrl"=>ym().'/api/notify/sumapaydaifunotify',//异步通知地址
            "refundNoticeUrl"=>"",//退票异步通知地址
            "transferPayType"=>"1",//付款审核类型 0：手工审核 1：自动实时 不传时默认为 0

        ];

        $arr["accountName"] = iconv('UTF-8','GBK',  $arr["accountName"]);
        //dump($arr);
//        $arr = array_map(function($item) {
//            return mb_convert_encoding($item, 'GBK', 'UTF-8');
//        }, $arr);
        //halt($arr);
        //把发送给接口的参数组成字符串：请求流水号()+商户编号()+
        //付 款 类 型 ()+ 收 款 方 商 户 编 码 ()+ 收 款 方 商 户 名 称
        //()+ 付 款 金 额 ()+ 银 行 联 行 号 ()+ 开 户 行
        //()+ 开 户 省 ()+ 开 户 市 ()+ 账 户 名 称
        //()+银行编码()+银行账户()+付款原因()+异
        //步 通 知 地 址 ()+退 票 异 步 通 知 地 址 ()+ 付 款 审 核 类 型
        //（）
        $merKey = $pay_channel["key"];//密钥
        $sbOld = "";
        $sbOld = $sbOld . $arr['requestId'];
        $sbOld = $sbOld . $arr['merchantCode'];
        $sbOld = $sbOld . $arr['transferType'];
        $sbOld = $sbOld . $arr['transToMerCode'];
        $sbOld = $sbOld . $arr['transToMerName'];
        $sbOld = $sbOld . $arr['sum'];
        $sbOld = $sbOld . $arr['unionBankNum'];
        $sbOld = $sbOld . $arr['openBankName'];
        $sbOld = $sbOld . $arr['openBankProvince'];
        $sbOld = $sbOld . $arr['openBankCity'];
        $sbOld = $sbOld . $arr['accountName'];
        $sbOld = $sbOld . $arr['bankCode'];
        $sbOld = $sbOld . $arr['bankAccount'];
        $sbOld = $sbOld . $arr['reason'];
        $sbOld = $sbOld . $arr['noticeUrl'];
        $sbOld = $sbOld . $arr['refundNoticeUrl'];
        $sbOld = $sbOld . $arr['transferPayType'];
        $signatrue = HmacMd5($sbOld, $merKey);//数字签名
        $arr['signature'] = $signatrue;
        //halt($arr['signature']);
        $returnData = send_posts($url, $arr);
        //halt($returnData);
        $returnData = iconv('GBK', 'UTF-8', $returnData);
        $res_data = json_decode($returnData,true);
        halt($res_data);
        //array(4) {
        //  ["requestId"] => string(15) "501428662699058"
        //  ["result"] => string(5) "00000"
        //  ["signature"] => string(32) "bf73c889ad8cb48f3521011347203305"
        //  ["sum"] => string(5) "15.09"
        //}
        if($res_data["result"] == "00000"){
            //$model::where(["id"=>$id])->update(["payment"=>2]);
            return AjaxReturn(1,"ok");
        }else{
            return AjaxReturn(0,$res_data["result"],$res_data);
        }
    }

    /**
     * 公户打款查询
     *  http://p.dinglianshop.cn/api/Sumadf/ghdakuanquery
     * https://pay.dinglianshop.cn/api/Sumadf/ghdakuanquery
     */
    public function ghdakuanquery(){
        //508323478572817
        $pay_channel_model = new FengfuChannel();
        $pay_channel = $pay_channel_model::where(["id"=>11])->find();
        if (!$pay_channel){
            return AjaxReturn(0,"通道不存在");
        }
        $url = "https://www.sumapay.com/main/singleTransfer_singleTransferQuery";
        $arr = [
            "requestId"=>orderNum(),
            "merchantCode"=>$pay_channel["merchant_id"],//商户编号
            "originalRequestId"=>"508323478572817"//原订单流水号
        ];
        $merKey = $pay_channel["key"];//密钥

        $sbOld = "";
        $sbOld = $sbOld . $arr['requestId'];
        $sbOld = $sbOld . $arr['merchantCode'];
        $sbOld = $sbOld . $arr['originalRequestId'];
        $signatrue = HmacMd5($sbOld, $merKey);//数字签名
        $arr['signature'] = $signatrue;
        $returnData = send_posts($url, $arr);
        //halt($returnData);
        $returnData = iconv('GBK', 'UTF-8', $returnData);
        $res_data = json_decode($returnData,true);
        halt($res_data);
        //array(5) {
        //  ["dealRemark"] => NULL  //失败原因
        //  ["requestId"] => string(15) "501473346187595"//请求流水号
        //  ["result"] => string(5) "00000"//00000：查询成功，交易结果 以 status 字段描述为准；
        //  ["signature"] => string(32) "dce7cb139fc8e3736ff2f855a21aeb08"
        //  ["status"] => string(1) "2"//查询成功，该字段有值
        //0：待复核（通过校验的初
        //始状态，只有付款流程才
        //有）
        //1：已接收（付款复核通过，
        //或提现校验通过）
        //2：成功（付款/提现成功）
        //3：失败（付款/提现失败）
        //4：复核拒绝（付款复核拒
        //绝）
        //5：风控拒绝（风控审核拒
        //绝）
        //6：待财务确认（风控审核
        //通过，等待财务确认）
        //7：财务拒绝（财务人员拒
        //绝）
        //9：已请求（发送给渠道，
        //等待结果）
        //10：已退票（成功付款交易
        //改为失败）
        //}
    }


    /**
     * 代付支付
     * http://p.dinglianshop.cn/api/Sumadf/index?id=1917
     * https://pay.dinglianshop.cn/api/Sumadf/index?id=1917
     */
    public function index(){

        $model = new FfXiafa();
        $id = input("post.id");
        $info = $model::where(["id"=>$id])->find();
        if(!$info){
            return AjaxReturn(0,"dingdanbucunzai");
        }
        if($info["payment"] > 0){
            return AjaxReturn(0,"dingdanbushidakuanzhuangtai");
        }
        $pay_channel_model = new FengfuChannel();
        $pay_channel = $pay_channel_model::where(["id"=>11])->find();
        if (!$pay_channel){
            return AjaxReturn(0,"tongdaobucunzai");
        }
        $url = "https://www.sumapay.com/main/singleTransfer_toTransfer";
        $arr = [
            "requestId"=>$info["order_num"],//请求流水号
            "merchantCode"=>$pay_channel["merchant_id"],//商户编号
            "transferType"=>"1",//付款类型  0：付款到预存账户 1：付款到个人银行账户 2：付款到企业银行账户
            "transToMerCode"=>"",
            "transToMerName"=>"",
            "sum"=>(string)$info["sum"],//付款金额,
            "unionBankNum"=>"",//银行联行号当付款类型为 2 付款到企 业银行账户时，不可为空
            "openBankName"=>"",//开户网点 当付款类型为 2 付款到企 业银行账户时，不可为空
            "openBankProvince"=>"",//开户省 收款方银行开户省当付款类型为 2 付款到企 业银行账户时，不可为空
            "openBankCity"=>"",//开户城市 收款方银行开户市 当付款类型为 2 付款到企 业银行账户时，不可为空
            "accountName"=> $info["accountName"],//账户开户名 收款方账户开户名当付款类型为 1 或 2 时，该字段必输
            "bankCode"=>"",//银行编码 收款方银行编码当付款类型为 2 时，该字段 必输，参考《银行编码表》
            "bankAccount"=>$info["bankAccount"],//银行账户收款方银行账户当付款类型为 1 或 2 时，该字段必输
            "reason"=>"业务提成",//付款原因
            "noticeUrl"=>ym().'/api/notify/sumapaydaifunotify',//异步通知地址
            "refundNoticeUrl"=>"",//退票异步通知地址
            "transferPayType"=>"1",//付款审核类型 0：手工审核 1：自动实时 不传时默认为 0

        ];

        $arr["accountName"] = iconv('UTF-8','GBK',  $arr["accountName"]);
        //dump($arr);
//        $arr = array_map(function($item) {
//            return mb_convert_encoding($item, 'GBK', 'UTF-8');
//        }, $arr);
        //halt($arr);
        //把发送给接口的参数组成字符串：请求流水号()+商户编号()+
        //付 款 类 型 ()+ 收 款 方 商 户 编 码 ()+ 收 款 方 商 户 名 称
        //()+ 付 款 金 额 ()+ 银 行 联 行 号 ()+ 开 户 行
        //()+ 开 户 省 ()+ 开 户 市 ()+ 账 户 名 称
        //()+银行编码()+银行账户()+付款原因()+异
        //步 通 知 地 址 ()+退 票 异 步 通 知 地 址 ()+ 付 款 审 核 类 型
        //（）
        $merKey = $pay_channel["key"];//密钥
        $sbOld = "";
        $sbOld = $sbOld . $arr['requestId'];
        $sbOld = $sbOld . $arr['merchantCode'];
        $sbOld = $sbOld . $arr['transferType'];
        $sbOld = $sbOld . $arr['transToMerCode'];
        $sbOld = $sbOld . $arr['transToMerName'];
        $sbOld = $sbOld . $arr['sum'];
        $sbOld = $sbOld . $arr['unionBankNum'];
        $sbOld = $sbOld . $arr['openBankName'];
        $sbOld = $sbOld . $arr['openBankProvince'];
        $sbOld = $sbOld . $arr['openBankCity'];
        $sbOld = $sbOld . $arr['accountName'];
        $sbOld = $sbOld . $arr['bankCode'];
        $sbOld = $sbOld . $arr['bankAccount'];
        $sbOld = $sbOld . $arr['reason'];
        $sbOld = $sbOld . $arr['noticeUrl'];
        $sbOld = $sbOld . $arr['refundNoticeUrl'];
        $sbOld = $sbOld . $arr['transferPayType'];
        $signatrue = HmacMd5($sbOld, $merKey);//数字签名
        $arr['signature'] = $signatrue;
        //halt($arr['signature']);
        $returnData = send_posts($url, $arr);
        //halt($returnData);
        $returnData = iconv('GBK', 'UTF-8', $returnData);
        $res_data = json_decode($returnData,true);
        //halt($res_data);
        //array(4) {
        //  ["requestId"] => string(15) "501428662699058"
        //  ["result"] => string(5) "00000"
        //  ["signature"] => string(32) "bf73c889ad8cb48f3521011347203305"
        //  ["sum"] => string(5) "15.09"
        //}
        if($res_data["result"] == "00000"){
             $model::where(["id"=>$id])->update(["payment"=>2]);
            return AjaxReturn(1,"ok");
        }else{
            return AjaxReturn(0,$res_data["result"],$res_data);
        }

    }
    /**
     * 代付支付
     * http://p.dinglianshop.cn/api/Sumadf/indexs
     * https://pay.dinglianshop.cn/api/Sumadf/indexs
     */
    public function indexs(){
        $pay_channel_model = new FengfuChannel();
        $pay_channel = $pay_channel_model::where(["id"=>11])->find();
        if (!$pay_channel){
            return AjaxReturn(0,"通道不存在");
        }
        $url = "https://www.sumapay.com/main/singleTransfer_toTransfer";
        $arr = [
            "requestId"=>"502268870667473",//请求流水号
            "merchantCode"=>$pay_channel["merchant_id"],//商户编号
            "transferType"=>"1",//付款类型  0：付款到预存账户 1：付款到个人银行账户 2：付款到企业银行账户
            "transToMerCode"=>"",
            "transToMerName"=>"",
            "sum"=>"3.09",//付款金额,
            "unionBankNum"=>"",//银行联行号当付款类型为 2 付款到企 业银行账户时，不可为空
            "openBankName"=>"",//开户网点 当付款类型为 2 付款到企 业银行账户时，不可为空
            "openBankProvince"=>"",//开户省 收款方银行开户省当付款类型为 2 付款到企 业银行账户时，不可为空
            "openBankCity"=>"",//开户城市 收款方银行开户市 当付款类型为 2 付款到企 业银行账户时，不可为空
            "accountName"=>"刘道平",//账户开户名 收款方账户开户名当付款类型为 1 或 2 时，该字段必输
            "bankCode"=>"",//银行编码 收款方银行编码当付款类型为 2 时，该字段 必输，参考《银行编码表》
            "bankAccount"=>"6226227906988070",//银行账户收款方银行账户当付款类型为 1 或 2 时，该字段必输
            "reason"=>"业务提成",//付款原因
            "noticeUrl"=>ym().'/api/notify/sumapaydaifunotify',//异步通知地址
            "refundNoticeUrl"=>"",//退票异步通知地址
            "transferPayType"=>"1",//付款审核类型 0：手工审核 1：自动实时 不传时默认为 0

        ];
        dump($arr);
        //把发送给接口的参数组成字符串：请求流水号()+商户编号()+
        //付 款 类 型 ()+ 收 款 方 商 户 编 码 ()+ 收 款 方 商 户 名 称
        //()+ 付 款 金 额 ()+ 银 行 联 行 号 ()+ 开 户 行
        //()+ 开 户 省 ()+ 开 户 市 ()+ 账 户 名 称
        //()+银行编码()+银行账户()+付款原因()+异
        //步 通 知 地 址 ()+退 票 异 步 通 知 地 址 ()+ 付 款 审 核 类 型
        //（）
        $merKey = $pay_channel["key"];//密钥
        $sbOld = "";
        $sbOld = $sbOld . $arr['requestId'];
        $sbOld = $sbOld . $arr['merchantCode'];
        $sbOld = $sbOld . $arr['transferType'];
        $sbOld = $sbOld . $arr['transToMerCode'];
        $sbOld = $sbOld . $arr['transToMerName'];
        $sbOld = $sbOld . $arr['sum'];
        $sbOld = $sbOld . $arr['unionBankNum'];
        $sbOld = $sbOld . $arr['openBankName'];
        $sbOld = $sbOld . $arr['openBankProvince'];
        $sbOld = $sbOld . $arr['openBankCity'];
        $sbOld = $sbOld . $arr['accountName'];
        $sbOld = $sbOld . $arr['bankCode'];
        $sbOld = $sbOld . $arr['bankAccount'];
        $sbOld = $sbOld . $arr['reason'];
        $sbOld = $sbOld . $arr['noticeUrl'];
        $sbOld = $sbOld . $arr['refundNoticeUrl'];
        $sbOld = $sbOld . $arr['transferPayType'];
        $signatrue = HmacMd5($sbOld, $merKey);//数字签名
        $arr['signature'] = $signatrue;
        halt($arr['signature']);
        $returnData = send_posts($url, $arr);
        //halt($returnData);
        $returnData = iconv('GBK', 'UTF-8', $returnData);
        $res_data = json_decode($returnData,true);
        halt($res_data);
        //array(4) {
        //  ["requestId"] => string(15) "501428662699058"
        //  ["result"] => string(5) "00000"
        //  ["signature"] => string(32) "bf73c889ad8cb48f3521011347203305"
        //  ["sum"] => string(5) "15.09"
        //}

    }

    /**
     * 商户单笔付款查询接口
     * http://p.dinglianshop.cn/api/Sumadf/singleTransferQuery
     * https://pay.dinglianshop.cn/api/Sumadf/singleTransferQuery
     */
    public function singleTransferQuery(){
        $model = new FfXiafa();
        $id = input("post.id");
        $info = $model::where(["id"=>$id])->find();
        if(!$info){
            return AjaxReturn(0,"dingdanbucunzai");
        }
        if($info["payment"] == 1){
            return AjaxReturn(0,"dingdanbushidakuanzhuangtai");
        }

        $pay_channel_model = new FengfuChannel();
        $pay_channel = $pay_channel_model::where(["id"=>11])->find();
        if (!$pay_channel){
            return AjaxReturn(0,"通道不存在");
        }
        $url = "https://www.sumapay.com/main/singleTransfer_singleTransferQuery";
        $arr = [
            "requestId"=>orderNum(),
            "merchantCode"=>$pay_channel["merchant_id"],//商户编号
            "originalRequestId"=>$info["order_num"]//原订单流水号
        ];
        $merKey = $pay_channel["key"];//密钥

        $sbOld = "";
        $sbOld = $sbOld . $arr['requestId'];
        $sbOld = $sbOld . $arr['merchantCode'];
        $sbOld = $sbOld . $arr['originalRequestId'];
        $signatrue = HmacMd5($sbOld, $merKey);//数字签名
        $arr['signature'] = $signatrue;
        $returnData = send_posts($url, $arr);
        //halt($returnData);
        $returnData = iconv('GBK', 'UTF-8', $returnData);
        $res_data = json_decode($returnData,true);
        if($res_data["result"] =="00000"){
            if($res_data["status"] == "2"){
                $model::where(["id"=>$id])->update(["payment"=>1,"pay_time"=>time()]);
            }
            if ($res_data["status"] == "3"){
                $model::where(["id"=>$id])->update(["payment"=>3,"dealRemark"=>$res_data['dealRemark']]);
            }
            return AjaxReturn(1,"ok",$res_data);
        }else{
            return AjaxReturn(0,"chaxunshibai");
        }
        //halt($res_data);
        //array(5) {
        //  ["dealRemark"] => NULL  //失败原因
        //  ["requestId"] => string(15) "501473346187595"//请求流水号
        //  ["result"] => string(5) "00000"//00000：查询成功，交易结果 以 status 字段描述为准；
        //  ["signature"] => string(32) "dce7cb139fc8e3736ff2f855a21aeb08"
        //  ["status"] => string(1) "2"//查询成功，该字段有值
        //0：待复核（通过校验的初
        //始状态，只有付款流程才
        //有）
        //1：已接收（付款复核通过，
        //或提现校验通过）
        //2：成功（付款/提现成功）
        //3：失败（付款/提现失败）
        //4：复核拒绝（付款复核拒
        //绝）
        //5：风控拒绝（风控审核拒
        //绝）
        //6：待财务确认（风控审核
        //通过，等待财务确认）
        //7：财务拒绝（财务人员拒
        //绝）
        //9：已请求（发送给渠道，
        //等待结果）
        //10：已退票（成功付款交易
        //改为失败）
        //}
    }
    public function singleTransferQuery_(){
        $pay_channel_model = new FengfuChannel();
        $pay_channel = $pay_channel_model::where(["id"=>11])->find();
        if (!$pay_channel){
            return AjaxReturn(0,"通道不存在");
        }
        $url = "https://www.sumapay.com/main/singleTransfer_singleTransferQuery";
        $arr = [
            "requestId"=>orderNum(),
            "merchantCode"=>$pay_channel["merchant_id"],//商户编号
            "originalRequestId"=>"501428662699058"//原订单流水号
        ];
        $merKey = $pay_channel["key"];//密钥

        $sbOld = "";
        $sbOld = $sbOld . $arr['requestId'];
        $sbOld = $sbOld . $arr['merchantCode'];
        $sbOld = $sbOld . $arr['originalRequestId'];
        $signatrue = HmacMd5($sbOld, $merKey);//数字签名
        $arr['signature'] = $signatrue;
        $returnData = send_posts($url, $arr);
        //halt($returnData);
        $returnData = iconv('GBK', 'UTF-8', $returnData);
        $res_data = json_decode($returnData,true);
        halt($res_data);
        //array(5) {
        //  ["dealRemark"] => NULL  //失败原因
        //  ["requestId"] => string(15) "501473346187595"//请求流水号
        //  ["result"] => string(5) "00000"//00000：查询成功，交易结果 以 status 字段描述为准；
        //  ["signature"] => string(32) "dce7cb139fc8e3736ff2f855a21aeb08"
        //  ["status"] => string(1) "2"//查询成功，该字段有值
        //0：待复核（通过校验的初
        //始状态，只有付款流程才
        //有）
        //1：已接收（付款复核通过，
        //或提现校验通过）
        //2：成功（付款/提现成功）
        //3：失败（付款/提现失败）
        //4：复核拒绝（付款复核拒
        //绝）
        //5：风控拒绝（风控审核拒
        //绝）
        //6：待财务确认（风控审核
        //通过，等待财务确认）
        //7：财务拒绝（财务人员拒
        //绝）
        //9：已请求（发送给渠道，
        //等待结果）
        //10：已退票（成功付款交易
        //改为失败）
        //}
    }


    /**
     * 查询余额接口
     * http://p.dinglianshop.cn/api/Sumadf/yuequery
     * https://pay.dinglianshop.cn/api/Sumadf/yuequery
     */
    public function yuequery(){
        $pay_channel_model = new FengfuChannel();
        $pay_channel = $pay_channel_model::where(["id"=>11])->find();
        if (!$pay_channel){
            return AjaxReturn(0,"通道不存在");
        }
        $url = "https://www.sumapay.com/main/MerchantAccountQueryAction_merchantAccountQuery";
        $arr = [
            "requestId"=>orderNum(),//请求流水号
            "merchantCode"=>$pay_channel["merchant_id"],//商户编号
        ];
        $merKey = $pay_channel["key"];//密钥

        $sbOld = "";
        $sbOld = $sbOld . $arr['requestId'];
        $sbOld = $sbOld . $arr['merchantCode'];
        $signatrue = HmacMd5($sbOld, $merKey);//数字签名
        $arr['signature'] = $signatrue;
        $returnData = send_posts($url, $arr);
        //halt($returnData);
        $returnData = iconv('GBK', 'UTF-8', $returnData);
        $res_data = json_decode($returnData,true);
        //halt($res_data);
        //array(10) {
        //  ["balance"] => string(5) "43.62"//账户总余额
        //  ["frozenBalance"] => string(4) "0.00"//账户冻结金额
        //  ["merchantCode"] => string(10) "3710000332"//商户号
        //  ["merchantName"] => string(36) "临沂鼎联网络科技有限公司"
        //  ["prepaidAccountBalance"] => string(4) "0.00"//预付手续费账户余额
        //  ["result"] => string(5) "00000"
        //  ["riskReserveAccountBalance"] => string(1) "0"//风险准备金账户余额
        //  ["signature"] => string(32) "acb86229f8594b38a2159da24ba0f1ee"
        //  ["unsettledBalance"] => string(4) "0.00"//未结金额
        //  ["withdrawAbleBalance"] => string(5) "43.62"//账户可提现金额
        //}
        if($res_data["result"] == "00000"){
            $model = new FfAccount();
            $info = $model::where(["bid"=>11])->find();
            if(!$info){
                //`bid` int(10) DEFAULT '0' COMMENT '商户id',
                //  `balance` decimal(10,2) DEFAULT '0.00' COMMENT '账户总余额',
                //  `frozenBalance` decimal(10,2) DEFAULT '0.00' COMMENT '账户冻结金额',
                //  `withdrawAbleBalance` decimal(10,2) DEFAULT '0.00' COMMENT '账户可提现金额',
                //  `merchantCode` varchar(255) DEFAULT '0' COMMENT '商户号',
                //  `unsettledBalance` decimal(10,2) DEFAULT '0.00' COMMENT '未结算金额',
                $arr = [
                    "bid"=>11,
                    "balance"=>$res_data["balance"],
                    "frozenBalance"=>$res_data["frozenBalance"],
                    "withdrawAbleBalance"=>$res_data["withdrawAbleBalance"],
                    "unsettledBalance"=>$res_data["unsettledBalance"],
                    "merchantCode"=>$pay_channel["merchant_id"]
                ];
                $model->save($arr);
            }else{
                $arr = [
                    "balance"=>$res_data["balance"],
                    "frozenBalance"=>$res_data["frozenBalance"],
                    "withdrawAbleBalance"=>$res_data["withdrawAbleBalance"],
                    "unsettledBalance"=>$res_data["unsettledBalance"],
                ];
                $model::where("id",$info["id"])->update($arr);
            }
            return AjaxReturn(1,"ok",$res_data);
        }else{
            return AjaxReturn(0,"error");
        }

    }

    /**
     * 微信退款
     * http://p.dinglianshop.cn/api/Sumadf/wechattk
     * https://pay.dinglianshop.cn/api/Sumadf/wechattk
     */
    public function wechattk(){
        $pay_channel_model = new FengfuChannel();
        $pay_channel = $pay_channel_model::where(["id"=>11])->find();
        if (!$pay_channel){
            return AjaxReturn(0,"通道不存在");
        }
        $url = 'https://www.sumapay.com/main/Refund_do';
        $arr = [
            "requestId"=>orderNum(),//请求流水号
            "originalRequestId"=>"506887274514828",//期望进行退款的原订单的请求流水编号,
            "tradeProcess"=>$pay_channel["merchant_id"],//商户编号
            "fund"=>"167.98",
            "noticeUrl"=>ym().'/api/notify/fengfutuikuan',//异步通知地址
            "reason"=>"yonghushuochongzhimeidaozhang,geituikuanchuli",//退款原因
            "refundMothed"=>"1",//固定为 1，实时退款(原路返回)
            "remark"=>"t".orderNum()
        ];
        //halt($arr);
        $merKey = $pay_channel["key"];//密钥

        $sbOld = "";
        $sbOld = $sbOld . $arr['requestId'];
        $sbOld = $sbOld . $arr['originalRequestId'];
        $sbOld = $sbOld . $arr['tradeProcess'];
        $sbOld = $sbOld . $arr['fund'];
        $sbOld = $sbOld . $arr['noticeUrl'];
        $sbOld = $sbOld . $arr['remark'];
        $signatrue = HmacMd5($sbOld, $merKey);//数字签名
        $arr['mersignature'] = $signatrue;
        $returnData = send_posts($url, $arr);
        //halt($returnData);
        $returnData = iconv('GBK', 'UTF-8', $returnData);
        $res_data = json_decode($returnData,true);
        halt($res_data);
    }


}