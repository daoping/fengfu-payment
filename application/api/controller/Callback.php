<?php

namespace app\api\controller;

use app\common\model\Agent;
use app\common\model\Order;
use app\common\service\Finance;
use app\common\service\SdCommon;
use think\Controller;

class Callback extends Controller
{
    /**
     * 支付宝h5异步回调
     * http://sd.yapin.shop/api/Callback/szO3X0eiuCHpC1WA
     */
     public function szO3X0eiuCHpC1WA(){
         $data = $_POST;
         db("test")->insert(["add_time"=>time(),"content"=>json_encode($data)]);
         $sign = $data["sign"];
         $resData = json_decode($data["data"],true);
         if($resData["body"]["orderStatus"] != 1){
             return "订单支付失败";
         }
         $orderCode = $resData["body"]["orderCode"];//订单号
         //验签开始
         $datas = stripslashes($data['data']); //支付数据 json转换成数组
         $client = new  SdCommon();
         $verifyFlag = $client->verify($datas, $sign);
         if(!$verifyFlag){
             return "验签失败";
         }
         //验签结束
         //todo 业务逻辑处理
         $model = new Order();
         $order = $model::get(["orderCode"=>$orderCode]);
         if(!$order){
             return AjaxReturn(0,"订单不存在");
         }
         if($order["payment"] == 1){
             return AjaxReturn(0,"订单已支付");
         }
         $order_data = [
             "pay_time"=>time(),
             "payment"=>1,
             "accLogonNo"=>$resData["body"]["accLogonNo"],
             "payOrderCode"=>$resData["body"]["payOrderCode"],
             "bankserial"=>$resData["body"]["bankserial"],
             "totalAmount"=>$resData["body"]["totalAmount"],
         ];
         $model::where("id",$order['id'])->update($order_data);
         //todo异步回调给其他平台  system_order_num
         $agent = Agent::get($order["agent_id"]);
         if ($agent) {
             Agent::where("id", $agent["id"])->setInc("money", $order["money"]);//统计代理总金额
             Agent::where("id", $agent["id"])->setInc("total_money", $order["money"]);//统计代余额
             $agentFianceModel = new Finance();
             $agentFianceModel->agentDay($order['id']);//代理日财务
             //异步回调给平台
             $notify_data = [
                 "order_num" => $order["system_order_num"],
                 "money" => $order["money"],
                 "token" => $agent["key"],
             ];
             if ($order["notify_url"]) {
                 //异步回调给其他平台
                 $this->send_post($order["notify_url"], $notify_data);
                 $model::where('id', $order['id'])->update(["return_status" => 1, "return_time" => time()]);//修改订单状态
             }
         }
         return "respCode=000000";//支付成功后给杉德返回数据
     }

    /**
     * 异步数据解析
     *  http://www.shandepay.com/api/Callback/test
     */
     public function test(){

         $json = '{"extend":"","charset":"UTF-8","data":"{\"head\":{\"version\":\"1.0\",\"respTime\":\"20211104081421\",\"respCode\":\"000000\",\"respMsg\":\"\u6210\u529f\"},\"body\":{\"mid\":\"6888802039772\",\"orderCode\":\"B04847198413394\",\"tradeNo\":\"B04847198413394\",\"clearDate\":\"20211104\",\"totalAmount\":\"000000000003\",\"orderStatus\":\"1\",\"payTime\":\"20211104081421\",\"settleAmount\":\"000000000003\",\"buyerPayAmount\":\"000000000003\",\"discAmount\":\"000000000000\",\"txnCompleteTime\":\"20211104081421\",\"payOrderCode\":\"20211104001319900000000000020606\",\"accLogonNo\":\"778***@qq.com\",\"accNo\":\"208820******7671\",\"midFee\":\"000000000000\",\"extraFee\":\"000000000000\",\"specialFee\":\"000000000000\",\"plMidFee\":\"000000000000\",\"bankserial\":\"862021110422001447675726391492\",\"externalProductCode\":\"00002022\",\"cardNo\":\"208820******7671\",\"creditFlag\":\"\",\"bid\":\"\",\"benefitAmount\":\"000000000000\",\"remittanceCode\":\"\",\"extend\":\"\"}}","sign":"htwHnwKKGTtz\/eEFPsQxSrmTCieYQTGD+zPjY2If1hBRBWMY+8Q3d3crv8H\/pjRNEWZe7cJzfRY5YnhhM\/iY0hMIvGInepCjEI6Aqgor7hWz3x5rFH2da3Xk1kPUAxVoM8WQGU0ZcNWVEzotfbK0AW4MLpUERXYgEUZTtUxgbc0y0lxAehJx\/PI9y3mCt960gL9TJyyftjVPTMHnu7pG17Yi5wUVQ\/cWpjNIWbNSRS4SqPXAB5T8yPZOSjkYFzIOvg2Vwp0pivWLvnkGW\/kIYI+zFsLKXieZzPE8orOIG5ySImjc98wCwHi1EsLoaLFBpmVmJMsxFSYspcTRbh1mUw==","signType":"01"}';
         $data = json_decode($json,true);

     }



    //发送post 数据
     public function  send_post($url, $post_data) {
        $postdata = http_build_query($post_data);
        $options = array(
            'http' => array(
                'method' => 'POST',
                'header' => 'Content-type:application/x-www-form-urlencoded',
                'content' => $postdata,
                'timeout' => 15 * 60 // 超时时间（单位:s）
            )
        );
        $context = stream_context_create($options);
        $result = file_get_contents($url, false, $context);
        return $result;
    }
}