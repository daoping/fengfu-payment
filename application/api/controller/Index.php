<?php

namespace app\api\controller;

use app\common\model\Agent;
use app\common\model\AkOrder;
use app\common\model\CzOrder;
use app\common\model\FwsChannel;
use app\common\model\Goods;
use app\common\model\HcOrder;
use app\common\model\HkChannel;
use app\common\model\HkOrder;
use app\common\model\LaoxieChannel;
use app\common\model\LaoxieOrder;
use app\common\model\Order;

use app\common\model\PayChannel;

use app\common\model\Provider;
use app\common\model\TlOrder;
use app\common\model\TonglianChannel;
use app\common\model\XinshengChannel;
use app\common\model\XsOrder;
use app\common\model\ZhifutongChannel;
use app\common\model\ZhifutongOrder;
use app\common\service\Finance;
use think\Controller;
use think\facade\Env;
use think\facade\Request;
use think\facade\Validate;

class Index extends Controller
{

    /**
     * 金运通微信支付签名
     */
    public function paysign(){
        $data = input("post.");
        $validate = Validate::make([
            'type|支付类型'              => 'require|number',//支付类型 1支付宝 2微信 3快捷
            'order_num|订单号'             => 'require|length:10,40',
            'money|支付金额'                => 'require|float',
            'notify_url|异步通知地址'         => 'require|url',
            'agent_id|代理ID'                => 'require',
        ]);

        if (!$validate->check($data)) {
            return AjaxReturn(0,$validate->getError());
        }
        if($data["money"] < 1){
            return AjaxReturn(0,"金额必须大于一元");
        }
        $agent = Agent::get($data["agent_id"]);
        if(!$agent){
            return AjaxReturn(0,'代理不存在');
        }
        if($agent["is_lock"]){
            return AjaxReturn(0,"代理暂停锁定");
        }
        $arr = [
            'agent_id'              => $data['agent_id'],
            'order_num'             => $data['order_num'],
            'notify_url'            => $data['notify_url'],
            'money'                 =>  $data['money'],
            'type'                  =>  $data['type'],
        ];
        $sign=createSign($arr,$agent["key"]);
        return $sign;
    }
    /**
     * 四方下发接口
     */
    public function pay(){
        $finance = new  Finance();
        $fws_channel_model = new FwsChannel();
        $data = input("post.");
        $validate = Validate::make([
            'type|支付类型'              => 'require|number',//支付类型 1支付宝 2微信 3快捷
            'order_num|订单号'             => 'require|length:10,40',
            'money|支付金额'                => 'require|float',
            'notify_url|异步通知地址'         => 'require|url',
            'agent_id|代理ID'                => 'require',
            'sign|签名'                => 'require|length:32',
        ]);

        if (!$validate->check($data)) {
            return AjaxReturn(0,$validate->getError());
        }
        if($data["money"] < 1){
            return AjaxReturn(0,"金额必须大于一元");
        }
        if($data["money"] > 10000){
            return AjaxReturn(0,"最大单笔10000元");
        }
        $agent = Agent::get($data["agent_id"]);
        if(!$agent){
            return AjaxReturn(0,'代理不存在');
        }
        if($agent["is_lock"]){
            return AjaxReturn(0,"代理暂停锁定");
        }
        $arr = [
            'agent_id'              => $data['agent_id'],
            'order_num'             => $data['order_num'],
            'notify_url'            => $data['notify_url'],
            'money'                 =>  $data['money'],
            'type'                  =>  $data['type'],
        ];
        $sign=createSign($arr,$agent["key"]);
        if($sign != $data['sign']){
            return AjaxReturn(0,"签名错误");
        }

        $model = new Order();
        $info =$model::get(["order_num"=>$data["order_num"]]);
        if($info){
            return AjaxReturn(0,"订单号已存在");
        }
        $pay_money =math_sub($data['money']*100,rand(1,40))/100;//实际支付金额
        //$pay_money = $data['money'];
        $new_data = [
            'order_num'    => $data['order_num'],
            'notify_url'        => $data['notify_url'],
            'money'                => $data['money'],
            'pay_money'                => $pay_money,
            'system_order_num'=>orderNum(),
            'agent_id'=>$data["agent_id"],
            "type"=>$data['type'],
            "app"=>0
        ];
        $res = $model->save($new_data);
        if(!$res){
            return AjaxReturn(0,"下单错误");
        }
        $order_info = $model::where(["order_num"=>$data["order_num"]])->find();

        if($new_data['type'] == 1){
            $pay_channel_model = new PayChannel();
            //此处改造轮训通道
            $pay_channel = $pay_channel_model::where(["open"=>1,"alipay"=>1])->find();
            if (!$pay_channel){
                return AjaxReturn(0,"暂无支付宝支付通道");
            }
            $model::where(["id"=>$order_info["id"]])->update(["pay_channel_id"=>$pay_channel["id"]]);
            $finance->jinexia($order_info['id']);//金额下单日财务统计
            //支付宝支付
            $url = ym()."/api/jytpay/apppayalipayindex?id=".$order_info['id'];
        }else if ($new_data['type'] == 2){
            //微信支付
            $pay_channel_model = new PayChannel();
            //此处改造轮训通道
            $pay_channel = $pay_channel_model::where(["open"=>1,"weixin"=>1])->find();
            if (!$pay_channel){
                return AjaxReturn(0,"暂无支付通道");
            }
            //渠道商
            $qid = $this->lunxunqds($new_data['system_order_num']);
            if($qid < 1){
                return AjaxReturn(0,"暂无渠道商");
            }
            $model::where(["id"=>$order_info["id"]])->update(["pay_channel_id"=>$pay_channel["id"],"qid"=>$qid]);
            //子商户
            $bid = $this->luxunzsh($new_data['system_order_num']);
            if($bid < 1){
                return AjaxReturn(0,"暂无子商户");
            }
            $fws_channel_info = $fws_channel_model::where(["id"=>$bid])->find();
            $model::where(["id"=>$order_info["id"]])->update(["bid"=>$bid,"billMerchantId"=>$fws_channel_info["billMerchantId"]]);
            $finance->jinexia($order_info['id']);//金额下单日财务统计
            $finance->zshdaybuy($order_info['id']);//子商户日财务统计
            $finance->zshbuy($order_info['id']);//子商户总财务统计
            $url = ym()."/api/jytpay/apppaywechatindex?id=".$order_info['id'];//付款url
        }else if ($new_data['type'] == 3){
            //银联扫码
            $pay_channel_model = new PayChannel();
            //此处改造轮训通道
            $pay_channel = $pay_channel_model::where(["open"=>1,"kuaijie"=>1])->find();
            if (!$pay_channel){
                return AjaxReturn(0,"暂无银联支付通道");
            }
            $model::where(["id"=>$order_info["id"]])->update(["pay_channel_id"=>$pay_channel["id"]]);
            $finance->jinexia($order_info['id']);//金额下单日财务统计
            //支付宝支付
            $url = ym()."/api/jytpay/yinlianpay?id=".$order_info['id'];
        }else{
            $url = "";
        }

        return AjaxReturn(1,"创建成功",["url"=>$url]);
    }

    /**
     * app支付下单
     * http://jytpay1.xiaochenpay.com/api/index/apppay
     */
    public function apppay(){
        $finance = new  Finance();
        $fws_channel_model = new FwsChannel();
        $data = input("post.");
        $validate = Validate::make([
            'type|支付类型'              => 'require|number',//支付类型 1支付宝 2微信 3快捷
            'order_num|订单号'             => 'require|length:10,40',
            'money|支付金额'                => 'require|float',
            'notify_url|异步通知地址'         => 'require|url',
            'agent_id|代理ID'                => 'require'
        ]);

        if (!$validate->check($data)) {
            return AjaxReturn(0,$validate->getError());
        }
        if($data["money"] < 1){
            return AjaxReturn(0,"金额必须大于一元");
        }



        $model = new Order();
        $info =$model::get(["order_num"=>$data["order_num"]]);
        if($info){
            return AjaxReturn(0,"订单号已存在");
        }
        $pay_money =math_sub($data['money']*100,rand(1,80))/100;//实际支付金额
        $new_data = [
            'order_num'    => $data['order_num'],
            'notify_url'        => $data['notify_url'],
            'money'                => $data['money'],
            'pay_money'                => $pay_money,
            'system_order_num'=>orderNum(),
            'agent_id'=>$data["agent_id"],
            "type"=>$data['type'],
            "app"=>1
        ];
        $res = $model->save($new_data);
        if(!$res){
            return AjaxReturn(0,"下单错误");
        }
        $order_info = $model::where(["order_num"=>$data["order_num"]])->find();

        if($new_data['type'] == 1){
            $pay_channel_model = new PayChannel();
            //此处改造轮训通道
            $pay_channel = $pay_channel_model::where(["open"=>1,"alipay"=>1])->find();
            if (!$pay_channel){
                return AjaxReturn(0,"暂无支付宝支付通道");
            }
            $model::where(["id"=>$order_info["id"]])->update(["pay_channel_id"=>$pay_channel["id"]]);
            $finance->jinexia($order_info['id']);//金额下单日财务统计
            //支付宝支付
            $url = ym()."/api/jytpay/apppayalipayindex?id=".$order_info['id'];
        }else if ($new_data['type'] == 2){
            //微信支付
            $pay_channel_model = new PayChannel();
            //此处改造轮训通道
            $pay_channel = $pay_channel_model::where(["open"=>1,"weixin"=>1])->find();
            if (!$pay_channel){
                return AjaxReturn(0,"暂无支付通道");
            }
            //渠道商
            $qid = $this->lunxunqds($new_data['system_order_num']);
            if($qid < 1){
                return AjaxReturn(0,"暂无渠道商");
            }
            $model::where(["id"=>$order_info["id"]])->update(["pay_channel_id"=>$pay_channel["id"],"qid"=>$qid]);
            //子商户
            $bid = $this->luxunzsh($new_data['system_order_num']);
            if($bid < 1){
                return AjaxReturn(0,"暂无子商户");
            }
            $fws_channel_info = $fws_channel_model::where(["id"=>$bid])->find();
            $model::where(["id"=>$order_info["id"]])->update(["bid"=>$bid,"billMerchantId"=>$fws_channel_info["billMerchantId"]]);
            $finance->jinexia($order_info['id']);//金额下单日财务统计
            $finance->zshdaybuy($order_info['id']);//子商户日财务统计
            $finance->zshbuy($order_info['id']);//子商户总财务统计
            $url = ym()."/api/jytpay/apppaywechatindex?id=".$order_info['id'];//付款url
        }else if ($new_data['type'] == 3){
            //银联扫码
            $pay_channel_model = new PayChannel();
            //此处改造轮训通道
            $pay_channel = $pay_channel_model::where(["open"=>1,"kuaijie"=>1])->find();
            if (!$pay_channel){
                return AjaxReturn(0,"暂无银联支付通道");
            }
            $model::where(["id"=>$order_info["id"]])->update(["pay_channel_id"=>$pay_channel["id"]]);
            $finance->jinexia($order_info['id']);//金额下单日财务统计
            //支付宝支付
            $url = ym()."/api/jytpay/yinlianpay?id=".$order_info['id'];
        }else{
            $url = "";
        }

        return AjaxReturn(1,"创建成功",["url"=>$url]);
    }




    /**
     * 金运通微信支付
     * /api/index/juypay
     */
    public function juypay_(){
        $finance = new  Finance();
        $data = input("post.");
        $validate = Validate::make([
            'type|支付类型'              => 'require|number',//支付类型 1支付宝 2微信 3快捷
            'order_num|订单号'             => 'require|length:10,40',
            'money|支付金额'                => 'require|float',
            'notify_url|异步通知地址'         => 'require|url',
            'agent_id|代理ID'                => 'require',
            'sign|签名'                => 'require|length:32',
        ]);

        if (!$validate->check($data)) {
            return AjaxReturn(0,$validate->getError());
        }
        if($data["money"] < 1){
            return AjaxReturn(0,"金额必须大于一元");
        }
        $agent = Agent::get($data["agent_id"]);
        if(!$agent){
            return AjaxReturn(0,'代理不存在');
        }
        if($agent["is_lock"]){
            return AjaxReturn(0,"代理暂停锁定");
        }
        $arr = [
            'agent_id'              => $data['agent_id'],
            'order_num'             => $data['order_num'],
            'notify_url'            => $data['notify_url'],
            'money'                 =>  $data['money'],
            'type'                  =>  $data['type'],
        ];
        $sign=createSign($arr,$agent["key"]);
        if($sign != $data['sign']){
            return AjaxReturn(0,"签名错误");
        }
        $model = new Order();
        $info =$model::get(["order_num"=>$data["order_num"]]);
        if($info){
            return AjaxReturn(0,"订单号已存在");
        }
        $pay_money =math_sub($data['money']*100,rand(2,20))/100;//实际支付金额
        //$pay_money =$data['money'];

        $new_data = [
            'order_num'    => $data['order_num'],
            'notify_url'        => $data['notify_url'],
            'money'                => $data['money'],
            'pay_money'                => $pay_money,
            'system_order_num'=>orderNum(),
            'agent_id'=>$agent["id"],
            "type"=>$data['type']
        ];
        $res = $model->save($new_data);
        if(!$res){
            return AjaxReturn(0,"下单错误");
        }
        $order_info = $model::where(["order_num"=>$data["order_num"]])->find();
        $pay_channel_model = new PayChannel();
        //此处改造轮训通道
        $pay_channel = $pay_channel_model::where(["open"=>1,"weixin"=>1])->find();
        if (!$pay_channel){
            return AjaxReturn(0,"暂无支付通道");
        }
        $fws_channel_model = new FwsChannel();
        $fws_channel_list = $fws_channel_model::where(["open"=>1,"cid"=>$pay_channel['id']])->select();
        if(count($fws_channel_list) < 1){
            return AjaxReturn(0,"子商户不存在");
            //return ;
        }
        if(count($fws_channel_list) == 1){
            //只有一条通道
            $fws_channel_id=$fws_channel_list[0]["id"];
        }else{
            //多条通道进行轮循
            $last_id = $order_info["id"]-1;
            $last_order = $model::where(["id"=>$last_id])->find();//查询最后一条订单记录
            if(!$last_order && $last_order["bid"] < 1){
                $fws_channel_id = $fws_channel_list[0]["id"];
            }else{
                $last_store = $fws_channel_model::where(["open"=>1,"cid"=>$pay_channel['id']])->order("id desc")->find();//最后一条通道
                if($last_order["bid"] == $last_store["id"]){
                    $fws_channel_id = $fws_channel_list[0]["id"];
                }else{
                    $arr = $fws_channel_model::where(["open"=>1,"cid"=>$pay_channel['id']])->column("id");
                    if(in_array($last_order["bid"],$arr)){
                        $count = count($fws_channel_list);
                        foreach ($fws_channel_list as $k=>$v){
                            if($last_order["bid"] == $v["id"]){
                                //最后一个通道
                                if($k+1 == $count){
                                    $fws_channel_id = $fws_channel_list[0]["id"];
                                }else{
                                    $fws_channel_id = $fws_channel_list[$k+1]["id"];
                                }
                            }
                        }
                    }else{
                        $fws_channel_id = $fws_channel_list[0]["id"];
                    }
                }
            }
        }
        $fws_channel_info = $fws_channel_model::where(["id"=>$fws_channel_id])->find();
        $model::where(["id"=>$order_info["id"]])->update(["pay_channel_id"=>$pay_channel["id"],"bid"=>$fws_channel_id,"billMerchantId"=>$fws_channel_info["billMerchantId"]]);
        $url = ym()."/api/jytpay/pay?id=".$order_info['id'];
        $finance->jinexia($order_info['id']);//金额下单日财务统计
        $finance->zshdaybuy($order_info['id']);//子商户日财务统计
        $finance->zshbuy($order_info['id']);//子商户总财务统计
        //todo 预警连续30单没有付款关闭
//        $yj = $finance->orderyj($fws_channel_id);//子商户id
//        if($yj){
//            return AjaxReturn(0,"已关闭");
//        }
        return AjaxReturn(1,"创建成功",["url"=>$url]);
    }
    /**
     * 批量测试商户支付轮训
     * http://pay1.2021621.com/api/index/test
     */
    public function test(){
        $finance = new  Finance();
        $fws_channel_model = new FwsChannel();
        $model = new Order();


        $new_data = [
            'order_num'    => orderNum(),
            'notify_url'        => "https://www.baidu.com",
            'money'                => 100,
            'pay_money'                => rand(1,99),
            'system_order_num'=>orderNum(),
            'agent_id'=>25,
            "type"=>2
        ];
        $res = $model->save($new_data);
        if(!$res){
            return AjaxReturn(0,"下单错误");
        }
        $order_info = $model::where(["order_num"=>$new_data["order_num"]])->find();
        $pay_channel_model = new PayChannel();
        //此处改造轮训通道
        $pay_channel = $pay_channel_model::where(["open"=>1,"weixin"=>1])->find();
        if (!$pay_channel){
            return AjaxReturn(0,"暂无支付通道");
        }
        //渠道商
        $qid = $this->lunxunqds($new_data['system_order_num']);
        //halt($qid);
        if($qid < 1){
            return AjaxReturn(0,"暂无渠道商");
        }
        $model::where(["id"=>$order_info["id"]])->update(["pay_channel_id"=>$pay_channel["id"],"qid"=>$qid]);
        //子商户
        $bid = $this->luxunzsh($new_data['system_order_num']);
        if($bid < 1){
            return AjaxReturn(0,"暂无子商户");
        }
        $fws_channel_info = $fws_channel_model::where(["id"=>$bid])->find();
        $model::where(["id"=>$order_info["id"]])->update(["bid"=>$bid,"billMerchantId"=>$fws_channel_info["billMerchantId"]]);
        $url = ym()."/api/jytpay/pay?id=".$order_info['id'];
        $finance->jinexia($order_info['id']);//金额下单日财务统计
        $finance->zshdaybuy($order_info['id']);//子商户日财务统计
        $finance->zshbuy($order_info['id']);//子商户总财务统计
        //todo 预警连续30单没有付款关闭
        $yj = $finance->orderyj($bid);//子商户id
        if($yj){
            return AjaxReturn(0,"已关闭");
        }
        return AjaxReturn(1,"创建成功",["url"=>$url]);
    }

    /**
     * 金运通微信支付
     * /api/index/juypay
     */
    public function juypay(){
        $finance = new  Finance();
        $fws_channel_model = new FwsChannel();
        $data = input("post.");
        $validate = Validate::make([
            'type|支付类型'              => 'require|number',//支付类型 1支付宝 2微信 3快捷
            'order_num|订单号'             => 'require|length:10,40',
            'money|支付金额'                => 'require|float',
            'notify_url|异步通知地址'         => 'require|url',
            'agent_id|代理ID'                => 'require',
            'sign|签名'                => 'require|length:32',
        ]);

        if (!$validate->check($data)) {
            return AjaxReturn(0,$validate->getError());
        }
        if($data["money"] < 1){
            return AjaxReturn(0,"金额必须大于一元");
        }
        $agent = Agent::get($data["agent_id"]);
        if(!$agent){
            return AjaxReturn(0,'代理不存在');
        }
        if($agent["is_lock"]){
            return AjaxReturn(0,"代理暂停锁定");
        }
        $arr = [
            'agent_id'              => $data['agent_id'],
            'order_num'             => $data['order_num'],
            'notify_url'            => $data['notify_url'],
            'money'                 =>  $data['money'],
            'type'                  =>  $data['type'],
        ];
        $sign=createSign($arr,$agent["key"]);
        if($sign != $data['sign']){
            return AjaxReturn(0,"签名错误");
        }
        $model = new Order();
        $info =$model::get(["order_num"=>$data["order_num"]]);
        if($info){
            return AjaxReturn(0,"订单号已存在");
        }
        $pay_money =math_sub($data['money']*100,rand(2,20))/100;//实际支付金额
        //$pay_money =$data['money'];

        $new_data = [
            'order_num'    => $data['order_num'],
            'notify_url'        => $data['notify_url'],
            'money'                => $data['money'],
            'pay_money'                => $pay_money,
            'system_order_num'=>orderNum(),
            'agent_id'=>$agent["id"],
            "type"=>$data['type']
        ];
        $res = $model->save($new_data);
        if(!$res){
            return AjaxReturn(0,"下单错误");
        }
        $order_info = $model::where(["order_num"=>$data["order_num"]])->find();
        $pay_channel_model = new PayChannel();
        //此处改造轮训通道
        $pay_channel = $pay_channel_model::where(["open"=>1,"weixin"=>1])->find();
        if (!$pay_channel){
            return AjaxReturn(0,"暂无支付通道");
        }
        //渠道商
        $qid = $this->lunxunqds($new_data['system_order_num']);
        if($qid < 1){
            return AjaxReturn(0,"暂无渠道商");
        }
        $model::where(["id"=>$order_info["id"]])->update(["pay_channel_id"=>$pay_channel["id"],"qid"=>$qid]);
        //子商户
        $bid = $this->luxunzsh($new_data['system_order_num']);
        if($bid < 1){
            return AjaxReturn(0,"暂无子商户");
        }
        $fws_channel_info = $fws_channel_model::where(["id"=>$bid])->find();
        $model::where(["id"=>$order_info["id"]])->update(["bid"=>$bid,"billMerchantId"=>$fws_channel_info["billMerchantId"]]);
        $url = ym()."/api/jytpay/pay?id=".$order_info['id'];
        $finance->jinexia($order_info['id']);//金额下单日财务统计
        $finance->zshdaybuy($order_info['id']);//子商户日财务统计
        $finance->zshbuy($order_info['id']);//子商户总财务统计
        //todo 预警连续30单没有付款关闭
//        $yj = $finance->orderyj($bid);//子商户id
//        if($yj){
//            return AjaxReturn(0,"已关闭");
//        }
        return AjaxReturn(1,"创建成功",["url"=>$url]);
    }
    /**
     * 轮巡渠道商
     * http://pay1.2021621.com/api/index/lunxunqds
     */
    public function lunxunqds($system_order_num){
        $orderModel = new Order();
        $order =  $orderModel::where(["system_order_num"=>$system_order_num])->find();
        //dump($order);
        $qds_model = new  Provider();
        $list = $qds_model::where(["open"=>1])->select();
        //halt($list);
        $count = count($list);
        //halt($count);
        if($count < 1){
            return 0;
        }
        //剩下最后一个渠道商
        if($count == 1){
            return $list[0]["id"];
        }
        $last_id = $order['id'] - 1;
        $last_order = $orderModel::where(["id"=>$last_id])->find();//查询最后一条订单记录
        if(!$last_order || $last_order["qid"] < 1){
            $qid = $list[0]["id"];
            return $qid;
        }else{
            $last_qds = $qds_model::where(["open"=>1])->order("id desc")->find();//最后一条渠道商
            if($last_order["qid"] == $last_qds["id"]){
                $qid = $list[0]["id"];//切换到第一条通道
                return $qid;
            }else{
                $arr = $qds_model::where(["open"=>1])->column("id");

                if(in_array($last_order["qid"],$arr)){
                    $count = count($list);
                    foreach ($arr as $k=>$v){
                        if($last_order["qid"] == $v){
                            if($k+1 == $count){
                                $qid =  $list[0]["id"];
                            }else{
                                $qid = $list[$k+1]["id"];
                            }
                        }
                    }
                    //dump(666);
                    return $qid;
                    //dump(555);
                }else{
                    $qid = $list[0]["id"];
                    //dump(666);
                    return $qid;
                }
            }
            //halt($qid);
        }
        return 0;

    }

    /**
     * 轮巡子商户
     * http://pay1.2021621.com/api/index/luxunzsh
     */
    public function luxunzsh($system_order_num){
//    public function luxunzsh(){
//        $system_order_num = "901502765108531";
        $orderModel = new Order();
        $order =  $orderModel::where(["system_order_num"=>$system_order_num])->find();
        $zsh_model = new FwsChannel();
        $list = $zsh_model::where(["open"=>1,"qid"=>$order["qid"]])->select();
        $count = count($list);
        if($count < 1){
            return 0;
        }
        //剩下最后一个渠道商
        if($count == 1){
            return $list[0]["id"];
        }

        if(count($list) == 1){
            //只有一条通道
            $bid=$list[0]["id"];
            return $bid;
        }else{
            //多条通道进行轮循

            $last_order = $orderModel::where(["qid"=>$order["qid"]])->where("bid",">",0)->whereTime('create_time', 'today')->order("id desc")->find();//查询最后一条订单记录
            if(!$last_order || $last_order["bid"] < 1){
                $bid = $list[0]["id"];
                return $bid;
            }else{
                //$bid = 0;
                $last_store = $zsh_model::where(["open"=>1,"qid"=>$order['qid']])->order("id desc")->find();//最后一条通道
                if($last_order["bid"] == $last_store["id"]){
                    $bid = $list[0]["id"];
                    return $bid;
                }else{
                    $arr = $zsh_model::where(["open"=>1,"qid"=>$order['qid']])->column("id");
                    if(in_array($last_order["bid"],$arr)){
                        $count = count($list);
                        foreach ($arr as $k=>$v){
                            if($last_order["bid"] == $v){
                                //最后一个通道
                                if($k+1 == $count){
                                    $bid = $list[0]["id"];
                                }else{
                                    $bid = $list[$k+1]["id"];
                                }
                            }
                        }
                        return $bid;
                    }else{
                        $bid = $list[0]["id"];
                        return $bid;
                    }
                }
                //return $bid;
            }
        }
        return 0;

    }


    /**
     * 金运通微信支付签名
     */
    public function juypaysign(){
        $data = input("post.");
        $validate = Validate::make([
            'type|支付类型'              => 'require|number',//支付类型 1支付宝 2微信 3快捷
            'order_num|订单号'             => 'require|length:10,40',
            'money|支付金额'                => 'require|float',
            'notify_url|异步通知地址'         => 'require|url',
            'agent_id|代理ID'                => 'require',
        ]);

        if (!$validate->check($data)) {
            return AjaxReturn(0,$validate->getError());
        }
        if($data["money"] < 1){
            return AjaxReturn(0,"金额必须大于一元");
        }
        $agent = Agent::get($data["agent_id"]);
        if(!$agent){
            return AjaxReturn(0,'代理不存在');
        }
        if($agent["is_lock"]){
            return AjaxReturn(0,"代理暂停锁定");
        }
        $arr = [
            'agent_id'              => $data['agent_id'],
            'order_num'             => $data['order_num'],
            'notify_url'            => $data['notify_url'],
            'money'                 =>  $data['money'],
            'type'                  =>  $data['type'],
        ];
        $sign=createSign($arr,$agent["key"]);
        return $sign;
    }





    /********************************************新生支付**********************************************************/

    /**
     * 新生支付拉单
     * 本地 : http://pay.cxlaimeng.cn/api/index/xinshengpay
     * 线上: https://pay.cxlaimeng.com/api/index/xinshengpay
     */
    public function xinshengpay()
    {
        // 在入口文件index.php中设置
        $finance = new  Finance();
        $data = input("post.");
        $validate = Validate::make([
            'type|支付类型'              => 'require|number',//支付类型 1支付宝 2微信 3 云闪付
            'order_num|订单号'             => 'require|length:10,40',
            'money|支付金额'                => 'require|float',
            'notify_url|异步通知地址'         => 'require|url',
            'agent_id|代理ID'                => 'require',
            'sign|签名'                => 'require|length:32',
            "bid|通道id"  =>'require|number'
        ]);

        if (!$validate->check($data)) {
            return AjaxReturn(0,$validate->getError());
        }
        if($data["money"] < 1){
            return AjaxReturn(0,"金额必须大于一元");
        }
//        if($data["money"] > 5000){
//            return AjaxReturn(0,"金额必须小于5000元");
//        }
        $agent = Agent::get($data["agent_id"]);
        if(!$agent){
            return AjaxReturn(0,'商户不存在');
        }
        if($agent["is_lock"]){
            return AjaxReturn(0,"商户暂停锁定");
        }
        $arr = [
            'agent_id'              => $data['agent_id'],
            'order_num'             => $data['order_num'],
            'notify_url'            => $data['notify_url'],
            'money'                 =>  $data['money'],
            'type'                  =>  $data['type'],
            'bid'                  =>  $data['bid'],
        ];
        $sign=createSign($arr,$agent["key"]);
        if($sign != $data['sign']){
            return AjaxReturn(0,"签名错误");
        }
        $model = new XsOrder();
        $info =$model::get(["order_num"=>$data["order_num"]]);
        if($info){
            return AjaxReturn(0,"订单号已存在");
        }

        //$pay_money =math_sub($data['money']*100,rand(3,30))/100;//实际支付金额
        $pay_money =$data['money'];

        $new_data = [
            'order_num'    => $data['order_num'],
            'notify_url'        => $data['notify_url'],
            'money'                => $data['money'],
            'pay_money'                => $pay_money,
            'system_order_num'=>orderNum(),
            'agent_id'=>$agent["id"],
            "type"=>$data['type'],
            //"bid"=>$data['bid'],
        ];
        $res = $model->save($new_data);
        if(!$res){
            return AjaxReturn(0,"下单错误");
        }
        $order_info = $model::where(["order_num"=>$data["order_num"]])->find();
        $pay_channel_model = new XinshengChannel();
        //此处改造轮训通道
        if($data['type'] == 1){
            $pay_channel = $pay_channel_model::where(["open"=>1,"weixin"=>1,"id"=>$data['bid']])->find();
            if (!$pay_channel){
                return AjaxReturn(0,"暂无微信通道");
            }
        }elseif ($data['type'] == 2){
            $pay_channel = $pay_channel_model::where(["open"=>1,"alipay"=>1,"id"=>$data['bid']])->find();
            if (!$pay_channel){
                return AjaxReturn(0,"暂无支付宝通道");
            }
        }else if ($data['type'] == 3){
            $pay_channel = $pay_channel_model::where(["open"=>1,"kuaijie"=>1,"id"=>$data['bid']])->find();
            if (!$pay_channel){
                return AjaxReturn(0,"暂无银联通道");
            }
        }

        //渠道商
        $model::where(["id"=>$order_info["id"]])->update(["bid"=>$pay_channel["id"],"merId"=>$pay_channel['merId']]);
//        if($data['type'] == 1){
//            $url = ym()."/api/sumapay/wechat?id=".$order_info['id'];
//        }else{
//            $url = ym()."/api/sumapay/aliapy?id=".$order_info['id'];
//        }

        $finance->jinexiaxs($order_info['id']);//金额下单日财务统计
        //todo 预警连续30单没有付款关闭
//        $yj = $finance->orderyj($bid);//子商户id
//        if($yj){
//            return AjaxReturn(0,"已关闭");
//        }
        //$url = ym().'/api/tonglianx/tonglianpay?id='.$order_info['id'];
        if($data['type'] == 3){
            $url = ym().'/api/Xspay/yinlian?id='.$order_info['id'];
            return AjaxReturn(1,"ok",["url"=>$url,'system_order_num'=>$new_data['system_order_num']]);
        }else{
            $url = ym().'/api/Xspay/pay?id='.$order_info['id'];
            return AjaxReturn(1,"ok",["url"=>$url,'system_order_num'=>$new_data['system_order_num']]);
        }

    }

    /**
     * 新生支付密钥
     * 本地 :http://pay.cxlaimeng.cn/api/index/xinshengpaysign
     * 线上: https://pay.cxlaimeng.com/api/index/xinshengpaysign
     */
    public function xinshengpaysign()
    {

        $data = input("post.");
        $validate = Validate::make([
            'type|支付类型'              => 'require|number',//支付类型 1支付宝 2微信 3快捷
            'order_num|订单号'             => 'require|length:10,40',
            'money|支付金额'                => 'require|float',
            'notify_url|异步通知地址'         => 'require|url',
            'agent_id|代理ID'                => 'require',
            'bid'                  =>  $data['bid'],
        ]);

        if (!$validate->check($data)) {
            return AjaxReturn(0,$validate->getError());
        }
        if($data["money"] < 1){
            return AjaxReturn(0,"金额必须大于一元");
        }
        $agent = Agent::get($data["agent_id"]);
        if(!$agent){
            return AjaxReturn(0,'代理不存在');
        }
        if($agent["is_lock"]){
            return AjaxReturn(0,"代理暂停锁定");
        }
        $arr = [
            'agent_id'              => $data['agent_id'],
            'order_num'             => $data['order_num'],
            'notify_url'            => $data['notify_url'],
            'money'                 =>  $data['money'],
            'type'                  =>  $data['type'],
            'bid'                  =>  $data['bid'],
        ];
        $sign=createSign($arr,$agent["key"]);
        return $sign;
    }




    /*****************************************大拿支付宝****************************************************/



    /**
     *  同行支付密钥
     * http://pay1.lnxinmeng.xyz/api/index/akpay
     * https://p.huiming888.xyz/api/index/akpay
     *
     */
    public function  akpaysign(){
        if(Request::isPost()){
            $data = input("post.");
            $validate = Validate::make([

                'order_num|订单号'             => 'require|length:10,40',
                'money|支付金额'                => 'require|float',
                'notify_url|异步通知地址'         => 'require|url',
                'agent_id|代理ID'                => 'require',
            ]);

            if (!$validate->check($data)) {
                return AjaxReturn(0,$validate->getError());
            }


            if($data["money"] < 1){
                return AjaxReturn(0,"金额必须大于一元");
            }
            $agent = Agent::get($data["agent_id"]);
            if(!$agent){
                return AjaxReturn(0,'商户不存在');
            }
            if($agent["is_lock"]){
                return AjaxReturn(0,"商户暂停锁定");
            }
            $arr = [
                'agent_id'              => $data['agent_id'],
                'order_num'             => $data['order_num'],
                'notify_url'       => $data['notify_url'],
                'money'                =>  $data['money'],

            ];
            $sign=createSign($arr,$agent["key"]);
            return $sign;
        }
    }






    /********************************************海科支付**********************************************************/

    /**
     * 海科支付拉单
     * 本地 : http://pay.cxlaimeng.cn/api/index/haikepay
     * 线上:  https://p.cxlaimeng.com/api/index/haikepay
     */
    public function haikepay()
    {
        // 在入口文件index.php中设置
        $data = input("post.");
        $validate = Validate::make([
            'type|支付类型'               => 'require|number',//支付类型 1微信 2支付宝 3 云闪付
            'order_num|订单号'            => 'require|length:10,40',
            'money|支付金额'              => 'require|float',
            'notify_url|异步通知地址'     => 'require|url',
            'agent_id|代理ID'             => 'require',
            'sign|签名'                   => 'require|length:32',
            "bid|通道id"                  =>'require|number'
        ]);

        if (!$validate->check($data)) {
            return AjaxReturn(0,$validate->getError());
        }
        if($data["money"] < 100){
            return AjaxReturn(0,"最少支付100元");
        }
        if($data["money"] > 2000){
            return AjaxReturn(0,"单笔金额最大2000元");
        }
        $agent = Agent::get($data["agent_id"]);
        if(!$agent){
            return AjaxReturn(0,'商户不存在');
        }
        if($agent["is_lock"]){
            return AjaxReturn(0,"商户暂停锁定");
        }
        $arr = [
            'agent_id'              => $data['agent_id'],
            'order_num'             => $data['order_num'],
            'notify_url'            => $data['notify_url'],
            'money'                 =>  $data['money'],
            'type'                  =>  $data['type'],
            'bid'                  =>  $data['bid'],
        ];
        $sign=createSign($arr,$agent["key"]);
        if($sign != $data['sign']){
            return AjaxReturn(0,"签名错误");
        }
        $model = new HkOrder();
        $info =$model::get(["order_num"=>$data["order_num"]]);
        if($info){
            return AjaxReturn(0,"订单号已存在");
        }
        //判断支付通道
        $pay_channel_model = new HkChannel();
        //此处改造轮训通道
        if($data['type'] == 1){
            $pay_channel = $pay_channel_model::where(["open"=>1,"weixin"=>1,"id"=>$data['bid']])->find();
            if (!$pay_channel){
                return AjaxReturn(0,"暂无微信通道");
            }
        }elseif ($data['type'] == 2){
            $pay_channel = $pay_channel_model::where(["open"=>1,"alipay"=>1,"id"=>$data['bid']])->find();
            if (!$pay_channel){
                return AjaxReturn(0,"暂无支付宝通道");
            }
        }else if ($data['type'] == 3){
            $pay_channel = $pay_channel_model::where(["open"=>1,"kuaijie"=>1,"id"=>$data['bid']])->find();
            if (!$pay_channel){
                return AjaxReturn(0,"暂无银联通道");
            }
        }
        if ($data['money'] > 100){
             $pay_money =math_sub($data['money']*100,rand(3,50))/100;//实际支付金额
        }else{
            $pay_money =$data['money'];
        }
        //$pay_money =math_sub($data['money']*100,rand(3,30))/100;//实际支付金额


        $new_data = [
            'order_num'    => $data['order_num'],
            'notify_url'        => $data['notify_url'],
            'money'                => $data['money'],
            'pay_money'                => $pay_money,
            'system_order_num'=>orderNum(),
            'agent_id'=>$agent["id"],
            "type"=>$data['type'],
            "bid"=>$pay_channel["id"],
            "mchid"=>$pay_channel['mchid'],
            "appid"=>$pay_channel['appid']
        ];
        $res = $model->save($new_data);
        if(!$res){
            return AjaxReturn(0,"下单错误");
        }
        $order_info = $model::where(["order_num"=>$data["order_num"]])->find();


        //渠道商
        $model::where(["id"=>$order_info["id"]])->update([]);

        if($data['type'] == 3){
            return AjaxReturn(0,"暂无云闪付");
        }else if($data['type'] == 2){
            $url = ym().'/api/haikepay/haikepay?id='.$order_info['id'];
            return AjaxReturn(1,"ok",["url"=>$url,'order_no'=>$new_data['system_order_num']]);
        }else {
            return AjaxReturn(0,"暂无微信通道");
        }

    }

    /**
     * 海科支付密钥
     * 本地   http://pay.cxlaimeng.cn/api/index/haikepaysign
     * 线上: https://p.cxlaimeng.com
     */
    public function haikepaysign()
    {

        $data = input("post.");
        $validate = Validate::make([
            'type|支付类型'              => 'require|number',//支付类型 1微信 2支付宝 3云闪付
            'order_num|订单号'             => 'require|length:10,40',
            'money|支付金额'                => 'require|float',
            'notify_url|异步通知地址'         => 'require|url',
            'agent_id|代理ID'                => 'require',
            'bid'                  =>  $data['bid'],
        ]);

        if (!$validate->check($data)) {
            return AjaxReturn(0,$validate->getError());
        }
        if($data["money"] < 1){
            return AjaxReturn(0,"金额必须大于一元");
        }
        $agent = Agent::get($data["agent_id"]);
        if(!$agent){
            return AjaxReturn(0,'代理不存在');
        }
        if($agent["is_lock"]){
            return AjaxReturn(0,"代理暂停锁定");
        }
        $arr = [
            'agent_id'              => $data['agent_id'],
            'order_num'             => $data['order_num'],
            'notify_url'            => $data['notify_url'],
            'money'                 =>  $data['money'],
            'type'                  =>  $data['type'],
            'bid'                  =>  $data['bid'],
        ];
        $sign=createSign($arr,$agent["key"]);
        return $sign;
    }

    //https://p.cxlaimeng.com/api/index/haikepayapp
    public function haikepayapp()
    {
        // 在入口文件index.php中设置
        $data = input("post.");
        $validate = Validate::make([
            'type|支付类型'               => 'require|number',//支付类型 1微信 2支付宝 3 云闪付
            'order_num|订单号'            => 'require|length:10,40',
            'money|支付金额'              => 'require|float',
            'notify_url|异步通知地址'     => 'require|url',
            'agent_id|代理ID'             => 'require',
            "bid|通道id"                  =>'require|number'
        ]);

        if (!$validate->check($data)) {
            return AjaxReturn(0,$validate->getError());
        }
        if($data["money"] < 100){
            return AjaxReturn(0,"最少支付100元");
        }
        if($data["money"] > 2000){
            return AjaxReturn(0,"单笔金额最大2000元");
        }
        $agent = Agent::get($data["agent_id"]);
        if(!$agent){
            return AjaxReturn(0,'商户不存在');
        }
        if($agent["is_lock"]){
            return AjaxReturn(0,"商户暂停锁定");
        }

        $model = new HkOrder();
        $info =$model::get(["order_num"=>$data["order_num"]]);
        if($info){
            return AjaxReturn(0,"订单号已存在");
        }
        //判断支付通道
        $pay_channel_model = new HkChannel();
        //此处改造轮训通道
        if($data['type'] == 1){
            $pay_channel = $pay_channel_model::where(["open"=>1,"weixin"=>1,"id"=>$data['bid']])->find();
            if (!$pay_channel){
                return AjaxReturn(0,"暂无微信通道");
            }
        }elseif ($data['type'] == 2){
            $pay_channel = $pay_channel_model::where(["open"=>1,"alipay"=>1,"id"=>$data['bid']])->find();
            if (!$pay_channel){
                return AjaxReturn(0,"暂无支付宝通道");
            }
        }else if ($data['type'] == 3){
            $pay_channel = $pay_channel_model::where(["open"=>1,"kuaijie"=>1,"id"=>$data['bid']])->find();
            if (!$pay_channel){
                return AjaxReturn(0,"暂无银联通道");
            }
        }
        if ($data['money'] > 100){
            $pay_money =math_sub($data['money']*100,rand(3,50))/100;//实际支付金额
        }else{
            $pay_money =$data['money'];
        }
        //$pay_money =math_sub($data['money']*100,rand(3,30))/100;//实际支付金额


        $new_data = [
            'order_num'    => $data['order_num'],
            'notify_url'        => $data['notify_url'],
            'money'                => $data['money'],
            'pay_money'                => $pay_money,
            'system_order_num'=>orderNum(),
            'agent_id'=>$agent["id"],
            "type"=>$data['type'],
            "bid"=>$pay_channel["id"],
            "mchid"=>$pay_channel['mchid'],
            "appid"=>$pay_channel['appid']
        ];
        $res = $model->save($new_data);
        if(!$res){
            return AjaxReturn(0,"下单错误");
        }
        $order_info = $model::where(["order_num"=>$data["order_num"]])->find();


        //渠道商
        $model::where(["id"=>$order_info["id"]])->update([]);

        if($data['type'] == 3){
            return AjaxReturn(0,"暂无云闪付");
        }else if($data['type'] == 2){
            $url = ym().'/api/haikepay/haikepay?id='.$order_info['id'];
            return AjaxReturn(1,"ok",["url"=>$url,'order_no'=>$new_data['system_order_num']]);
        }else {
            return AjaxReturn(0,"暂无微信通道");
        }

    }





    /********************************************海科支付**********************************************************/

    /**
     * 海科支付拉单
     * 本地 : http://pay.cxlaimeng.cn/api/index/laoxiepay
     * 线上:  https://p.cxlaimeng.com/api/index/laoxiepay
     */
    public function laoxiepay()
    {
        // 在入口文件index.php中设置
        $data = input("post.");
        $validate = Validate::make([
            'type|支付类型'               => 'require|number',//支付类型 1微信 2支付宝 3 云闪付
            'order_num|订单号'            => 'require|length:10,40',
            'money|支付金额'              => 'require|float',
            'notify_url|异步通知地址'     => 'require|url',
            'agent_id|代理ID'             => 'require',
            'sign|签名'                   => 'require|length:32',
            "bid|通道id"                  =>'require|number'
        ]);

        if (!$validate->check($data)) {
            return AjaxReturn(0,$validate->getError());
        }
        if($data["money"] < 1){
            return AjaxReturn(0,"最少支付1元");
        }
        if($data["money"] > 1000){
            return AjaxReturn(0,"单笔金额最大1000元");
        }
        $agent = Agent::get($data["agent_id"]);
        if(!$agent){
            return AjaxReturn(0,'商户不存在');
        }
        if($agent["is_lock"]){
            return AjaxReturn(0,"商户暂停锁定");
        }
        $arr = [
            'agent_id'              => $data['agent_id'],
            'order_num'             => $data['order_num'],
            'notify_url'            => $data['notify_url'],
            'money'                 =>  $data['money'],
            'type'                  =>  $data['type'],
            'bid'                  =>  $data['bid'],
        ];
        $sign=createSign($arr,$agent["key"]);
        if($sign != $data['sign']){
            return AjaxReturn(0,"签名错误");
        }
        $model = new LaoxieOrder();
        $info =$model::get(["order_num"=>$data["order_num"]]);
        if($info){
            return AjaxReturn(0,"订单号已存在");
        }
        //判断支付通道
        $pay_channel_model = new LaoxieChannel();
        //此处改造轮训通道
        if($data['type'] == 1){
            $pay_channel = $pay_channel_model::where(["open"=>1,"weixin"=>1,"id"=>$data['bid']])->find();
            if (!$pay_channel){
                return AjaxReturn(0,"暂无微信通道");
            }
        }elseif ($data['type'] == 2){
            $pay_channel = $pay_channel_model::where(["open"=>1,"alipay"=>1,"id"=>$data['bid']])->find();
            if (!$pay_channel){
                return AjaxReturn(0,"暂无支付宝通道");
            }
        }else if ($data['type'] == 3){
            $pay_channel = $pay_channel_model::where(["open"=>1,"kuaijie"=>1,"id"=>$data['bid']])->find();
            if (!$pay_channel){
                return AjaxReturn(0,"暂无银联通道");
            }
        }
        if ($data['money'] > 100){
             $pay_money =math_sub($data['money']*100,rand(2,6))/100;//实际支付金额
        }else{
            $pay_money =$data['money'];
        }
        //$pay_money =math_sub($data['money']*100,rand(3,30))/100;//实际支付金额


        $new_data = [
            'order_num'    => $data['order_num'],
            'notify_url'        => $data['notify_url'],
            'money'                => $data['money'],
            'pay_money'                => $pay_money,
            'system_order_num'=>orderNum(),
            'agent_id'=>$agent["id"],
            "type"=>$data['type'],
            "bid"=>$pay_channel["id"],
            "mchid"=>$pay_channel['mchid'],
            //"appid"=>$pay_channel['appid']
        ];
        $res = $model->save($new_data);
        if(!$res){
            return AjaxReturn(0,"下单错误");
        }
        $order_info = $model::where(["order_num"=>$data["order_num"]])->find();


        //渠道商
        $model::where(["id"=>$order_info["id"]])->update([]);

        if($data['type'] == 3){
            return AjaxReturn(0,"暂无云闪付");
        }else if($data['type'] == 2){
            $url = ym().'/api/alipayzft/pay?id='.$order_info['id'];
            return AjaxReturn(1,"ok",["url"=>$url,'system_order_num'=>$new_data['system_order_num']]);
        }else {
            return AjaxReturn(0,"暂无微信通道");
        }

    }

    /**
     * 海科支付密钥
     * 本地   http://pay.cxlaimeng.cn/api/index/laoxiepaysign
     * 线上: https://p.cxlaimeng.com/api/index/laoxiepaysign
     */
    public function laoxiepaysign()
    {

        $data = input("post.");
        $validate = Validate::make([
            'type|支付类型'              => 'require|number',//支付类型 1微信 2支付宝 3云闪付
            'order_num|订单号'             => 'require|length:10,40',
            'money|支付金额'                => 'require|float',
            'notify_url|异步通知地址'         => 'require|url',
            'agent_id|代理ID'                => 'require',
            'bid'                  => 'require|number',
        ]);

        if (!$validate->check($data)) {
            return AjaxReturn(0,$validate->getError());
        }
        if($data["money"] < 1){
            return AjaxReturn(0,"金额必须大于一元");
        }
        $agent = Agent::get($data["agent_id"]);
        if(!$agent){
            return AjaxReturn(0,'代理不存在');
        }
        if($agent["is_lock"]){
            return AjaxReturn(0,"代理暂停锁定");
        }
        $arr = [
            'agent_id'              => $data['agent_id'],
            'order_num'             => $data['order_num'],
            'notify_url'            => $data['notify_url'],
            'money'                 =>  $data['money'],
            'type'                  =>  $data['type'],
            'bid'                  =>  $data['bid'],
        ];
        $sign=createSign($arr,$agent["key"]);
        return $sign;
    }

    /*********************************************直付通****************************************************/

    /**
     * 直付通
     * 本地 http://pay.cxlaimeng.cn/api/index/zhifutomgpay
     * 线上 https://p.cxlaimeng.com//api/index/zhifutomgpay
     */
    public function zhifutomgpay(){
        if(Request::isPost()){
            $data = input("post.");
            $validate = Validate::make([
                'order_num|订单号'             => 'require|length:10,40',
                'money|支付金额'                => 'require|float',
                'notify_url|异步通知地址'         => 'require|url',
                'agent_id|代理ID'                => 'require',
                "bid|通道id"                  =>  'require|number',
                "type|支付类型"                  =>  'require|number',//1微信 2支付宝 3云闪付
                'sign|签名'                => 'require|length:32',
            ]);

            if (!$validate->check($data)) {
                return AjaxReturn(0,$validate->getError());
            }


            if($data["money"] < 1){
                return AjaxReturn(0,"金额不能少于1元");
            }
            $model = new ZhifutongOrder();
            $info =$model::get(["order_num"=>$data["order_num"]]);
            if($info){
                return AjaxReturn(0,"订单号已存在");
            }
            $agent = Agent::get($data["agent_id"]);
            if(!$agent){
                return AjaxReturn(0,'商户不存在');
            }
            if($agent["is_lock"]){
                return AjaxReturn(0,"商户暂停锁定");
            }
            //判断支付通道
            $pay_channel_model = new ZhifutongChannel();
            //此处改造轮训通道
            if($data['type'] == 1){
                $pay_channel = $pay_channel_model::where(["open"=>1,"weixin"=>1,"id"=>$data['bid']])->find();
                if (!$pay_channel){
                    return AjaxReturn(0,"暂无微信通道");
                }
            }elseif ($data['type'] == 2){
                $pay_channel = $pay_channel_model::where(["open"=>1,"alipay"=>1,"id"=>$data['bid']])->find();
                if (!$pay_channel){
                    return AjaxReturn(0,"暂无支付宝通道");
                }
            }else if ($data['type'] == 3){
                $pay_channel = $pay_channel_model::where(["open"=>1,"kuaijie"=>1,"id"=>$data['bid']])->find();
                if (!$pay_channel){
                    return AjaxReturn(0,"暂无银联通道");
                }
            }

            $arr = [
                'agent_id'              => $data['agent_id'],
                'order_num'             => $data['order_num'],
                'notify_url'       => $data['notify_url'],
                'money'                =>  $data['money'],
                'bid'                =>  $data['bid'],
                'type'                =>  $data['type'],
            ];
            $sign=createSign($arr,$agent["key"]);
            if($sign != $data['sign']){
                return AjaxReturn(0,"签名错误");
            }

            //$pay_money =math_sub($data['money']*100,rand(7,50))/100;
            $pay_money =$data['money'];
            $new_data = [
                'order_num'    => $data['order_num'],
                'notify_url'        => $data['notify_url'],
                'money'                => $data['money'],
                'type'                => $data['type'],
                'pay_money'                => $pay_money,
                'system_order_num'  =>orderNum(),
                'agent_id'      =>$agent["id"],
                "bid"       =>$pay_channel["id"],
            ];
            $res = $model->save($new_data);
            if(!$res){
                return AjaxReturn(0,"下单错误");
            }
            $goodsmodel = new Goods();
            $goods = $goodsmodel::where(["bid"=>$data["bid"],"money"=>$data['money']])->find();
            if($goods){
                $chamoney = $data['money'];
            }else{
                $chamoney = format_money($data['money']/0.58);
            }



           
            $goods = $goodsmodel::where(["bid"=>$data["bid"],"money"=>$chamoney])->select();
            if(count($goods)){
                //一条数组
                if(count($goods) == 1){
                    $model::where(["system_order_num"=>$new_data['system_order_num']])->update(["title"=>$goods[0]['title']]);
                }else{
                    $randomIndex = rand(0,count($goods)-1);
                    $model::where(["system_order_num"=>$new_data['system_order_num']])->update(["title"=>$goods[$randomIndex]['title']]);
                }

            }else{
                return AjaxReturn(0,"商品不存在");
            }
            if($res){
                $order = $model::where(["system_order_num"=>$new_data['system_order_num']])->find();
                $url = ym()."/api/Zhifutong/pay?id=".$order['id'];
                //$url = ym()."/api/Zhifutong/alipaypayment?id=".$order['id'];
                return AjaxReturn(1,"创建成功",["url"=>$url,"order_no"=>$new_data['system_order_num']]);
            }else{
                return AjaxReturn(0,"下单错误");
            }

        }
    }


    /**
     * 海科支付密钥
     * 本地   http://pay.cxlaimeng.cn/api/index/zhifutomgpaysign
     * 线上: https://p.cxlaimeng.com/api/index/zhifutomgpaysign
     */
    public function zhifutomgpaysign()
    {

        $data = input("post.");
        $validate = Validate::make([
            'type|支付类型'              => 'require|number',//支付类型 1微信 2支付宝 3云闪付
            'order_num|订单号'             => 'require|length:10,40',
            'money|支付金额'                => 'require|float',
            'notify_url|异步通知地址'         => 'require|url',
            'agent_id|代理ID'                => 'require',
            'bid'                  => 'require|number',
        ]);

        if (!$validate->check($data)) {
            return AjaxReturn(0,$validate->getError());
        }
        if($data["money"] < 1){
            return AjaxReturn(0,"金额必须大于一元");
        }
        $agent = Agent::get($data["agent_id"]);
        if(!$agent){
            return AjaxReturn(0,'代理不存在');
        }
        if($agent["is_lock"]){
            return AjaxReturn(0,"代理暂停锁定");
        }
        $arr = [
            'agent_id'              => $data['agent_id'],
            'order_num'             => $data['order_num'],
            'notify_url'            => $data['notify_url'],
            'money'                 =>  $data['money'],
            'type'                  =>  $data['type'],
            'bid'                  =>  $data['bid'],
        ];
        $sign=createSign($arr,$agent["key"]);
        return $sign;
    }

    

}