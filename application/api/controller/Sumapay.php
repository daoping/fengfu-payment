<?php

namespace app\api\controller;

use app\common\model\FengfuChannel;
use app\common\model\FfOrder;
use think\facade\Validate;
use think\Controller;
use app\common\model\Agent;
use think\facade\Env;
//include_once  Env::get("root_path"). "extend/ldpay/umfPayService/UmfService.class.php";
//include_once  Env::get("root_path"). "vendor/adapay/AdapaySdk/init.php";
class Sumapay extends Controller
{
    /**
     * 下单
     * http://p.dinglianshop.cn/api/sumapay/index
     * https://pay.dinglianshop.cn/api/sumapay/index
     */
    public function index(){
        // 在入口文件index.php中设置
        header('Content-Type:text/html;charset=GBK');
        //halt(rand(0,3));
        $data = input("post.");
        $validate = Validate::make([
            'type|支付类型'              => 'require|number',//支付类型 1支付宝 2微信
            'order_num|订单号'             => 'require|length:10,40',
            'money|支付金额'                => 'require|float',
            'notify_url|异步通知地址'         => 'require|url',
            'agent_id|代理ID'                => 'require',
            'sign|签名'                => 'require|length:32',
        ]);

        if (!$validate->check($data)) {
            return AjaxReturn(0,$validate->getError());
        }
        if($data["money"] < 1){
            return AjaxReturn(0,"金额必须大于一元");
        }
        $agent = Agent::get($data["agent_id"]);
        if(!$agent){
            return AjaxReturn(0,'代理不存在');
        }
        if($agent["is_lock"]){
            return AjaxReturn(0,"代理暂停锁定");
        }
        $arr = [
            'agent_id'              => $data['agent_id'],
            'order_num'             => $data['order_num'],
            'notify_url'            => $data['notify_url'],
            'money'                 =>  $data['money'],
            'type'                  =>  $data['type'],
        ];
        $sign=createSign($arr,$agent["key"]);
        if($sign != $data['sign']){
            return AjaxReturn(0,"签名错误");
        }
        $model = new FfOrder();
        $info =$model::get(["order_num"=>$data["order_num"]]);
        if($info){
            return AjaxReturn(0,"订单号已存在");
        }
        if($data['money'] <= 50){
            $pay_money =math_sub($data['money']*100,rand(1,20))/100;//实际支付金额
        }else{
            $pay_money =math_sub($data['money']*100,rand(5,30))/100;//实际支付金额
        }

        //$pay_money =$data['money'];

        $new_data = [
            'order_num'    => $data['order_num'],
            'notify_url'        => $data['notify_url'],
            'money'                => $data['money'],
            'pay_money'                => $pay_money,
            'system_order_num'=>orderNum(),
            'agent_id'=>$agent["id"],
            "type"=>$data['type']
        ];
        $res = $model->save($new_data);
        if(!$res){
            return AjaxReturn(0,"下单错误");
        }
        $order_info = $model::where(["order_num"=>$data["order_num"]])->find();
        $pay_channel_model = new FengfuChannel();
        //此处改造轮训通道
        if($data['type'] == 1){
            $pay_channel = $pay_channel_model::where(["open"=>1,"weixin"=>1])->find();
            if (!$pay_channel){
                return AjaxReturn(0,"暂无微信通道");
            }
        }elseif ($data['type'] == 2){
            $pay_channel = $pay_channel_model::where(["open"=>1,"alipay"=>1])->find();
            if (!$pay_channel){
                return AjaxReturn(0,"暂无支付宝通道");
            }
        }else{
            return AjaxReturn(0,"暂无支付通道");
        }

        //渠道商

        $model::where(["id"=>$order_info["id"]])->update(["bid"=>$pay_channel["id"],"passThrough"=>orderNum()]);
        if($data['type'] == 1){
            $url = ym()."/api/sumapay/wechat?id=".$order_info['id'];
        }else{
            $url = ym()."/api/sumapay/aliapy?id=".$order_info['id'];
        }


        //todo 预警连续30单没有付款关闭
//        $yj = $finance->orderyj($bid);//子商户id
//        if($yj){
//            return AjaxReturn(0,"已关闭");
//        }
        return AjaxReturn(1,"ok",["url"=>$url]);
    }


    /**
     * 金运通微信支付签名
     */
    public function indexsign(){
        $data = input("post.");
        $validate = Validate::make([
            'type|支付类型'              => 'require|number',//支付类型 1支付宝 2微信 3快捷
            'order_num|订单号'             => 'require|length:10,40',
            'money|支付金额'                => 'require|float',
            'notify_url|异步通知地址'         => 'require|url',
            'agent_id|代理ID'                => 'require',
        ]);

        if (!$validate->check($data)) {
            return AjaxReturn(0,$validate->getError());
        }
        if($data["money"] < 1){
            return AjaxReturn(0,"金额必须大于一元");
        }
        $agent = Agent::get($data["agent_id"]);
        if(!$agent){
            return AjaxReturn(0,'代理不存在');
        }
        if($agent["is_lock"]){
            return AjaxReturn(0,"代理暂停锁定");
        }
        $arr = [
            'agent_id'              => $data['agent_id'],
            'order_num'             => $data['order_num'],
            'notify_url'            => $data['notify_url'],
            'money'                 =>  $data['money'],
            'type'                  =>  $data['type'],
        ];
        $sign=createSign($arr,$agent["key"]);
        return $sign;
    }

    /**
     * 微信支付
     * https://pay.dinglianshop.cn/api/sumapay/wechat?id=2
     */
    public function wechat(){
        $model = new FfOrder();
        $id = input("get.id");
        //halt($id);
        $order = $model::where("id",$id)->find();
        if(!$order){
            return AjaxReturn(0,"订单不存在");
        }
        if($order["payment"] > 0){
            return AjaxReturn(0,"订单已支付");
        }
        if($order["type"] != 1){
            return AjaxReturn(0,"不是微信订单");
        }
        $ip = request()->ip();
        //增加用户付款的ip信息
        db("order_ips")->insert(["order_id"=>$id,"ip"=>$ip,"create_time"=>time()]);
        $model->where("id",$id)->update(["buy_ip"=>$ip]);//更新付款用户下单ip
        $pay_channel_model = new FengfuChannel();
        $pay_channel = $pay_channel_model::where(["open"=>1,"weixin"=>1,"id"=>$order["bid"]])->find();
        if (!$pay_channel){
            return AjaxReturn(0,"暂无微信通道");
        }

        $url = "https://www.sumapay.com/wechatTransitGateway/merchant.do";
        $pay_money= $order["pay_money"]*100;//支付计算用金额
        $arr = [
            "requestType"=>"IOZ1004",//交易代码
            "requestId"=>$order["system_order_num"],//请求流水编号
            "requestStartTime"=>date("YmdHis",time()),//发起时间
            "merchantCode"=>$pay_channel["merchant_id"],//商户编号
            "totalBizType"=>"BIZ01104",//业务类型
            "totalPrice"=>(string)$order["pay_money"],//交易总价格

            "goodsDesc"=>"游戏道具",//商品描述
            "envFlag"=>"3",//环境标识 0：微信小程序，3：H5
            "rePayTimeOut"=>"0",
            "noticeUrl"=>ym().'/api/notify/sumapaynotify',//异步通知地址
            "backUrl"=>ym().'/api/Sumapay/wechatbackUrl?system_order_num='.$order["system_order_num"],//同步跳转地址
            "passThrough"=>orderNum(),//passThrough
            "terminalIp"=>request()->ip(),//终端 ip
            //下面的不参与签名
            "productId"=>(string)$pay_money,//产品编码
            "productName"=>$pay_money."金券",//产品名称
            "fund"=>(string)$order["pay_money"],//产品定价
            "merAcct"=>"wzlr",//供应商编码
            "bizType"=>"BIZ01104",//产品业务类型
            "productNumber"=>"1",//产品订购数量
        ];
        //dump($arr);
        $merKey = $pay_channel['key'];//密钥

        $sbOld = "";
        $sbOld = $sbOld . $arr['requestType'];
        $sbOld = $sbOld . $arr['requestId'];
        $sbOld = $sbOld . $arr['merchantCode'];
        $sbOld = $sbOld . $arr['totalBizType'];
        $sbOld = $sbOld . $arr['totalPrice'];
        $sbOld = $sbOld . $arr['goodsDesc'];
        $sbOld = $sbOld . $arr['envFlag'];
        $sbOld = $sbOld . $arr['rePayTimeOut'];
        $sbOld = $sbOld . $arr['noticeUrl'];
        $sbOld = $sbOld . $arr['backUrl'];
        $sbOld = $sbOld . $arr['passThrough'];
        $signatrue = HmacMd5($sbOld, $merKey);//数字签名
        $arr['signature'] = $signatrue;
        $returnData = send_posts($url, $arr);
        $returnData = iconv('GBK', 'UTF-8', $returnData);
        //dump($returnData);
        $res_data = json_decode($returnData,true);
        //halt($res_data);
        if($res_data["result"] == "00000"){
            $payurl = $res_data["payUrl"];
            //$model->where("id",$id)->update(["buy_ip"=>$ip]);//更新付款用户下单ip
            header("Location: {$payurl}");//打开url
            //确保重定向后，后续代码不会被执行
            exit;
        }else{
            return AjaxReturn("0","errorcode:".$res_data['result']."errormsg:".$res_data['errorMsg']);
        }
        //halt($returnData);
        //{"result":"00000","signature":"28ad9c64b06ca40ba1726c86ba8b8550","requestId":"430594396911744","payUrl":"https://www.sumapay.com/wechatTransitGateway/merchant.do?requestType=IOZ1100&scheme=weixin%3A%2F%2Fdl%2Fbusiness%2F%3Ft%3Da9pDkoOYhZu&backUrl=https%3A%2F%2Fpay.dinglianshop.cn%2Fapi%2FSumapay%2FalipaybackUrl","errorMsg":""}
    }
    /**
     * 微信同步地址
     */
    public function wechatbackUrl(){

    }


    /**
     * 支付宝支付
     * http://p.dinglianshop.cn/api/sumapay/aliapy
     * https://pay.dinglianshop.cn/api/sumapay/aliapy
     */
    public function aliapy(){
        $model = new FfOrder();
        $id = input("get.id");
        //halt($id);
        $order = $model::where("id",$id)->find();
        if(!$order){
            return AjaxReturn(0,"订单不存在");
        }
        if($order["payment"] > 0){
            return AjaxReturn(0,"订单已支付");
        }
        if($order["type"] != 2){
            return AjaxReturn(0,"不是alipay订单");
        }
        $ip = request()->ip();
        //增加用户付款的ip信息
        db("order_ips")->insert(["order_id"=>$id,"ip"=>$ip,"create_time"=>time()]);
        $model->where("id",$id)->update(["buy_ip"=>$ip]);//更新付款用户下单ip
        $pay_channel_model = new FengfuChannel();
        $pay_channel = $pay_channel_model::where(["open"=>1,"alipay"=>1,"id"=>$order["bid"]])->find();
        if (!$pay_channel){
            return AjaxReturn(0,"wualipay");
        }
        $pay_money= $order["pay_money"]*100;//支付计算用金额
        $url = 'https://www.sumapay.com/wechatTransitGateway/merchant.do';
        $arr = [
            "requestType"=>"IOZ2003",//交易代码
            "requestId"=>$order["system_order_num"],//请求流水编号
            "requestStartTime"=>date("YmdHis",time()),//发起时间
            "merchantCode"=>$pay_channel["merchant_id"],//商户编号
            "totalBizType"=>"BIZ01104",//业务类型
            "totalPrice"=>(string)$order["pay_money"],//交易总价格
            "goodsDesc"=>"游戏道具",//商品描述
            "noticeUrl"=>ym().'/api/notify/sumapaynotify',//异步通知地址
            "backUrl"=>ym().'/api/Sumapay/alipaybackUrl',//同步跳转地址
            "passThrough"=>orderNum(),//透传信息
            "terminalIp"=>$ip,//终端 ip
            //下面的不参与签名
            "productId"=>(string)$pay_money,//产品编码
            "productName"=>$pay_money."金券",//产品名称
            "fund"=>(string)$order["pay_money"],//产品定价
            "merAcct"=>"wzlr",//供应商编码
            "bizType"=>"BIZ01104",//产品业务类型
            "productNumber"=>"1",//产品订购数量

        ];
        $merKey = $pay_channel['key'];//密钥

        $sbOld = "";
        $sbOld = $sbOld . $arr['requestType'];
        $sbOld = $sbOld . $arr['requestId'];
//        $sbOld = $sbOld . $arr['requestStartTime'];
        $sbOld = $sbOld . $arr['merchantCode'];
        $sbOld = $sbOld . $arr['totalBizType'];
        $sbOld = $sbOld . $arr['totalPrice'];
        $sbOld = $sbOld . $arr['goodsDesc'];
        $sbOld = $sbOld . $arr['noticeUrl'];
        $sbOld = $sbOld . $arr['backUrl'];
        $sbOld = $sbOld . $arr['passThrough'];
//        $sbOld = $sbOld . $arr['terminalIp'];
        $signatrue = HmacMd5($sbOld, $merKey);//数字签名
        $arr['signature'] = $signatrue;
        $returnData = send_posts($url, $arr);
        //{
        //    "result": "00000",
        //    "signature": "15b6f465a4d3b2c03f00e69bee938ef4",
        //    "requestId": "430578290155797",
        //    "payUrl": "https://www.sumapay.com/wechatTransitGateway/merchant.do?requestType=IOZ1100&scheme=alipays%253A%252F%252Fplatformapi%252Fstartapp%253FappId%253D2021004128630013%2526page%253Dpages%25252Falipay_web%25252Falipay_web%25253FwebUrl%25253Dhttps%252525253A%252525252F%252525252Fopenauth.alipay.com%252525252Foauth2%252525252FpublicAppAuthorize.htm%252525253Fapp_id%252525253D2021003157696023%2525252526redirect_uri%252525253Dhttps%25252525253A%25252525252F%25252525252Fwww.sumapay.com%25252525252FaggregatePayMobile%25252525252FalipayOfficialAccountsToScanPayOrder.do%25252525253Ftoken%25252525253D46d5cb588d95427ea25e256d8114abde%252525252526tt%25252525253D20240430141709%252525252526mc%25252525253D3710000332%252525252526s%25252525253Df92483837f3b4fa1416301f7bd93ce26%2525252526scope%252525253Dauth_base%2525252526state%252525253Dminiprogram%2526query%253D&backUrl=https%3A%2F%2Fpay.dinglianshop.cn%2Fapi%2FSumapay%2FalipaybackUrl",
        //    "errorMsg": ""
        //}
        //echo $returnData;
        $res_data = json_decode($returnData,true);
        //halt($res_data);
        if($res_data["result"] == "00000"){
            $payurl = $res_data["payUrl"];
            //$model->where("id",$id)->update(["buy_ip"=>$ip]);//更新付款用户下单ip
            header("Location: {$payurl}");//打开url
            //确保重定向后，后续代码不会被执行
            exit;
        }else{
            return AjaxReturn("0","errorcode:".$res_data['result']."errormsg:".$res_data['errorMsg']);
        }
    }

    /**
     * 支付宝同步地址
     */
    public function alipaybackUrl(){

    }
    /*******************************************demo************************************************************/

    /**
     * 支付宝支付
     * http://p.dinglianshop.cn/api/sumapay/aliapy
     * https://pay.dinglianshop.cn/api/sumapay/aliapy
     */
    public function aliapy_(){
        $url = 'https://www.sumapay.com/wechatTransitGateway/merchant.do';
        $arr = [
            "requestType"=>"IOZ2003",//交易代码
            "requestId"=>orderNum(),//请求流水编号
            "requestStartTime"=>date("YmdHis",time()),//发起时间
            "merchantCode"=>"3710000332",//商户编号
            "totalBizType"=>"BIZ01104",//业务类型
            "totalPrice"=>"5.93",//交易总价格
            "goodsDesc"=>"游戏道具",//商品描述
            "noticeUrl"=>ym().'/api/notify/sumapaynotify',//异步通知地址
            "backUrl"=>ym().'/api/Sumapay/alipaybackUrl',//同步跳转地址
            "passThrough"=>orderNum(),//透传信息
            "terminalIp"=>request()->ip(),//终端 ip
            //下面的不参与签名
            "productId"=>"593",//产品编码
            "productName"=>"593金券",//产品名称
            "fund"=>"5.93",//产品定价
            "merAcct"=>"wzlr",//供应商编码
            "bizType"=>"BIZ01104",//产品业务类型
            "productNumber"=>"1",//产品订购数量

        ];
        $merKey = "HnmKniP4ww71RIAEvozfKgrKdgYsl0Si";//密钥

        $sbOld = "";
        $sbOld = $sbOld . $arr['requestType'];
        $sbOld = $sbOld . $arr['requestId'];
//        $sbOld = $sbOld . $arr['requestStartTime'];
        $sbOld = $sbOld . $arr['merchantCode'];
        $sbOld = $sbOld . $arr['totalBizType'];
        $sbOld = $sbOld . $arr['totalPrice'];
        $sbOld = $sbOld . $arr['goodsDesc'];
        $sbOld = $sbOld . $arr['noticeUrl'];
        $sbOld = $sbOld . $arr['backUrl'];
        $sbOld = $sbOld . $arr['passThrough'];
//        $sbOld = $sbOld . $arr['terminalIp'];
        $signatrue = HmacMd5($sbOld, $merKey);//数字签名
        $arr['signature'] = $signatrue;
        $returnData = send_posts($url, $arr);
        //{
        //    "result": "00000",
        //    "signature": "15b6f465a4d3b2c03f00e69bee938ef4",
        //    "requestId": "430578290155797",
        //    "payUrl": "https://www.sumapay.com/wechatTransitGateway/merchant.do?requestType=IOZ1100&scheme=alipays%253A%252F%252Fplatformapi%252Fstartapp%253FappId%253D2021004128630013%2526page%253Dpages%25252Falipay_web%25252Falipay_web%25253FwebUrl%25253Dhttps%252525253A%252525252F%252525252Fopenauth.alipay.com%252525252Foauth2%252525252FpublicAppAuthorize.htm%252525253Fapp_id%252525253D2021003157696023%2525252526redirect_uri%252525253Dhttps%25252525253A%25252525252F%25252525252Fwww.sumapay.com%25252525252FaggregatePayMobile%25252525252FalipayOfficialAccountsToScanPayOrder.do%25252525253Ftoken%25252525253D46d5cb588d95427ea25e256d8114abde%252525252526tt%25252525253D20240430141709%252525252526mc%25252525253D3710000332%252525252526s%25252525253Df92483837f3b4fa1416301f7bd93ce26%2525252526scope%252525253Dauth_base%2525252526state%252525253Dminiprogram%2526query%253D&backUrl=https%3A%2F%2Fpay.dinglianshop.cn%2Fapi%2FSumapay%2FalipaybackUrl",
        //    "errorMsg": ""
        //}
        //echo $returnData;
        $res_data = json_decode($returnData,true);
        halt($res_data);
        if($res_data['result'] == "00000"){
            //$res_data['payUrl']支付url
        }
    }

    /**
     * 微信支付
     * https://pay.dinglianshop.cn/api/sumapay/wechats
     */
    public function wechats_(){
        $url = "https://www.sumapay.com/wechatTransitGateway/merchant.do";
        $arr = [
            "requestType"=>"IOZ1004",//交易代码
            "requestId"=>orderNum(),//请求流水编号
            "requestStartTime"=>date("YmdHis",time()),//发起时间
            "merchantCode"=>"3710000332",//商户编号
            "totalBizType"=>"BIZ01104",//业务类型
            "totalPrice"=>"22.00",//交易总价格
            "goodsDesc"=>"游戏道具",//商品描述
            "envFlag"=>"3",//环境标识 0：微信小程序，3：H5
            "rePayTimeOut"=>"0",
            "noticeUrl"=>ym().'/api/notify/sumapaynotify',//异步通知地址
            "backUrl"=>ym().'/api/Sumapay/wechatbackUrl',//同步跳转地址
            "passThrough"=>orderNum(),//passThrough
            "terminalIp"=>request()->ip(),//终端 ip
            //下面的不参与签名
            "productId"=>"2200",//产品编码
            "productName"=>"2200金券",//产品名称
            "fund"=>"22.00",//产品定价
            "merAcct"=>"wzlr",//供应商编码
            "bizType"=>"BIZ01104",//产品业务类型
            "productNumber"=>"1",//产品订购数量
        ];
        //dump($arr);
        $merKey = "HnmKniP4ww71RIAEvozfKgrKdgYsl0Si";//密钥

        $sbOld = "";
        $sbOld = $sbOld . $arr['requestType'];
        $sbOld = $sbOld . $arr['requestId'];
        $sbOld = $sbOld . $arr['merchantCode'];
        $sbOld = $sbOld . $arr['totalBizType'];
        $sbOld = $sbOld . $arr['totalPrice'];
        $sbOld = $sbOld . $arr['goodsDesc'];
        $sbOld = $sbOld . $arr['envFlag'];
        $sbOld = $sbOld . $arr['rePayTimeOut'];
        $sbOld = $sbOld . $arr['noticeUrl'];
        $sbOld = $sbOld . $arr['backUrl'];
        $sbOld = $sbOld . $arr['passThrough'];
        $signatrue = HmacMd5($sbOld, $merKey);//数字签名
        $arr['signature'] = $signatrue;
        $returnData = send_posts($url, $arr);
        //halt($returnData);
        $returnData = iconv('GBK', 'UTF-8', $returnData);
        $res_data = json_decode($returnData,true);
        //halt($res_data);
        if($res_data["result"] == "00000"){
            header("Location: {$url}");//打开url
            //确保重定向后，后续代码不会被执行
            exit;
        }else{
            return AjaxReturn("0","错误代码:");
        }
        //halt($returnData);
        //{"result":"00000","signature":"28ad9c64b06ca40ba1726c86ba8b8550","requestId":"430594396911744","payUrl":"https://www.sumapay.com/wechatTransitGateway/merchant.do?requestType=IOZ1100&scheme=weixin%3A%2F%2Fdl%2Fbusiness%2F%3Ft%3Da9pDkoOYhZu&backUrl=https%3A%2F%2Fpay.dinglianshop.cn%2Fapi%2FSumapay%2FalipaybackUrl","errorMsg":""}

    }



}