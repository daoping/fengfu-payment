<?php

namespace app\api\controller;


use app\common\model\Order;
use app\common\model\TlOrder;
use app\common\model\TonglianChannel;
use think\Controller;
use think\facade\Env;
//include_once  Env::get("root_path"). "extend/ldpay/umfPayService/UmfService.class.php";
//include_once  Env::get("root_path"). "vendor/adapay/AdapaySdk/init.php";

/**
 * tonglianpay
 */
class Tonglianpay extends Controller
{

    /**
     * 支付
     * 本地地址:  http://p.dinglianshop.cn/api/Tonglianpay/pay
     * 网络地址:  https://pay.dinglianshop.cn/api/Tonglianpay/pay
     */
    public function pay(){
        $model = new TlOrder();
        $id = input("get.id");
        if(!$id){
            return AjaxReturn(0,"参数错误");
        }
        $order = $model::get($id);
        if(!$order){
            return AjaxReturn(0,"订单不存在");
        }
        if($order["payment"] == 1){
            return AjaxReturn(0,"订单已支付");
        }
        $ip = request()->ip();
        $model->where("id",$id)->update(["buy_ip"=>$ip]);//更新付款用户下单ip
        $PayChannelModel  = new TonglianChannel();
        $PayChannel = $PayChannelModel::get($order['bid']);
        if(!$PayChannel){
            return AjaxReturn(1,"支付通道不存在");
        }
        if($PayChannel['open'] != 1){
            return AjaxReturn(1,"通道未开启");
        }

        //array(15) { ["serverUrl"]=> string(53) "https://vsp.allinpay.com/apiweb/h5unionpay/unionorder" ["version"]=> string(2) "12" ["trxamt"]=> string(1) "1" ["reqsn"]=> string(16) "NO20240723093410" ["charset"]=> string(5) "UTF-8" ["returl"]=> string(46) "https://test.allinpaygd.com/JWeb/recparams.jsp" ["notify_url"]=> string(44) "http://121.8.157.114:9090/JWeb/NotifyServlet" ["body"]=> string(26) "收银宝H5收银台测试" ["remark"]=> string(11) "made in syb" ["validtime"]=> string(2) "10" ["limit_pay"]=> string(0) "" ["randomstr"]=> string(9) "randomstr" ["asinfo"]=> string(0) "" ["ishide"]=> string(1) "1" ["signtype"]=> string(3) "RSA" }
        //$serverUrl = "https://syb-test.allinpay.com/apiweb/h5unionpay/unionorder";//测试环境
        $serverUrl = "https://vsp.allinpay.com/apiweb/h5unionpay/unionorder";//正式环境
        $app = [
            "cusid"=>$order['cusid'],//商户号
            "appid"=>$order['appid']//appid
        ];
        $money = $order['pay_money'];//支付金额
        $arr = [
            "version"=>  "12",
            "trxamt"=> strval($money*100),
            "reqsn"=>  $order['system_order_num'],
            "charset"=>  "UTF-8",
            "returl"=>  ym()."/api/tonglianpay/returl",
            "notify_url"=> ym()."/api/tlnotify/tongliannotify",
            "body"=>  "1",
            "remark"=> "",
            "validtime"=> "10",
            "limit_pay"=> "",
            "randomstr"=> md5(rand_string(8)),
            "asinfo"=>  "",
            "ishide"=>  "1",
            "signtype"=>  "RSA"
        ];
        $params = array();
        $params["cusid"] =$app['cusid'];//商户号  1
        $params["appid"] = $app['appid'];//appid   1
        $params["version"] = $arr['version'];//版本号 1
        $params["randomstr"] =$arr['randomstr'];//随机串  1
        $params["trxamt"] = $arr['trxamt'];//支付金额 分 1
        $params["reqsn"] = $arr["reqsn"];//商户订单号  1
        $params["charset"] = $arr["charset"];//参数字符编码集  1
        $params["returl"] = $arr["returl"];//页面跳转同步通知页面路径  1
        $params["notify_url"] =$arr["notify_url"];//异步地址  1
        //$params["body"] = $arr["body"];//订单标题  1
        $params["body"] = "1";//订单标题  1
        //$params["remark"] = $arr["remark"];//备注  1
        $params["remark"] = "1";//备注  1
//        $params["validtime"] = $arr["validtime"];//有效时间分钟
//        $params["limit_pay"] =$arr["limit_pay"];//支付限制 no_credit--指定不能使用信用卡支付
//        $params["asinfo"] =$arr["asinfo"];//分账信息
//        $params["ishide"] =$arr["ishide"];//是否直接支付
        $params["signtype"] =$arr["signtype"];//签名类型 1
        //$params["paytype"] ="VSP551";//签名类型


        ksort($params);
        $bufSignSrc = $this->ToUrlParams($params);
        $signMsg = $this->Sign($params);
        $params['sign'] = $signMsg;  //1
        //dump($params);
        //$sign = urlencode($params['sign']);
//        dump($bufSignSrc);
//        dump($signMsg);
        //云闪付支付
        $queryString = http_build_query($params);
        $serverUrl= $serverUrl.'?'.$queryString;
//        halt($serverUrl);
//        $url = $serverUrl.'?cusid='.$params["cusid"].'&appid='.$params["appid"].'&version='.$params["version"].'&randomstr='.$params["randomstr"].'&trxamt='.$params["trxamt"].'&reqsn='.$params["reqsn"].'&charset='.$params["charset"].'&=returl'.$params["returl"].'&=notify_url'.$params["notify_url"].'&body='.$params["body"].'&remark='.$params["remark"].'&signtype='.$params["signtype"].'&sign='.$sign;
        //header('Location:'.$serverUrl);

// 停止脚本执行
        exit;
        //halt($url);
        //$res = httpPost($serverUrl, $params);
        //halt($res);
        //db("test")->insert(["add_time"=>time(),"content"=>$serverUrl,"ip"=>"","explain"=>"通联异步支付返回参数"]);
//        dump(["serverUrl"=>$serverUrl,"params"=>$params,"money"=>$money]);
        //return view("",["serverUrl"=>$serverUrl,"params"=>$params,"money"=>$money]);
//        $res = http_post($serverUrl, $params);
//        dump($res);

        //微信支付
        //return view("",["serverUrl"=>$serverUrl,"params"=>$params,"money"=>$money]);
    }

    /**
     * 支付
     * 本地地址:  http://p.dinglianshop.cn/api/Tonglianpay/pay
     * 网络地址:  https://pay.dinglianshop.cn/api/Tonglianpay/pay
     */
    public function pay_(){
        //array(15) { ["serverUrl"]=> string(53) "https://vsp.allinpay.com/apiweb/h5unionpay/unionorder" ["version"]=> string(2) "12" ["trxamt"]=> string(1) "1" ["reqsn"]=> string(16) "NO20240723093410" ["charset"]=> string(5) "UTF-8" ["returl"]=> string(46) "https://test.allinpaygd.com/JWeb/recparams.jsp" ["notify_url"]=> string(44) "http://121.8.157.114:9090/JWeb/NotifyServlet" ["body"]=> string(26) "收银宝H5收银台测试" ["remark"]=> string(11) "made in syb" ["validtime"]=> string(2) "10" ["limit_pay"]=> string(0) "" ["randomstr"]=> string(9) "randomstr" ["asinfo"]=> string(0) "" ["ishide"]=> string(1) "1" ["signtype"]=> string(3) "RSA" }
        //$serverUrl = "https://syb-test.allinpay.com/apiweb/h5unionpay/unionorder";//测试环境
        $serverUrl = "https://vsp.allinpay.com/apiweb/h5unionpay/unionorder";//正式环境
        $app = [
            "cusid"=>"660303073752HVR",//商户号
            "appid"=>"00318060"//appid
        ];
        $money = 0.02;//支付金额
        $arr = [
            "version"=>  "12",
            "trxamt"=> strval($money*100),
            "reqsn"=>  orderNum(),
            "charset"=>  "UTF-8",
            "returl"=>  ym()."/api/tonglianpay/returl",
            "notify_url"=> ym()."/api/tlnotify/tongliannotify",
            "body"=>  "1",
            "remark"=> "",
            "validtime"=> "10",
            "limit_pay"=> "",
            "randomstr"=> md5(rand_string(8)),
            "asinfo"=>  "",
            "ishide"=>  "1",
            "signtype"=>  "RSA"
        ];
        $params = array();
        $params["cusid"] =$app['cusid'];//商户号  1
        $params["appid"] = $app['appid'];//appid
        $params["version"] = $arr['version'];//版本号
        $params["randomstr"] =$arr['randomstr'];//随机串
        $params["trxamt"] = $arr['trxamt'];//支付金额 分
        $params["reqsn"] = $arr["reqsn"];//商户订单号
        $params["charset"] = $arr["charset"];//参数字符编码集
        $params["returl"] = $arr["returl"];//页面跳转同步通知页面路径
        $params["notify_url"] =$arr["notify_url"];//异步地址
        $params["body"] = $arr["body"];//订单标题
        $params["remark"] = $arr["remark"];//备注
        $params["validtime"] = $arr["validtime"];//有效时间分钟
        $params["limit_pay"] =$arr["limit_pay"];//支付限制 no_credit--指定不能使用信用卡支付
        $params["asinfo"] =$arr["asinfo"];//分账信息
        $params["ishide"] =$arr["ishide"];//是否直接支付
        $params["signtype"] =$arr["signtype"];//签名类型
        dump($params);
        ksort($params);
        $bufSignSrc = $this->ToUrlParams($params);
        $signMsg = $this->Sign($params);
        $params['sign'] = $signMsg;
//        dump($bufSignSrc);
//        dump($signMsg);

//        $res = request_post($serverUrl, $params);
//        halt($res);
        return view("",["serverUrl"=>$serverUrl,"params"=>$params,"money"=>$money]);
//        $res = http_post($serverUrl, $params);
//        dump($res);
    }

    /**
     * 通联支付同步跳转
     * 本地地址:  http://p.dinglianshop.cn/api/Tonglianpay/returl
     * 网络地址:
     */
    public function returl(){
        //return view("pay");
        $get = $_GET;
        halt($_GET);
    }



    /***********************************************通联支付参数***************************************************/

    /**
     * 将参数数组签名
     */

    //RSA签名
    public static function Sign(array $array){
        ksort($array);
        $bufSignSrc = self::ToUrlParams($array);
        $private_key='MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCx5JyAR+wbKQ2+5WhSTDDSk73oIevbGaMiRHtMz/xA5iqnIMbhp7B0fo+8Uo/GYWkuXJWZ2PcFJpRWVo5y7wTaivmlh3PHdj2yGZ7L5bEs7pG55hPpuCQo7debpeBdL3H/Rs3wHqpUGitKy8d6CoNTrFZ1hx9tW/atx00ciGuj76RnvzIhZrSQnMxgF2yD520C68FwW0U/jsMu4ucp6d504dDqgbk0r5d0bKpSAVCDNLpX40ooz1jHowygjZQU65NZA1HCtEofrVO6GUfK6WkxgYyPe8i6jkLFjrVcoBsfQpaDBXsgoztcAOFdYtWCxw1qI3AMw3p/2qnlt7qEOuwtAgMBAAECggEAEvJKSM9gMjRZJm/AgKNkv8jEfSi/ugItAcVRbIjqUO8ys8Il7HqzrHSeUmxoq7RMQ4fQ1yXoiT/mpJtraIpUdgI/PIYEqsXJJGLeKtE34ZU3KBl9HXNjRoiuYgF0t/gJqCqeXGins3VmDv1NLY5ZFlxQiQvvKPKWf0Ouzs8ox7jctpaVA2QHE2IiL33PZl/N69FkdnjPhS7qeM/DTjiw/lrmoP4/DfWA45B535ZS+CGHVMOqoTCGe29EynPwL4ZhJw/avgfKnmFE/71CXiVGs5HKh6SN33ZQAKiv+25hH1eVn/AqBBUTJVpbjuat4gIW38Jz4NrhiZazCJDlQnNbcQKBgQDkK269d3nzQo1hk7N5NFU+RIdrd/RZE4O4lYY3nHQG8rFovbOmZq/7pm2o+8x5lj05WUfRdvfA7I5ZChToMsepjM8O1AeP5V09nUv4YEVGRs41I54TtGnQ8VYmDKSjcBC4BqznFo8u/bKNDRAvdob16MrxoBzuS/VxdwTTHw2wDwKBgQDHl0wRtltHtUvNUxDPNvQrrUCtHjSHmyf0dOW0LKKdmFhr2LYDGYfwj5iy/ng642mo9f0NbAqolORH1+AAZ7HAhU8K0moZgZ2Rv2o1CBGMErugbYNtk//JvjCdPrsww0R3E4h6pn/CC/MLphApne3nnJWJTlQDBDUm2tKsAPRkAwKBgQDT29AzdKBzUzRbif13aTRNYOwsyXDE1O3VkmDReh/x457/FkWdGHQHQf1IjqTJsMqgWVnqEIDzTbLO0iGfiKcDs47+wblTzzDIaSmFMj6ghlYp2SyKN1aZf7zyD4M/jmq3tNsOp0/D3iTP+3Uv3Oprov/tYzH5weXguRZtwcO/2QKBgGeHGRQO7OYTKhCEx7FUJe8J3QRvKHw2hNEOWZ/Nj0QDo8m9DRwqgBLNyTz/NcoF7+aOQgZWRmkPYiHJ3g0XvGHKRCeu4Q3954eULDj5yHBJvz0F0Shjnkg8+OZ3hY1TnA3P+fj0qEw0+orORo/vtVlwJTilgnpWEmPnGWPS0vGVAoGAGbMrmvdmDL+XPe6ff8ye9mDCl5k96/Ddc3+P5FWS4sL0CNcmif+gOvSTK+yxuJoJKlWxssZjoRqueqRFDanR6ccOxdbYzPoHl38WN1ldlTBCeW2oItg4U0+Y4on0UdM8+vRBja8YcWOXX5+ZpQHCgknLMAop5rVG5ucG9W1mfT0=';
        $private_key = chunk_split($private_key , 64, "\n");
        $key = "-----BEGIN RSA PRIVATE KEY-----\n".wordwrap($private_key)."-----END RSA PRIVATE KEY-----";
        //   echo $key;
        if(openssl_sign($bufSignSrc, $signature, $key )){
            //		echo 'sign success';
        }else{
            echo 'sign fail';
        }
        $sign = base64_encode($signature);//加密后的内容通常含有特殊字符，需要编码转换下，在网络间通过url传输时要注意base64编码是否是url安全的

        return $sign;
        //return $signature;
    }

    public static function signrsa(array $array){
        ksort($array);
        $bufSignSrc = self::ToUrlParams($array);
        $private_key='MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCx5JyAR+wbKQ2+5WhSTDDSk73oIevbGaMiRHtMz/xA5iqnIMbhp7B0fo+8Uo/GYWkuXJWZ2PcFJpRWVo5y7wTaivmlh3PHdj2yGZ7L5bEs7pG55hPpuCQo7debpeBdL3H/Rs3wHqpUGitKy8d6CoNTrFZ1hx9tW/atx00ciGuj76RnvzIhZrSQnMxgF2yD520C68FwW0U/jsMu4ucp6d504dDqgbk0r5d0bKpSAVCDNLpX40ooz1jHowygjZQU65NZA1HCtEofrVO6GUfK6WkxgYyPe8i6jkLFjrVcoBsfQpaDBXsgoztcAOFdYtWCxw1qI3AMw3p/2qnlt7qEOuwtAgMBAAECggEAEvJKSM9gMjRZJm/AgKNkv8jEfSi/ugItAcVRbIjqUO8ys8Il7HqzrHSeUmxoq7RMQ4fQ1yXoiT/mpJtraIpUdgI/PIYEqsXJJGLeKtE34ZU3KBl9HXNjRoiuYgF0t/gJqCqeXGins3VmDv1NLY5ZFlxQiQvvKPKWf0Ouzs8ox7jctpaVA2QHE2IiL33PZl/N69FkdnjPhS7qeM/DTjiw/lrmoP4/DfWA45B535ZS+CGHVMOqoTCGe29EynPwL4ZhJw/avgfKnmFE/71CXiVGs5HKh6SN33ZQAKiv+25hH1eVn/AqBBUTJVpbjuat4gIW38Jz4NrhiZazCJDlQnNbcQKBgQDkK269d3nzQo1hk7N5NFU+RIdrd/RZE4O4lYY3nHQG8rFovbOmZq/7pm2o+8x5lj05WUfRdvfA7I5ZChToMsepjM8O1AeP5V09nUv4YEVGRs41I54TtGnQ8VYmDKSjcBC4BqznFo8u/bKNDRAvdob16MrxoBzuS/VxdwTTHw2wDwKBgQDHl0wRtltHtUvNUxDPNvQrrUCtHjSHmyf0dOW0LKKdmFhr2LYDGYfwj5iy/ng642mo9f0NbAqolORH1+AAZ7HAhU8K0moZgZ2Rv2o1CBGMErugbYNtk//JvjCdPrsww0R3E4h6pn/CC/MLphApne3nnJWJTlQDBDUm2tKsAPRkAwKBgQDT29AzdKBzUzRbif13aTRNYOwsyXDE1O3VkmDReh/x457/FkWdGHQHQf1IjqTJsMqgWVnqEIDzTbLO0iGfiKcDs47+wblTzzDIaSmFMj6ghlYp2SyKN1aZf7zyD4M/jmq3tNsOp0/D3iTP+3Uv3Oprov/tYzH5weXguRZtwcO/2QKBgGeHGRQO7OYTKhCEx7FUJe8J3QRvKHw2hNEOWZ/Nj0QDo8m9DRwqgBLNyTz/NcoF7+aOQgZWRmkPYiHJ3g0XvGHKRCeu4Q3954eULDj5yHBJvz0F0Shjnkg8+OZ3hY1TnA3P+fj0qEw0+orORo/vtVlwJTilgnpWEmPnGWPS0vGVAoGAGbMrmvdmDL+XPe6ff8ye9mDCl5k96/Ddc3+P5FWS4sL0CNcmif+gOvSTK+yxuJoJKlWxssZjoRqueqRFDanR6ccOxdbYzPoHl38WN1ldlTBCeW2oItg4U0+Y4on0UdM8+vRBja8YcWOXX5+ZpQHCgknLMAop5rVG5ucG9W1mfT0=';
        $private_key = chunk_split($private_key , 64, "\n");
        $key = "-----BEGIN RSA PRIVATE KEY-----\n".wordwrap($private_key)."-----END RSA PRIVATE KEY-----";
        //   echo $key;
        if(openssl_sign($bufSignSrc, $signature, $key )){
            //		echo 'sign success';
        }else{
            echo 'sign fail';
        }
        $sign = strtoupper(md5($signature));//加密后的内容通常含有特殊字符，需要编码转换下，在网络间通过url传输时要注意base64编码是否是url安全的

        return $sign;
        //return $signature;
    }


    public static function ToUrlParams(array $array)
    {
        $buff = "";
        foreach ($array as $k => $v)
        {
            if($v != "" && !is_array($v)){
                $buff .= $k . "=" . $v . "&";
            }
        }

        $buff = trim($buff, "&");
        return $buff;
    }

    /**
     * 校验签名
     * @param array 参数
     * @param unknown_type appkey
     */


    public static function ValidSign(array $array){
        $sign =$array['sign'];
        unset($array['sign']);
        ksort($array);
        $bufSignSrc = self::ToUrlParams($array);
        $public_key='MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCm9OV6zH5DYH/ZnAVYHscEELdCNfNTHGuBv1nYYEY9FrOzE0/4kLl9f7Y9dkWHlc2ocDwbrFSm0Vqz0q2rJPxXUYBCQl5yW3jzuKSXif7q1yOwkFVtJXvuhf5WRy+1X5FOFoMvS7538No0RpnLzmNi3ktmiqmhpcY/1pmt20FHQQIDAQAB';
        $public_key = chunk_split($public_key , 64, "\n");
        $key = "-----BEGIN PUBLIC KEY-----\n$public_key-----END PUBLIC KEY-----\n";
        $result= openssl_verify($bufSignSrc,base64_decode($sign), $key );
        return $result;
    }

    /**
     * php get 参数转化成数组
     * http://p.dinglianshop.cn/api/Tonglianpay/getshuz
     */
    public function getshuz(){
        $str = "appid=00190273&body=1&charset=UTF-8&cusid=56033105399AT07&notify_url=http://api.bkqqg.com/Payback/Scan/scanNotify_order&randomstr=1&remark=1&reqsn=1&returl=https://a111111.minitool.top/aaaa&sign=VFV7+8/SJ0JZBvUodJIraYRxqBkeoEj2w4wJ1bbhYpxrpAGWgGU6hpEVqiBYx/tXLgo94Ia+Es7/pZJitbGLXf9aWLDlY9peenRtlkPmld6ymv+KzEmWRi6ZV1wHzb1aUpTTqFIyE25aWQkexy6MLnTHCSR2BGTsY096UzdZ63M=&signtype=RSA&trxamt=1&version=12";
        $array = $this->query_to_array($str);
        halt($array);

    }


    public  function query_to_array($query) {
        parse_str($query, $output);
        return $output;
    }


    /**
     * 银联扫码
     * 本地地址:  http://p.dinglianshop.cn/api/Tonglianpay/yinlian
     * 网络地址:  https://p.cxlaimeng.com/api/Tonglianpay/yinlian
     */
    public function yinlian(){
        $model = new TlOrder();
        $id = input("get.id");
        if(!$id){
            return AjaxReturn(0,"参数错误");
        }
        $order = $model::get($id);
        if(!$order){
            return AjaxReturn(0,"订单不存在");
        }
        if($order["payment"] == 1){
            return AjaxReturn(0,"订单已支付");
        }
        $ip = request()->ip();
        $model->where("id",$id)->update(["buy_ip"=>$ip]);//更新付款用户下单ip
        $PayChannelModel  = new TonglianChannel();
        $PayChannel = $PayChannelModel::get($order['bid']);
        if(!$PayChannel){
            return AjaxReturn(1,"支付通道不存在");
        }
        if($PayChannel['open'] != 1){
            return AjaxReturn(1,"通道未开启");
        }
        $money = $order['pay_money'];//支付金额
        if($order['codeUrl'] && !$order["payment"]){
            //return view("yinlian",["order"=>$order,"url"=>$data['body']['codeImgUrl']]);
            //$model::where(["id"=>$order["id"]])->update(["codeUrl"=>$data['body']['codeImgUrl']]);
            //halt(["orderCode"=>$order['system_order_num'],"money"=>$order['money'],"url"=>$order['codeUrl']]);
            return view("",["orderCode"=>$order['system_order_num'],"money"=>$order['money'],"url"=>$order['codeUrl']]);
        }
        $app = [
            "cusid"=>$order['cusid'],//商户号
            "appid"=>$order['appid']//appid
        ];
        //  $params["cusid"] =$app['cusid'];//商户号  1
        //        $params["appid"] = $app['appid'];//appid   1
        //        $params["version"] = $arr['version'];//版本号 1
        //        $params["randomstr"] =$arr['randomstr'];//随机串  1
        //        $params["trxamt"] = $arr['trxamt'];//支付金额 分 1
        //        $params["reqsn"] = $arr["reqsn"];//商户订单号  1
        //        $params["charset"] = $arr["charset"];//参数字符编码集  1
        //        $params["returl"] = $arr["returl"];//页面跳转同步通知页面路径  1
        //        $params["notify_url"] =$arr["notify_url"];//异步地址  1
        //        //$params["body"] = $arr["body"];//订单标题  1
        //        $params["body"] = "1";//订单标题  1
        //        //$params["remark"] = $arr["remark"];//备注  1
        //        $params["remark"] = "1";//备注  1
        ////        $params["validtime"] = $arr["validtime"];//有效时间分钟
        ////        $params["limit_pay"] =$arr["limit_pay"];//支付限制 no_credit--指定不能使用信用卡支付
        ////        $params["asinfo"] =$arr["asinfo"];//分账信息
        ////        $params["ishide"] =$arr["ishide"];//是否直接支付
        //        $params["signtype"] =$arr["signtype"];//签名类型 1
        //        //$params["paytype"] ="VSP551";//签名类型
        $params = array();
        $params["cusid"] = $app['cusid'];
        $params["appid"] = $app['appid'];
        $params["version"] = "11";
        $params["orgid"] = $app['cusid'];
        $params["trxamt"] = strval($money*100);
        $params["reqsn"] = $order['system_order_num'];
        $params["paytype"] = 'U01';
        $params["body"] = '1';
        $params["remark"] = '';
        $params["validtime"] = 12;
        //$params["acct"] = 'oM4tN4x6Cdp51AYTZCYzPN2E8gLU';
        $params["notify_url"] = ym()."/api/tlnotify/tongliannotifyyinlian";
        $params["limit_pay"] ='';
        //$params["sub_appid"] = 'wx58959207ed2673ca';
        $params["subbranch"] = '';
        $params["cusip"] = '';
        $params["idno"] = '';
        $params["truename"] ='';
        $params["fqnum"] ='';
        $params["randomstr"] =12;
        $params["signtype"] ='RSA';
        $params["front_url"] =ym()."/api/tonglianpay/returl";
        $params["sign"] = urlencode($this::Sign($params));//签名
        $paramsStr = $this::ToUrlParams($params);
        $url =  "https://vsp.allinpay.com/apiweb/unitorder/pay";
        $rsp = $this->request($url, $paramsStr);
//        echo "请求返回:".$rsp;
//        echo "<br/>";
        $rspArray = json_decode($rsp, true);
        //<pre>array(10) {
        //  ["appid"] =&gt; string(8) "00328484"
        //  ["cusid"] =&gt; string(15) "660551051372STF"
        //  ["payinfo"] =&gt; string(62) "https://qr.95516.com/00010000/01329152087872303724669641817784"
        //  ["randomstr"] =&gt; string(12) "630923328156"
        //  ["reqsn"] =&gt; string(17) "jlC23182754649679"
        //  ["retcode"] =&gt; string(7) "SUCCESS"
        //  ["sign"] =&gt; string(172) "ehrVYpdhzmsBD0tCY3FmSkSnBxYgSkn5DQgerq+AJ6PkaDHFzXnqkuCEaV3WckPXSn4ozZDsMHLdOgTnqFjrKnRVJI3ZfazOc6HU1XSqcvRKccW+Jnb17WQdAnTQUDW4DEOpfnTgHMPDvgycOlamQy1+Efvm0A5sJgoq1pthLVw="
        //  ["trxcode"] =&gt; string(6) "VSP551"
        //  ["trxid"] =&gt; string(18) "241223128111291607"
        //  ["trxstatus"] =&gt; string(4) "0000"
        //}
        //</pre>
        //dump($rspArray);
        if($rspArray['retcode'] == "FAIL"){
            return AjaxReturn(0,"错误原因".$rspArray['retmsg']);
        }
//        if($rspArray['retcode'] == "FAIL"){
//
//        }
        if($this::validSign($rspArray)){
            if(!$rspArray['payinfo']){
                return AjaxReturn(0,$rspArray['errmsg']);
            }else{
                //echo "验签正确,进行业务处理";
                $model::where(["id"=>$order["id"]])->update(["codeUrl"=>$rspArray['payinfo']]);
                return view("",["orderCode"=>$order['system_order_num'],"money"=>$order['money'],"url"=>$rspArray['payinfo']]);
            }
        }else{
            return AjaxReturn(0,"签名失败");
        }
    }


    //发送请求操作仅供参考,不为最佳实践
    public  function request($url,$params){
        $ch = curl_init();
        $this_header = array("content-type: application/x-www-form-urlencoded;charset=UTF-8");
        curl_setopt($ch,CURLOPT_HTTPHEADER,$this_header);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; MSIE 5.01; Windows NT 5.0)');
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);

        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);//如果不加验证,就设false,商户自行处理
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

        $output = curl_exec($ch);
        curl_close($ch);
        return  $output;
    }


    /**
     * 查询订单
     */
    public function orderdetails(){
        $order_num = input("get.order_num");
        $model = new TlOrder();
        $order = $model::where(["system_order_num"=>$order_num])->find();
        //halt($order);
        if(!$order){
            return AjaxReturn(0,"订单不存在");
        }
        if($order["payment"] == 1){
            return AjaxReturn(1,"付款成功");
        }
    }

    /**
     * 支付状态
     * https://pay.hnyouke888.cn/api/sandesm/payment.html?order_num=ym815649473909234
     * http://pay1.lnxinmeng.xyz/api/sandesm/payment.html?order_num=ym815649473909234
     */
    public function payment(){
        $order_num = input("get.order_num");
        $model = new TlOrder();
        $order = $model::where(["system_order_num"=>$order_num])->find();
        if(!$order){
            return AjaxReturn(0,"订单不存在");
        }
//        if($order["payment"] == 1){
//            return AjaxReturn(1,"付款成功");
//        }
        $this->assign("order",$order);
        return view();
    }




    /**
     * 银联扫码
     * 本地地址:  http://p.dinglianshop.cn/api/Tonglianpay/yinlianxcx
     * 网络地址:  https://pay.dinglianshop.cn/api/Tonglianpay/yinlianxcx
     */
    public function yinlianxcx(){
        $model = new TlOrder();
        $id = input("get.id");
        if(!$id){
            return AjaxReturn(0,"参数错误");
        }
        $order = $model::get($id);
        if(!$order){
            return AjaxReturn(0,"订单不存在");
        }
        if($order["payment"] == 1){
            return AjaxReturn(0,"订单已支付");
        }
        $ip = request()->ip();
        $model->where("id",$id)->update(["buy_ip"=>$ip]);//更新付款用户下单ip
        $PayChannelModel  = new TonglianChannel();
        $PayChannel = $PayChannelModel::get($order['bid']);
        if(!$PayChannel){
            return AjaxReturn(1,"支付通道不存在");
        }
        if($PayChannel['open'] != 1){
            return AjaxReturn(1,"通道未开启");
        }
        $money = $order['pay_money'];//支付金额
        if($order['codeUrl'] && !$order["payment"]){
            //return view("yinlian",["order"=>$order,"url"=>$data['body']['codeImgUrl']]);
            //$model::where(["id"=>$order["id"]])->update(["codeUrl"=>$data['body']['codeImgUrl']]);
            //halt(["orderCode"=>$order['system_order_num'],"money"=>$order['money'],"url"=>$order['codeUrl']]);
            return view("",["orderCode"=>$order['system_order_num'],"money"=>$order['money'],"url"=>$order['codeUrl']]);
        }
        $app = [
            "cusid"=>$order['cusid'],//商户号
            "appid"=>$order['appid']//appid
        ];
        //  $params["cusid"] =$app['cusid'];//商户号  1
        //        $params["appid"] = $app['appid'];//appid   1
        //        $params["version"] = $arr['version'];//版本号 1
        //        $params["randomstr"] =$arr['randomstr'];//随机串  1
        //        $params["trxamt"] = $arr['trxamt'];//支付金额 分 1
        //        $params["reqsn"] = $arr["reqsn"];//商户订单号  1
        //        $params["charset"] = $arr["charset"];//参数字符编码集  1
        //        $params["returl"] = $arr["returl"];//页面跳转同步通知页面路径  1
        //        $params["notify_url"] =$arr["notify_url"];//异步地址  1
        //        //$params["body"] = $arr["body"];//订单标题  1
        //        $params["body"] = "1";//订单标题  1
        //        //$params["remark"] = $arr["remark"];//备注  1
        //        $params["remark"] = "1";//备注  1
        ////        $params["validtime"] = $arr["validtime"];//有效时间分钟
        ////        $params["limit_pay"] =$arr["limit_pay"];//支付限制 no_credit--指定不能使用信用卡支付
        ////        $params["asinfo"] =$arr["asinfo"];//分账信息
        ////        $params["ishide"] =$arr["ishide"];//是否直接支付
        //        $params["signtype"] =$arr["signtype"];//签名类型 1
        //        //$params["paytype"] ="VSP551";//签名类型
        $params = array();
        $params["cusid"] = $app['cusid'];
        $params["appid"] = $app['appid'];
        $params["version"] = "11";
        $params["orgid"] = '';
        $params["trxamt"] = strval($money*100);
        $params["reqsn"] = $order['system_order_num'];
        $params["paytype"] = 'U01';
        $params["body"] = '1';
        $params["remark"] = '';
        $params["validtime"] = 12;
        //$params["acct"] = 'oM4tN4x6Cdp51AYTZCYzPN2E8gLU';
        $params["notify_url"] = ym()."/api/tlnotify/tongliannotifyyinlian";
        $params["limit_pay"] ='';
        //$params["sub_appid"] = 'wx58959207ed2673ca';
        $params["subbranch"] = '';
        $params["cusip"] = '';
        $params["idno"] = '';
        $params["truename"] ='';
        $params["fqnum"] ='';
        $params["randomstr"] =12;
        $params["signtype"] ='RSA';
        $params["front_url"] =ym()."/api/tonglianpay/returl";
        $params["sign"] = urlencode($this::Sign($params));//签名
        $paramsStr = $this::ToUrlParams($params);
        $url =  "https://vsp.allinpay.com/apiweb/unitorder/pay";
        $rsp = $this->request($url, $paramsStr);
//        echo "请求返回:".$rsp;
//        echo "<br/>";
        $rspArray = json_decode($rsp, true);
        //dump($rspArray);
        if($this::validSign($rspArray)){
            //echo "验签正确,进行业务处理";
            $model::where(["id"=>$order["id"]])->update(["codeUrl"=>$rspArray['payinfo']]);
            return AjaxReturn(1,"成功",["orderCode"=>$order['system_order_num'],"money"=>$order['money'],"url"=>$rspArray['payinfo']]);
        }else{
            return AjaxReturn(0,"签名失败");
        }
    }


    /**
     * 银联扫码
     * 本地地址:  http://p.dinglianshop.cn/api/Tonglianpay/yinlian
     * 网络地址:  https://pay.dinglianshop.cn/api/Tonglianpay/yinlian
     */
    public function yinlians(){
        $model = new TlOrder();
        $id = input("get.id");
        if(!$id){
            return AjaxReturn(0,"参数错误");
        }
        $order = $model::get($id);
        if(!$order){
            return AjaxReturn(0,"订单不存在");
        }
        if($order["payment"] == 1){
            return AjaxReturn(0,"订单已支付");
        }
        $ip = request()->ip();
        $model->where("id",$id)->update(["buy_ip"=>$ip]);//更新付款用户下单ip
        $PayChannelModel  = new TonglianChannel();
        $PayChannel = $PayChannelModel::get($order['bid']);
        if(!$PayChannel){
            return AjaxReturn(1,"支付通道不存在");
        }
        if($PayChannel['open'] != 1){
            return AjaxReturn(1,"通道未开启");
        }
        $money = $order['pay_money'];//支付金额
        if($order['codeUrl'] && !$order["payment"]){
            //return view("yinlian",["order"=>$order,"url"=>$data['body']['codeImgUrl']]);
            //$model::where(["id"=>$order["id"]])->update(["codeUrl"=>$data['body']['codeImgUrl']]);
            //halt(["orderCode"=>$order['system_order_num'],"money"=>$order['money'],"url"=>$order['codeUrl']]);
            return view("",["orderCode"=>$order['system_order_num'],"money"=>$order['money'],"url"=>$order['codeUrl']]);
        }
        $app = [
            "cusid"=>$order['cusid'],//商户号
            "appid"=>$order['appid']//appid
        ];

        $params = array();
        $params["cusid"] = $app['cusid'];
        $params["appid"] = $app['appid'];
        $params["version"] = "11";
        $params["orgid"] = '';
        $params["trxamt"] = strval($money*100);
        $params["reqsn"] = $order['system_order_num'];
        $params["paytype"] = 'U01';
        $params["body"] = '主题';
        $params["remark"] = '网课购买';
        $params["validtime"] = 12;
        //$params["acct"] = 'oM4tN4x6Cdp51AYTZCYzPN2E8gLU';
        $params["notify_url"] = ym()."/api/tlnotify/tongliannotifyyinlian";
        $params["limit_pay"] ='';
        //$params["sub_appid"] = 'wx58959207ed2673ca';
        $params["subbranch"] = '';
        $params["cusip"] = '';
        $params["idno"] = '';
        $params["truename"] ='';
        $params["fqnum"] ='';
        $params["randomstr"] =12;
        $params["signtype"] ='RSA';
        $params["front_url"] =ym()."/api/tonglianpay/returl";
        $params["sign"] = urlencode($this::Sign($params));//签名
        $paramsStr = $this::ToUrlParams($params);
        $url =  "https://vsp.allinpay.com/apiweb/unitorder/pay";
        $rsp = $this->request($url, $paramsStr);
//        echo "请求返回:".$rsp;
//        echo "<br/>";
        $rspArray = json_decode($rsp, true);
        //halt($rspArray);
        //dump($rspArray);
        if($this::validSign($rspArray)){
            if(!$rspArray['payinfo']){
                return AjaxReturn(0,$rspArray['errmsg']);
            }else{
                //echo "验签正确,进行业务处理";
                $model::where(["id"=>$order["id"]])->update(["codeUrl"=>$rspArray['payinfo']]);
                return view("",["orderCode"=>$order['system_order_num'],"money"=>$order['money'],"url"=>$rspArray['payinfo']]);
            }

        }else{
            return AjaxReturn(0,"签名失败");
        }
    }


    /**
     * 微信支付
     * 本地地址:  http://pay.cxlaimeng.cn/api/Tonglianpay/wechat?id=77632
     *  网络地址:  https://p.cxlaimeng.com/api/Tonglianpay/wechat
     */
    public function wechat()
    {
        $model = new TlOrder();
        $id = input("get.id");
        if(!$id){
            return AjaxReturn(0,"参数错误");
        }
        $order = $model::get($id);
        if(!$order){
            return AjaxReturn(0,"订单不存在");
        }
        if($order["payment"] == 1){
            return AjaxReturn(0,"订单已支付");
        }
        $ip = request()->ip();
        $model->where("id",$id)->update(["buy_ip"=>$ip]);//更新付款用户下单ip
        $PayChannelModel  = new TonglianChannel();
        $PayChannel = $PayChannelModel::get($order['bid']);
        if(!$PayChannel){
            return AjaxReturn(1,"支付通道不存在");
        }
        if($PayChannel['open'] != 1){
            return AjaxReturn(1,"通道未开启");
        }
        $money = $order['pay_money'];//支付金额
        if($order['codeUrl'] && !$order["payment"]){
            //return view("yinlian",["order"=>$order,"url"=>$data['body']['codeImgUrl']]);
            //$model::where(["id"=>$order["id"]])->update(["codeUrl"=>$data['body']['codeImgUrl']]);
            //halt(["orderCode"=>$order['system_order_num'],"money"=>$order['money'],"url"=>$order['codeUrl']]);
            if($order['type'] == 1){
                return view("",["orderCode"=>$order['system_order_num'],"money"=>$order['money'],"url"=>$order['codeUrl']]);
            }else{
                return view("alipay",["url"=>$order['codeUrl'],"money"=>$order['money']]);
            }
        }
        $url =  "https://tp.allinpay.com/pay/api/applet/getAppletUrl";
        $params = array();
        $params["mchNo"] = "E249974058305";//通企付商户号，示例：E249974037860
        $params["appId"] = "67247a83e4b072ece9d793b5";//默认为商户应用ID，若为服务商模式，需传入服务商appId
        $params["reqTime"] = round(microtime(true) * 1000);//请求时间，13位时间戳
        $params["version"] = "1.0";//版本（默认为1.0）
        $params["signType"] = "MD5";//签名类型（MD5/RSA/SM2）
        $params["mchOrderNo"] = $order['system_order_num'];//商户订单号
        $params["amount"] = strval($money * 100);//金额（单位：分）
        $params["body"] = "1";//商品描述
        $params["urlType"] = "URL_Common";//商品描述
        $params["notifyUrl"] = ym()."/api/tlnotify/tongliannotifywechat";//通知地址
        $params["channelExtra"] = json_encode(["orgid"=>"660551051372STF","cusid"=>"660551073992T0N"]);//channelExtra={"orgid":"660551051372STF","cusid":"660551073992T0N"}

        $key = "h4fi361gqce4qu4vsx7cego8p7ndrx3dqwy3an12y3ek580qbuhnw0k1eantjxts4rlu3r0teyac1slnt1cp0jsgyb639dh01wx6o68j2tks6yww2zijkp2pqr83j30e";
        $sign =  createSign($params,$key);
        $params["sign"] = $sign;//签名
        //dump($params);

        //$rsp = request_post_jsons($url,$params);
        $rsp = http_post($url,json_encode($params));
        $res = json_decode($rsp,true);
        if($res['data']['appletUrl']){
            $model::where(["id"=>$order["id"]])->update(["codeUrl"=>$res['data']['appletUrl']]);
            if ($order['type'] == 1) {
                return view("",["orderCode"=>$order['system_order_num'],"money"=>$order['money'],"url"=>$res['data']['appletUrl']]);
            }else{
                return view("alipay",["url"=>$res['data']['appletUrl'],"money"=>$order['money']]);
            }
        }else{
            return AjaxReturn(0,$res['msg']);
        }

        //array(4) {
        //  ["code"] => int(0)
        //  ["data"] => array(2) {
        //    ["urlType"] => string(10) "URL_Common"
        //    ["appletUrl"] => string(96) "https://tp.allinpay.com/j?cache=E24997405830514fd84c28f37437a9847553e59bfa57aymjlC20805993903806"
        //  }
        //  ["msg"] => string(7) "SUCCESS"
        //  ["sign"] => string(32) "AC36FF9F73F0B4739B7A1CCEC4FAEAE2"
        //}
    }



    /**
     * 微信支付
     * 本地地址:  http://pay.cxlaimeng.cn/api/Tonglianpay/wechat
     *  网络地址:  https://p.cxlaimeng.com/api/Tonglianpay/wechat
     */
    public function wechatshop()
    {

//        $url =  "https://tp.allinpay.com/pay/api/applet/getAppletUrl";
//        $params = array();
//        $params["mchNo"] = "E249974058305";//通企付商户号，示例：E249974037860
//        $params["appId"] = "67247a83e4b072ece9d793b5";//默认为商户应用ID，若为服务商模式，需传入服务商appId
//        $params["reqTime"] = round(microtime(true) * 1000);//请求时间，13位时间戳
//        $params["version"] = "1.0";//版本（默认为1.0）
//        $params["signType"] = "MD5";//签名类型（MD5/RSA/SM2）
//        $params["mchOrderNo"] = "ym".orderNum();//商户订单号
//        $params["amount"] = "3";//金额（单位：分）
//        $params["body"] = "商品购买";//商品描述
//        $params["urlType"] = "URL_Common";//商品描述
//        $params["notifyUrl"] = ym()."/api/tlnotify/tongliannotifyyinlian";//通知地址
//        $params["channelExtra"] = json_encode(["orgid"=>"660551051372STF","cusid"=>"660551073992T0N"]);//channelExtra={"orgid":"660551051372STF","cusid":"交易商户号"}
//
//        $key = "h4fi361gqce4qu4vsx7cego8p7ndrx3dqwy3an12y3ek580qbuhnw0k1eantjxts4rlu3r0teyac1slnt1cp0jsgyb639dh01wx6o68j2tks6yww2zijkp2pqr83j30e";
//        $sign =  createSign($params,$key);
//        $params["sign"] = $sign;//签名
//        dump($params);
//
//        //$rsp = request_post_jsons($url,$params);
//        $rsp = http_post($url,json_encode($params));
//        halt(json_decode($rsp,true));
        $model = new TlOrder();
        $id = input("get.id");
        if(!$id){
            return AjaxReturn(0,"参数错误");
        }
        $order = $model::get($id);
        if(!$order){
            return AjaxReturn(0,"订单不存在");
        }
        if($order["payment"] == 1){
            return AjaxReturn(0,"订单已支付");
        }
        $ip = request()->ip();
        $model->where("id",$id)->update(["buy_ip"=>$ip]);//更新付款用户下单ip
        $PayChannelModel  = new TonglianChannel();
        $PayChannel = $PayChannelModel::get($order['bid']);
        if(!$PayChannel){
            return AjaxReturn(1,"支付通道不存在");
        }
        if($PayChannel['open'] != 1){
            return AjaxReturn(1,"通道未开启");
        }
        $money = $order['pay_money'];//支付金额
        if($order['codeUrl'] && !$order["payment"]){
            //return view("yinlian",["order"=>$order,"url"=>$data['body']['codeImgUrl']]);
            //$model::where(["id"=>$order["id"]])->update(["codeUrl"=>$data['body']['codeImgUrl']]);
            //halt(["orderCode"=>$order['system_order_num'],"money"=>$order['money'],"url"=>$order['codeUrl']]);
            //return view("wechat",["orderCode"=>$order['system_order_num'],"money"=>$order['money'],"url"=>$order['codeUrl']]);
            return AjaxReturn(1,"ok",["orderCode"=>$order['system_order_num'],"money"=>$order['money'],"url"=>$order['codeUrl']]);
        }
        $url =  "https://tp.allinpay.com/pay/api/applet/getAppletUrl";
        $params = array();
        $params["mchNo"] = "E249974058305";//通企付商户号，示例：E249974037860
        $params["appId"] = "67247a83e4b072ece9d793b5";//默认为商户应用ID，若为服务商模式，需传入服务商appId
        $params["reqTime"] = round(microtime(true) * 1000);//请求时间，13位时间戳
        $params["version"] = "1.0";//版本（默认为1.0）
        $params["signType"] = "MD5";//签名类型（MD5/RSA/SM2）
        $params["mchOrderNo"] = $order['system_order_num'];//商户订单号
        $params["amount"] = strval($money * 100);//金额（单位：分）
        $params["body"] = "";//商品描述
        $params["urlType"] = "URL_Common";//商品描述
        $params["notifyUrl"] = ym()."/api/tlnotify/tongliannotifywechat";//通知地址
        $params["channelExtra"] = json_encode(["orgid"=>"660551051372STF","cusid"=>"660551073992T0N"]);//channelExtra={"orgid":"660551051372STF","cusid":"交易商户号"}

        $key = "h4fi361gqce4qu4vsx7cego8p7ndrx3dqwy3an12y3ek580qbuhnw0k1eantjxts4rlu3r0teyac1slnt1cp0jsgyb639dh01wx6o68j2tks6yww2zijkp2pqr83j30e";
        $sign =  createSign($params,$key);
        $params["sign"] = $sign;//签名
        //dump($params);

        //$rsp = request_post_jsons($url,$params);
        $rsp = http_post($url,json_encode($params));
        $res = json_decode($rsp,true);
        if($res['data']['appletUrl']){
            $model::where(["id"=>$order["id"]])->update(["codeUrl"=>$res['data']['appletUrl']]);
            //return view("wechat",["orderCode"=>$order['system_order_num'],"money"=>$order['money'],"url"=>$res['data']['appletUrl']]);
            return AjaxReturn(1,"ok",["orderCode"=>$order['system_order_num'],"money"=>$order['money'],"url"=>$res['data']['appletUrl']]);
        }else{
            return AjaxReturn(0,$res['msg']);
        }
    }



    public function yinlianshop(){
        $model = new TlOrder();
        $id = input("get.id");
        if(!$id){
            return AjaxReturn(0,"参数错误");
        }
        $order = $model::get($id);
        if(!$order){
            return AjaxReturn(0,"订单不存在");
        }
        if($order["payment"] == 1){
            return AjaxReturn(0,"订单已支付");
        }
        $ip = request()->ip();
        $model->where("id",$id)->update(["buy_ip"=>$ip]);//更新付款用户下单ip
        $PayChannelModel  = new TonglianChannel();
        $PayChannel = $PayChannelModel::get($order['bid']);
        if(!$PayChannel){
            return AjaxReturn(1,"支付通道不存在");
        }
        if($PayChannel['open'] != 1){
            return AjaxReturn(1,"通道未开启");
        }
        $money = $order['pay_money'];//支付金额
        if($order['codeUrl'] && !$order["payment"]){
            //return view("yinlian",["order"=>$order,"url"=>$data['body']['codeImgUrl']]);
            //$model::where(["id"=>$order["id"]])->update(["codeUrl"=>$data['body']['codeImgUrl']]);
            //halt(["orderCode"=>$order['system_order_num'],"money"=>$order['money'],"url"=>$order['codeUrl']]);
            return AjaxReturn(1,"ok",["orderCode"=>$order['system_order_num'],"money"=>$order['money'],"url"=>$order['codeUrl']]);
        }
        $app = [
            "cusid"=>$order['cusid'],//商户号
            "appid"=>$order['appid']//appid
        ];
        //  $params["cusid"] =$app['cusid'];//商户号  1
        //        $params["appid"] = $app['appid'];//appid   1
        //        $params["version"] = $arr['version'];//版本号 1
        //        $params["randomstr"] =$arr['randomstr'];//随机串  1
        //        $params["trxamt"] = $arr['trxamt'];//支付金额 分 1
        //        $params["reqsn"] = $arr["reqsn"];//商户订单号  1
        //        $params["charset"] = $arr["charset"];//参数字符编码集  1
        //        $params["returl"] = $arr["returl"];//页面跳转同步通知页面路径  1
        //        $params["notify_url"] =$arr["notify_url"];//异步地址  1
        //        //$params["body"] = $arr["body"];//订单标题  1
        //        $params["body"] = "1";//订单标题  1
        //        //$params["remark"] = $arr["remark"];//备注  1
        //        $params["remark"] = "1";//备注  1
        ////        $params["validtime"] = $arr["validtime"];//有效时间分钟
        ////        $params["limit_pay"] =$arr["limit_pay"];//支付限制 no_credit--指定不能使用信用卡支付
        ////        $params["asinfo"] =$arr["asinfo"];//分账信息
        ////        $params["ishide"] =$arr["ishide"];//是否直接支付
        //        $params["signtype"] =$arr["signtype"];//签名类型 1
        //        //$params["paytype"] ="VSP551";//签名类型
        $params = array();
        $params["cusid"] = $app['cusid'];
        $params["appid"] = $app['appid'];
        $params["version"] = "11";
        $params["orgid"] = $app['cusid'];
        $params["trxamt"] = strval($money*100);
        $params["reqsn"] = $order['system_order_num'];
        $params["paytype"] = 'U01';
        $params["body"] = '';
        $params["remark"] = '';
        $params["validtime"] = 12;
        //$params["acct"] = 'oM4tN4x6Cdp51AYTZCYzPN2E8gLU';
        $params["notify_url"] = ym()."/api/tlnotify/tongliannotifyyinlian";
        $params["limit_pay"] ='';
        //$params["sub_appid"] = 'wx58959207ed2673ca';
        $params["subbranch"] = '';
        $params["cusip"] = '';
        $params["idno"] = '';
        $params["truename"] ='';
        $params["fqnum"] ='';
        $params["randomstr"] =12;
        $params["signtype"] ='RSA';
        $params["front_url"] =ym()."/api/tonglianpay/returl";
        $params["sign"] = urlencode($this::Sign($params));//签名
        $paramsStr = $this::ToUrlParams($params);
        $url =  "https://vsp.allinpay.com/apiweb/unitorder/pay";
        $rsp = $this->request($url, $paramsStr);
//        echo "请求返回:".$rsp;
//        echo "<br/>";
        $rspArray = json_decode($rsp, true);
        //<pre>array(10) {
        //  ["appid"] =&gt; string(8) "00328484"
        //  ["cusid"] =&gt; string(15) "660551051372STF"
        //  ["payinfo"] =&gt; string(62) "https://qr.95516.com/00010000/01329152087872303724669641817784"
        //  ["randomstr"] =&gt; string(12) "630923328156"
        //  ["reqsn"] =&gt; string(17) "jlC23182754649679"
        //  ["retcode"] =&gt; string(7) "SUCCESS"
        //  ["sign"] =&gt; string(172) "ehrVYpdhzmsBD0tCY3FmSkSnBxYgSkn5DQgerq+AJ6PkaDHFzXnqkuCEaV3WckPXSn4ozZDsMHLdOgTnqFjrKnRVJI3ZfazOc6HU1XSqcvRKccW+Jnb17WQdAnTQUDW4DEOpfnTgHMPDvgycOlamQy1+Efvm0A5sJgoq1pthLVw="
        //  ["trxcode"] =&gt; string(6) "VSP551"
        //  ["trxid"] =&gt; string(18) "241223128111291607"
        //  ["trxstatus"] =&gt; string(4) "0000"
        //}
        //</pre>
        //dump($rspArray);
        if($rspArray['retcode'] == "FAIL"){
            return AjaxReturn(0,"错误原因".$rspArray['retmsg']);
        }
//        if($rspArray['retcode'] == "FAIL"){
//
//        }
        if($this::validSign($rspArray)){
            if(!$rspArray['payinfo']){
                return AjaxReturn(0,$rspArray['errmsg']);
            }else{
                //echo "验签正确,进行业务处理";
                $model::where(["id"=>$order["id"]])->update(["codeUrl"=>$rspArray['payinfo']]);
                return AjaxReturn(1,"ok",["orderCode"=>$order['system_order_num'],"money"=>$order['money'],"url"=>$rspArray['payinfo']]);
            }
        }else{
            return AjaxReturn(0,"签名失败");
        }
    }



}