<?php

namespace app\api\controller;


use app\common\model\XinshengChannel;
use think\Controller;
use think\facade\Env;
use think\facade\Request;
use think\facade\Validate;



class Xinshengpay extends Controller
{


    /**
     * @var string
     */
    //正式
    private $url = 'https://gateway.hnapay.com/merchant/acct/queryBalance.do';

    private $pubic_key = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDAfRdhlZPcc0p/ICyKGF2hJkrXtnvxGXEdi23J++9xe07lrg/vNms1trVLvbQFhTFPdHbXf3cQAab3zDFNDKoiliM2uFxsrenV4SapS/kPn50Nb/HbMQDrXWPBmCZ8QSAfZeaAouUdKoMWYbdDKugOV1sO5CXA7G/4czF2mV1bbQIDAQAB";

    private $private_key = "MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAMB9F2GVk9xzSn8gLIoYXaEmSte2e/EZcR2Lbcn773F7TuWuD+82azW2tUu9tAWFMU90dtd/dxABpvfMMU0MqiKWIza4XGyt6dXhJqlL+Q+fnQ1v8dsxAOtdY8GYJnxBIB9l5oCi5R0qgxZht0Mq6A5XWw7kJcDsb/hzMXaZXVttAgMBAAECgYAGRg11CMMA9GEBM7M/7PtgPTdDQJdZjFK7e8fgRW3QbQxtP/cU/NkRrxrJlS1A4M5yoIEfFPB0fRlMCkOxFzCUUYL4i54g+sFgwd5t/xrooXis3DQWG022zHG6slxQjlZKTFxAEKZ65ZFfDQZLSvVX0O5wE3KkaYx7iPd+Rwk6oQJBAOnjkB+PzeLbcGa0K03BNDMwrtIRe1IAvRejNovms0GQl398nqgKY8xO5xJUNSa0s/Q7hAri3/ImBozV2KneVUcCQQDSr5WB8mve6JUHfQ4b1SHZkfZ7PwTbqphw1b1fyItRMnNykpSwPgOrWvEuJadGSB07EBSwqnI2lgos8j1F2POrAkEAzanI5bfx9vW21NjA61JksE6yqw2sPuNfYxWltUdbgN/AF95hNKpOLy2Cit95DVwAkVZ0OY5eFfPR9q1kI5rZqQJAUFflC8kGmdrX+iMWXkbIwc+HzzgZAivsNhtMPFqdxjkXDXoYeZ0siQwFLTPutj6J5xwax8Q4CdjtyHYYJ6q0QQJBAJE2rOoPe39ZuZOlM9Ncf2p48NA/XAP4CjacC5AOPO2MXCP+niONnsYsGjXoomxjbqcKX0eSqilTO9xNE+omCO0=";

    /**
     * 查询余额列表
     * http://pay.cxlaimeng.cn/api/Xinshengpay/test
     */
    public function test()
    {
        $list = XinshengChannel::all();
        foreach ($list as $k=>$v) {
            $this->chaxunBalance($v['merId']);
        }
        return AjaxReturn(1,"请求成功");
    }


    /**
     * @return void
     * http://pay.cxlaimeng.cn/api/Xinshengpay/chaxunBalance
     */
    public function chaxunBalance($merId)
    {

        $arr = [
            "version" => "2.0",
            "tranCode" => "QB01",
            "merId" => $merId,
            "acctType" => "11",
            "signType" => "1",
            "remark" => "",
            "charset" => "1",
        ];
        $res = $this->unifiedOrder($arr);
        $res = json_decode($res,true);
        //halt($res);
        //array(13) {
        //  ["charset"] => string(1) "1"
        //  ["avaBalance"] => string(6) "383.93"//账户可用余额
        //  ["resultCode"] => string(4) "0000"
        //  ["errorCode"] => string(0) ""
        //  ["remark"] => string(0) ""
        //  ["version"] => string(3) "2.0"
        //  ["signValue"] => string(172) "rZ/Gi27vSGMmpPRbN52EHL8Qb4sDSA5ccxKjQMOYeirIe70FTf1amt8wiwgZOCpb+DildHpY6Y+8booMPBJVPDyYhme+EcS81f0Th3daQtKSgefcjwW6Kox/qjDQrzL4HAuYxh8VtyU5vtNB+gRLK7I3I9FkUZh3WIMiA2neFpM="
        //  ["errorMsg"] => string(0) ""
        //  ["pendAmt"] => string(1) "0" //待结转金额
        //  ["acctType"] => string(2) "11"
        //  ["signType"] => string(1) "1"
        //  ["merId"] => string(11) "11000010694"
        //  ["tranCode"] => string(4) "QB01"
        //}
        if($res['resultCode'] == '0000'){
            //修改数据库
            XinshengChannel::where(["merId"=>$merId])->update(["avaBalance"=>$res['avaBalance'],"pendAmt"=>$res['pendAmt']]);
            //return AjaxReturn(1,"请求成功");
        }else{
            //return AjaxReturn(0,"请求失败,请重试");
        }
    }


    /**
     * @param array $parameter_data
     * @return false|void
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function unifiedOrder(array $parameter_data = [])
    {

        $parameter = array_merge([
            "version" => "2.0",
            "tranCode" => "QB01",
            "merId" => "",
            "acctType" => "11",
            "signType" => "1",
            "remark" => "",
            "charset" => "1",
        ], $parameter_data);
//        dump(json_encode($parameter));
        $info = XinshengChannel::where(["merId"=>$parameter_data['merId']])->find();
        $parameter['signValue'] = self::sign($parameter);


//        dump($parameter);
        // 请求
        $result =request_post($this->url, $parameter);

        //var_dump("结果", $result);
//        if (empty($result['return_code'])) {
//            return 0;
//        }
        //var_dump($result);
        return $result;
    }



    public static function httpPost( $url = '',  $form_params = [],  $headers = [],  $timeout = 10)
    {
        try {
            $client = new Client([
                'timeout' => $timeout,
                'verify' => false,
            ]);
            $response = $client->request('POST', $url, [
                'form_params' => $form_params,
                'headers' => $headers
            ]);
            return json_decode($response->getBody()->getContents(), true);
        } catch (\Exception $exception) {
            return false;
        }
    }

    protected function sign( $data = [])
    {

        $list = ["version", "tranCode", "merId", "acctType", "charset", "signType"];
        $str = "";
        foreach ($list as $key) {
            $str .= $key . '=[' . $data[$key] . "]";
        }
//        $priKeys = $this->privateKey();
//        dump($priKeys);
        $priKeyss = XinshengChannel::where(["merId"=>$data['merId']])->value("private_key");
//        //dump($priKey);
//        $priKey = 'MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAMB9F2GVk9xzSn8gLIoYXaEmSte2e/EZcR2Lbcn773F7TuWuD+82azW2tUu9tAWFMU90dtd/dxABpvfMMU0MqiKWIza4XGyt6dXhJqlL+Q+fnQ1v8dsxAOtdY8GYJnxBIB9l5oCi5R0qgxZht0Mq6A5XWw7kJcDsb/hzMXaZXVttAgMBAAECgYAGRg11CMMA9GEBM7M/7PtgPTdDQJdZjFK7e8fgRW3QbQxtP/cU/NkRrxrJlS1A4M5yoIEfFPB0fRlMCkOxFzCUUYL4i54g+sFgwd5t/xrooXis3DQWG022zHG6slxQjlZKTFxAEKZ65ZFfDQZLSvVX0O5wE3KkaYx7iPd+Rwk6oQJBAOnjkB+PzeLbcGa0K03BNDMwrtIRe1IAvRejNovms0GQl398nqgKY8xO5xJUNSa0s/Q7hAri3/ImBozV2KneVUcCQQDSr5WB8mve6JUHfQ4b1SHZkfZ7PwTbqphw1b1fyItRMnNykpSwPgOrWvEuJadGSB07EBSwqnI2lgos8j1F2POrAkEAzanI5bfx9vW21NjA61JksE6yqw2sPuNfYxWltUdbgN/AF95hNKpOLy2Cit95DVwAkVZ0OY5eFfPR9q1kI5rZqQJAUFflC8kGmdrX+iMWXkbIwc+HzzgZAivsNhtMPFqdxjkXDXoYeZ0siQwFLTPutj6J5xwax8Q4CdjtyHYYJ6q0QQJBAJE2rOoPe39ZuZOlM9Ncf2p48NA/XAP4CjacC5AOPO2MXCP+niONnsYsGjXoomxjbqcKX0eSqilTO9xNE+omCO0=';
//        halt($priKey == $priKeyss);
        $priKey = $this->privateKeys($priKeyss);
        if (!$priKey) {
            throw new \Exception("Failed to read private key file！");
        }
        if (!openssl_pkey_get_private($priKey)) {
            throw new \Exception("Private key is not available");
        }

        $res = openssl_get_privatekey($priKey);
        openssl_sign($str, $signature, $res, OPENSSL_ALGO_SHA1);
        openssl_free_key($res);
        return base64_encode($signature);
    }

    function signCheck($data, $sign)
    {
        return (bool)openssl_verify($data, base64_decode($sign), $this->pubicKey());
    }

    private function pubicKey()
    {
        // 加载公钥
        return "-----BEGIN PUBLIC KEY-----\n" .
            wordwrap($this->pubic_key, 64, "\n", true) .
            "\n-----END PUBLIC KEY-----";
    }
    private function privateKeys($private_key)
    {

        return "-----BEGIN RSA PRIVATE KEY-----\n" .
            wordwrap($private_key, 64, "\n", true) .
            "\n-----END RSA PRIVATE KEY-----";
    }

    private function privateKey()
    {

        return "-----BEGIN RSA PRIVATE KEY-----\n" .
            wordwrap($this->private_key, 64, "\n", true) .
            "\n-----END RSA PRIVATE KEY-----";
    }

    public function publicEncrypt($data)
    {

        $pubKey = $this->pubicKey();
        $encrypted = "";
        if (!$pubKey) {
            throw new \Exception("Failed to read public key file！");
        }
        if (!openssl_pkey_get_public($pubKey)) {
            throw new \Exception("Public key is not available");
        }
        foreach (str_split($data, 117) as $chunk) {
            openssl_public_encrypt($chunk, $encryptData, $pubKey);
            $encrypted .= $encryptData;
        }
        if (!$encrypted)
            throw new \Exception('Unable to encrypt data.');
        return base64_encode($encrypted);

    }








}