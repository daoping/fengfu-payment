<?php

namespace app\api\controller;

use app\common\model\Agent;
use app\common\model\Order;
use app\common\model\PayChannel;
use app\common\model\XinshengChannel;
use app\common\model\XsOrder;
use JytPay\Client_ONEPAY\JytJsonClient;
use RSAUtils;
use think\Controller;
use think\facade\Env;
use think\facade\Request;
use think\facade\Validate;


include_once  Env::get("root_path"). "extend/utils/ExpUtils.php";
include_once  Env::get("root_path"). "extend/utils/RSAUtils.php";
class Xspay extends Controller
{

    /**
     * 新生银联扫码支付
     * http://pay.cxlaimeng.cn/api/Xspay/yinlian
     *  https://pay.cxlaimeng.com/api/Xspay/yinlian
     */
    public function yinlian()
    {
        $model = new XsOrder();
        $id = input("get.id");
        if(!$id){
            return AjaxReturn(0,"参数错误");
        }
        $order = $model::get($id);
        if(!$order){
            return AjaxReturn(0,"订单不存在");
        }
        if($order["payment"] == 1){
            return AjaxReturn(0,"订单已支付");
        }
        $ip = request()->ip();
        $model->where("id",$id)->update(["buy_ip"=>$ip]);//更新付款用户下单ip
        $PayChannelModel  = new XinshengChannel();
        $PayChannel = $PayChannelModel::get($order['bid']);
        if(!$PayChannel){
            return AjaxReturn(1,"支付通道不存在");
        }
        if($PayChannel['open'] != 1){
            return AjaxReturn(1,"通道未开启");
        }
        $money = $order['pay_money'];//支付金额
        if($order['codeUrl'] && !$order["payment"]){
            //return view("yinlian",["order"=>$order,"url"=>$data['body']['codeImgUrl']]);
            //$model::where(["id"=>$order["id"]])->update(["codeUrl"=>$data['body']['codeImgUrl']]);
            //halt(["orderCode"=>$order['system_order_num'],"money"=>$order['money'],"url"=>$order['codeUrl']]);
            return view("",["orderCode"=>$order['system_order_num'],"money"=>$order['money'],"url"=>$order['codeUrl']]);
        }
        $serverIP = $_SERVER['SERVER_ADDR'];
        if($serverIP == "127.0.0.1"){
            $serverIP = "47.121.219.250";
        }
        $merId = $PayChannel['merId'];// 方哥 11000010741    11000010694 荣光
        $url = "https://gateway.hnapay.com/website/scanPay.do";
        $arr = [
            "version"=>"2.1",
            "tranCode"=>"WS01",
            "merId"=>$merId,
            "merOrderNum"=>$order['system_order_num'],
            "tranAmt"=>strval($money*100),
            "submitTime"=>date("YmdHis"),//存数据库
            "payType"=>"QRCODE_B2C",
            "orgCode"=>"UNIONPAY",
            "tranIP"=>$serverIP,//换系统这边要换
            "notifyUrl"=>ym()."/api/xsnotify/xinshengnotify",
            "charset"=>"1",
            "signType"=>"1",
        ];
//        $str = "tranCode=[{$arr['tranCode']}]version=[{$arr['version']}]merId=[{$arr['merId']}]submitTime=[{$arr['submitTime']}]merOrderNum=[{$arr['merOrderNum']}]tranAmt=[{$arr['tranAmt']}]payType=[{$arr['payType']}]orgCode=[{$arr['orgCode']}]notifyUrl=[{$arr['notifyUrl']}]charset=[{$arr['charset']}]signType=[{$arr['signType']}]";
//        echo $str;
        $ExpUtils = new \ExpUtils();
        $signField =  ["tranCode", "version", "merId", "submitTime", "merOrderNum", "tranAmt", "payType",
            "orgCode", "notifyUrl", "charset", "signType"];
        $arr['signMsg'] = $ExpUtils::sign($signField, $arr,1);
        //halt($res);
        try {
//            echo "---------------调用交易请求开始<br />";
//            dump($arr);
            $result = $ExpUtils::post($url, $arr, 1);
//            dump($result);
            $resultMap = json_decode($result, true);
            //dump($resultMap);
            //返回结果
            if ("0000" != $resultMap['resultCode']){
                echo $result;
                echo "调用交易失败:".$resultMap['resultCode'];  return;
            }
            if($resultMap['resultCode'] == "0000" && $resultMap['qrCodeUrl']){
                $model::where(["id"=>$order["id"]])->update(["codeUrl"=>$resultMap['qrCodeUrl'],"trxid"=>$resultMap['hnapayOrderId']]);
                return view("",["orderCode"=>$order['system_order_num'],"money"=>$order['money'],"url"=>$resultMap['qrCodeUrl']]);
            }
            //halt($resultMap);
            //array(14) {
            //  ["charset"] => string(1) "1"
            //  ["msgExt"] => string(0) ""
            //  ["hnapayOrderId"] => string(16) "2024110979244543"
            //  ["resultCode"] => string(4) "0000"
            //  ["errorCode"] => string(0) ""
            //  ["version"] => string(3) "2.1"
            //  ["merOrderNum"] => string(17) "jlB08778366035976"
            //  ["submitTime"] => string(14) "20241109123851"
            //  ["qrCodeUrl"] => string(149) "https://qrcode.hnapay.com/qrcode.shtml?qrContent=https://qr.95516.com/00010000/01413615207089945753149009118971&sign=11A030ED4479E39B684C0B04FFECA33A"
            //  ["tranAmt"] => string(1) "2"
            //  ["signType"] => string(1) "1"
            //  ["merId"] => string(11) "11000010741"
            //  ["tranCode"] => string(4) "WS01"
            //  ["signMsg"] => string(256) "9d2b7b940e2f93167c53256fb5e7641fc367134917adc326d88a2e21e6a3960d5980c3a67316f17041d7cdbc2c3e0ede0e4da6c348d0578dacf156cdf9f88543e4d33c23f3fe91d7f2f5da28110383bb9475e12ff21d94b2ca39c3c4ff0b73358e370fef1b498ab50e2c460908ef110c219102e73d938d0b68730e51159ef8b1"
            //}

//            $verifyField =  ["tranCode", "version", "merId", "merOrderNum", "tranAmt", "submitTime", "qrCodeUrl",
//                "hnapayOrderId", "resultCode", "charset", "signType"];
//            //验签
//            $verify = $ExpUtils::verify($verifyField, $resultMap, $resultMap['signMsg']);
//            if(!$verify) {
//                echo "交易返回结果验签失败";  return;
//            }
//            echo "---------------调用交易请求结束";
        } catch (Exception $e){
            echo "exception:".$e->getMessage();
        }

    }

    /**
     * 查询订单
     */
    public function orderdetails(){
        $order_num = input("get.order_num");
        $model = new XsOrder();
        $order = $model::where(["system_order_num"=>$order_num])->find();
        //halt($order);
        if(!$order){
            return AjaxReturn(0,"订单不存在");
        }
        if($order["payment"] == 1){
            return AjaxReturn(1,"付款成功");
        }
    }

    public function payment(){
        $order_num = input("get.order_num");
        $model = new XsOrder();
        $order = $model::where(["system_order_num"=>$order_num])->find();
        if(!$order){
            return AjaxReturn(0,"订单不存在");
        }
//        if($order["payment"] == 1){
//            return AjaxReturn(1,"付款成功");
//        }
        $this->assign("order",$order);
        return view();
    }

    /**
     * 新生扫码支付
     * http://pay.cxlaimeng.cn/api/Xspay/scanPaytest
     * https://pay.cxlaimeng.com/api/Xspay/scanPaytest
     */
    public function scanPaytest()
    {


        $merId = "11000010694";// 方哥 11000010741    11000010694 荣光
        $url = "https://gateway.hnapay.com/website/scanPay.do";
        $arr = [
            "version"=>"2.1",
            "tranCode"=>"WS01",
            "merId"=>$merId,
            "merOrderNum"=>orderNum(),
            "tranAmt"=>"200",
            "submitTime"=>date("YmdHis"),//存数据库
            "payType"=>"QRCODE_B2C",
            "orgCode"=>"UNIONPAY",
            "tranIP"=>"47.121.219.250",
            "notifyUrl"=>"https://pay.cxlaimeng.com/api/xsnotify/xinshengnotify",
            "charset"=>"1",
            "signType"=>"1",
        ];
//        $str = "tranCode=[{$arr['tranCode']}]version=[{$arr['version']}]merId=[{$arr['merId']}]submitTime=[{$arr['submitTime']}]merOrderNum=[{$arr['merOrderNum']}]tranAmt=[{$arr['tranAmt']}]payType=[{$arr['payType']}]orgCode=[{$arr['orgCode']}]notifyUrl=[{$arr['notifyUrl']}]charset=[{$arr['charset']}]signType=[{$arr['signType']}]";
//        echo $str;
        $ExpUtils = new \ExpUtils();
        $signField =  ["tranCode", "version", "merId", "submitTime", "merOrderNum", "tranAmt", "payType",
            "orgCode", "notifyUrl", "charset", "signType"];
        $arr['signMsg'] = $ExpUtils::sign($signField, $arr,1);
        //halt($res);
        try {
//            echo "---------------调用交易请求开始<br />";
//            dump($arr);
            $result = $ExpUtils::post($url, $arr, 1);
//            dump($result);
            $resultMap = json_decode($result, true);
            //dump($resultMap);
            //返回结果
            if ("0000" != $resultMap['resultCode']){
                echo $result;
                echo "调用交易失败:".$resultMap['resultCode'];  return;
            }
            $verifyField =  ["tranCode", "version", "merId", "merOrderNum", "tranAmt", "submitTime", "qrCodeUrl",
                "hnapayOrderId", "resultCode", "charset", "signType"];
            //验签
            $verify = $ExpUtils::verify($verifyField, $resultMap, $resultMap['signMsg']);
            if(!$verify) {
                echo "交易返回结果验签失败";  return;
            }
            echo "---------------调用交易请求结束";
        } catch (Exception $e){
            echo "exception:".$e->getMessage();
        }
    }

    //jlB07642615164000
    /**
     * 订单查询
     * http://pay.cxlaimeng.cn/api/Xspay/querypay
     * https://pay.cxlaimeng.com/api/Xspay/querypay
     */
    public function querypay()
    {
        $data = input("post.");
        $validate = Validate::make([
            'order_num|订单号'             => 'require|length:10,40',
            'agent_id|代理ID'                => 'require',
            'sign|签名'                => 'require|length:32',
        ]);
        if (!$validate->check($data)) {
            return AjaxReturn(0,$validate->getError());
        }

        $agent = Agent::get($data["agent_id"]);
        if(!$agent){
            return AjaxReturn(0,'商户不存在');
        }
        if($agent["is_lock"]){
            return AjaxReturn(0,"商户暂停锁定");
        }
        $arr = [
            'agent_id'              => $data['agent_id'],
            'order_num'             => $data['order_num'],
        ];
        $sign=createSign($arr,$agent["key"]);
        if($sign != $data['sign']){
            return AjaxReturn(0,"签名错误");
        }
        $model = new XsOrder();
        $info =$model::where(["order_num"=>$data["order_num"],"agent_id"=>$data['agent_id']])->find();
        if(!$info){
            return AjaxReturn(0,"订单不存在");
        }
        $pay_channel_model = new XinshengChannel();
        $pay_channel = $pay_channel_model::where(["open"=>1,"id"=>$info['bid']])->find();
        $partnerID = $pay_channel['merId'];// 方哥 11000010741    11000010694 荣光
        $url = "https://gateway.hnapay.com/website/queryOrderResult.htm";//查询接口
        $arr = [
            "version"=>"2.7",
            "serialID"=>orderNum(),
            "mode"=>"1",
            "type"=>"1",
            "orderID"=>$info['system_order_num'],//失败 jlB08310609938748   成功 jlB08300054770662
            "beginTime"=>date("YmdHis",time()-86400),
            "endTime"=>date("YmdHis",time()+6400),
            "partnerID"=>$partnerID,
            "remark"=>"chaxun",
            "charset"=>"1",
            "signType"=>"1",

        ];
        $ExpUtils = new \ExpUtils();
        $data = array_string($arr);
        //halt($data);
        $priKey = file_get_contents( Env::get("root_path"). "extend/xinsheng/".$partnerID."/pay/private.pem");
        $RSAUtils = new \RSAUtils();
        $arr['signMsg'] = $RSAUtils->sign($data,$priKey);
        try {
            $result = request_post($url, $arr);
            parse_str($result,$resultMap);
            if(!$resultMap['queryDetails']){
                return AjaxReturn(0,"支付公司订单查询失败");
            }
            $res = explode(",",$resultMap['queryDetails']);
            //dump($res);
            if(!$res){
                return AjaxReturn(0,"订单查询失败");
            }
            if ("0000" != $resultMap['resultCode']){
                //echo $result;
                //echo "调用交易查询失败:".$resultMap['resultCode'];  return;
                return AjaxReturn(0,"调用交易查询失败:".$resultMap['resultCode']);
            }
            if($res[6] == 1){
                $notify_data = [
                    "order_num" => $info["order_num"],
                    "money" => $info["money"],
                    "agent_id" => $info["id"],
                    "system_order_num" => $info["system_order_num"],
                    "status" => "INIT", //INIT(待支付)/SUCCESS(成功)/CANCEL(已取消)/REFUND(已退款)/REFUNDING(退款中)/REFUNDFAIL(退款失败)
                    "pay_time" => 0,
                ];
                return AjaxReturn(2,"付款中",$notify_data);
            }elseif ($res[6] == 2){
                $notify_data = [
                    "order_num" => $info["order_num"],
                    "money" => $info["money"],
                    "agent_id" => $info["id"],
                    "system_order_num" => $info["system_order_num"],
                    "status" => "SUCCESS", //INIT(待支付)/SUCCESS(成功)/CANCEL(已取消)/REFUND(已退款)/REFUNDING(退款中)/REFUNDFAIL(退款失败)
                    "pay_time" => time(),
                ];
                return AjaxReturn(1,"交易成功",$notify_data);
            }elseif ($res[6] == 3){
                $notify_data = [
                    "order_num" => $info["order_num"],
                    "money" => $info["money"],
                    "agent_id" => $info["id"],
                    "system_order_num" => $info["system_order_num"],
                    "status" => "CANCEL", //INIT(待支付)/SUCCESS(成功)/CANCEL(已取消)/REFUND(已退款)/REFUNDING(退款中)/REFUNDFAIL(退款失败)
                    "pay_time" => 0,
                ];
                return AjaxReturn(0,"交易失败",$notify_data);
            }else{
                $notify_data = [
                    "order_num" => $info["order_num"],
                    "money" => $info["money"],
                    "agent_id" => $info["id"],
                    "system_order_num" => $info["system_order_num"],
                    "status" => "INIT", //INIT(待支付)/SUCCESS(成功)/CANCEL(已取消)/REFUND(已退款)/REFUNDING(退款中)/REFUNDFAIL(退款失败)
                    "pay_time" => 0,
                ];
                return AjaxReturn(0,"已接受",$notify_data);
            }
        } catch (Exception $e){
            echo "exception:".$e->getMessage();
        }
    }

    /**
     * 订单查询密钥
     * http://pay.cxlaimeng.cn/api/Xspay/querypaysign
     * https://pay.cxlaimeng.com/api/Xspay/querypaysign
     */
    public function querypaysign()
    {
        $data = input("post.");
        $validate = Validate::make([
            'order_num|订单号'             => 'require|length:10,40',
            'agent_id|代理ID'                => 'require',
        ]);
        if (!$validate->check($data)) {
            return AjaxReturn(0,$validate->getError());
        }

        $agent = Agent::get($data["agent_id"]);
        if(!$agent){
            return AjaxReturn(0,'商户不存在');
        }
        if($agent["is_lock"]){
            return AjaxReturn(0,"商户暂停锁定");
        }
        $arr = [
            'agent_id'              => $data['agent_id'],
            'order_num'             => $data['order_num'],
        ];
        $sign=createSign($arr,$agent["key"]);
        echo $sign;
    }










}