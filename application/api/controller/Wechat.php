<?php

namespace app\api\controller;

use app\common\model\CzOrder;
use app\common\model\Order;
use app\common\service\SdCommon;
use think\Controller;
use think\facade\Env;

class Wechat extends Controller
{
    /**
     * 微信支付拉单
     * https://dld.yapin.shop/api/wechat/index
     */
    public function index(){
        $url = "https://openapi.duolabao.com/v1/customer/order/payurl/create";
        $data = [
            "customerNum"=>'10001112167818878308515',
            "shopNum"=>'10001210167818925405945',
            "machineNum"=>'10011024167828718503943',
            'requestNum'=>orderNum(),
            'amount'=>'0.10',
            'source'=>'API',
            'callbackUrl'=>ym().'/api/wechat/notify',
            "extraInfo"=>"自定义扩展信息",
            "completeUrl"=>ym().'/api/wechat/completeUrl',
            "secretKey"=> "d006176193b4453da9ae983fdea6cb3f2a031b7c",
            "accessKey"=>"bdba5f1b907745799d064410f819e62f0995d77c"
        ];
        $json = json_encode($data);
        //halt($json);
        $request = $this->send_post_json($url,$json);
        halt($request);
        //{
        //2	customerNum: '10001114991390825070154',
        //3	shopNum: '10001214677149716953054',
        //4	requestNum: '3454354444432',
        //5	amount: '0.01',
        //6	bankType: 'WX',
        //7	authId: 'ojiuXuGWcejS0HwGkU8R_R2MKjY8',
        //8	callbackUrl:' https://www.duolabao.com'
        //9	}
//        "callbackUrl":"openapi.duolabao.com",
//10	"extraInfo":"自定义扩展信息"
//11	"completeUrl":"https://openapi.duolabao.com"
    }

    /**
     * 回调
     */
    public function notify(){

    }


    /**
     * 发送post请求
     * @param string $url 请求地址
     * @param array $post_data post键值对数据
     * @return string
     */
    public function send_post($url, $post_data)
    {
        $postdata = http_build_query($post_data);
        $options = array(
            'http' => array(
                'method' => 'POST',
                'header' => 'Content-type:application/x-www-form-urlencoded',
                'content' => $postdata,
                'timeout' => 15 * 60 // 超时时间（单位:s）
            )
        );
        $context = stream_context_create($options);
        $result = file_get_contents($url, false, $context);
        return $result;
    }


    /**
     * PHP发送Json对象数据
     * @param $url 请求url
     * @param $jsonStr 发送的json字符串
     * @return array
     */
//   public  function send_post_json($url, $jsonStr)
//     {
//         $ch = curl_init();
//         curl_setopt($ch, CURLOPT_POST, 1);
//         curl_setopt($ch, CURLOPT_URL, $url);
//         curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonStr);
//         curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//         curl_setopt($ch, CURLOPT_HTTPHEADER, array(
//                 'Content-Type: application/json; charset=utf-8',
//                 'Content-Length: ' . strlen($jsonStr)
//             )
//         );
//         $response = curl_exec($ch);
//         $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
//         curl_close($ch);
//         return array($httpCode, $response);
//     }

    /**
     * PHP发送Json对象数据
     * @param $url 请求url
     * @param $jsonStr 发送的json字符串
     * @return array
     */
    public  function send_post_json($url, $jsonStr)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonStr);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json; charset=utf-8',
                'Content-Length: ' . strlen($jsonStr)
            )
        );
        $response = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        return array($httpCode, $response);
    }



    /**
     * 创建订单接口
     */
    public function createDLBPay($orderid,$money){
        $data['dlb_customer_num'] ="10001112167818878308515";
        $data['dlb_shop_num'] = "10001210167818925405945";
        $data['dlb_machine_num'] = '10011024167828718503943';
        $data['request_num'] = $orderid; //注：必须为18-32位纯数字
        $data['amount'] =$money;
        $data['callback_url'] = "http://".$_SERVER['HTTP_HOST']."/dlbpay/notify.php";
        $req_bak = $this->request_createpay($data);

        if($req_bak['code']==200){
            header("Location:".$req_bak['url']['payurl']);
        }else{

            var_dump($req_bak);
        }

    }

    /**
     * 退款接口
     */
    public function refundDLBPay(){
        $data['dlb_customer_num'] = $_POST['customer_num'];
        $data['dlb_shop_num'] = $_POST['shop_num'];
        $data['request_num'] = $_POST['request_num']; //注：必须为18-32位纯数字
        $req_bak = $this->request_refundpay($data);
        echo json_encode($req_bak);
    }

    /**
     * 创建哆啦宝微信支付
     */
    protected function request_createpay($data){
        // "secretKey"=> "d006176193b4453da9ae983fdea6cb3f2a031b7c",
        //            "accessKey"=>"bdba5f1b907745799d064410f819e62f0995d77c"
        if(1==1){
            $pay_data['accesskey'] = "bdba5f1b907745799d064410f819e62f0995d77c";
            $pay_data['secretkey'] = "d006176193b4453da9ae983fdea6cb3f2a031b7c";

            $pay_data['timestamp'] = $this->getMillisecond();
            $pay_data['path'] = "/v1/customer/order/payurl/create"; //'/v1/agent/order/payurl/create';


            $sign_data = array(
                'customerNum' => $data['dlb_customer_num'],
                'shopNum' => $data['dlb_shop_num'],
                'machineNum' => $data['dlb_machine_num'],
                'requestNum' => $data['request_num'],
                'amount' => $data['amount'],
                'source' => 'API',
                'tableNum' => '',
                'callbackUrl' => $data['callback_url'],
                'extraInfo' => '',
                'completeUrl' => '',
            );
            $pay_data['body'] = json_encode($sign_data);
            $infoArr = $this->creatTokenPost($pay_data);
//			var_dump($sign_data);var_dump($infoArr);exit;
            switch ($infoArr['result']) {
                case 'success'://成功
                    $payurl = $infoArr['data']['url'];
                    return  array('code'=>200,'msg'=>'订单支付创建成功','url'=>array('payurl'=>$payurl));
                    break;
                case 'fail'://失败
                    return array('code'=>502,'msg'=>'订单支付创建失败');
                    break;
                case 'error'://异常
                    return array('code'=>501,'msg'=>'服务器繁忙，支付调用失败');
                    break;
                default:
                    break;
            }
        }else{
            return array('code'=>502,'msg'=>'订单支付创建失败');
        }
    }

    /**
     * 申请哆啦宝微信退款
     */
    protected function request_refundpay($data){
        $PayConfig = $this->config('dlbpay');
        if(is_array($PayConfig)){
            $pay_data['accesskey'] = $PayConfig['dlb_access_key'];
            $pay_data['secretkey'] = $PayConfig['dlb_secret_key'];
            $pay_data['timestamp'] = $this->getMillisecond();
            $pay_data['path'] = $PayConfig['dlb_path_refund'];      //'/v1/agent/order/payurl/create';
            $sign_data = array(
                'agentNum'=>$PayConfig['dlb_agent_num'],            // 代理商编号
                'customerNum'=>$data['dlb_customer_num'],           // 哆啦宝商户号--请求传递
                'requestNum'=>$data['request_num'],                 // 订单号--请求传递  注：必须为18-32位纯数字
                'shopNum'=>$data['dlb_shop_num'],                   // 哆啦宝店铺号--请求传递
            );
            $pay_data['body'] = json_encode($sign_data);
            $infoArr = $this->creatTokenPost($pay_data);
            switch ($infoArr['result']) {
                case 'success'://成功
                    $payurl = $infoArr['data']['url'];
                    return array('code'=>200,'msg'=>'订单退款成功','url'=>array('payurl'=>$payurl));
                    break;
                case 'fail'://失败
                    return array('code'=>502,'msg'=>'订单退款失败');
                    break;
                case 'error'://异常
                    return array('code'=>501,'msg'=>'服务器繁忙，退款失败，请稍后再试试');
                    break;
                default:
                    break;
            }
        }else{
            return array('code'=>502,'msg'=>'订单退款失败');
        }
    }

    //生成token并提交
    protected function creatTokenPost($data) {
        $str = "secretKey={$data['secretkey']}&timestamp={$data['timestamp']}&path={$data['path']}&body={$data['body']}";
        $token = strtoupper(sha1($str));
        $url = 'http://openapi.duolabao.cn'.$data['path'];

        $post_data = $data['body'];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; MSIE 5.01; Windows NT 5.0)');
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_AUTOREFERER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'accesskey: ' . $data['accesskey'],
                'timestamp: ' . $data['timestamp'],
                'token: ' . $token)
        );
        $info = curl_exec($ch);
        $infoArr = json_decode($info,true);
        //    put_contents('log/payurl_result',$infoArr,1);
        curl_close($ch);
        return $infoArr;
    }

    /**13位时间戳**/
    function getMillisecond() {
        list($t1, $t2) = explode(' ', microtime());
        return $t2.ceil( ($t1 * 1000) );
    }

    // https://dld.yapin.shop/api/wechat/order
    public function order(){
        $orderid=date('YmdHis').rand(1000,9999);
        $money=50.00;
        $res= $this->createDLBPay($orderid,$money);
        halt($res);
    }


}