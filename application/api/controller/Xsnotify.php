<?php

namespace app\api\controller;

use app\common\model\Agent;
use app\common\model\CzOrder;
use app\common\model\FengfuChannel;
use app\common\model\FfOrder;
use app\common\model\FfXiafa;
use app\common\model\Order;

use app\common\model\PayChannel;

use app\common\model\Xiafa;
use app\common\model\XsOrder;
use app\common\model\XsXiafa;
use app\common\service\Finance;
use think\Controller;
use think\facade\Env;


include_once  Env::get("root_path"). "extend/utils/ExpUtils.php";
include_once  Env::get("root_path"). "extend/utils/RSAUtils.php";
class Xsnotify extends Controller
{
    //https://pay.cxlaimeng.com/api/xsnotify/xinshengnotify
    public function xinshengnotify()
    {
        $notify_ip = request()->ip();//回调异步ip
        //上线前开启
        $data = $_POST;
        db("test")->insert(["add_time"=>time(),"content"=>json_encode($data),"ip"=>$notify_ip,"explain"=>"新生支付异步支付返回参数"]);
        $verifyField =  ["tranCode","version","merId","merOrderNum","tranAmt","submitTime","hnapayOrderId",
            "tranFinishTime","respCode","charset","signType"];
        $ExpUtils = new \ExpUtils();
        //验签
        $verify = $ExpUtils::verify($verifyField, $data, $data['signMsg']);
        if(!$verify) {
            echo "交易返回结果验签失败";  return;
        }
        if($data['respCode']=="0000"){
            $model = new XsOrder();
            $order = $model::where(["system_order_num"=>$data["merOrderNum"]])->find();
            //halt($order);
            if(!$order){
                return AjaxReturn(0,'订单不存在');
            }
            if($order["payment"]){
                return AjaxReturn(0,'订单已支付');
            }
            $orderData = [
                "notify_ip"=>$notify_ip,//异步回调支付ip
                "payment" => 1,
                "pay_time" => time(),
                "bankOrderId"=>$data['bankOrderId'],


            ];
            $model::where(["id" => $order['id']])->update($orderData);//更改订单信息
            //支付时间统计
            $agentFianceModel = new Finance();
            $agentFianceModel->agentDayxs($order['id']);//平台日财务
            $agentFianceModel->payDaytongdaoxs($order['id']);//通道日财务
            $agent =  Agent::get($order["agent_id"]);
            if($agent['id'] == 35){
                //支付回调
                $notify_data = [
                    "order_num" => $order["order_num"],
                    "money" => $order["money"],
                    "agent_id" => $agent["id"],
                    "system_order_num" => $order["system_order_num"],
                    "status" => "SUCCESS", //INIT(待支付)/SUCCESS(成功)/CANCEL(已取消)/REFUND(已退款)/REFUNDING(退款中)/REFUNDFAIL(退款失败)
                    "pay_time" => time(),
                ];
                $sign=createSign($notify_data,$agent["key"]);
                $notify_data['sign'] = $sign;
                if ($order["notify_url"]) {
                    $notify_data['notify_url'] = $order["notify_url"];
                    //异步回调给其他平台
                    $url = "http://8.218.220.87/api/test/tennotify";
                    $res = request_post($url, $notify_data);
                    if($res == "success"){
                        $order::where('id', $order['id'])->update(["return_status" => 1, "return_time" => time()]);//修改订单状态
                    }

                }
            }else{
                //支付回调
                $notify_data = [
                    "order_num" => $order["order_num"],
                    "money" => $order["money"],
                    "agent_id" => $agent["id"],
                    "system_order_num" => $order["system_order_num"],
                    "status" => "SUCCESS", //INIT(待支付)/SUCCESS(成功)/CANCEL(已取消)/REFUND(已退款)/REFUNDING(退款中)/REFUNDFAIL(退款失败)
                    "pay_time" => time(),
                ];
                $sign=createSign($notify_data,$agent["key"]);
                $notify_data['sign'] = $sign;
                if ($order["notify_url"]) {
                    //异步回调给其他平台
                    request_post($order["notify_url"], $notify_data);
                    $order::where('id', $order['id'])->update(["return_status" => 1, "return_time" => time()]);//修改订单状态
                }
            }



            //此处进行业务逻辑处理
            echo "200"; exit();

        }
    }

    /**
     * 新生支付异步业务处理逻辑
     * http://pay.cxlaimeng.cn/api/Xsnotify/notifytest
     */
    public function notifytest(){
        $notify_ip = request()->ip();
        $json = '{"charset":"1","msgExt":"\u4ea4\u6613\u6210\u529f","hnapayOrderId":"2024110979256521","bankOrderId":"2411091312067656105","tranFinishTime":"20241109131228","remark":"","version":"2.1","merOrderNum":"jlB09291148229710","realBankOrderId":"434459","userId":"","buyerLogonId":"","submitTime":"20241109131205","tranAmt":"1.22","signType":"1","merId":"11000010741","tranCode":"WS01","signMsg":"06f77e9b4186eaf53c339095c47e4171677d670faba13ebb9f581cc9b8dbd7d4dfa21a060b2891bab127ec1f3b1b8677a4b4517660503cbb0f33cc8da0a1d24fad1f11ae1072b2158249eb3978ad4403da372da4164e65482da80ee5d4b462080412064fead8c0d305c9ffc24c806b1d15698f4dde9bcd865ab840bece713356","respCode":"0000"}';
        $data = json_decode($json,true);
        dump($data);
                    $verifyField =  ["tranCode","version","merId","merOrderNum","tranAmt","submitTime","hnapayOrderId",
"tranFinishTime","respCode","charset","signType"];
            $ExpUtils = new \ExpUtils();
            //验签
            $verify = $ExpUtils::verify($verifyField, $data, $data['signMsg']);
            if(!$verify) {
                echo "交易返回结果验签失败";  return;
            }
            if($data['respCode']=="0000"){
                $model = new XsOrder();
                $order = $model::where(["system_order_num"=>$data["merOrderNum"]])->find();
                //halt($order);
                if(!$order){
                    return AjaxReturn(0,'订单不存在');
                }
                if($order["payment"]){
                    return AjaxReturn(0,'订单已支付');
                }
                $orderData = [
                    "notify_ip"=>$notify_ip,//异步回调支付ip
                    "payment" => 1,
                    "pay_time" => time(),
                    "bankOrderId"=>$data['bankOrderId'],


                ];
                $model::where(["id" => $order['id']])->update($orderData);//更改订单信息
                //支付时间统计
                $agentFianceModel = new Finance();
                $agentFianceModel->agentDayxs($order['id']);//平台日财务
                $agentFianceModel->payDaytongdaoxs($order['id']);//通道日财务
                $agent =  Agent::get($order["agent_id"]);


                //支付回调
                $notify_data = [
                    "order_num" => $order["order_num"],
                    "money" => $order["money"],
                    "agent_id" => $agent["id"],
                    "system_order_num" => $order["system_order_num"],
                    "status" => "SUCCESS", //INIT(待支付)/SUCCESS(成功)/CANCEL(已取消)/REFUND(已退款)/REFUNDING(退款中)/REFUNDFAIL(退款失败)
                    "pay_time" => time(),
                ];
                $sign=createSign($notify_data,$agent["key"]);
                $notify_data['sign'] = $sign;
                if ($order["notify_url"]) {
                    //异步回调给其他平台
                    request_post($order["notify_url"], $notify_data);
                    $order::where('id', $order['id'])->update(["return_status" => 1, "return_time" => time()]);//修改订单状态
                }

                //此处进行业务逻辑处理
                echo "200"; exit();
            }

    }

    /**
     * 代付异步支付
     * /api/xsnotify/daifunotify
     */
    public function daifunotify()
    {
        $notify_ip = request()->ip();//回调异步ip
        //上线前开启
        $data = $_POST;

        db("xs_test_df")->insert(["add_time"=>time(),"content"=>json_encode($data),"ip"=>$notify_ip,"explain"=>"新生代付异步代付返回参数"]);
        //异步返回成功的数据
        //{"charset":"1","hnapayOrderId":"2024111481888543","successTime":"20241114235652","resultCode":"0000","signType":"1","merId":"11000010694","tranCode":"SGP01","merAttach":"https:\/\/pay.cxlaimeng.com\/api\/xsnotify\/daifunotify","version":"2.1","signValue":"Jtqua2gi1xcnWYNF9mWV8CGFo7luzQbR9mNQ9L3hUejR7hmjb4TSFhO7mAUg5wXRGbo8QbocHTAa7C4wkBwWAHQoXwLoxaOtIJbhRpweYCuwm+dG8rLHKJ5nkmRjHXVbng0nYz2TG9aXGGJa2KBL+hsqIN8RMMt8JVbWYPDaSZ8=","merOrderId":"jlB14998100430971"}
        //异步返回失败的数据
        //{"charset":"1","hnapayOrderId":"2024111581887499","resultCode":"4444","errorCode":"A0000699","version":"2.1","signValue":"UX8PYgRuD4MooEE5o8dzogIoMysT\/7manibffxgiknx6tC4W9dplm7eF60Xxm0hhlu\/1Q0Qzb0371gegOiY110ojjw593VHkNIiFDZP02ZHcOkFhHgfe4T6z\/GaTIVqDiozvnE2DVO3OT1qMdw2WXtmO+R90oeiebl+0biRmaSA=","errorMsg":"\u4eb2\uff0c\u60a8\u8f93\u5165\u7684\u5361\u53f7\u65e0\u6548\uff0c\u8bf7\u6838\u5b9e\u540e\u518d\u8bd5","successTime":"20241115000518","signType":"1","merId":"11000010741","tranCode":"SGP01","merAttach":"https:\/\/pay.cxlaimeng.com\/api\/xsnotify\/daifunotify","merOrderId":"jlB15003161210557"}
        //dump($data);
        $model = new XsXiafa();
        $order = $model::where(["order_num"=>$data["merOrderId"]])->find();
        if(!$order){
            return AjaxReturn(0,'订单不存在');
        }
        if($order["payment"] == 1){
            return AjaxReturn(0,'订单已打款');
        }
        $agent_model = new Agent();
        $agent =  Agent::get($order["agent_id"]);
        if($data['resultCode'] == "0000"){
            $arr = [
                "payment"=>1,
                "feeAmt"=>1,
                "hnapayOrderId"=>$data['hnapayOrderId'],
                "successTime"=>$data['successTime'],
                "pay_time"=>time()
            ];
            $model::where(["order_num"=>$data["merOrderId"]])->update($arr);
            $pay_money = math_add($order['sum'],1);//需要把手续费算上
            //halt($pay_money);

            //扣除商户金额
            $agent_model::where(["id"=>$order["agent_id"]])->setDec("money",$pay_money);
            //支付回调
            $notify_data = [
                "df_order" => $order["df_order"],
                "sum" => $order["sum"],
                "agent_id" => $agent["id"],
                "order_num" => $order["order_num"],
                "payment" => 1, //INIT(待支付)/SUCCESS(成功)/CANCEL(已取消)/REFUND(已退款)/REFUNDING(退款中)/REFUNDFAIL(退款失败)
                "pay_time" => time(),
            ];
            $sign=createSign($notify_data,$agent["key"]);
            $notify_data['sign'] = $sign;
            if ($order["notify_url"]) {
                //异步回调给其他平台
                request_post($order["notify_url"], $notify_data);
                $order::where('id', $order['id'])->update(["return_status" => 1, "return_time" => time()]);//修改订单状态
            }
            $agentFianceModel = new Finance();
            $agentFianceModel->agentDayxsdf($order['id']);//平台日财务
            $agentFianceModel->payDaytongdaoxsdf($order['id']);//通道日财务
            //此处进行业务逻辑处理
            //echo "200"; exit();
        }else{
            $model::where(["order_num"=>$data["merOrderId"]])->update(["payment"=>3,"dealRemark"=>$data['errorCode']]);
            $notify_data = [
                "df_order" => $order["df_order"],
                "sum" => $order["sum"],
                "agent_id" => $agent["id"],
                "order_num" => $order["order_num"],
                "payment" => 3, //INIT(待支付)/SUCCESS(成功)/CANCEL(已取消)/REFUND(已退款)/REFUNDING(退款中)/REFUNDFAIL(退款失败)
                //"pay_time" => time(),
            ];
            $sign=createSign($notify_data,$agent["key"]);
            $notify_data['sign'] = $sign;
            if ($order["notify_url"]) {
                //异步回调给其他平台
                $res_stu = request_post($order["notify_url"], $notify_data);
                if($res_stu == "ok"){
                    $order::where('id', $order['id'])->update(["notify_state" => 1, "notify_time" => time()]);//修改订单状态
                }

            }

        }
    }

    /**
     * 代付异步公式
     * http://pay.cxlaimeng.cn/api/Xsnotify/daifunotify2
     */
    public function daifunotify2()
    {
        $notify_ip = request()->ip();//回调异步ip
        $json = '{"charset":"1","msgExt":"\u4ea4\u6613\u6210\u529f","hnapayOrderId":"2024111783293142","bankOrderId":"2411171416461986245","tranFinishTime":"20241117141837","remark":"","version":"2.1","merOrderNum":"jlB17242056157376","realBankOrderId":"907209","userId":"","buyerLogonId":"","submitTime":"20241117141646","tranAmt":"300","signType":"1","merId":"11000010694","tranCode":"WS01","signMsg":"19c0eb5655e22c402279c897e426fb8a5d60ef0600304c8536fda1b5e08d75568a10fd7a260b26969b86d99df498530a0acdc76c622cfd7c0249e476273c4fe9be549acc8ed3c65a4f985a064ba606b9e5d44ab6198b70a35ca46f44150c66ff2bd9789150673d365e931ca607e5b7136271ed22b6b6d1dd9f9a5aa809cf52bf","respCode":"0000"}';
        $data = json_decode($json,true);

        //dump($data);
        $model = new XsXiafa();
        $order = $model::where(["order_num"=>$data["merOrderId"]])->find();
        if(!$order){
            return AjaxReturn(0,'订单不存在');
        }
        if($order["payment"]){
            return AjaxReturn(0,'订单已打款');
        }
        $agent_model = new Agent();
        $agent =  Agent::get($order["agent_id"]);
        if($data['resultCode'] == "0000"){
            $arr = ["payment"=>1,
                "feeAmt"=>1,
                "hnapayOrderId"=>$data['hnapayOrderId'],
                "successTime"=>$data['successTime'],
                "pay_time"=>time()
            ];
            $model::where(["order_num"=>$data["merOrderId"]])->update($arr);
            $order::where('id', $order['id'])->update(["notify_state" => 1, "notify_time" => time()]);//修改订单状态
            if($agent){


                $pay_money = math_add($order['sum'],1);//需要把手续费算上
                //halt($pay_money);

                //扣除商户金额
                $agent_model::where(["id"=>$order["agent_id"]])->setDec("money",$pay_money);
                //支付回调
                $notify_data = [
                    "df_order" => $order["df_order"],
                    "sum" => $order["sum"],
                    "agent_id" => $agent["id"],
                    "order_num" => $order["order_num"],
                    "payment" => 1, //INIT(待支付)/SUCCESS(成功)/CANCEL(已取消)/REFUND(已退款)/REFUNDING(退款中)/REFUNDFAIL(退款失败)
                    "pay_time" => time(),
                ];
                $sign=createSign($notify_data,$agent["key"]);
                $notify_data['sign'] = $sign;
                if ($order["notify_url"]) {
                    //异步回调给其他平台
                    request_post($order["notify_url"], $notify_data);
                    $order::where('id', $order['id'])->update(["return_status" => 1, "return_time" => time()]);//修改订单状态
                }
                $agentFianceModel = new Finance();
                $agentFianceModel->agentDayxsdf($order['id']);//平台日财务
                $agentFianceModel->payDaytongdaoxsdf($order['id']);//通道日财务
            //此处进行业务逻辑处理
            //echo "200"; exit();
            }else{
                $agentFianceModel = new Finance();
                //$agentFianceModel->agentDayxsdf($order['id']);//平台日财务
                $agentFianceModel->payDaytongdaoxsdf($order['id']);//通道日财务
            }
        }else{
            $model::where(["order_num"=>$data["merOrderId"]])->update(["payment"=>3,"dealRemark"=>$data['errorCode']]);
            $notify_data = [
                "df_order" => $order["df_order"],
                "sum" => $order["sum"],
                "agent_id" => $agent["id"],
                "order_num" => $order["order_num"],
                "payment" => 2, //INIT(待支付)/SUCCESS(成功)/CANCEL(已取消)/REFUND(已退款)/REFUNDING(退款中)/REFUNDFAIL(退款失败)
                //"pay_time" => time(),
            ];
            $sign=createSign($notify_data,$agent["key"]);
            $notify_data['sign'] = $sign;
            if ($order["notify_url"]) {
                //异步回调给其他平台
                request_post($order["notify_url"], $notify_data);
                $order::where('id', $order['id'])->update(["notify_state" => 1, "notify_time" => time()]);//修改订单状态
            }

        }

    }
}