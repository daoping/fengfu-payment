<?php

namespace app\api\controller;

use app\common\model\AgentFinance;
use app\common\model\FfXiafa;
use app\common\model\FwsChannel;
use app\common\model\Order;
use app\common\model\PayChannel;
use app\common\model\ZshWarning;
use app\common\service\Finance;
use app\common\service\SdCommon;
use think\Controller;
use think\facade\Env;


include_once  Env::get("root_path"). "vendor/sande/URLRequest.php";
class Test extends Controller
{

    public $privateKeyPwd = '123456';



    /**
     * 快捷支付
     * https://pay.lnxinmeng.xyz/api/test/xinmengkuaijie
     * http://pay1.lnxinmeng.xyz/api/test/xinmengkuaijie
     */
//    public function xinmengkuaijie(){
//        $model = new Order();
////        $id = input("get.id");
////        if(!$id){
////            return AjaxReturn(0,"参数错误");
////        }
////        $order = $model::get($id);
////        if(!$order){
////            return AjaxReturn(0,"订单不存在");
////        }
////        if($order["payment"] == 1){
////            return AjaxReturn(0,"订单已支付");
////        }
//        $ip = request()->ip();
////        $model->where("id",$id)->update(["buy_ip"=>$ip]);//更新付款用户下单ip
////
////
////        $ip = str_replace(".","_",$ip);
////
////
////        $PayChannelModel  = new PayChannel();
////        $PayChannel = $PayChannelModel::get($order['cid']);
////        if(!$PayChannel){
////            return AjaxReturn(1,"支付通道不存在");
////        }
////        if($PayChannel['open'] != 1){
////            return AjaxReturn(1,"通道未开启");
////        }
//        //$order_num = $order['system_order_num'];
//        $order_num = orderNum();
//        //halt($order_num);
//        $data = [
//            'version' => 10,
//            'mer_no' => "6888800120057" , //商户号
//            'mer_order_no' =>$order_num, //商户唯一订单号
//            'create_time' => date('YmdHis'),
//            'expire_time' => date('YmdHis', time()+30*60),
//            'order_amt' => 2.99, //订单支付金额
//            'notify_url' => ym().'/api/notify/xinmengsytnotify', //订单支付异步通知
//            'return_url' => ym()."/home/fivepay/front?order_num=".$order_num, //订单前端页面跳转地址
//            'create_ip' => $ip,
//            'goods_name' => '购买产品',//产品名称
//            'store_id' => '000000',
//            'product_code' => '05030001',
//            'clear_cycle' => '3',
//            'pay_extra' =>json_encode(["userId"=>orderNum()]), //pay_extra参考语雀文档4.3
//            'accsplit_flag' => 'NO',
//            'jump_scheme' => '',
//            'meta_option' => json_encode([["s" => "Android","n" => "wxDemo","id" => "com.pay.paytypetest","sc" => "com.pay.paytypetest"]]),
//            'sign_type' => 'RSA',
//            //'merch_extend_params'=>'{"mchReceiveRemark":"S0"}',
//
//        ];
//        //halt($data);
//        //"version=" + version + "" +
//        //            "&mer_no=" + mer_no + "" +
//        //            "&mer_order_no=" + mer_order_no + "" +
//        //            "&create_time=" + createTime + "" +
//        //            "&expire_time=" + endTime + "" +  //endTime
//        //            "&order_amt=0.11" +
//        //            "&notify_url=" + URLEncoder.encode(notify_url, "UTF-8") + "" +
//        //            "&return_url=" + URLEncoder.encode(return_url, "UTF-8") + "" +
//        //            "&create_ip=127_0_0_1" +
//        //            "&goods_name=" + URLEncoder.encode(goods_name, "UTF-8") + "" +
//        //            "&store_id=000000" +
//        //            //产品编码: 云函数h5：  02010006  ；支付宝H5：  02020002  ；微信公众号H5：02010002   ；
//        //            //一键快捷：  05030001  ；H5快捷：  06030001   ；支付宝扫码：  02020005 ；快捷充值：  06030003
//        //            //电子钱包【云账户】：开通账户并支付product_code应为：04010001；消费（C2C）product_code 为：04010003 ; 我的账户页面 product_code 为：00000001
//        //            "&product_code=05030001" + "" +
//        //            "&clear_cycle=3" +
//        //            "&pay_extra=" + URLEncoder.encode(pay_extra, "UTF-8") + "" +
//        //            "&meta_option=%5B%7B%22s%22%3A%22Android%22,%22n%22%3A%22wxDemo%22,%22id%22%3A%22com.pay.paytypetest%22,%22sc%22%3A%22com.pay.paytypetest%22%7D%5D" +
//        //            "&accsplit_flag=NO" +
//        //            "&jump_scheme=" +
//        //            "&sign_type=RSA" +
//        //            "&sign=" + URLEncoder.encode(sign, "UTF-8") + "";
//
//        //goods_name , notify_url，return_url，pay_extra，meta_option，extend，merch_extend_params，sign
//        $temp = $data;
//        unset($temp['goods_name']);
//        unset($temp['jump_scheme']);
//        unset($temp['expire_time']);
//        unset($temp['product_code']);
//        unset($temp['clear_cycle']);
//        unset($temp['meta_option']);
//
//        //file_put_contents('log.txt', date('Y-m-d H:i:s', time()) . " 签名串:" . $this->getSignContent($temp) . "\r\n", FILE_APPEND);
//
//        // echo $this->getSignContent($temp);exit;
//        $sign = $this->sign($this->getSignContent($temp),$data['mer_no']);
//        $data['sign'] = $sign;
//        $query = http_build_query($data) ;
//        //$query = http_build_query($data, null, '&');
//
//        //$payurl = "https://sandcash.mixienet.com.cn/pay/h5/alipay?".$query;
//        $payurl = "https://sandcash.mixienet.com.cn/pay/h5/fastpayment?".$query;
//        //halt($payurl);
//        //return $payurl;
//        header('Location:'.$payurl);
//        die();
//    }


//    public function sign($str,$privateKeynum) {
//        $privateKeyPath =   Env::get("root_path")."extend/sande/".$privateKeynum."/".$privateKeynum.".pfx";
//        $file = file_get_contents($privateKeyPath);
//        if (!$file) {
//            throw new \Exception('loadPk12Cert::file
//                    _get_contents');
//        }
//        if (!openssl_pkcs12_read($file, $cert, $this->privateKeyPwd)) {
//            throw new \Exception('loadPk12Cert::openssl_pkcs12_read ERROR');
//        }
//        $pem = $cert['pkey'];
//        openssl_sign($str, $sign, $pem);
//        $sign = base64_encode($sign);
//        return $sign;
//    }

//    public function getSignContent($params) {
//        ksort($params);
//
//        $stringToBeSigned = "";
//        $i = 0;
//        foreach ($params as $k => $v) {
//            if (false === $this->checkEmpty($v) && "@" != substr($v, 0, 1)) {
//
//                if ($i == 0) {
//                    $stringToBeSigned .= "$k" . "=" . "$v";
//                } else {
//                    $stringToBeSigned .= "&" . "$k" . "=" . "$v";
//                }
//                $i++;
//            }
//        }
//
//        unset ($k, $v);
//        return $stringToBeSigned;
//    }

//    public function checkEmpty($value)
//    {
//        if (!isset($value))
//            return true;
//        if ($value === null)
//            return true;
//        if (trim($value) === "")
//            return true;
//
//        return false;
//    }




    /**
     * 下单
     * http://pay1.2021621.com/api/test/buy
     */
//    public function buy(){
//
//        $model = new Order();
//        $finance = new  Finance();
//        $new_data = [
//            'order_num'    => orderNum(),
//            'notify_url'        => "https://www.baidu.com",
//            'money'                => 120.00,
//            'pay_money'                => 119.88,
//            'system_order_num'=>orderNum(),
//            'agent_id'=>25,
//            "type"=>2,
//            "pay_channel_id"=>10,//总商户
//            "qid"=>1,//渠道商id
//            "bid"=>9,//子商户id
//            "billMerchantId"=>"587107264"//子商户号
//        ];
//        //连续20单没有付款关闭
////        $yj = $finance->orderyj($new_data['bid']);
////        if($yj){
////            return AjaxReturn(0,"已关闭");
////        }
//        $res = $model->save($new_data);//保存订单
//        if(!$res){
//            return AjaxReturn(0,"下单失败");
//        }
//        $order_info = $model::where(["system_order_num"=>$new_data['system_order_num']])->find();
//
//
//        $finance->jinexia($order_info['id']);//金额日财务统计
//        $finance->zshdaybuy($order_info['id']);//子商户日财务统计
//        $finance->zshbuy($order_info['id']);//子商户总财务统计
//
//        $url = ym()."/api/jytpay/pay?id=".$order_info['id'];
//        return AjaxReturn(1,"创建成功",["url"=>$url]);
//    }

    /**
     * 当日下单金额统计
     * http://p.dinglianshop.cn/api/test/paynotify
     */
//    public function paynotify(){
//        $id = 19764;
//        $finance = new  Finance();
////        $finance->jinedaypay($id);//当日单笔金额支付统计
////        $finance->zshdaypay($id);//当日子商户财务统计
////        $finance->zshpay($id);//子商总户财务统计
//        $finance->jinedaypaytl($id);//通联日财务统计
//    }

    /**
     * 下发统计
     * http://p.dinglianshop.cn/api/test/xaifatotal
     */
//    public function xaifatotal(){
//        $model = new FfXiafa();
//        $moeny = $model::where("payment",1)->whereTime('create_time', 'today')->sum("sum");
//        halt($moeny);
//    }
    /**
     * 查询ip归属地
     *  本地地址:  http://p.dinglianshop.cn/api/test/getipdizhi
     * 网络地址:  https://pay.dinglianshop.cn/api/test/getipdizhi?ip=222.90.19.26
     */
//    public function getipdizhi(){
//        $ip = '27.12.44.58';
//        $info = db("ipkus")->where(["ip"=>$ip])->find();
//        if($info){
//            if($info['country'] == "中国"){
//                return 1;
//            }else{
//                return 0;
//            }
//        }else{
//            $url = 'https://api.ip138.com/ipdata/?ip='.$ip.'&datatype=jsonp&token=a8d86cd307f1fbe85dd57d7254b8c04a';
//            $res = file_get_contents($url);
//           $res = json_decode($res,true);
//            if($res['ret'] != "ok"){
//                return 0;
//            }
//            $arr = [
//                "ip"=>$ip,
//                "country"=>$res['data'][0],
//                "region"=>$res['data'][1],
//                "city"=>$res['data'][2],
//                "district"=>$res['data'][3],
//                "isp"=>$res['data'][4],
//                "zip"=>$res['data'][5],
//                "zone"=>$res['data'][6],
//                "tag"=>$res['data'][7],
//                "create_time"=>time()
//            ];
//            db("ipkus")->insert($arr);
//            if($arr['country'] == "中国"){
//                return 1;
//            }else{
//                return 0;
//            }
//        }
//
//    }


    /**
     * 异步支付回调
     * http://8.218.220.87/api/test/tennotify
     */
    public function tennotify()
    {
        $data = $_POST;
        if(!$data){
            return 0;
        }
        $notify_url = $data['notify_url'];
        unset($data['notify_url']);
        $res = request_post($notify_url, $data);
        return $res;
    }

    /**
     * 测试支付
     * http://pay.cxlaimeng.cn/api/test/pay
     */
    public function pay(){
        $url = "https://payment.qingj.top/api/pay/unifiedOrder";
        $data = [
            "mchNo"=>"M1734350850",
            "appId"=>"67601802e4b0df375e7f4be2",
            "mchOrderNo"=>orderNum(),
            "wayCode"=>"ADAYE_RE",
            "amount"=>1,
            "currency"=>"CNY",
            "subject"=>"标题",
            "body"=>"描述",
            "notifyUrl"=>"https://www.baidu.com",
            "version"=>"1.0",
            "signType"=> "MD5",
            "reqTime"=>round(microtime(true) * 1000)
        ];
        $key = "bcd7FXhRhblNJloTLGD4S9KqFGOiWs1z6AJ53f6HqCJGIv09SuL8apJncTbpqAdeOrhSFpSG5elXWBSAypqnSBgTq9VTmpJK5PdwvMg9FxvxAhWoXhsdTAs77eOUbeHg";
        $sign = createSign($data, $key);
        //dump($sign);
        $data['sign'] = $sign;
        $res = request_post($url, $data);
        halt($res);
    }



    /**
     * 测试财务
     * http://pay.cxlaimeng.cn/api/test/testfinse
     */
    public function testfinse(){
        $order = [
          "id"=>15632
        ];
        $agentFianceModel = new Finance();
        $agentFianceModel->agentDayhk($order['id']);//平台日财务
        $agentFianceModel->payDaytongdaoxshk($order['id']);//通道日财务
    }


    /**
     * 测试随机数组
     * 本地 http://pay.cxlaimeng.cn/api/test/testpayChannel
     * 线上 https://p.cxlaimeng.com/api/test/testpayChannel
     */
    public function testpayChannel(){
        // 定义十个一维数组
        $arrays = [
            ['a1', 'b1', 'c1'],
            ['a2', 'b2', 'c2'],
            ['a3', 'b3', 'c3'],
            ['a4', 'b4', 'c4'],
            ['a5', 'b5', 'c5'],
            ['a6', 'b6', 'c6'],
            ['a7', 'b7', 'c7'],
            ['a8', 'b8', 'c8'],
            ['a9', 'b9', 'c9'],
            ['a10', 'b10', 'c10']
        ];

// 使用 array_rand() 随机选择一个父数组的索引
        $randomIndex = array_rand($arrays);
        dump($randomIndex);
// 获取对应的一维数组
        $randomArray = $arrays[$randomIndex];

// 输出结果
        echo "随机选择的一维数组: \n";
        dump($randomArray);
    }



    /**
     * 支付拉单
     * 金额支付100元起步
     * 本地 http://pay.cxlaimeng.cn/api/test/alipay
     *  线上 https://p.cxlaimeng.com/api/test/alipay
     */
    public function alipay(){
        $post = $_POST;
        //商户号：shancheng123
        //密钥：BD2799E025A04FABA499CEDE78F21A6B
        //支付宝：productId=1
        //商户地址：https://mer.qinewwandi.com
        //http://103.41.106.136:9117/api/commonpay/pay  下单地址
        //http://103.41.106.136:9117/api/trade/query  查询地址
        //回调地址:103.41.106.136
        /**
         * 拉取支付
         */
        $url = "http://103.41.106.136:9117/api/commonpay/pay";
        //// 获取当前时间的13位时间戳
        $timestamp = microtime(true) * 1000;
        //echo $timestamp;
        $arr = [
            "merchantId" => "shancheng123",
            "orderNo" =>$post['order_num'],
            "money" => strval($post['money']),
            "timeSpan" => strval($timestamp),
            "callBackUrl" => ym().'/api/notify/luzhishenyunshanfupay',
            "accountName" => "yonghuming",
            "ip" => request()->ip(),
            "productName" => "商品名称",
            "productId" => "1",
            "returnUrl" => ym().'/api/Lzspay/returnUrl'
        ];

        $key = "BD2799E025A04FABA499CEDE78F21A6B";
        $sign = createSignlzs($arr, $key);
        $arr['sign'] = $sign;
        $res = https_request($url, $arr, 'json');
        $res = json_decode($res, true);
        //dump($res);
        $res = json_decode($res, true);
        //array(10) {
        //  ["state"] => int(0)
        //  ["payState"] => int(2)
        //  ["data"] => string(149) "http://8.138.3.157:6156/cashier.html?orderid=aHR0cHM6Ly9xci5hbGlwYXkuY29tL2JheDAzNzMzY2d4OG1pdmxxYmlqNTU1ZDsxMDAuMDA7MjAyNTAzMTExNTUxMjk3NzQ5NDgzNzI="
        //  ["message"] => string(9) "交易中"
        //  ["merchantId"] => string(12) "shancheng123"
        //  ["money"] => string(3) "100"
        //  ["timeSpan"] => string(13) "1741679490923"
        //  ["orderNo"] => string(17) "jl311794901022937"
        //  ["platOrderNo"] => string(23) "20250311155129774948372"
        //  ["sign"] => string(32) "84B7E94B4F068C59F886E940AD8C11F4"
        //}
        if($res['payState'] == 2){
        //dump($res);
            $pay_url = $res['data'];
//            header('Location:'.$pay_url);
//            die();
            return AjaxReturn(1,$res['message'],["url"=>$pay_url]);
        }else{
            return AjaxReturn(0,$res['message']);
        }
    }





}