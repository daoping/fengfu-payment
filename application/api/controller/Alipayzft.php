<?php

namespace app\api\controller;


use app\common\model\Agent;
use app\common\model\AkOrder;
use app\common\model\CzOrder;
use app\common\model\LaoxieChannel;
use app\common\model\LaoxieOrder;
use app\common\model\Order;
use app\common\model\PayChannel;
use app\common\service\Finance;
use think\Controller;
use think\facade\Env;

/**
 * 支付通支付
 */
class Alipayzft extends Controller
{

    /**
     * 老谢直付通支付
     *  本地 : http://pay.cxlaimeng.cn/api/alipayzft/pay?id=15735
     *  线上:  https://p.cxlaimeng.com/api/alipayzft/pay?id=15735
     */
    public function pay(){
        $model = new LaoxieOrder();
        $PayChannelModel = new laoxieChannel();
        $id = input("get.id");
        if(!$id){
            return AjaxReturn(0,"参数错误");
        }
        $order = $model::get($id);
        if(!$order){
            return AjaxReturn(0,"订单不存在");
        }
        if($order["payment"] == 1){
            return AjaxReturn(0,"订单已支付");
        }
        $ip = request()->ip();
        $model->where("id",$id)->update(["buy_ip"=>$ip]);//更新付款用户下单ip
        $PayChannel = $PayChannelModel::get($order['bid']);
        if(!$PayChannel){
            return AjaxReturn(1,"支付通道不存在");
        }
        if($PayChannel['open'] != 1){
            return AjaxReturn(1,"通道未开启");
        }
        $money = $order['pay_money'];//支付金额
        if($order['codeUrl'] && !$order["payment"]){
            $pay_url = $order['codeUrl'];
            header('Location:'.$pay_url);
            exit();
        }
        $url = "http://47.76.118.174/Pay_Index.html";
        $arr = [
            "pay_memberid"=>$order['mchid'],
            "pay_orderid"=>$order['system_order_num'],
            "pay_amount"=>$money,
            "pay_applydate"=>date("Y-m-d H:i:s",time()),
            "pay_bankcode"=>1078,
            "pay_notifyurl"=>ym()."/api/alipayzft/notifyurl",
            "pay_callbackurl"=>ym()."/api/alipayzft/callbackurl",
        ];
        //dump($arr);
        $key = $PayChannel['key'];
        //  pay_memberid  =  商户号
        //            pay_orderid       =  订单号
        //            pay_amount       =  金额,单位元
        //            pay_applydate   =  时间  格式 2025-01-01 12:00:00
        //            pay_bankcode   =  支付编码  (测试请写1078)
        //            pay_notifyurl       = 异步返回地址
        //            pay_callbackurl    = 同步返回地址
        $key = createSign($arr,$key);
        $arr["pay_md5sign"] = $key;
        $res = request_post($url,$arr);
        //dump($res);
        //{"code":"0","msg":"ok","data":{"payurl":"http:\/\/zft.shyig.com\/pay\/submit\/2025011422053427325\/"}}
        $res = json_decode($res,true);
        //dump($res);
        if($res["code"] == 0){
            //$url = $res["data"]["payurl"];
            //return redirect($res);
            $model->where("id",$order['id'])->update(["codeUrl"=> $res["data"]["payurl"]]);
            $pay_url =  $res["data"]["payurl"];
            header('Location:'.$pay_url);
            exit();
        }else{
            //dump($res);
            return AjaxReturn(1,"支付失败,失败原因:".$res["msg"]);
        }
    }

    /**
     * 白名单支付
     * http://pay.cxlaimeng.cn/api/alipayzft/pays
     *  https://p.cxlaimeng.com/api/Alipayzft/pays
     */
    public function pays(){
       $url = "http://47.76.118.174/Pay_Index.html";
       $arr = [
            "pay_memberid"=>"210254442",
           "pay_orderid"=>orderNum(),
           "pay_amount"=>1.23,
           "pay_applydate"=>date("Y-m-d H:i:s",time()),
           "pay_bankcode"=>1078,
           "pay_notifyurl"=>ym()."/api/alipayzft/notifyurl",
           "pay_callbackurl"=>ym()."/api/alipayzft/callbackurl",
       ];
       //dump($arr);
       $key = "u4orwf366ml0wjxw9ykjgpr1fy0a66yr";
        //  pay_memberid  =  商户号
        //            pay_orderid       =  订单号
        //            pay_amount       =  金额,单位元
        //            pay_applydate   =  时间  格式 2025-01-01 12:00:00
        //            pay_bankcode   =  支付编码  (测试请写1078)
        //            pay_notifyurl       = 异步返回地址
        //            pay_callbackurl    = 同步返回地址
        $key = createSign($arr,$key);
        $arr["pay_md5sign"] = $key;
        $res = request_post($url,$arr);
        dump($res);
        //{"code":"0","msg":"ok","data":{"payurl":"http:\/\/zft.shyig.com\/pay\/submit\/2025011422053427325\/"}}
        $res = json_decode($res,true);
        dump($res);
        if($res["code"] == 0){
            $url = $res["data"]["payurl"];
            //return redirect($res);
        }
    }

    /**
     * 异步回调
     * @return void
     */
    public function notifyurl(){
        $notify_ip = request()->ip();//回调异步ip
        //上线前开启
        $data = $_POST;
        db("test")->insert(["add_time"=>time(),"content"=>json_encode($data),"ip"=>$notify_ip,"explain"=>"老谢支付宝异步支付返回参数"]);
        $model = new LaoxieOrder();
        $PayChannelModel = new laoxieChannel();
        $order = $model::where(["system_order_num"=>$data["orderid"]])->find();
        if(!$order){
            return AjaxReturn(0,'订单不存在');
        }
        if($order["payment"]){
            return AjaxReturn(0,'订单已支付');
        }
        $fan_sign = $data["sign"];
        unset($data['sign']);
        unset($data['attach']);
        $PayChannel = $PayChannelModel::get($order['bid']);
        $md5key = $PayChannel['key'];

        $sign = createSign($data,$md5key);;
        //halt($sign .'|'. $fan_sign);
        if ($sign == $fan_sign) {
            if ($data["returncode"] == "00") {
                $orderData = [
                    "notify_ip"=>$notify_ip,//异步回调支付ip
                    "payment" => 1,
                    "pay_time" => time(),
                    "transaction_id"=>$data['transaction_id'],
                ];
                $model::where(["id" => $order['id']])->update($orderData);//更改订单信息
                //支付时间统计
                $agentFianceModel = new Finance();
                $agentFianceModel->agentDaylx($order['id']);//平台日财务
                $agentFianceModel->payDaytongdaoxslx($order['id']);//通道日财务
                $agent =  Agent::get($order["agent_id"]);

                //支付回调
                $notify_data = [
                    "order_num" => $order["order_num"],
                    "money" => $order["money"],
                    "agent_id" => $agent["id"],
                    "system_order_num" => $order["system_order_num"],
                    "status" => "SUCCESS", //INIT(待支付)/SUCCESS(成功)/CANCEL(已取消)/REFUND(已退款)/REFUNDING(退款中)/REFUNDFAIL(退款失败)
                    "pay_time" => time(),
                ];
                $sign=createSign($notify_data,$agent["key"]);
                $notify_data['sign'] = $sign;
                if ($order["notify_url"]) {
                    //异步回调给其他平台
                    request_post($order["notify_url"], $notify_data);
                    $order::where('id', $order['id'])->update(["return_status" => 1, "return_time" => time()]);//修改订单状态
                }
                exit("ok");
            }
        }


    }
    /**
     * 异步测试
     * https://p.cxlaimeng.com/api/Alipayzft/notifytesty
     *
     * 本地地址:  http://pay.cxlaimeng.cn/api/Alipayzft/notifytesty
     *  网络地址:  https://p.cxlaimeng.com/api/Alipayzft/notifytesty
     */
    public function notifytesty(){
        $json = '{"memberid":"210254442","orderid":"jl114636402612764","transaction_id":"20250114220720565348","amount":"1.2300","datetime":"20250114221218","returncode":"00","sign":"737AC126D9888E46E64E9CD279196E3D","attach":""}';
        $data = json_decode($json,true);
        // 返回字段
//        $returnArray = array(
//            "memberid" => $data["memberid"],  // 会员ID
//            "merchantid" => $data["merchantid"],  // 商户ID（注意：原代码中"merchantid"的注释缺失，此处根据上下文添加）
//            "orderid" => $data["orderid"],  // 订单ID
//            "amount" => $data["amount"],  // 交易金额
//            "datetime" => $data["datetime"],  // 交易时间
//            "transaction_id" => $data["transaction_id"],  // 支付流水号
//            "returncode" => $data["returncode"]  // 返回码
//        );
        dump($data);
        $fan_sign = $data["sign"];
        unset($data['sign']);
        unset($data['attach']);
        $md5key = "u4orwf366ml0wjxw9ykjgpr1fy0a66yr";
//        ksort($returnArray);
//        reset($returnArray);
//
//        $md5str = "";
//        foreach ($returnArray as $key => $val) {
//            $md5str .= $key;
//        }

        $sign = createSign($data,$md5key);;
        //halt($sign .'|'. $fan_sign);
        if ($sign == $fan_sign) {
            if ($data["returncode"] == "00") {

                exit("ok");
            }
        }
    }

    /**
     * 同步返回
     * @return void
     */

    public function callbackurl(){

    }




}