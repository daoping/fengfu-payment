<?php

namespace app\api\controller;

use JytPay\Client_ONEPAY\JytJsonClient;
use think\Controller;
use think\facade\Env;
use think\facade\Request;
include_once  Env::get("root_path"). "extend/Jytfy/JytJsonClient.php";
class Jytdf extends Controller
{
    /**
     * 金运通支付宝支付
     * http://pay1.2021621.com/api/jytdf/index 本地
     * https://pay.2021621.com/api/jytdf/index 线上
     */
    public function index(){
        die();
       if(Request::isPost()){

           $post = $_POST;
           //array(9) {
           //  ["tc1002_test"] => string(36) "点击【代付】按钮发起测试"
           //  ["bankName"] => string(18) "中国建设银行"
           //  ["accountNo"] => string(19) "6200601234567892227"
           //  ["accountName"] => string(10) "李四1号"
           //  ["accountType"] => string(2) "00"
           //  ["tranAmt"] => string(4) "5.00"
           //  ["currency"] => string(3) "CNY"
           //  ["bsnCode"] => string(5) "09100"
           //  ["remark"] => string(42) "我已经打款了啊，不要说没有打"
           //}
           //halt($post);
           $client = new \JytPay\DaiFu\JytJsonClient;
           $client->init();

// 点击提交按钮后才执行
           if (!empty($_POST['tc1002_test'])) {
               // 报文头信息
               $data['head']['version']='1.0.0';
               $data['head']['tranType']='01';
               $data['head']['merchantId']= $client->config->merchant_id;
               $data['head']['tranDate']=date('Ymd',time());
               $data['head']['tranTime']=date('His',time());
               $data['head']['tranFlowid']= $client->config->merchant_id . date('YmdHis',time()) . substr(rand(),4);//代付订单号
               $data['head']['tranCode']= 'TC1002';
               // 报文体
               $data['body']['bankName'] = $_POST["bankName"];
               $data['body']['accountNo'] = $_POST["accountNo"]; // 测试：卡尾号1失败，2处理中，其它成功
               $data['body']['accountName'] = $_POST["accountName"];
               $data['body']['accountType'] = "00";
               $data['body']['tranAmt'] = $_POST["tranAmt"];
               $data['body']['currency'] = "CNY";
               $data['body']['bsnCode'] = "";
               $data['body']['remark'] = "";
               $res = $client->sendReq($data);
               halt(json_decode($res,true));
               //失败
               //array(2) {
               //  ["head"] => array(9) {
               //    ["merchantId"] => string(12) "261058120001"
               //    ["respCode"] => string(8) "E0000000"
               //    ["respDesc"] => string(18) "交易状态未知"
               //    ["tranCode"] => string(6) "TC1002"
               //    ["tranDate"] => string(8) "20230727"
               //    ["tranFlowid"] => string(31) "2610581200012023072715363182439"
               //    ["tranTime"] => string(6) "153631"
               //    ["tranType"] => string(2) "01"
               //    ["version"] => string(5) "1.0.0"
               //  }
               //  ["body"] => array(1) {
               //    ["tranState"] => string(2) "02"
               //  }
               //}
               //正常
               //array(2) {
               //  ["head"] => array(9) {
               //    ["merchantId"] => string(12) "473071040006"
               //    ["respCode"] => string(8) "S0000000"
               //    ["respDesc"] => string(12) "交易成功"
               //    ["tranCode"] => string(6) "TC1002"
               //    ["tranDate"] => string(8) "20230730"
               //    ["tranFlowid"] => string(32) "47307104000620230730143201956897"
               //    ["tranTime"] => string(6) "143201"
               //    ["tranType"] => string(2) "01"
               //    ["version"] => string(5) "1.0.0"
               //  }
               //  ["body"] => array(3) {
               //    ["feeAmt"] => string(4) "2.00"
               //    ["remark"] => string(0) ""
               //    ["tranState"] => string(2) "01"
               //  }
               //}
               echo $res;
           }else{
               halt("请点击支付按钮");
           }
       }else{
           $ip = getIp();
           return view("",["ip"=>$ip]);
       }
    }





}