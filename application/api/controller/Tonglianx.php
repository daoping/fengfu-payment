<?php

namespace app\api\controller;

use app\common\model\Agent;

use app\common\model\TlOrder;
use app\common\model\TonglianChannel;
use app\common\service\Finance;
use think\Controller;
use think\facade\Env;
use think\facade\Request;
use think\facade\Validate;

/**
 * 通联支付下单
 */
class Tonglianx extends Controller
{

    /**
     * 自有项目对接银联支付
     * https://pay.dinglianshop.cn/api/Tonglianx/unionpayapp
     */
    public function unionpayapp(){
        $data = input("post.");
        $validate = Validate::make([
            'type|支付类型'              => 'require|number',//支付类型 1支付宝 2微信 3 云闪付
            'order_num|订单号'             => 'require|length:10,40',
            'money|支付金额'                => 'require|float',
            'notify_url|异步通知地址'         => 'require|url',
            'agent_id|代理ID'                => 'require',
            'cid|通道id'=>'require',
        ]);

        if (!$validate->check($data)) {
            return AjaxReturn(0,$validate->getError());
        }
        if($data["money"] < 1){
            return AjaxReturn(0,"金额必须大于一元");
        }
//        if($data["money"] > 5000){
//            return AjaxReturn(0,"金额必须小于5000元");
//        }
        $agent = Agent::get($data["agent_id"]);
        if(!$agent){
            return AjaxReturn(0,'代理不存在');
        }
        if($agent["is_lock"]){
            return AjaxReturn(0,"代理暂停锁定");
        }
//        $arr = [
//            'agent_id'              => $data['agent_id'],
//            'order_num'             => $data['order_num'],
//            'notify_url'            => $data['notify_url'],
//            'money'                 =>  $data['money'],
//            'type'                  =>  $data['type'],
//        ];
//        $sign=createSign($arr,$agent["key"]);
//        if($sign != $data['sign']){
//            return AjaxReturn(0,"签名错误");
//        }
        $model = new TlOrder();
        $info =$model::get(["order_num"=>$data["order_num"]]);
        if($info){
            return AjaxReturn(0,"订单号已存在");
        }
//        if($data['money'] <= 50){
//            $pay_money =math_sub($data['money']*100,rand(1,20))/100;//实际支付金额
//        }else{
//            $pay_money =math_sub($data['money']*100,rand(5,30))/100;//实际支付金额
//        }
        //$pay_money =math_sub($data['money']*100,rand(3,30))/100;//实际支付金额
        $pay_money =$data['money'];

        $new_data = [
            'order_num'    => $data['order_num'],
            'notify_url'        => $data['notify_url'],
            'money'                => $data['money'],
            'pay_money'                => $pay_money,
            'system_order_num'=>orderNum(),
            'agent_id'=>$agent["id"],
            "type"=>$data['type']
        ];
        $res = $model->save($new_data);
        if(!$res){
            return AjaxReturn(0,"下单错误");
        }
        $order_info = $model::where(["order_num"=>$data["order_num"]])->find();
        $pay_channel_model = new TonglianChannel();
        //此处改造轮训通道
        if($data['type'] == 1){
            $pay_channel = $pay_channel_model::where(["open"=>1,"weixin"=>1,'id'=>$data['cid']])->find();
            if (!$pay_channel){
                return AjaxReturn(0,"暂无微信通道");
            }
        }elseif ($data['type'] == 2){
            $pay_channel = $pay_channel_model::where(["open"=>1,"alipay"=>1,'id'=>$data['cid']])->find();
            if (!$pay_channel){
                return AjaxReturn(0,"暂无支付宝通道");
            }
        }else if ($data['type'] == 3){
            $pay_channel = $pay_channel_model::where(["open"=>1,"kuaijie"=>1,'id'=>$data['cid']])->find();
            if (!$pay_channel){
                return AjaxReturn(0,"暂无支付宝通道");
            }
        }
        //渠道商
        $model::where(["id"=>$order_info["id"]])->update(["bid"=>$pay_channel["id"],"cusid"=>$pay_channel['cusid'],"appid"=>$pay_channel['appid']]);
//        if($data['type'] == 1){
//            $url = ym()."/api/sumapay/wechat?id=".$order_info['id'];
//        }else{
//            $url = ym()."/api/sumapay/aliapy?id=".$order_info['id'];
//        }


        //todo 预警连续30单没有付款关闭
//        $yj = $finance->orderyj($bid);//子商户id
//        if($yj){
//            return AjaxReturn(0,"已关闭");
//        }
        //$url = ym().'/api/tonglianx/tonglianpay?id='.$order_info['id'];
        if($data['type'] == 3){
            //网页模式
//            $url = ym().'/api/Tonglianpay/yinlian?id='.$order_info['id'];
//            return AjaxReturn(1,"ok",["url"=>$url]);

            //小程序模式
            $res = file_get_contents("https://pay.dinglianshop.cn/api/Tonglianpay/yinlianxcx?id=".$order_info['id']);
            return $res;

        }else{
            $url = ym().'/api/Tonglianpay/pay?id='.$order_info['id'];
            return AjaxReturn(1,"ok",["url"=>$url]);
        }
    }


    /************************************************通联支付********************************************************************/

    /**
     * 下单
     * http://p.dinglianshop.cn/api/Tonglianx/tonglian
     * https://pay.dinglianshop.cn/api/Tonglianx/tonglian
     */
    public function tonglian(){
        // 在入口文件index.php中设置

        $data = input("post.");
        $validate = Validate::make([
            'type|支付类型'              => 'require|number',//支付类型 1支付宝 2微信 3 云闪付
            'order_num|订单号'             => 'require|length:10,40',
            'money|支付金额'                => 'require|float',
            'notify_url|异步通知地址'         => 'require|url',
            'agent_id|代理ID'                => 'require',
            'sign|签名'                => 'require|length:32',
        ]);

        if (!$validate->check($data)) {
            return AjaxReturn(0,$validate->getError());
        }
        if($data["money"] < 1){
            return AjaxReturn(0,"金额必须大于一元");
        }
//        if($data["money"] > 5000){
//            return AjaxReturn(0,"金额必须小于5000元");
//        }
        $agent = Agent::get($data["agent_id"]);
        if(!$agent){
            return AjaxReturn(0,'代理不存在');
        }
        if($agent["is_lock"]){
            return AjaxReturn(0,"代理暂停锁定");
        }
        $arr = [
            'agent_id'              => $data['agent_id'],
            'order_num'             => $data['order_num'],
            'notify_url'            => $data['notify_url'],
            'money'                 =>  $data['money'],
            'type'                  =>  $data['type'],
        ];
        $sign=createSign($arr,$agent["key"]);
        if($sign != $data['sign']){
            return AjaxReturn(0,"签名错误");
        }
        $model = new TlOrder();
        $info =$model::get(["order_num"=>$data["order_num"]]);
        if($info){
            return AjaxReturn(0,"订单号已存在");
        }
//        if($data['money'] <= 50){
//            $pay_money =math_sub($data['money']*100,rand(1,20))/100;//实际支付金额
//        }else{
//            $pay_money =math_sub($data['money']*100,rand(5,30))/100;//实际支付金额
//        }
        //$pay_money =math_sub($data['money']*100,rand(3,30))/100;//实际支付金额
        $pay_money =$data['money'];

        $new_data = [
            'order_num'    => $data['order_num'],
            'notify_url'        => $data['notify_url'],
            'money'                => $data['money'],
            'pay_money'                => $pay_money,
            'system_order_num'=>orderNum(),
            'agent_id'=>$agent["id"],
            "type"=>$data['type']
        ];
        $res = $model->save($new_data);
        if(!$res){
            return AjaxReturn(0,"下单错误");
        }
        $order_info = $model::where(["order_num"=>$data["order_num"]])->find();
        $pay_channel_model = new TonglianChannel();
        //此处改造轮训通道
        if($data['type'] == 1){
            $pay_channel = $pay_channel_model::where(["open"=>1,"weixin"=>1])->find();
            if (!$pay_channel){
                return AjaxReturn(0,"暂无微信通道");
            }
        }elseif ($data['type'] == 2){
            $pay_channel = $pay_channel_model::where(["open"=>1,"alipay"=>1])->find();
            if (!$pay_channel){
                return AjaxReturn(0,"暂无支付宝通道");
            }
        }else if ($data['type'] == 3){
            $pay_channel = $pay_channel_model::where(["open"=>1,"kuaijie"=>1])->find();
            if (!$pay_channel){
                return AjaxReturn(0,"暂无支付宝通道");
            }
        }
        //渠道商
        $model::where(["id"=>$order_info["id"]])->update(["bid"=>$pay_channel["id"],"cusid"=>$pay_channel['cusid'],"appid"=>$pay_channel['appid']]);
//        if($data['type'] == 1){
//            $url = ym()."/api/sumapay/wechat?id=".$order_info['id'];
//        }else{
//            $url = ym()."/api/sumapay/aliapy?id=".$order_info['id'];
//        }


        //todo 预警连续30单没有付款关闭
//        $yj = $finance->orderyj($bid);//子商户id
//        if($yj){
//            return AjaxReturn(0,"已关闭");
//        }
        //$url = ym().'/api/tonglianx/tonglianpay?id='.$order_info['id'];
        if($data['type'] == 3){
            $url = ym().'/api/Tonglianpay/yinlian?id='.$order_info['id'];
            return AjaxReturn(1,"ok",["url"=>$url]);
        }else if($data['type'] == 1 || $data['type'] == 2){
            $url = ym().'/api/Tonglianpay/wechat?id='.$order_info['id'];
            return AjaxReturn(1,"ok",["url"=>$url]);
        }else{
            $url = ym().'/api/Tonglianpay/pay?id='.$order_info['id'];
            return AjaxReturn(1,"ok",["url"=>$url]);
        }

    }


    /**
     * 金运通微信支付签名
     */
    public function tongliansign(){
        $data = input("post.");
        $validate = Validate::make([
            'type|支付类型'              => 'require|number',//支付类型 1支付宝 2微信 3快捷
            'order_num|订单号'             => 'require|length:10,40',
            'money|支付金额'                => 'require|float',
            'notify_url|异步通知地址'         => 'require|url',
            'agent_id|代理ID'                => 'require',
        ]);

        if (!$validate->check($data)) {
            return AjaxReturn(0,$validate->getError());
        }
//        if($data["money"] < 1){
//            return AjaxReturn(0,"金额必须大于一元");
//        }
        $agent = Agent::get($data["agent_id"]);
        if(!$agent){
            return AjaxReturn(0,'代理不存在');
        }
        if($agent["is_lock"]){
            return AjaxReturn(0,"代理暂停锁定");
        }
        $arr = [
            'agent_id'              => $data['agent_id'],
            'order_num'             => $data['order_num'],
            'notify_url'            => $data['notify_url'],
            'money'                 =>  $data['money'],
            'type'                  =>  $data['type'],
        ];
        $sign=createSign($arr,$agent["key"]);
        return $sign;
    }

    /**
     * 通联支付二维码过度页
     */
    public function tonglianpay(){
        $id = input("get.id");
        $model = new TlOrder();
        $info =$model::get($id);
        if(!$info){
            return AjaxReturn(0,"订单号已存在");
        }
        if($info['payment']){
            return AjaxReturn(0,"订单已支付");
        }
        //todo 增加ip判断  境外的ip直接不能进行下一步
        $url = ym().'/api/Tonglianpay/pay?id='.$id;
        $money = $info['money'];
        return view("",["orderCode"=>$info['system_order_num'],"url"=>$url,"orderMoney"=>$money]);
    }


    /**
     * 查询订单
     */
    public function orderdetails(){
        $order_num = input("get.order_num");
        $model = new TlOrder();
        $order = $model::where(["system_order_num"=>$order_num])->find();
        //halt($order);
        if(!$order){
            return AjaxReturn(0,"订单不存在");
        }
        if($order["payment"] == 1){
            return AjaxReturn(1,"付款成功");
        }
    }

    /**
     * 支付状态
     * https://pay.hnyouke888.cn/api/sandesm/payment.html?order_num=ym815649473909234
     * http://pay1.lnxinmeng.xyz/api/sandesm/payment.html?order_num=ym815649473909234
     */
    public function payment(){
        $order_num = input("get.order_num");
        $model = new TlOrder();
        $order = $model::where(["system_order_num"=>$order_num])->find();
        if(!$order){
            return AjaxReturn(0,"订单不存在");
        }
//        if($order["payment"] == 1){
//            return AjaxReturn(1,"付款成功");
//        }
        $this->assign("order",$order);
        return view();
    }


    /**
     * 银联支付下单
     * http://p.dinglianshop.cn/api/Tonglianx/tonglian
     * https://pay.dinglianshop.cn/api/Tonglianx/tonglian
     */
    public function tongliannew(){
        // 在入口文件index.php中设置
        $finance = new  Finance();
        $data = input("post.");
        $validate = Validate::make([
            'type|支付类型'              => 'require|number',//支付类型 1支付宝 2微信 3 云闪付
            'order_num|订单号'             => 'require|length:10,40',
            'money|支付金额'                => 'require|float',
            'notify_url|异步通知地址'         => 'require|url',
            'agent_id|代理ID'                => 'require',
            'sign|签名'                => 'require|length:32',
            "cusid"  =>'require'
        ]);

        if (!$validate->check($data)) {
            return AjaxReturn(0,$validate->getError());
        }
//        if($data["money"] < 1){
//            return AjaxReturn(0,"金额必须大于一元");
//        }
//        if($data["money"] > 5000){
//            return AjaxReturn(0,"金额必须小于5000元");
//        }
        $agent = Agent::get($data["agent_id"]);
        if(!$agent){
            return AjaxReturn(0,'代理不存在');
        }
        if($agent["is_lock"]){
            return AjaxReturn(0,"代理暂停锁定");
        }
        $arr = [
            'agent_id'              => $data['agent_id'],
            'order_num'             => $data['order_num'],
            'notify_url'            => $data['notify_url'],
            'money'                 =>  $data['money'],
            'type'                  =>  $data['type'],
            'cusid'                  =>  $data['cusid'],
        ];
        $sign=createSign($arr,$agent["key"]);
        if($sign != $data['sign']){
            return AjaxReturn(0,"签名错误");
        }
        $model = new TlOrder();
        $info =$model::get(["order_num"=>$data["order_num"]]);
        if($info){
            return AjaxReturn(0,"订单号已存在");
        }
//        if($data['money'] <= 50){
//            $pay_money =math_sub($data['money']*100,rand(1,20))/100;//实际支付金额
//        }else{
//            $pay_money =math_sub($data['money']*100,rand(5,30))/100;//实际支付金额
//        }
        //$pay_money =math_sub($data['money']*100,rand(3,30))/100;//实际支付金额
        $pay_money =$data['money'];

        $new_data = [
            'order_num'    => $data['order_num'],
            'notify_url'        => $data['notify_url'],
            'money'                => $data['money'],
            'pay_money'                => $pay_money,
            'system_order_num'=>orderNum(),
            'agent_id'=>$agent["id"],
            "type"=>$data['type']
        ];
        $res = $model->save($new_data);
        if(!$res){
            return AjaxReturn(0,"下单错误");
        }
        $order_info = $model::where(["order_num"=>$data["order_num"]])->find();
        $pay_channel_model = new TonglianChannel();
        //此处改造轮训通道
        if($data['type'] == 1){
            $pay_channel = $pay_channel_model::where(["open"=>1,"weixin"=>1,"cusid"=>$data['cusid']])->find();
            if (!$pay_channel){
                return AjaxReturn(0,"暂无微信通道");
            }
        }elseif ($data['type'] == 2){
            $pay_channel = $pay_channel_model::where(["open"=>1,"alipay"=>1,"cusid"=>$data['cusid']])->find();
            if (!$pay_channel){
                return AjaxReturn(0,"暂无支付宝通道");
            }
        }else if ($data['type'] == 3){
            $pay_channel = $pay_channel_model::where(["open"=>1,"kuaijie"=>1,"cusid"=>$data['cusid']])->find();
            if (!$pay_channel){
                return AjaxReturn(0,"暂无银联通道");
            }
        }
//        if($arr['money'] > 1000){
//            $cusid = $data['cusid'];
//            $appid = "00318152";
//            $pay_channel = $pay_channel_model::where(["open"=>1,"kuaijie"=>1,"cusid"=>$cusid])->find();
//        }else{
//            $cusid = "660303051992MXK";
//            $appid = "00324169";
//            $pay_channel = $pay_channel_model::where(["open"=>1,"kuaijie"=>1,"cusid"=>$cusid])->find();
//        }
        //渠道商
        $model::where(["id"=>$order_info["id"]])->update(["bid"=>$pay_channel["id"],"cusid"=>$pay_channel['cusid'],"appid"=>$pay_channel['appid']]);
//        if($data['type'] == 1){
//            $url = ym()."/api/sumapay/wechat?id=".$order_info['id'];
//        }else{
//            $url = ym()."/api/sumapay/aliapy?id=".$order_info['id'];
//        }

        $finance->jinexiatl($order_info['id']);//金额下单日财务统计
        //todo 预警连续30单没有付款关闭
//        $yj = $finance->orderyj($bid);//子商户id
//        if($yj){
//            return AjaxReturn(0,"已关闭");
//        }
        //$url = ym().'/api/tonglianx/tonglianpay?id='.$order_info['id'];
        if($data['type'] == 3){
            $url = ym().'/api/Tonglianpay/yinlian?id='.$order_info['id'];
            return AjaxReturn(1,"ok",["url"=>$url]);
        }else if($data['type'] == 1){
            $url = ym().'/api/Tonglianpay/wechat?id='.$order_info['id'];
            return AjaxReturn(1,"ok",["url"=>$url]);
        }else{
            $url = ym().'/api/Tonglianpay/pay?id='.$order_info['id'];
            return AjaxReturn(1,"ok",["url"=>$url]);
        }

    }


    /**
     * 金运通微信支付签名
     */
    public function tongliannewsign(){
        $data = input("post.");
        $validate = Validate::make([
            'type|支付类型'              => 'require|number',//支付类型 1支付宝 2微信 3快捷
            'order_num|订单号'             => 'require|length:10,40',
            'money|支付金额'                => 'require|float',
            'notify_url|异步通知地址'         => 'require|url',
            'agent_id|代理ID'                => 'require',
            'cusid'                  =>  $data['cusid'],
        ]);

        if (!$validate->check($data)) {
            return AjaxReturn(0,$validate->getError());
        }
//        if($data["money"] < 1){
//            return AjaxReturn(0,"金额必须大于一元");
//        }
        $agent = Agent::get($data["agent_id"]);
        if(!$agent){
            return AjaxReturn(0,'代理不存在');
        }
        if($agent["is_lock"]){
            return AjaxReturn(0,"代理暂停锁定");
        }
        $arr = [
            'agent_id'              => $data['agent_id'],
            'order_num'             => $data['order_num'],
            'notify_url'            => $data['notify_url'],
            'money'                 =>  $data['money'],
            'type'                  =>  $data['type'],
            'cusid'                  =>  $data['cusid'],
        ];
        $sign=createSign($arr,$agent["key"]);
        return $sign;
    }




    /*******************************************通联商城*******************************************************/

    /**
     * 下单
     * http://p.dinglianshop.cn/api/Tonglianx/tonglianshop
     * https://p.cxlaimeng.com/api/Tonglianx/tonglianshop
     */
    public function tonglianshop(){
        // 在入口文件index.php中设置

        $data = input("post.");
        $validate = Validate::make([
            'type|支付类型'              => 'require|number',//支付类型 1支付宝 2微信 3 云闪付
            'order_num|订单号'             => 'require|length:10,40',
            'money|支付金额'                => 'require|float',
            'notify_url|异步通知地址'         => 'require|url',
            'agent_id|代理ID'                => 'require',
        ]);

        if (!$validate->check($data)) {
            return AjaxReturn(0,$validate->getError());
        }
//        if($data["money"] < 1){
//            return AjaxReturn(0,"金额必须大于一元");
//        }
//        if($data["money"] > 5000){
//            return AjaxReturn(0,"金额必须小于5000元");
//        }
        $agent = Agent::get($data["agent_id"]);
        if(!$agent){
            return AjaxReturn(0,'代理不存在');
        }
        if($agent["is_lock"]){
            return AjaxReturn(0,"代理暂停锁定");
        }
        $arr = [
            'agent_id'              => $data['agent_id'],
            'order_num'             => $data['order_num'],
            'notify_url'            => $data['notify_url'],
            'money'                 =>  $data['money'],
            'type'                  =>  $data['type'],
        ];

        $model = new TlOrder();
        $info =$model::get(["order_num"=>$data["order_num"]]);
        if($info){
            return AjaxReturn(0,"订单号已存在");
        }
//        if($data['money'] <= 50){
//            $pay_money =math_sub($data['money']*100,rand(1,20))/100;//实际支付金额
//        }else{
//            $pay_money =math_sub($data['money']*100,rand(5,30))/100;//实际支付金额
//        }
        //$pay_money =math_sub($data['money']*100,rand(3,30))/100;//实际支付金额
        $pay_money =$data['money'];

        $new_data = [
            'order_num'    => $data['order_num'],
            'notify_url'        => $data['notify_url'],
            'money'                => $data['money'],
            'pay_money'                => $pay_money,
            'system_order_num'=>orderNum(),
            'agent_id'=>$agent["id"],
            "type"=>$data['type']
        ];
        $res = $model->save($new_data);
        if(!$res){
            return AjaxReturn(0,"下单错误");
        }
        $order_info = $model::where(["order_num"=>$data["order_num"]])->find();
        $pay_channel_model = new TonglianChannel();
        //此处改造轮训通道
        if($data['type'] == 1){
            $pay_channel = $pay_channel_model::where(["open"=>1,"weixin"=>1])->find();
            if (!$pay_channel){
                return AjaxReturn(0,"暂无微信通道");
            }
        }elseif ($data['type'] == 2){
            $pay_channel = $pay_channel_model::where(["open"=>1,"alipay"=>1])->find();
            if (!$pay_channel){
                return AjaxReturn(0,"暂无支付宝通道");
            }
        }else if ($data['type'] == 3){
            $pay_channel = $pay_channel_model::where(["open"=>1,"kuaijie"=>1])->find();
            if (!$pay_channel){
                return AjaxReturn(0,"暂无支付宝通道");
            }
        }
        //渠道商
        $model::where(["id"=>$order_info["id"]])->update(["bid"=>$pay_channel["id"],"cusid"=>$pay_channel['cusid'],"appid"=>$pay_channel['appid']]);
//        if($data['type'] == 1){
//            $url = ym()."/api/sumapay/wechat?id=".$order_info['id'];
//        }else{
//            $url = ym()."/api/sumapay/aliapy?id=".$order_info['id'];
//        }


        //todo 预警连续30单没有付款关闭
//        $yj = $finance->orderyj($bid);//子商户id
//        if($yj){
//            return AjaxReturn(0,"已关闭");
//        }
        //$url = ym().'/api/tonglianx/tonglianpay?id='.$order_info['id'];
        if($data['type'] == 3){
            $url = ym().'/api/Tonglianpay/yinlian?id='.$order_info['id'];
            return AjaxReturn(1,"ok",["url"=>$url]);
        }else if($data['type'] == 1){
            $url = ym().'/api/Tonglianpay/wechatshop?id='.$order_info['id'];
            return AjaxReturn(1,"ok",["url"=>$url]);
        }else{
            $url = ym().'/api/Tonglianpay/pay?id='.$order_info['id'];
            return AjaxReturn(1,"ok",["url"=>$url]);
        }

    }


    /**
     * 银联app支付下单
     * http://p.dinglianshop.cn/api/Tonglianx/tonglian
     * https://p.cxlaimeng.com/api/Tonglianx/tonglian
     */
    public function tongliannewapp(){
        // 在入口文件index.php中设置
        $finance = new  Finance();
        $data = input("post.");
        $validate = Validate::make([
            'type|支付类型'              => 'require|number',//支付类型 1支付宝 2微信 3 云闪付
            'order_num|订单号'             => 'require|length:10,40',
            'money|支付金额'                => 'require|float',
            'notify_url|异步通知地址'         => 'require|url',
            'agent_id|代理ID'                => 'require',
            "cusid"  =>'require'
        ]);

        if (!$validate->check($data)) {
            return AjaxReturn(0,$validate->getError());
        }
//        if($data["money"] < 1){
//            return AjaxReturn(0,"金额必须大于一元");
//        }
//        if($data["money"] > 5000){
//            return AjaxReturn(0,"金额必须小于5000元");
//        }
        $agent = Agent::get($data["agent_id"]);
        if(!$agent){
            return AjaxReturn(0,'代理不存在');
        }
        if($agent["is_lock"]){
            return AjaxReturn(0,"代理暂停锁定");
        }
        $arr = [
            'agent_id'              => $data['agent_id'],
            'order_num'             => $data['order_num'],
            'notify_url'            => $data['notify_url'],
            'money'                 =>  $data['money'],
            'type'                  =>  $data['type'],
            'cusid'                  =>  $data['cusid'],
        ];
//        $sign=createSign($arr,$agent["key"]);
//        if($sign != $data['sign']){
//            return AjaxReturn(0,"签名错误");
//        }
        $model = new TlOrder();
        $info =$model::get(["order_num"=>$data["order_num"]]);
        if($info){
            return AjaxReturn(0,"订单号已存在");
        }
//        if($data['money'] <= 50){
//            $pay_money =math_sub($data['money']*100,rand(1,20))/100;//实际支付金额
//        }else{
//            $pay_money =math_sub($data['money']*100,rand(5,30))/100;//实际支付金额
//        }
        //$pay_money =math_sub($data['money']*100,rand(3,30))/100;//实际支付金额
        $pay_money =$data['money'];

        $new_data = [
            'order_num'    => $data['order_num'],
            'notify_url'        => $data['notify_url'],
            'money'                => $data['money'],
            'pay_money'                => $pay_money,
            'system_order_num'=>orderNum(),
            'agent_id'=>$agent["id"],
            "type"=>$data['type']
        ];
        $res = $model->save($new_data);
        if(!$res){
            return AjaxReturn(0,"下单错误");
        }
        $order_info = $model::where(["order_num"=>$data["order_num"]])->find();
        $pay_channel_model = new TonglianChannel();
        //此处改造轮训通道
        if($data['type'] == 1){
            $pay_channel = $pay_channel_model::where(["open"=>1,"weixin"=>1,"cusid"=>$data['cusid']])->find();
            if (!$pay_channel){
                return AjaxReturn(0,"暂无微信通道");
            }
        }elseif ($data['type'] == 2){
            $pay_channel = $pay_channel_model::where(["open"=>1,"alipay"=>1,"cusid"=>$data['cusid']])->find();
            if (!$pay_channel){
                return AjaxReturn(0,"暂无支付宝通道");
            }
        }else if ($data['type'] == 3){
            $pay_channel = $pay_channel_model::where(["open"=>1,"kuaijie"=>1,"cusid"=>$data['cusid']])->find();
            if (!$pay_channel){
                return AjaxReturn(0,"暂无银联通道");
            }
        }
//        if($arr['money'] > 1000){
//            $cusid = $data['cusid'];
//            $appid = "00318152";
//            $pay_channel = $pay_channel_model::where(["open"=>1,"kuaijie"=>1,"cusid"=>$cusid])->find();
//        }else{
//            $cusid = "660303051992MXK";
//            $appid = "00324169";
//            $pay_channel = $pay_channel_model::where(["open"=>1,"kuaijie"=>1,"cusid"=>$cusid])->find();
//        }
        //渠道商
        $model::where(["id"=>$order_info["id"]])->update(["bid"=>$pay_channel["id"],"cusid"=>$pay_channel['cusid'],"appid"=>$pay_channel['appid']]);
//        if($data['type'] == 1){
//            $url = ym()."/api/sumapay/wechat?id=".$order_info['id'];
//        }else{
//            $url = ym()."/api/sumapay/aliapy?id=".$order_info['id'];
//        }

        $finance->jinexiatl($order_info['id']);//金额下单日财务统计
        //todo 预警连续30单没有付款关闭
//        $yj = $finance->orderyj($bid);//子商户id
//        if($yj){
//            return AjaxReturn(0,"已关闭");
//        }
        //$url = ym().'/api/tonglianx/tonglianpay?id='.$order_info['id'];
        if($data['type'] == 3){
            $url = ym().'/api/Tonglianpay/yinlianshop?id='.$order_info['id'];
            return AjaxReturn(1,"ok",["url"=>$url]);
        }else if($data['type'] == 1){
            $url = ym().'/api/Tonglianpay/wechat?id='.$order_info['id'];
            return AjaxReturn(1,"ok",["url"=>$url]);
        }else{
            $url = ym().'/api/Tonglianpay/pay?id='.$order_info['id'];
            return AjaxReturn(1,"ok",["url"=>$url]);
        }

    }

}