<?php

namespace app\api\controller;

use app\common\model\Agent;
use app\common\model\CzOrder;
use app\common\model\HkChannel;
use app\common\model\HkOrder;
use app\common\model\Order;
use app\common\service\SdCommon;
use app\common\service\Finance;
use think\Controller;
use think\facade\Env;
//include_once  Env::get("root_path"). "extend/ldpay/umfPayService/UmfService.class.php";
class Haikepay extends Controller
{


    /**
     * 海科支付
     * mchid: 10003474
     * appid: 10001256
     * 对接文档里的两个参数
     * 本地地址:  http://pay.cxlaimeng.cn/api/Haikepay/haikepay
     * 网络地址:  https://p.cxlaimeng.com/api/Haikepay/haikepay
     */
    public function haikepay_(){
        $url = "https://api.jitepay.com/v3/pay/transactions/hkrt/native";
        $timeExpire_y = date("Y-m-d",time() + 1200);
        $timeExpire_t = date("H:i:s",time() + 1200);
        $param=[
            "channel"=>"alipay_qr",
            "appid"=>"10001256",
            "mchid"=>"10003474",
            "description"=>"付款",
            "outTradeNo"=>orderNum(),
            "timeExpire"=>$timeExpire_y."T".$timeExpire_t."+08:00",
            "notifyUrl"=>ym()."/api/Haikepay/haikepaynotify",
            "amount"=>["total"=>100,"currency"=>"CNY"],
            "orderType"=>"ON_LINE",
            "payer"=>["openId"=>generateUniqueId()],
            "sceneInfo"=>["payerClientIp"=>request()->ip()],
        ];
        //array(11) {
        //  ["channel"] => string(9) "alipay_qr"
        //  ["appid"] => string(8) "10001256"
        //  ["mchid"] => string(8) "10003474"
        //  ["description"] => string(12) "商城付款"
        //  ["outTradeNo"] => string(17) "jlC27673388467221"
        //  ["timeExpire"] => string(25) "2024-12-27T11:02:18+08:00"
        //  ["notifyUrl"] => string(52) "https://p.cxlaimeng.com/api/Haikepay/haikepay_notify"
        //  ["amount"] => array(2) {
        //    ["total"] => int(100)
        //    ["currency"] => string(3) "CNY"
        //  }
        //  ["orderType"] => string(7) "ON_LINE"
        //  ["payer"] => array(1) {
        //    ["openId"] => string(13) "676e140acebd9"
        //  }
        //  ["sceneInfo"] => array(1) {
        //    ["payerClientIp"] => string(14) "39.144.114.192"
        //  }
        //}
        dump($param);
        $res = request_post_jsons($url,$param);
        //halt($res);
        //string(87) "{"qr_code":"https://qr.alipay.com/bax01876ffirlogj8jla003f","appId":"2021003167624148"}"
        $res = json_decode($res,true);
        halt($res);
    }


    /**
     * 海科支付
     * mchid: 10003474
     * appid: 10001256
     * 对接文档里的两个参数
     * 本地地址:  http://pay.cxlaimeng.cn/api/Haikepay/haikepay
     * 网络地址:  https://p.cxlaimeng.com/api/Haikepay/haikepay?id=15632
     */
    public function haikepay(){
        $model = new HkOrder();
        $id = input("get.id");
        if(!$id){
            return AjaxReturn(0,"参数错误");
        }
        $order = $model::get($id);
        if(!$order){
            return AjaxReturn(0,"订单不存在");
        }
        if($order["payment"] == 1){
            return AjaxReturn(0,"订单已支付");
        }
        $ip = request()->ip();
        $model->where("id",$id)->update(["buy_ip"=>$ip]);//更新付款用户下单ip
        $PayChannelModel  = new HkChannel();
        $PayChannel = $PayChannelModel::get($order['bid']);
        if(!$PayChannel){
            return AjaxReturn(1,"支付通道不存在");
        }
        if($PayChannel['open'] != 1){
            return AjaxReturn(1,"通道未开启");
        }
        $money = $order['pay_money'];//支付金额
        if($order['codeUrl'] && !$order["payment"]){
            $pay_url = $order['codeUrl'];
            header('Location:'.$pay_url);
            exit();
        }
        $url = "https://api.jitepay.com/v3/pay/transactions/hkrt/native";
        $timeExpire_y = date("Y-m-d",time() + 1200);
        $timeExpire_t = date("H:i:s",time() + 1200);
        $param=[
            "channel"=>"alipay_qr",
            "appid"=>$order['appid'],
            "mchid"=>$order['mchid'],
            "description"=>"付款",
            "outTradeNo"=>$order['system_order_num'],
            "timeExpire"=>$timeExpire_y."T".$timeExpire_t."+08:00",
            "notifyUrl"=>ym()."/api/Haikepay/haikepaynotify",
            "amount"=>["total"=>$money,
            "currency"=>"CNY"],
            "orderType"=>"ON_LINE",
            "payer"=>["openId"=>generateUniqueId()],
            "sceneInfo"=>["payerClientIp"=>request()->ip()],
        ];
        dump($param);
        //array(11) {
        //  ["channel"] => string(9) "alipay_qr"
        //  ["appid"] => string(8) "10001256"
        //  ["mchid"] => string(8) "10003474"
        //  ["description"] => string(12) "商城付款"
        //  ["outTradeNo"] => string(17) "jlC27673388467221"
        //  ["timeExpire"] => string(25) "2024-12-27T11:02:18+08:00"
        //  ["notifyUrl"] => string(52) "https://p.cxlaimeng.com/api/Haikepay/haikepay_notify"
        //  ["amount"] => array(2) {
        //    ["total"] => int(100)
        //    ["currency"] => string(3) "CNY"
        //  }
        //  ["orderType"] => string(7) "ON_LINE"
        //  ["payer"] => array(1) {
        //    ["openId"] => string(13) "676e140acebd9"
        //  }
        //  ["sceneInfo"] => array(1) {
        //    ["payerClientIp"] => string(14) "39.144.114.192"
        //  }
        //}
        //dump($param);
        $res = request_post_jsons($url,$param);
        //halt($res);
        //string(87) "{"qr_code":"https://qr.alipay.com/bax01876ffirlogj8jla003f","appId":"2021003167624148"}"
        $res = json_decode($res,true);
        halt($res);
        if($res['qr_code']){
            $model->where("id",$order['id'])->update(["codeUrl"=>$res["qr_code"]]);
            $pay_url = $res['qr_code'];
            header('Location:'.$pay_url);
            exit();
        }else{
            return AjaxReturn(1,"支付失败".$res['msg']);
        }


    }

    /**
     * 海科支付回调
     *  本地地址:  http://pay.cxlaimeng.cn/api/Haikepay/haikepaynotify
     *  网络地址:  https://p.cxlaimeng.com/api/Haikepay/haikepaynotify
     */
    public function haikepaynotify(){
        $notify_ip = request()->ip();//回调异步ip
        //上线前开启
        $data =  file_get_contents('php://input');
        db("test")->insert(["add_time"=>time(),"content"=>$data,"ip"=>$notify_ip,"explain"=>"海科支付回调数据"]);
//        $json ='"{\"payer_total\":100.00,\"in_trade_no\":\"2024122722001447671406955746\",\"amount\":{\"currency\":\"CNY\",\"payer_currency\":\"\",\"total\":100},\"out_trade_no\":\"jlC27714269676895\",\"trade_state\":\"SUCCESS\",\"mch_name\":\"\u957f\u6c99\u83b1\u6aac\",\"pay_amount\":100.00,\"trade_no\":\"AL2412271150280000813434672\",\"receipt_amount\":100.00,\"mch_id\":10003474,\"payer\":{\"openid\":\"676e2402ec464\"}}"';
//        $data = json_decode($json,true);
        $data = json_decode($data,true);
        //dump($data);
        //array(11) {
        //  ["payer_total"] => float(100)
        //  ["in_trade_no"] => string(28) "2024122722001447671406955746"
        //  ["amount"] => array(3) {
        //    ["currency"] => string(3) "CNY"
        //    ["payer_currency"] => string(0) ""
        //    ["total"] => int(100)
        //  }
        //  ["out_trade_no"] => string(17) "jlC27714269676895"
        //  ["trade_state"] => string(7) "SUCCESS" //支付成功状态
        //  ["mch_name"] => string(12) "长沙莱檬"
        //  ["pay_amount"] => float(100) //实际支付金额
        //  ["trade_no"] => string(27) "AL2412271150280000813434672"
        //  ["receipt_amount"] => float(100)
        //  ["mch_id"] => int(10003474)
        //  ["payer"] => array(1) {
        //    ["openid"] => string(13) "676e2402ec464"
        //  }
        //}
        if($data['trade_state'] == 'SUCCESS' && $data['out_trade_no']){
            $model = new Hkorder();
            $order = $model::where(["system_order_num"=>$data["out_trade_no"]])->find();
            //halt($order);
            if(!$order){
                return AjaxReturn(0,'订单不存在');
            }
            if($order["payment"]){
                return AjaxReturn(0,'订单已支付');
            }
            $orderData = [
                "notify_ip"=>$notify_ip,//异步回调支付ip
                "payment" => 1,
                "pay_time" => time(),
                "in_trade_no"=>$data['in_trade_no'],
                "trade_no"=>$data['trade_no'],


            ];
            $model::where(["id" => $order['id']])->update($orderData);//更改订单信息
            //支付时间统计
            $agentFianceModel = new Finance();
            $agentFianceModel->agentDayhk($order['id']);//平台日财务
            $agentFianceModel->payDaytongdaoxshk($order['id']);//通道日财务
            $agent =  Agent::get($order["agent_id"]);

                //支付回调
            $notify_data = [
                "order_num" => $order["order_num"],
                "money" => $order["money"],
                "agent_id" => $agent["id"],
                "system_order_num" => $order["system_order_num"],
                "status" => "SUCCESS", //INIT(待支付)/SUCCESS(成功)/CANCEL(已取消)/REFUND(已退款)/REFUNDING(退款中)/REFUNDFAIL(退款失败)
                "pay_time" => time(),
            ];
            $sign=createSign($notify_data,$agent["key"]);
            $notify_data['sign'] = $sign;
            if ($order["notify_url"]) {
                //异步回调给其他平台
                request_post($order["notify_url"], $notify_data);
                $order::where('id', $order['id'])->update(["return_status" => 1, "return_time" => time()]);//修改订单状态
            }
            return json_encode(["resultCode"=>"SUCCESS","resultMsg"=>"回调成功"]);
            exit();

        }


    }




}