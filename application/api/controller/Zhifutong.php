<?php

namespace app\api\controller;


use app\common\model\Agent;
use app\common\model\AkOrder;
use app\common\model\CzOrder;
use app\common\model\Order;
use app\common\model\PayChannel;
use app\common\model\ZhifutongChannel;
use app\common\model\ZhifutongOrder;
use app\common\service\Finance;
use think\Controller;
use think\facade\Env;

/**
 * 支付宝支付白名单
 * https://pay.dinglianshop.cn/api/Zhifutong
 */
class Zhifutong extends Controller
{

    /**
     * 直付通
     * 本地 http://pay.cxlaimeng.cn/api/Zhifutong/pay
     *  线上 https://p.cxlaimeng.com//api/Zhifutong/pay?id=212221
     */
    public function pay()
    {
        $model = new ZhifutongOrder();
        $PayChannelModel = new ZhifutongChannel();
        $id = input("get.id");
        //halt($id);
        $order = $model::where("id",$id)->find();
        if(!$order){
            return AjaxReturn(0,"订单不存在");
        }
//            $order_time = strtotime($order['create_time']) + 1200;
//            if($order_time < time()){
//                return AjaxReturn(0,"订单已过期");
//            }
        if($order["payment"] > 0){
            return AjaxReturn(0,"订单已支付");
        }

        $ip = request()->ip();
        //增加用户付款的ip信息
        db("order_ips")->insert(["order_id"=>$id,"ip"=>$ip,"create_time"=>time()]);
        $model->where("id",$id)->update(["buy_ip"=>$ip]);//更新付款用户下单ip
        $PayChannel = $PayChannelModel::get($order['bid']);
        if(!$PayChannel){
            return AjaxReturn(1,"支付通道不存在");
        }
        if($PayChannel['open'] != 1){
            return AjaxReturn(1,"通道未开启");
        }
        if($order['codeUrl'] && !$order["payment"]){
            $pay_url = $order['codeUrl'];
            header('Location:'.$pay_url);
            exit();
        }
        $money = $order['pay_money'] * 100;
        $title = $order['title'] ? $order['title'] : '购买商品';
        $params = [
            'mchNo'=>$PayChannel['mchid'],//商户号
            'wayCode'=>'ALIPAY_H5',//ALIPAY_H5：支付宝Wap支付，ALIPAY_SDK：支付宝app支付,ALIPAY_QR：支付宝当面付
            'amount' => $money, // 价格:单位分
            'clientIp' => $ip,//用户真实ip
            'subject' => $title,
            'mchOrderNo' => $order['system_order_num'],//订单号
            'body' => '9',//订单商品类型1游戏币2游戏账号3游戏什盲盒7二手者侈品8二手手表9基他
            'channelExtra'=>'',
            'version' =>  "1.0",//版本号
            'notifyUrl' => ym().'/api/Zhifutong/zhifutongpay',  // 异步通知地址
            'returnUrl' => ym().'/api/Zhifutong/returnUrl',//同步回调地址
            'reqTime' => date('Y-m-d H:i:s'),
            'expiredTime'=>'600',//订单失效时间,单位秒,默认2小时.订单在(创建时间+失效时间)后失效
        ];
        $key = $PayChannel['key'];
        // 生成签名
        $params['sign'] = $this->makeSign($params,$key);
        $result = $this->post('https://hwgpay.api.huaweigu.com/hwgPay/api/unifiedOrder', $params);
        $prepay = json_decode($result,true);
        //halt($prepay);
        //{
        //    "code": 0,
        //    "data": {
        //        "mchOrderNo": "2024041953950",
        //        "orderState": "0001",
        //        "payData": "https://opena%2F%2FCJLDo7fLo6LnzKxq5vX3b8pD78Dsboecxg%3D%3D&sign_type=RSA2&timestamp=2024-04-19+08%3A02%3A08&version=1.0",
        //        "payDataType": "payurl",
        //        "payOrderId": "20240419100183875"
        //    },
        //    "msg": "SUCCESS",
        //    "sign": "39e549cb13938b16da15c495b09f5"
        //}
        // 请求失败
        if ($prepay['code'] == '0' && $prepay['msg']=='SUCCESS') {
            $model->where("id",$order['id'])->update(["codeUrl"=> $prepay["data"]['payData']]);
            //处理业务逻辑 返回支付链接
            $pay_url = $prepay["data"]['payData'];
            header('Location:'.$pay_url);
            die();
        }else{
            //业务逻辑错误
            return AjaxReturn(0,$prepay['msg']);
        }
    }
    /**
     * 直付通
     * https://pay.dinglianshop.cn//api/Zhifutong/pay
     */
    public function pay_()
    {
        $params = [
            'mchNo'=>'M3028676240',//商户号
            'wayCode'=>'ALIPAY_H5',//ALIPAY_H5：支付宝Wap支付，ALIPAY_SDK：支付宝app支付,ALIPAY_QR：支付宝当面付
            'amount' => 2000, // 价格:单位分
            'clientIp' => request()->ip(),//用户真实ip
            'subject' => '购买商品',
            'mchOrderNo' => orderNum(),//订单号
            'body' => '9',//订单商品类型1游戏币2游戏账号3游戏什盲盒7二手者侈品8二手手表9基他
            'channelExtra'=>'',
            'version' =>  "1.0",//版本号
            'notifyUrl' => ym().'/api/notify/zhifutongpay',  // 异步通知地址
            'returnUrl' => ym().'/api/Zhifutong/returnUrl',//同步回调地址
            'reqTime' => date('Y-m-d H:i:s'),
            'expiredTime'=>'600',//订单失效时间,单位秒,默认2小时.订单在(创建时间+失效时间)后失效
        ];
        // 生成签名
        $params['sign'] = $this->makeSign($params);
        $result = $this->post('https://hwgpay.api.huaweigu.com/hwgPay/api/unifiedOrder', $params);
        $prepay = json_decode($result,true);
        //halt($prepay);
        //{
        //    "code": 0,
        //    "data": {
        //        "mchOrderNo": "2024041953950",
        //        "orderState": "0001",
        //        "payData": "https://opena%2F%2FCJLDo7fLo6LnzKxq5vX3b8pD78Dsboecxg%3D%3D&sign_type=RSA2&timestamp=2024-04-19+08%3A02%3A08&version=1.0",
        //        "payDataType": "payurl",
        //        "payOrderId": "20240419100183875"
        //    },
        //    "msg": "SUCCESS",
        //    "sign": "39e549cb13938b16da15c495b09f5"
        //}
        // 请求失败
        if ($prepay['code'] == '0' && $prepay['msg']=='SUCCESS') {
            //处理业务逻辑 返回支付链接
            $pay_url = $prepay["data"]['payData'];
            header('Location:'.$pay_url);
            die();
        }else{
            //业务逻辑错误
            return AjaxReturn(0,$prepay['msg']);
        }
    }

    /**
     * 模拟POST请求 [第二种方式, 用于兼容微信api]
     * @param $url
     * @param array $data
     * @return mixed
     * @throws \cores\exception\BaseException
     */
    protected function post($url, array $data = [])
    {
        $header = [
            'Content-Type: application/json;charset=UTF-8',
        ];
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_POST, TRUE);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
        $result = curl_exec($curl);
//        if ($result === false) {
//            throwError(curl_error($curl));
//        }
        curl_close($curl);
        return $result;
    }
    /**
     * 生成签名
     * @param array $values
     * @return string 本函数不覆盖sign成员变量，如要设置签名需要调用SetSign方法赋值
     */
    private function makeSign($values,$key)
    {
        //签名步骤一：按字典序排序参数
        ksort($values,SORT_STRING);

        $string = $this->toUrlParams($values);
        //签名步骤二：在string后加入KEY
        $string = $string . $key;
        //throwError($string);

        //签名步骤三：MD5加密
        $string = md5($string);
        //签名步骤四：所有字符转为大写
        return $string;
    }

    /**
     * 格式化参数格式化成url参数
     * @param array $values
     * @return string
     */
    private function toUrlParams(array $values)
    {
        $buff = '';
        foreach ($values as $k => $v) {
            if ($k != 'sign' && $v != '' && !is_array($v)) {
                $buff .= $k . '=' . $v . '&';
            }
        }
        return trim($buff, '&');
    }

    /**
     * 同步订单
     * 本地 http://pay.cxlaimeng.cn/api/Zhifutong/returnUrl
     *    线上 https://p.cxlaimeng.com//api/Zhifutong/returnUrl
     */
    public function returnUrl(){
        //同步跳转返回的url
        //http://pay.cxlaimeng.cn/api/Zhifutong/returnUrl?charset=UTF-8&out_trade_no=20250116122847322115386518&method=alipay.trade.wap.pay.return&total_amount=1.00&sign=GSn0MV6nIQBAL%2BLEtXktDyh2IV7%2FX5m2LN4Yo70Ij7lUOKgDjWLGnsxAtu%2BYWEISazmM3FYFpn0NwVW%2BHXbI7W29tsuTrOE7VofFUKB1LebiMnCyLRfVZD2t3IcZeZlCR2iRp2wwaHF4XTVe73XuZpPdBbwLfdznkmZ3TTza01%2BTGwfoyi21tveGPc9YEEEYadfDGXjmkRV1APY%2B0BD1i00%2B5U2xI1QuSlgVJVd81SsXyPxTyD25LeIPNebuUYuEuBi9Y03XA4oKFRzeiX9ZsLDQ4jyQ2Ix9RaAGAADf9NCNigBbPhCKrOWsWHQNcXrIf4%2FjMCfEcBuscYHH1HIKKA%3D%3D&trade_no=2025011623001456911448378546&auth_app_id=2021004190659212&version=1.0&app_id=2021004190659212&sign_type=RSA2&seller_id=2088060508880030&timestamp=2025-01-16+12%3A29%3A38
        $data = input("get.");
        //dump($data);
        $model = new ZhifutongOrder();
        $order = $model::where(["trade_no"=>$data['out_trade_no']])->find();
        if(!$order){
            return AjaxReturn(0,'订单不存在');
        }
        if($order["payment"] ){
            return view("ok",["order"=>$order]);

        }else{
            //支付失败
            return view("fail",["order"=>$order]);
        }
    }


    /**
     * 异步订单
     *
     * 本地 http://pay.cxlaimeng.cn/api/Zhifutong/zhifutongpay
     *    线上 https://p.cxlaimeng.com//api/Zhifutong/zhifutongpay
     */
    public function zhifutongpay()
    {
        $notify_ip = request()->ip();//回调异步ip
        //上线前开启
        $data =$_POST;
        db("test")->insert(["add_time"=>time(),"content"=>json_encode($data),"ip"=>$notify_ip,"explain"=>"直付通异步接收参数"]);
        //异步参数处理
        $model = new ZhifutongOrder();
        $order = $model::where(["system_order_num"=>$data['mchOrderNo']])->find();
        if(!$order){
            return AjaxReturn(0,'订单不存在');
        }
        if($order["payment"]){
            return AjaxReturn(0,'订单已支付');
        }
        $PayChannelModel = new ZhifutongChannel();
        $PayChannel = $PayChannelModel::get($order['bid']);
        if(!$PayChannel){
            return AjaxReturn(1,"支付通道不存在");
        }
        $key = $PayChannel['key'];
        $sign = $data['sign'];
        unset($data['sign']);
        $datasign = $this->makeSign($data,$key);
        if($sign == $datasign){


            //修改订单信息
            $orderData = [
                "notify_ip"=>$notify_ip,//异步回调支付ip
                "payment" => 1,  //支付
                "pay_time" => time(),
                "trade_no" => $data['payOrderId'],//第三方商户订单号
                "nonceStr" => $data['nonceStr']
            ];
            //第三方商户订单号 third_order_no
            //平台订单号  transaction_id
            $model::where(["id" => $order['id']])->update($orderData);//更改订单信息
            //支付时间统计
            $agentFianceModel = new Finance();
            $agentFianceModel->agentDayzft($order['id']);//平台日财务
            $agentFianceModel->payDaytongdaoxszft($order['id']);//通道日财务
            $agent =  Agent::get($order["agent_id"]);
            //支付回调
            $notify_data = [
                "order_num" => $order["order_num"],
                "money" => $order["money"],
                "agent_id" => $agent["id"],
                "system_order_num" => $order["system_order_num"],
                "status" => "SUCCESS", //INIT(待支付)/SUCCESS(成功)/CANCEL(已取消)/REFUND(已退款)/REFUNDING(退款中)/REFUNDFAIL(退款失败)
                "pay_time" => time(),
            ];
            $sign=createSign($notify_data,$agent["key"]);
            $notify_data['sign'] = $sign;
            if ($order["notify_url"]) {
                //异步回调给其他平台
                request_post($order["notify_url"],$notify_data);
                $model::where('id',$order['id'])->update(["return_status"=>1,"return_time"=>time()]);//修改订单状态
            }
            return "success";
            exit();

        }else{
            return AjaxReturn(0,"签名错误");
        }
    }

    /**
     * 测试异步支付
     * 本地 http://pay.cxlaimeng.cn/api/Zhifutong/ceshi
     *   线上 https://p.cxlaimeng.com//api/Zhifutong/ceshi
     */
    public function ceshi(){
        $notify_ip = request()->ip();//回调异步ip
        $json = '{"respDesc":"\u6210\u529f","amount":"1.00","payOrderId":"20250116122847322115386518","mchOrderNo":"jl116999990110332","wayCode":"ALIPAY_H5","sign":"f64e47f46d446556b619d98220392841","reqTime":"2025-01-16 12:28:47","respCode":"0000","nonceStr":"2662040335330357"}';
        $data = json_decode($json,true);
        $model = new ZhifutongOrder();
        $order = $model::where(["system_order_num"=>$data['mchOrderNo']])->find();
        if(!$order){
            return AjaxReturn(0,'订单不存在');
        }
        if($order["payment"]){
            return AjaxReturn(0,'订单已支付');
        }
        $PayChannelModel = new ZhifutongChannel();
        $PayChannel = $PayChannelModel::get($order['bid']);
        if(!$PayChannel){
            return AjaxReturn(1,"支付通道不存在");
        }
        $key = $PayChannel['key'];
        $sign = $data['sign'];
        unset($data['sign']);
        $datasign = $this->makeSign($data,$key);
        if($sign == $datasign){


            //修改订单信息
            $orderData = [
                "notify_ip"=>$notify_ip,//异步回调支付ip
                "payment" => 1,  //支付
                "pay_time" => time(),
                "trade_no" => $data['payOrderId'],//第三方商户订单号
                "nonceStr" => $data['nonceStr']
            ];
            //第三方商户订单号 third_order_no
            //平台订单号  transaction_id
            $model::where(["id" => $order['id']])->update($orderData);//更改订单信息
            //支付时间统计
            $agentFianceModel = new Finance();
            $agentFianceModel->agentDayzft($order['id']);//平台日财务
            $agentFianceModel->payDaytongdaoxszft($order['id']);//通道日财务
            $agent =  Agent::get($order["agent_id"]);
            //支付回调
            $notify_data = [
                "order_num" => $order["order_num"],
                "money" => $order["money"],
                "agent_id" => $agent["id"],
                "system_order_num" => $order["system_order_num"],
                "status" => "SUCCESS", //INIT(待支付)/SUCCESS(成功)/CANCEL(已取消)/REFUND(已退款)/REFUNDING(退款中)/REFUNDFAIL(退款失败)
                "pay_time" => time(),
            ];
            $sign=createSign($notify_data,$agent["key"]);
            $notify_data['sign'] = $sign;
            if ($order["notify_url"]) {
                //异步回调给其他平台
                request_post($order["notify_url"],$notify_data);
                $model::where('id',$order['id'])->update(["return_status"=>1,"return_time"=>time()]);//修改订单状态
            }
            return "success";
            exit();

        }else{
            return AjaxReturn(0,"签名错误");
        }
    }


}