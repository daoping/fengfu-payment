<?php
namespace app\api\controller;
use think\Controller;
use think\facade\Env;
class Efalipay extends Controller{
    //测试环境主扫接口路径
    protected $gateway = 'http://test-efps.epaylinks.cn/api/txs/pay/NativePayment';
    //测试环境单笔提现接口路径
    protected $withdrawalToCard = 'http://test-efps.epaylinks.cn/api/txs/pay/withdrawalToCard';
    //进件
    protected $apply_url = 'http://test-efps.epaylinks.cn/api/cust/SP/Merchant/apply';
    //生产环境接口路径
    //protected $gateway = 'https://efps.epaylinks.cn/api/txs/pay/NativePayment';
    //私钥文件路径

    //易票联公钥    
    //public $publicKeyFilePath = "D:\\efps.cer";
    //证书序列号
    public  $sign_no='562265003122220003';
    //证书密码
    public $password='123456';
    //编码格式
    public $charset = "UTF-8";    
    public  $signType = "RSA2";    
    //商户号
    protected $config     = array(
        'customer_code'   => '562265003122220',
        'notify_url' => 'http://www.baidu.com',
        'return_url' => 'http://www.baidu.com'
    );
    
    
    public function check() {
        if (!$this->config['customer_code'] ) {
            E("支付设置有误！");
        }
        return true;
    }
    /** 测试主扫
     * @return mixed|void
     * http://pay1.lnxinmeng.xyz/api/Efalipay/buildRequestForm
     */
    //
    public function buildRequestForm() {
        $orderNo = "123456".date('YmdHis');
        
        echo '订单号:'.$orderNo;
        echo '<br>';
        $client_ip = "127.0.0.1";
        if (getenv('HTTP_CLIENT_IP')) {
            $client_ip = getenv('HTTP_CLIENT_IP');
        } elseif (getenv('HTTP_X_FORWARDED_FOR')) {
            $client_ip = getenv('HTTP_X_FORWARDED_FOR');
        } elseif (getenv('REMOTE_ADDR')) {
            $client_ip = getenv('REMOTE_ADDR');
        } else {
            $client_ip = $_SERVER['REMOTE_ADDR'];
        }
        
        $orderInfo=array();
        $orderInfo['Id'] = $orderNo;
        $orderInfo['businessType'] = '130001';
        $orderInfo['goodsList'] = array(array('name'=>'pay','number'=>'one','amount'=>1));
        //$orderInfo = json_encode($orderInfo);
        
        $param = array(
            'outTradeNo' => $orderNo,
            'customerCode' => $this->config['customer_code'],
            'clientIp' => $client_ip,
            'orderInfo' => $orderInfo,
            'payMethod'  => 7,
            'payAmount' => 10,
            'payCurrency' => 'CNY',
            'channelType' =>'02',
            'notifyUrl' =>$this->config['notify_url'],
            'redirectUrl' =>$this->config['return_url'],
            'transactionStartTime' =>date('YmdHis'),
            'nonceStr' => 'pay'.rand(100,999),
            'version' => '3.0'
        );
        $sign = $this->sign(json_encode($param));
        
        echo '发送的参数'.json_encode($param);
        echo '<br>签名值'.$sign;
        
        $request = $this->http_post_json($this->gateway,json_encode($param),$sign);
        if($request && $request[0] == 200){
            //           $re_data = json_decode($request[1],true);
            //           if($re_data['returnCode'] == '0000'){
            //                $payurl = $re_data['codeUrl'];
            //         $sHtml="<script language='javascript' type='text/javascript'>window.location.href='{$payurl}';</script>";
            echo '<br>'.'获取到的参数：';
            //               echo $request[1];
            return $request[1];
            /*             }else{
             echo $request[1];
             exit;
             } */
            
        }else{
            print_r($request);
            exit;
        }
        exit;
        //return "";
    }

    /**
     * 绑卡
     * http://pay1.lnxinmeng.xyz/api/Efalipay/bindCard
     */
    public function bindCard(){
        $url = "http://test-efps.epaylinks.cn/api/txs/protocol/bindCard";
        $orderNo = "mELqwUeyREfyo8V646yx";
        $param = array(
            'outTradeNo' => $orderNo,
            'customerCode' => $this->config['customer_code'],
            'memberId'=>orderNum(),
            "mchtOrderNo"=>orderNum(),
            "transType"=>"01",
            "userName"=>$this->public_encrypt("王德富"),
            "phoneNum"=>$this->public_encrypt("13782521003"),
            "bankCardNo"=>$this->public_encrypt("6215581704000126948"),
            "bankCardType"=>"debit",//debit：储蓄卡 credit：信用卡
            "certificatesType"=>"01",
            "certificatesNo"=>$this->public_encrypt("410721199106170516"),
            "nonceStr"=>md5(orderNum()),
            'version' => '2.0'
        );
        dump($param);
        $sign = $this->sign(json_encode($param));

        echo '发送的参数'.json_encode($param);
        echo '<br>签名值'.$sign;

        $request = $this->http_post_json($url,json_encode($param),$sign);
        if($request && $request[0] == 200){
            dump("正确返回信息");
            halt($request);

        }else{
            print_r($request);
            exit;
        }
        exit;
        //array(13) {
        //  ["outTradeNo"] => string(20) "mELqwUeyREfyo8V646yx"
        //  ["customerCode"] => string(15) "562265003122220"
        //  ["memberId"] => string(15) "616007698613796"
        //  ["mchtOrderNo"] => string(15) "616007698613938"
        //  ["transType"] => string(2) "01"
        //  ["userName"] => string(344) "nSOXMFlBu8b9HHUz+El6mLkMYEd1UaPnu/Jzh+UnoLJWmOkAWh/I2enqfkDqfnqDioMU3ulZW+0UAJKbDfQv9Y64yHvpBw+IMQuPpmcD8Z4XKe+PlY1JlOmbDVRp0ZaH14K9vgY0dGglefO05QqpFuG3yMaYatEh+/xVKg1O0CMSpMJx5zYchgaEYZRFl8ZIUZN+7xNFvo5IXwVJ9RfXc2DbGgRn/a1aMC+O2PDvykFBNzbyDw6iG8l6lXSfXxUVjnmjT24XihovyRGBvXmTUuWepsgojimcFufnmS/sCLcIvIL7owwHYM1TkLWR+b8Bhn0XWG6hh5fwgbWUROtnzA=="
        //  ["phoneNum"] => string(344) "TSUKlyy3kD/AWnvww7AZnB/jHH8nSilnOZYHSNU3Spc54bBmgek2QW/GU9y7soc9ojZc04wYeCOwrSXLra0m0PApv+aRPWnK3mDo+k8WLrmzwCvLXQJj6kL2jEFaABKT9N1FtaPMlh4tsmHkB/hzo2n3sx3vNdExw19tFTgUWkL5ULK8cdJwGnWZRf5RFhs2kWM8U6kXf3Pq876SUaVCq7tXicEiDu32jjzd9Aq6YUecyrpnOa1OrT/tdgce0cGCiO2KuHgSPMTRqpUQNgEn8Ty11vDmygx57Dly3CBsFUyxtNVxhSxP1ZCkW0V4WJh7xMnwZCYgyT81Neh9sSqrug=="
        //  ["bankCardNo"] => string(344) "Zt5ayqiMaBaiZ58iTWxrOMXkKfJTogCKOhLPBY/zTA/BZ3/BxjPa2UO6Q9o231utmBeWZ+JQUkwUam1/JZmyap8fmmJXx29JdP+vpcHMwfqaN9nrMaM0N1mUqwgShd3qGurG0Rn95fwp6pfOChsF8vTnFcv2p2DtFm2pqYVR1m94QKLzU8lf5uGFYQIli4LlJbn1rYpvsQmXknss3g0a6Q9fCLCzlzg8qTEf9rEbeLW7hYyRRmKQSu0og3R7bN6RF3xAioKUZCk4wH+1Lz1tEVsXLFDClHVFhe9Jgu6+do3zzrizvmTDXyG6HM18lokBpQu2t5s6H0NZeTtltRVeCw=="
        //  ["bankCardType"] => string(5) "debit"
        //  ["certificatesType"] => string(2) "01"
        //  ["certificatesNo"] => string(344) "cRCwUGUcinsepvG0AMfcBWrqTAiXW2b9gzR/MTmfB0yjVH33CX2TqZOq7IlSdGhvOfo/pqaiDvJuJBwilJqEU/ZNuNEhkSTGNV6cTXhhO6ezVPnOS96V0nzZq6K4ovOHg7OWSAri/4p+CDyuPB0q9vs1GQrtSfqFH7TchKAN//m/DqzMfdtsc4NHifdoVw9+m+CqzcGGLNuiHmo6KDQXvmiNoLblIHWXMgCHJwaF5xiPylKApC48xlmJGILAUfasysluQyzkugQYm9PNZ2+/POu7bgaKQ0QeD4cevJLymlsqS3Iz3u8df3QFPFCJTf0DGDwfkathC0zp79D4WnXROA=="
        //  ["nonceStr"] => string(32) "075347892951627299f3ffccb0952cb1"
        //  ["version"] => string(3) "2.0"
        //}
        //发送的参数{"outTradeNo":"mELqwUeyREfyo8V646yx","customerCode":"562265003122220","memberId":"616007698613796","mchtOrderNo":"616007698613938","transType":"01","userName":"nSOXMFlBu8b9HHUz+El6mLkMYEd1UaPnu\/Jzh+UnoLJWmOkAWh\/I2enqfkDqfnqDioMU3ulZW+0UAJKbDfQv9Y64yHvpBw+IMQuPpmcD8Z4XKe+PlY1JlOmbDVRp0ZaH14K9vgY0dGglefO05QqpFuG3yMaYatEh+\/xVKg1O0CMSpMJx5zYchgaEYZRFl8ZIUZN+7xNFvo5IXwVJ9RfXc2DbGgRn\/a1aMC+O2PDvykFBNzbyDw6iG8l6lXSfXxUVjnmjT24XihovyRGBvXmTUuWepsgojimcFufnmS\/sCLcIvIL7owwHYM1TkLWR+b8Bhn0XWG6hh5fwgbWUROtnzA==","phoneNum":"TSUKlyy3kD\/AWnvww7AZnB\/jHH8nSilnOZYHSNU3Spc54bBmgek2QW\/GU9y7soc9ojZc04wYeCOwrSXLra0m0PApv+aRPWnK3mDo+k8WLrmzwCvLXQJj6kL2jEFaABKT9N1FtaPMlh4tsmHkB\/hzo2n3sx3vNdExw19tFTgUWkL5ULK8cdJwGnWZRf5RFhs2kWM8U6kXf3Pq876SUaVCq7tXicEiDu32jjzd9Aq6YUecyrpnOa1OrT\/tdgce0cGCiO2KuHgSPMTRqpUQNgEn8Ty11vDmygx57Dly3CBsFUyxtNVxhSxP1ZCkW0V4WJh7xMnwZCYgyT81Neh9sSqrug==","bankCardNo":"Zt5ayqiMaBaiZ58iTWxrOMXkKfJTogCKOhLPBY\/zTA\/BZ3\/BxjPa2UO6Q9o231utmBeWZ+JQUkwUam1\/JZmyap8fmmJXx29JdP+vpcHMwfqaN9nrMaM0N1mUqwgShd3qGurG0Rn95fwp6pfOChsF8vTnFcv2p2DtFm2pqYVR1m94QKLzU8lf5uGFYQIli4LlJbn1rYpvsQmXknss3g0a6Q9fCLCzlzg8qTEf9rEbeLW7hYyRRmKQSu0og3R7bN6RF3xAioKUZCk4wH+1Lz1tEVsXLFDClHVFhe9Jgu6+do3zzrizvmTDXyG6HM18lokBpQu2t5s6H0NZeTtltRVeCw==","bankCardType":"debit","certificatesType":"01","certificatesNo":"cRCwUGUcinsepvG0AMfcBWrqTAiXW2b9gzR\/MTmfB0yjVH33CX2TqZOq7IlSdGhvOfo\/pqaiDvJuJBwilJqEU\/ZNuNEhkSTGNV6cTXhhO6ezVPnOS96V0nzZq6K4ovOHg7OWSAri\/4p+CDyuPB0q9vs1GQrtSfqFH7TchKAN\/\/m\/DqzMfdtsc4NHifdoVw9+m+CqzcGGLNuiHmo6KDQXvmiNoLblIHWXMgCHJwaF5xiPylKApC48xlmJGILAUfasysluQyzkugQYm9PNZ2+\/POu7bgaKQ0QeD4cevJLymlsqS3Iz3u8df3QFPFCJTf0DGDwfkathC0zp79D4WnXROA==","nonceStr":"075347892951627299f3ffccb0952cb1","version":"2.0"}
        //签名值C5FVviAxRv/DrGQo67CnAio6h5bYMk2WknlyreiHZSkRCgXm0jKt0gJCQz3aez+bO3Z4XeyuCFRhQV6oz+AaclKDIrz3OvHRGy7vCMSMj/f5V9/1A8rmpNuZRCuuK/2IHIS0xRlvtKv4b01bL1V8KdpaFaa24qq7eVH0Gq6w46Rk1xQ3OYvKr7CxKa4A787taa0hwzElIWquf5QBEce9wEeYXH1FP8dJFu56yqxnDmbSfy46r7s7SCrHkGFN5pzFuQeqAbs3O+oZlLZzAs30z7UpYoTFnzSb/mUu9MOGDljINLaeOCtt5TnNIXa5bvM6TDxw8qvck2W6tZgY+5GlGA==
        //string(18) "正确返回信息"
        //array(2) {
        //  [0] => int(200)
        //  [1] => string(202) "{"returnCode":"0000","returnMsg":"Success","nonceStr":"b794740df2134eb4bb2aa1b848826612","customerCode":"562265003122220","smsNo":"QY202306161532493144179","memberId":"616007698613796","isOpen":"false"}"
        //}
    }

    /**
     * 确认绑卡
     * @return mixed|void
     */
    public function bindCardConfirm(){
        $url = "http://test-efps.epaylinks.cn/api/txs/protocol/bindCardConfirm";
        $orderNo = "mELqwUeyREfyo8V646yx";
        $param = array(
            'version' => '2.0',

            'customerCode' => $this->config['customer_code'],
            'smsNo'=>'QY202306161532493144179',//上个接口返回的
            'memberId'=>"616007698613796",
            "nonceStr"=>md5(orderNum()),

        );
        dump($param);
        $sign = $this->sign(json_encode($param));

        echo '发送的参数'.json_encode($param);
        echo '<br>签名值'.$sign;

        $request = $this->http_post_json($url,json_encode($param),$sign);
        if($request && $request[0] == 200){
            dump("正确返回信息");
            halt($request);

        }else{
            print_r($request);
            exit;
        }
        exit;
    }
    
    //测试单笔提现
    public function withDraw() {
        $orderNo = "tx123".date('YmdHis');
        
        echo '订单号:'.$orderNo;
        echo '<br>';
        $param = array(
            'outTradeNo' => $orderNo,
            'customerCode' => $this->config['customer_code'],
            'amount' => 10,
            'bankUserName' =>$this->public_encrypt('张三'),
            'bankCardNo' => $this->public_encrypt('6214858888883338'),
            'bankName' => '招商银行',
            'bankAccountType' =>'2',
            'payCurrency' => 'CNY',
            'notifyUrl' =>$this->config['notify_url'],
            'nonceStr' => 'pay'.rand(100,999),
        );        
        $sign = $this->sign(json_encode($param));        
        echo '发送的参数'.json_encode($param);
        echo '<br>签名值'.$sign;        
        $request = $this->http_post_json($this->withdrawalToCard,json_encode($param),$sign);
        if($request && $request[0] == 200){
            echo '<br>'.'获取到的参数：';
            return $request[1];            
        }else{
            print_r($request);
            exit;
        }
        exit;

    }
    
    //进件
    //发起进件
    public function apply(){        
        $paper = '{"certificateName":"李四","contactPhone":"13531231222","email":"test1@test.cn","lawyerCertNo":"430481198104234557","lawyerCertType":"0","merchantType":"3","openBank":"中国银行","openingLicenseAccountPhoto":"https://www.epaylinks.cn/www/wimages/epl_logo.png","settleAccount":"李四","settleAccountNo":"6214830201234567","settleAccountType":"2","settleTarget":"2"}';        
        $business = array(
            array(
                "businessCode"=>"WITHDRAW_TO_SETTMENT_DEBIT",
                "creditcardsEnabled"=>0,
                "refundEnabled"=>1,
                "refundFeePer"=>0,
                "refundFeeRate"=>0,
                "settleCycle"=>"D+0",
                "stage"=>array(
                        array(
                            "amountFrom"=>0,
                            "feePer"=>50
                        )
                    )
            ) 
        );  
        $param =array(
            'acqSpId'       =>  $this->config['customer_code'],
            'merchantName'  => "测试商户20211202",
            'acceptOrder'   => 0,
            'openAccount'   => 1,
            'paper' => $paper,
            'business' =>$business
       );
        $sign = $this->sign(json_encode($param));


        echo json_encode($param);        

       $res = $this->http_post_json($this->apply_url,json_encode($param),$sign);
        var_dump($res);
        die;
    }
  
    
    public function generateSign($params) {
        return $this->sign($this->getSignContent($params));
    }
    
    public function rsaSign($params) {
        return $this->sign($this->getSignContent($params));
    }
    
    protected function getSignContent($params) {
        ksort($params);
        
        $stringToBeSigned = "";
        $i = 0;
        foreach ($params as $k => $v) {
            
            if (false === $this->checkEmpty($v) && "@" != substr($v, 0, 1)) {
                // 转换成目标字符集
                $v = $this->characet($v, $this->charset);
                
                if ($i == 0) {
                    $stringToBeSigned .= "$k" . "=" . "$v";
                } else {
                    $stringToBeSigned .= "&" . "$k" . "=" . "$v";
                }
                
                $i++;
            }
        }
        
        unset ($k, $v);
        
        return $stringToBeSigned;
    }

    /**
     * 签名
     * @param $data
     * @return string|void
     */
    protected function sign($data) {
        $rsaPrivateKeyFilePath = Env::get("root_path")."extend/ypl/user.pfx";
        $certs = array();
        openssl_pkcs12_read(file_get_contents($rsaPrivateKeyFilePath), $certs, $this->password); //其中password为你的证书密码
        
        ($certs) or die('请检查RSA私钥配置');
        
        openssl_sign($data, $sign, $certs['pkey'],OPENSSL_ALGO_SHA256);
        
        $sign = base64_encode($sign);
        return $sign;
    }
    
    /**
     * 校验$value是否非空
     *  if not set ,return true;
     *    if is null , return true;
     **/
    protected function checkEmpty($value) {
        if (!isset($value))
            return true;
            if ($value === null)
                return true;
                if (trim($value) === "")
                    return true;
                    
                    return false;
    }
    

    public function rsaCheckV2($params, $rsaPublicKeyFilePath,$sign) {
        //$sign = $params['sign'];
        //$params['sign'] = null;
        
        return $this->verify($params, $sign, $rsaPublicKeyFilePath);
    }
    
    //使用易票联公钥验签    //返回的验签字段有中文需要加JSON_UNESCAPED_UNICODE才能验签通过
	//$data2 = json_encode($data, JSON_UNESCAPED_UNICODE);
   public function verify($data, $sign, $rsaPublicKeyFilePath) {
         $publicKeyFilePath = Env::get("root_path")."extend/ypl/efps.cer";
        //读取公钥文件
        $pubKey = file_get_contents($publicKeyFilePath);
        
        $res = openssl_get_publickey($pubKey);
        
        ($res) or die('RSA公钥错误。请检查公钥文件格式是否正确');
        //调用openssl内置方法验签，返回bool值
        
        $result = (bool)openssl_verify($data, base64_decode($sign), $res, OPENSSL_ALGO_SHA256);
        
        if(!$this->checkEmpty($publicKeyFilePath)) {
            //释放资源
            openssl_free_key($res);
        }
        
        return $result;
    }

    /**
     * 公钥加密
     * @param $data
     * @return string|void
     */
    //使用易票联公钥加密
   public function public_encrypt($data)
    {
        $publicKeyFilePath = Env::get("root_path")."extend/ypl/efps.cer";
        //读取公钥文件
        $pubKey = file_get_contents($publicKeyFilePath);
        
        $res = openssl_get_publickey($pubKey);
        
        ($res) or die('RSA公钥错误。请检查公钥文件格式是否正确');
        
        $crypttext = "";
        
        openssl_public_encrypt($data,$crypttext, $res );
        
       
        if(!$this->checkEmpty($publicKeyFilePath)) {
            //释放资源
            openssl_free_key($res);
        }
        
        return(base64_encode($crypttext));

    }
    
    /**
     * 转换字符集编码
     * @param $data
     * @param $targetCharset
     * @return string
     */
   public function characet($data, $targetCharset) {
        
        
        if (!empty($data)) {
            $fileType = $this->charset;
            if (strcasecmp($fileType, $targetCharset) != 0) {
                
                $data = mb_convert_encoding($data, $targetCharset);
                //				$data = iconv($fileType, $targetCharset.'//IGNORE', $data);
            }
        }
        
        
        return $data;
    }
    
    protected function getParam($para) {
        $arg = "";
        while (list ($key, $val) = each($para)) {
            $arg.=$key . "=" . $val . "&";
        }
        //去掉最后一个&字符
        $arg = substr($arg, 0, -1);
        return $arg;
    }
    
    /**
     * 获取远程服务器ATN结果,验证返回URL
     * @param $notify_id
     * @return
     * 验证结果集：
     * invalid命令参数不对 出现这个错误，请检测返回处理中partner和key是否为空
     * true 返回正确信息
     * false 请检查防火墙或者是服务器阻止端口问题以及验证时间是否超过一分钟
     */
    protected function getResponse2($Params) {
        $veryfy_url = $this->gateway . "?" . $Params;
        $responseTxt = $this->fsockOpen($veryfy_url);
        return $responseTxt;
    }
    
    protected function http_post_json($url, $jsonStr,$sign)
    {
        $ch = curl_init();
        $headers = array(
            'Content-Type: application/json; charset=utf-8',
            'Content-Length: ' . strlen($jsonStr),
            'x-efps-sign-no:'.$this->sign_no,
            'x-efps-sign-type:SHA256withRSA',
            'x-efps-sign:'.$sign,
            'x-efps-timestamp:'.date('YmdHis'),
        );
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonStr);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);  // 跳过检查        
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);  // 跳过检查
        //curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        
        $response = curl_exec($ch);
        
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        
        return array($httpCode, $response);
    }
    
   
}


//$efalipay = new Efalipay();
//
//echo $efalipay->buildRequestForm();
