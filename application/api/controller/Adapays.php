<?php

namespace app\api\controller;


use think\Controller;
use think\facade\Env;
//include_once  Env::get("root_path"). "extend/ldpay/umfPayService/UmfService.class.php";
include_once  Env::get("root_path"). "vendor/adapay/AdapaySdk/init.php";
class Adapays extends Controller
{

    /**
     * adapay支付
     * https://pay.lnxinmeng.xyz/api/adapays/pay?money=1
     */
    public function pay(){

        $data = input("get.");

        $arr = [
            "api_key_live"=>"api_live_5aed9160-e1fc-45e8-99e3-5ed759f3e812",
            "api_key_test"=>"api_test_8c95a4d4-be5b-46ff-a3f5-b73cda30c96e",
            "rsa_private_key"=>config("app.adapays.rsa_private_key")
        ];
        $json = json_encode($arr);

        \AdaPay\AdaPay::inits($json, "live");
        $payment = new \AdaPaySdk\Payment();

        # 支付设置
        $payment_params = array(
            'app_id'=> "app_b1589186-da22-4ea5-a076-777f77f0151f",
            'order_no'=> orderNum(),
            'pay_channel'=> 'alipay_wap',//alipay_qr  支付宝正扫   alipay_wap: 支付宝 H5 支付  alipay : 支付宝 App 支付
            'time_expire'=> date("YmdHis",time()+86400),
            'pay_amt'=> format_money($data["money"]),
            'goods_title'=> 'subject',
            'goods_desc'=> '会员到店',
            'description'=> '会员到店时间:'.time(),
            'device_id'=> ['device_id'=>"1111"],
            'notify_url'=>ym()."/pay/notify/alipayNotify",
//            "route_flag"=>"Y",//开启路由模式
//            "device_info"=>[
//                "device_ip"=>$ip,//ip
//            ]

        );


        $payment->create($payment_params);
        //halt($payment->result);
        if($payment->isError()){
            //halt($payment->result);
            //db("test")->insert(["add_time"=>time(),"content"=>"支付宝错误信息".json_encode($payment->result)]);
            return AjaxReturn(0,"支付失败,请重试");
        }else{
            $info = $payment->result;
            //halt($info);
            $url =  $info["expend"]["pay_info"];//h5模式
            //return AjaxReturn(1,"成功",["url"=>$url]);
            header("Location: {$url}");
            //确保重定向后，后续代码不会被执行
            exit;
        }
    }
}