<?php

/**
 * Created by PhpStorm.
 * User: zty
 * Date: 2017/3/22
 * Time: 15:41
 */
class DateUtils
{
    const DEFAULT_PATTERN = "YmdHms";
    const FILEDATE_PATTERM = "Ymd";

    /**
     * 获取当前时间
     */
    public static function getCurrDate($timestamp = self::DEFAULT_PATTERN) {
        return date($timestamp);
    }
    /**
     * 获取一年后时间
     * @return false|int
     */
    public static function getOneYearLater($timestamp = self::DEFAULT_PATTERN)
    {
        return  date($timestamp, strtotime("+1 year"));
    }

}