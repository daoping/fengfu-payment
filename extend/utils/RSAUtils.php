<?php

use think\facade\Env;

header('Content-type: text/html; charset=utf-8');

/**
 * Created by PhpStorm.
 * User: zty
 * Date: 2017/3/23
 * Time: 13:31
 */
class RSAUtils
{

    public $pubKey;
    public $privKey;
    public $hnaPubKey;

    function __construct()
    {
         //商户加密私钥
//        $this->privKey = file_get_contents( Env::get("root_path"). "extend/xinsheng/private_new.pem");
//        //商户加密公钥
//        $this->pubKey = file_get_contents(Env::get("root_path"). "extend/xinsheng/hnapayExpPublicKey.pem");
//        //快捷类新生公钥
//        $this->hnaPubKey = file_get_contents(Env::get("root_path"). "extend/xinsheng/hnapayExpPublicKey.pem");
        $this->privKey = "";
        $this->pubKey = "";
        $this->hnaPubKey = "";
    }

    /**
     * 生成签名
     * @param $signData
     * @return string
     * @throws Exception
     */
    public function sign($signParam,$priKey)
    {
        //$priKey = $this->privKey;
        if (!$priKey) {
            throw new Exception("Failed to read private key file！");
        }
        if (!openssl_pkey_get_private($priKey)) {
            throw new Exception("Private key is not available");
        }
        $signature = "";
        $res = openssl_get_privatekey($priKey);
        openssl_sign($signParam, $signature, $res, OPENSSL_ALGO_SHA1);
        openssl_free_key($res);
        //echo "---------------签名生成: " .bin2hex($signature) ."---------------<br />";
        return bin2hex($signature);
    }

    /**
     * 验证签名：
     * data：原文
     * signature：签名
     * 返回：签名结果，true为验签成功，false为验签失败
     */
    public function verity($signParam, $signature)
    {
        //halt(hex2bin($signature));
        $pubKey = file_get_contents(Env::get("root_path"). "extend/xinsheng/hnapayExpPublicKey.pem");
        //halt($pubKey);
        $res = openssl_pkey_get_public($pubKey);
        //halt($res);
        $result = (bool)openssl_verify($signParam, hex2bin($signature), $res);
        openssl_free_key($res);
        return $result;
    }

    /**
     * 公钥加密
     * @param $data
     * @return string
     * @throws Exception
     */
    public function encrypt($data)
    {
        $pubKey = file_get_contents(Env::get("root_path"). "extend/xinsheng/hnapayExpPublicKey.pem");
        $encrypted = "";
        if (!$pubKey) {
            throw new Exception("Failed to read public key file！");
        }
        if (!openssl_pkey_get_public($pubKey)) {
            throw new Exception("Public key is not available");
        }


        foreach (str_split($data, 117) as $chunk) {
            openssl_public_encrypt($chunk, $encryptData, $pubKey);
            $encrypted .= $encryptData;
        }
        if (!$encrypted)
            throw new Exception('Unable to encrypt data.');
        return base64_encode($encrypted);
    }

    /**
     * 私钥解密
     * @param $encryptData
     * @return string
     * @throws Exception
     */
    public function decrypt($encryptData)
    {
        $priKey = $this->privKey;
        if (!$priKey) {
            throw new Exception("Failed to read private key file！");
        }
        if (!openssl_pkey_get_private($priKey)) {
            throw new Exception("Private key is not available");
        }
        $decrypted = '';
        foreach (str_split(base64_decode($encryptData), 128) as $chunk) {
            openssl_private_decrypt($chunk, $decryptData, $priKey);
            $decrypted .= $decryptData;
        }
        if (!$decrypted)
            throw new Exception('Unable to decrypt data.');
        return $decrypted;
    }
}