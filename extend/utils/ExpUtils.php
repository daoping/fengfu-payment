<?php
use think\facade\Env;

include_once  Env::get("root_path"). "extend/utils/RSAUtils.php";

/**
 * Created by PhpStorm.
 * User: zty
 * Date: 2017/3/23
 * Time: 13:31
 */
class ExpUtils {

    /**
     * 生成签名
     * @param $tranCode
     * @param $params
     * @param $num 1支付 2支付新 3代付
     * @return string
     * @throws Exception
     */
    public static function sign($signField, $params,$num){
        $signParam = self::getStringData($signField, $params);
        //echo "---------------签名字符串: " .$signParam ."";
        $resUtils = new \RSAUtils();
        if($num == 1){
            $priKey = file_get_contents( Env::get("root_path"). "extend/xinsheng/".$params['merId']."/pay/private.pem");
        }elseif ($num == 2){
            $priKey =  file_get_contents( Env::get("root_path"). "extend/xinsheng/".$params['merId']."/newpay/private.pem");
        }else{
            $priKey =  file_get_contents( Env::get("root_path"). "extend/xinsheng/".$params['merId']."/daifu/private.pem");
        }
        return $resUtils->sign($signParam,$priKey);
    }

    public static function verify($verifyField, $params, $signature){
        $signParam = self::getStringData($verifyField, $params);
        $resUtils = new \RSAUtils();
        return $resUtils->verity($signParam, $signature);
    }

    /**
     * @param $tranCode
     * @param $params
     * @return string
     * @throws Exception
     */
    public static function encrypt($tranCode,$params){
        $expCode = ExpConstant::getExpParamByTranCode($tranCode);
        if (empty($expCode)){
            throw new Exception("TranCode coding error！");
        }
        $encryptData = self::getJsonData($expCode['encryptField'], $params);
        $resUtils = new \RSAUtils();
        return $resUtils->encrypt($encryptData);
    }

    /**
     * 私钥解密, 本demo中暂时用不到
     * @param $tranCode
     * @param $decrptData
     * @return string
     * @throws Exception
     */
    public static function decrypt($tranCode,$decryptData){

        $resUtils = new \RSAUtils();
        return $resUtils->decrypt($decryptData);
    }

    /**
     * @param $data
     * @param $params
     * @return 返回签名明文串
     * @throws Exception
     */
    private static function getStringData($data, $params) {
        $fieldString = "";
        foreach($data as $field){
            if (!isset($params[$field])){
                throw new Exception("参数无效！".$field );
                break;
            }
            $fieldString .= $field . "=[" . $params[$field] . "]";
        }
        //halt($fieldString);
        return $fieldString;
    }

    /**
     * @param $data
     * @param $params
     * @return string
     * @throws Exception
     */
    private static function getJsonData($data, $params) {
        $fieldString = "";
        foreach($data as $field){
            if (!isset($params[$field])){
                throw new Exception("参数无效！");
                break;
            }
            $fieldString[$field] = $params[$field];
        }
        return json_encode($fieldString);
    }

    public static function checkPostParam($data){
        $tranCode = $data['tranCode'];
        if(!$tranCode){
            throw new Exception("TranCode coding error！");
        }
        $expCode = ExpConstant::getExpParamByTranCode($tranCode);
        if (empty($expCode)){
            throw new Exception("TranCode coding error！");
        }
        $flag = 1;
        foreach ($expCode['submitField'] as $index){
            if(!isset($data[$index])){
                $flag = 0;
                break;
            }
        }
        return $flag;
    }

    public static function http_build_url($url_arr){
        $new_arr = [];
        $tmp_arr = explode('&', $url_arr);
        foreach ($tmp_arr as $item){
            $url_key_url = explode("=", $item);
            $new_arr[$url_key_url[0]] = $url_key_url[1];
        }
        return $new_arr;
    }

    public static function getVerifyField($verifyField, $query){
        $result = [];
        foreach ($verifyField as $key){
            if (isset($query[$key])){
                $result[$key] = $query[$key];
            }
        }
        return $result;
    }


    /**
     * POST 方式请求地址
     * @param $url 请求的地址
     * @param $data 提交的数据
     * @param bool $ssl 是否是ｈｔｔｐｓ协议
     * @return bool|mixed
     * @throws Exception
     */
    public static function post($url, $data, $ssl = FALSE) {
        //模拟提交数据函数
        $curl = curl_init(); // 启动一个CURL会话
        curl_setopt($curl, CURLOPT_URL, $url); // 要访问的地址
        if ($ssl) {
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0); // 对认证证书来源的检查
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2); // 从证书中检查SSL加密算法是否存在
            curl_setopt($curl, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1);
        }
//        curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']); // 模拟用户使用的浏览器
        curl_setopt($curl, CURLOPT_AUTOREFERER, 1); // 自动设置Referer
        curl_setopt($curl, CURLOPT_POST, 1); // 发送一个常规的Post请求
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data); // Post提交的数据包
        curl_setopt($curl, CURLOPT_TIMEOUT, 30); // 设置超时限制防止死循环
        curl_setopt($curl, CURLOPT_HEADER, 0); // 显示返回的Header区域内容
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); // 获取的信息以文件流的形式返回
        $tmpInfo = curl_exec($curl); // 执行操作
        if (curl_errno($curl)) {
            return FALSE;
        }
        curl_close($curl); // 关闭CURL会话
        return $tmpInfo; // 返回数据
    }

    /**
     * 通curl发送post请求
     * @param array $param
     * @param $postStr
     * @return mixed
     */
    public static function doCurlPost(array $param, $postStr, $url = '') {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 7); //Timeout after 7 seconds
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); //Return data instead printing directly in Browser
        curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded; charset=UTF-8', 'Connection: Keep-Alive'));
        curl_setopt($curl, CURLOPT_POST, count($param) + 1); //number of parameters sent
        curl_setopt($curl, CURLOPT_POSTFIELDS, $postStr); //parameters data
        $result = curl_exec($curl);
        curl_close($curl);
        return $result;
    }



}
