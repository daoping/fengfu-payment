<?php
namespace JytPay\Client_ONEPAY;
use think\facade\Env;
/**
 * @author liyabin
 * Class Config
 * @package JytPay\Client
 */
class Config{

//    // 商户测试服务器地址
//    public $url='https://onepay.jytpay.com/onePayService/onePay.do';
//
//	// 测试商户号（可替换成自己入网的商户号）
//    public $merchant_id='473071040006';
//
//    // 自签证书：pem格式密钥文件
//    public $cer_path=ROOT_PATH.'extend/JytPay/certs/JytPayPublicKey.pem'; // 平台公钥文件
//    public $pfx_path=ROOT_PATH.'extend/JytPay/certs/rsa_private_key_2048.pem'; // 商户私钥文件
//
//    // 使用三方证书的私钥（目前未用到）
//    public $pfx_password = 'password';
// 商户测试服务器地址
    public $url='https://test.jytpay.com/onePayService/onePay.do';

    // 测试商户号（可替换成自己入网的商户号）
    public $merchant_id='290071040001';

    // 自签证书：pem格式密钥文件
    ///Users/liudaoping/www/daochang-jinyuntong-payment/extend\JytPay\certs\290071040001_jyt_pub.pem
    public $cer_path=ROOT_PATH.'extend/JytPay/certs/290071040001_jyt_pub.pem'; // 平台公钥文件
    public $pfx_path=ROOT_PATH.'extend/JytPay/certs/290071040001_mer_pri.pem'; // 商户私钥文件

    // 使用三方证书的私钥（目前未用到）
    public $pfx_password = 'password';
}