<?php
namespace JytPay\DaiFu;

/**
 * @author liyabin
 * Class Config
 * @package JytPay\Client
 */
class Config{

        // 商户测试服务器地址
    public $url='https://agentpay.jytpay.com/JytAgentPay/tranCenter/encXmlReq.do';

	// 测试商户号（可替换成自己入网的商户号）
    public $merchant_id='473071040007';

    // 自签证书：pem格式密钥文件
    public $cer_path=ROOT_PATH.'extend/JytPay/certs/JytPayPublicKey.pem'; // 平台公钥文件
    public $pfx_path=ROOT_PATH.'extend/JytPay/certs/rsa_private_key_2048.pem'; // 商户私钥文件

    // 使用三方证书的私钥（目前未用到）
    public $pfx_password = 'password';

//    // 商户测试服务器地址
//    public $url='https://test.jytpay.com/JytAgentPay/tranCenter/encXmlReq.do';
//
//    // 测试商户号（可替换成自己入网的商户号）
//    public $merchant_id='261058120001';
//
//    // 自签证书：pem格式密钥文件
//    public $cer_path=ROOT_PATH.'extend/Jytfy/certs/261058120001_plat_pub_test.pem'; // 平台公钥文件
//    public $pfx_path=ROOT_PATH.'extend/Jytfy/certs/261058120001_mer_pri_test.pem'; // 商户私钥文件
//
//    // 使用三方证书的私钥（目前未用到）
//    public $pfx_password = 'password';
}