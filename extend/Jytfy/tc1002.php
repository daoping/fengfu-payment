<?php
/**
 * Created by PhpStorm.
 * User: yabin
 * Date: 2021/9/23
 * Time: 16:53
 */
namespace JytPay\DaiFu;

include 'JytJsonClient.php';

header("Content-Type:text/html;   charset=utf-8");
ini_set("display_errors", "On");
error_reporting(E_ALL | E_STRICT);

$client = new JytJsonClient;
$client->init();

// 点击提交按钮后才执行
if (!empty($_POST['tc1002_test'])) {
        // 报文头信息
        $data['head']['version']='1.0.0';
        $data['head']['tranType']='01';
        $data['head']['merchantId']= $client->config->merchant_id;
      	$data['head']['tranDate']=date('Ymd',time());
        $data['head']['tranTime']=date('His',time());
        $data['head']['tranFlowid']= $client->config->merchant_id . date('YmdHis',time()) . substr(rand(),4);
        $data['head']['tranCode']= 'TC1002';
        // 报文体
		$data['body']['bankName'] = $_POST["bankName"];
		$data['body']['accountNo'] = $_POST["accountNo"]; // 测试：卡尾号1失败，2处理中，其它成功
		$data['body']['accountName'] = $_POST["accountName"];
		$data['body']['accountType'] = $_POST["accountType"];
		$data['body']['tranAmt'] = $_POST["tranAmt"];
		$data['body']['currency'] = $_POST["currency"];
		$data['body']['bsnCode'] = $_POST["bsnCode"];
		$data['body']['remark'] = $_POST["remark"];
        $res = $client->sendReq($data);

        echo $res;
}