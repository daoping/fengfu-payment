<!DOCTYPE html>
<html>
<head>
    <title>金运通代付测试页面</title>
    <script type="text/javascript" src="jquery.min.js"></script>
</head>
<style>
    .editor-label {
        margin: 0px 0 0 0;
        height: 33px;
        display: inline-block;
        width: 600px;
        min-width: 600px;
        position: relative;
    }
    .editor-label label {
        width: 120px;
        font-size: 14px;
        line-height: 30px;
        display: inline-block;
        text-align: right;
    }
    .editor-label input[type='text'], .editor-label select, .editor-box input[type='text'], .editor-box select {
        height: 23px;
        border: 1px solid #ddd;
        padding-left: 10px;
        font-size: 13px;
        border-radius: 3px;
        width: 200px;
    }
</style>
<body>
<form action="" method="post" id="form" class="editor-label">
    <br>
    <label></label>
    <input type="text" name="tc1002_test" value="点击【代付】按钮发起测试"/>
    <br>
    <label>银行名称：</label>
    <input type="text" name="bankName" value="中国建设银行" placeholder="请输入银行名称（如：中国银行）"/></td>
    <br>
    <label>银行账号：</label>
    <input type="text" name="accountNo" value="6200601234567892227" placeholder="请输入银行账号（银行卡号）"/></td>
    <br>
    <label>银行账户名：</label>
    <input type="text" name="accountName" value="李四1号" placeholder="请输入银行账户名（持卡人姓名）"/></td>
    <br>
    <label>账户类型：</label>
    <select name = "accountType">
        <option value="00" selected="selected">对私</option>
        <option value="01">对公</option>
    </select>
    <br>
    <label>交易金额：</label>
    <input type="text" name="tranAmt" value="5.00" placeholder="请输入交易金额"/></td>
    <br>
    <label>币种：</label>
    <input type="text" name="currency" value="CNY" placeholder="人民币：CNY" readonly="readonly"/></td>
    <br>
    <label>业务类型代码：</label>
    <select name = "bsnCode">
        <option value="00101">卡验证</option>
        <option value="00600">保险理赔</option>
        <option value="00601">保险分红</option>
        <option value="05100">代发佣金</option>
        <option value="05101">代发工资</option>
        <option value="05102">代发奖金</option>
        <option value="05103">代发养老金
        <option value="09000">基金赎回</option>
        <option value="09001">基金分红</option>
        <option value="09100" selected="selected">汇款</option>
        <option value="09200">商户退款</option>
        <option value="09300">信用卡还款
        <option value="09400">虚拟账户取现
        <option value="09500">货款</option>
        <option value="09501">退款</option>
    </select>
    <br>
    <label>摘要信息：</label>
    <input type="text" name="remark" value="我已经打款了啊，不要说没有打" placeholder="" readonly="readonly"/></td>
    <br>
    <label></label>
    <input type="button" name="button" value="代付" id="button"/>
</form>
</body>
<script>
    $(function() {
        $('#button').click(function() {
            $.ajax({
                type: 'post',
                url: 'tc1002.php',
                data: $("#form").serialize(),
                dataType : 'json',
                success: function(data) {
                    if (data.body) {
                        if (data.body.tranState == '01') {
                            alert(data.head.respDesc);
                            setTimeout(function() {
                                // 刷新页面
                                location.reload();
                            }, 1000);
                        } else {
                            alert(data.head.respDesc);
                        }
                    } else {
                        alert(data.head.respDesc);
                    }
                }
            });
        });
    });
</script>
</html>