<?php

// cli 模式执行 目录改为文件所在目录
chdir(dirname(__FILE__));

require_once("./UmfHttp.class.php");
require_once("../api/common.php");
require_once("../api/mer2Plat.php");

class UmfService {

    //当前实例化的商户号
    public static $merId = array();
    //公钥文件路径
    //public static $publicKeyPath = "";
    //私钥文件路径
    public static $privateKeyPath = array();

    /**
     * http://php.net/manual/zh/language.oop5.decon.php
     * the easiest way to use and understand multiple constructors:
     */
    public function __construct(){
        $a = func_get_args();
        $i = func_num_args();
        if (method_exists($this,$f='__construct'.$i)) {
            call_user_func_array(array($this,$f),$a);
        }
    }

    function __construct1($a1){
        echo('__construct with 1 param called: '.$a1.PHP_EOL);
    }

    function __construct3($a1,$a2,$a3){
        echo('__construct with 3 params called: '.$a1.','.$a2.','.$a3.PHP_EOL);
    }

    //构造方法
    //public function __construct($merId,$publicKeyPath,$privateKeyPath){
    public function __construct2($merId,$privateKeyPath){
        $mer_id = StringUtil::trim($merId);
        //$public_key_path = StringUtil::trim($publicKeyPath);
        $private_key_path = StringUtil::trim($privateKeyPath);
        if(empty($mer_id)){
            die("[UMF SDK] 实例化 UmfService 请传入商户号");
        }
        //if(empty($public_key_path)){
        //    die("[UMF SDK] 实例化 UmfService 请传入公钥文件绝对路径");
        //}
        if(empty($private_key_path)){
            die("[UMF SDK] 实例化 UmfService 请传入私钥文件绝对路径");
        }
        self::$merId[$mer_id] = $mer_id;
        //self::$publicKeyPath = $public_key_path;
        self::$privateKeyPath[$mer_id] = $private_key_path;
    }

    public static function addMerPrivateCertMap($merId,$privateKeyPath){
        $mer_id = StringUtil::trim($merId);
        $private_key_path = StringUtil::trim($privateKeyPath);
        if(empty($mer_id)){
            die("[UMF SDK] 添加私钥 请传入商户号");
        }
        if(empty($private_key_path)){
            die("[UMF SDK] 添加私钥 请传入私钥文件绝对路径");
        }
        self::$merId[$mer_id] = $mer_id;
        self::$privateKeyPath[$mer_id] = $private_key_path;
    }


    //------------------------------------------------------------
    // Web收银台
    //------------------------------------------------------------
    /**
     * Web收银台—生成get后的请求参数，商户只需要拼接URL进行get请求即可
     */
    public function WebFrontPagePayMap($paramsArr=array()){
        $map = self::publicParams();
        // 业务参数处理
        foreach($paramsArr as $key => $value){
            $map->put($key,$value);
        }
        // 公共参数处理
        $map->put("service","req_front_page_pay");
        $reqDataPost = MerToPlat::makeRequestDataByGet($map);
        $get_url = $reqDataPost->getUrl();
        $log = new Logger();
        $log->logInfo("[UMF SDK] Web收银台 请求 service = req_front_page_pay 生成的getUrl = ".$get_url);
        return $get_url;
    }



    //------------------------------------------------------------
    // H5收银台
    //------------------------------------------------------------
    /**
     * H5收银台—生成get后的请求参数，商户只需要拼接URL进行get请求即可
     */
    public function H5FrontPageMap($paramsArr=array()){
        $map = self::publicParams();
        // 业务参数处理
        foreach($paramsArr as $key => $value){
            $map->put($key,$value);
        }
        // 公共参数处理
        $map->put("service","pay_req_h5_frontpage");
        $reqDataPost = MerToPlat::makeRequestDataByGet($map);
        $get_url = $reqDataPost->getUrl();
        $log = new Logger();
        $log->logInfo("[UMF SDK] H5收银台 请求 service = pay_req_h5_frontpage 生成的getUrl = ".$get_url);
        return $get_url;
    }



    //------------------------------------------------------------
    // 公众号支付
    //------------------------------------------------------------
    /**
     * 公众号支付–生成get后的请求参数，商户只需要拼接URL进行get请求即可
     */
    public function PublicPaymentMap($paramsArr=array()){
        $map = self::publicParams();
        // 业务参数处理
        foreach($paramsArr as $key => $value){
            $map->put($key,$value);
        }
        // 公共参数处理
        $map->put("service","publicnumber_and_verticalcode");
        $reqDataPost = MerToPlat::makeRequestDataByGet($map);
        $get_url = $reqDataPost->getUrl();
        $log = new Logger();
        $log->logInfo("[UMF SDK] 公众号支付 请求 service = publicnumber_and_verticalcode 生成的getUrl = ".$get_url);
        return $get_url;
    }



    //------------------------------------------------------------
    // 收款 : 网银直连
    //------------------------------------------------------------
    /**
     * 网银直连SDK提供请求的最终URL生成，商户可以直接加载URL调起对应的支付方式
     */
    public function pBankDirectMap($paramsArr=array()){
        $map = self::publicParams();
        // 业务参数处理
        foreach($paramsArr as $key => $value){
            $map->put($key,$value);
        }
        // 公共参数处理
        $map->put("service","req_front_page_pay");
        $map->put("interface_type","02");
        $reqDataPost = MerToPlat::makeRequestDataByGet($map);
        $get_url = $reqDataPost->getUrl();
        $log = new Logger();
        $log->logInfo("[UMF SDK] 网银直连 请求 service = req_front_page_pay 生成的getUrl = ".$get_url);
        return $get_url;
    }



    //------------------------------------------------------------
    // 收款 : 借记卡直连
    //------------------------------------------------------------
    /**
     * 收款—借记卡直连
     */
    public function debitDirectMap($paramsArr=array()){
        $map = self::publicParams();
        // 业务参数处理
        foreach($paramsArr as $key => $value){
            $map->put($key,$value);
        }
        // 公共参数处理
        $map->put("service","debit_direct_pay");
        $map->put("pay_type","DEBITCARD");
        $reqDataPost = MerToPlat::makeRequestDataByPost($map);
        $post_url = $reqDataPost->getUrl();
        $mapfield = $reqDataPost->getField();
        // response
        $http = new UmfHttp();
        return $http->httpPost($post_url, $mapfield);
    }



    //------------------------------------------------------------
    // 收款 : 借记卡直连
    //------------------------------------------------------------
    /**
     * 收款—信用卡直连
     */
    public function creditDirectMap($paramsArr=array()){
        $map = self::publicParams();
        // 业务参数处理
        foreach($paramsArr as $key => $value){
            $map->put($key,$value);
        }
        // 公共参数处理
        $map->put("service","credit_direct_pay");
        $map->put("pay_type","CREDITCARD");
        $reqDataPost = MerToPlat::makeRequestDataByPost($map);
        $post_url = $reqDataPost->getUrl();
        $mapfield = $reqDataPost->getField();
        // response
        $http = new UmfHttp();
        return $http->httpPost($post_url, $mapfield);
    }


    //------------------------------------------------------------
    // 收款 : 扫码支付
    //------------------------------------------------------------
    /**
     * 主扫支付
     */
    public function activeScanPaymentMap($paramsArr=array()){
        $map = self::publicParams();
        // 业务参数处理
        foreach($paramsArr as $key => $value){
            $map->put($key,$value);
        }
        // 公共参数处理
        $map->put("service","active_scancode_order");

        $reqDataPost = MerToPlat::makeRequestDataByPost($map);
        $post_url = $reqDataPost->getUrl();
        //halt($post_url);
        //$post_url = "http://pay.soopay.net/spay/pay/payservice.do";
        $mapfield = $reqDataPost->getField();

        // response
        $http = new UmfHttp();
        $response = $http->httpPost($post_url,$mapfield);
        $res_activeScanPayment = array();
        $res_activeScanPayment['mer_id'] = $response['mer_id'];
        $res_activeScanPayment['token'] = $response['token'];
        $res_activeScanPayment['order_id'] = $response['order_id'];
        $res_activeScanPayment['mer_date'] = $response['mer_date'];
        $res_activeScanPayment['trade_state'] = $response['trade_state'];
        $res_activeScanPayment['trade_no'] = $response['trade_no'];
        $res_activeScanPayment['ret_code'] = $response['ret_code'];
        $res_activeScanPayment['ret_msg'] = $response['ret_msg'];
        $res_activeScanPayment['bank_payurl'] = $response['bank_payurl'];
        return $res_activeScanPayment;
    }



    public function activeNewScanPaymentMap($paramsArr=array()){
        $map = self::publicParams();
        // 业务参数处理
        foreach($paramsArr as $key => $value){
            $map->put($key,$value);
        }
        // 公共参数处理
        $map->put("service","active_scancode_order_new");

        $reqDataPost = MerToPlat::makeRequestDataByPost($map);
        $post_url = $reqDataPost->getUrl();
        $mapfield = $reqDataPost->getField();

        // response
        $http = new UmfHttp();
        $response = $http->httpPost($post_url,$mapfield);
        $res_activeNewScanPayment = array();
        $res_activeNewScanPayment['mer_id'] = $response['mer_id'];
        $res_activeNewScanPayment['token'] = $response['token'];
        $res_activeNewScanPayment['order_id'] = $response['order_id'];
        $res_activeNewScanPayment['mer_date'] = $response['mer_date'];
        $res_activeNewScanPayment['trade_state'] = $response['trade_state'];
        $res_activeNewScanPayment['trade_no'] = $response['trade_no'];
        $res_activeNewScanPayment['ret_code'] = $response['ret_code'];
        $res_activeNewScanPayment['ret_msg'] = $response['ret_msg'];
        $res_activeNewScanPayment['bank_payurl'] = $response['bank_payurl'];
        return $res_activeNewScanPayment;
    }


    /**
     * H5直连-范围URL地址
     */
    public function H5AutoConnectMap($paramsArr=array()){
        $map = self::publicParams();
        // 业务参数处理
        foreach ($paramsArr as $key => $value) {
            $map->put($key, $value);
        }
        // 公共参数处理
        $map->put("service", "active_scancode_order_new");
        $map->put("auto_pay", "TRUE");
        $reqDataGet = MerToPlat::makeRequestDataByGet($map);
        //halt($reqDataGet);
        $get_url = $reqDataGet->getUrl();
        $log = new Logger();
        $log->logInfo("[UMF SDK] 公众号支付 请求 service = active_scancode_order_new 生成的getUrl = ".$get_url);
        return $get_url;
    }

    /**
     * H5直连-返回请求参数
     */
    public function H5ConnectMap($paramsArr=array()){
        $map = self::publicParams();
        // 业务参数处理
        foreach ($paramsArr as $key => $value) {
            $map->put($key, $value);
        }
        // 公共参数处理
        $map->put("service", "active_scancode_order_new");

        $reqDataPost = MerToPlat::makeRequestDataByPost($map);
        $post_url = $reqDataPost->getUrl();
        $mapfield = $reqDataPost->getField();
        // response
        $http = new UmfHttp();
        $response = $http->httpPost($post_url, $mapfield);
        return $response;
    }



    /**
     * 被扫支付
     */
    public function passiveScanPaymentMap($paramsArr=array()){
        $map = self::publicParams();
        // 业务参数处理
        foreach($paramsArr as $key => $value){
            $map->put($key,$value);
        }
        // 公共参数处理
        $map->put("service","passive_scancode_pay");

        $reqDataPost = MerToPlat::makeRequestDataByPost($map);
        $post_url = $reqDataPost->getUrl();
        $mapfield = $reqDataPost->getField();

        // response
        $http = new UmfHttp();
        $response = $http->httpPost($post_url,$mapfield);
        $res_passiveScanPayment = array();
        $res_passiveScanPayment['mer_id'] = $response['mer_id'];
        $res_passiveScanPayment['token'] = $response['token'];
        $res_passiveScanPayment['order_id'] = $response['order_id'];
        $res_passiveScanPayment['mer_date'] = $response['mer_date'];
        $res_passiveScanPayment['trade_state'] = $response['trade_state'];
        $res_passiveScanPayment['trade_no'] = $response['trade_no'];
        $res_passiveScanPayment['ret_code'] = $response['ret_code'];
        $res_passiveScanPayment['ret_msg'] = $response['ret_msg'];
        return $res_passiveScanPayment;
    }


    //------------------------------------------------------------
    // 收款 : 快捷支付
    //------------------------------------------------------------
    /**
     * 收款---快捷支付下单方法
     */
    public function quickPayOrderMap($paramsArr=array()){
        $map = self::publicParams();
        // 业务参数处理
        foreach($paramsArr as $key => $value){
            $map->put($key,$value);
        }
        // 公共参数处理
        $map->put("service","apply_pay_shortcut");

        $reqDataPost = MerToPlat::makeRequestDataByPost($map);
        $post_url = $reqDataPost->getUrl();
        $mapfield = $reqDataPost->getField();

        // response
        $http = new UmfHttp();
        $response = $http->httpPost($post_url,$mapfield);
        $res_quickPayOrder = array();
        $res_quickPayOrder['mer_id'] = $response['mer_id'];
        $res_quickPayOrder['order_id'] = $response['order_id'];
        $res_quickPayOrder['mer_date'] = $response['mer_date'];
        $res_quickPayOrder['ret_code'] = $response['ret_code'];
        $res_quickPayOrder['trade_state'] = $response['trade_state'];
        $res_quickPayOrder['trade_no'] = $response['trade_no'];
        $res_quickPayOrder['payElements'] = $response['payElements'];
        $res_quickPayOrder['ret_msg'] = $response['ret_msg'];

        return $res_quickPayOrder;
    }


    /**
     * 收款---快捷支付向平台获取短信验证码方法
     */
    public function quickGetMessageMap($paramsArr=array()){
        $map = self::publicParams();
        // 业务参数处理
        foreach($paramsArr as $key => $value){
            $map->put($key,$value);
        }
        // 公共参数处理
        $map->put("service","sms_req_shortcut");

        $reqDataPost = MerToPlat::makeRequestDataByPost($map);
        $post_url = $reqDataPost->getUrl();
        $mapfield = $reqDataPost->getField();

        // response
        $http = new UmfHttp();
        $response = $http->httpPost($post_url,$mapfield);
        $res_quickGetMessageMap = array();
        $res_quickGetMessageMap['mer_id'] = $response['mer_id'];
        $res_quickGetMessageMap['ret_code'] = $response['ret_code'];
        $res_quickGetMessageMap['ret_msg'] = $response['ret_msg'];
        return $res_quickGetMessageMap;
    }

    /**
     * 收款---快捷支付确认支付
     */
    public function quickPayConfirmMap($paramsArr=array()){
        $map = self::publicParams();
        // 业务参数处理
        foreach($paramsArr as $key => $value){
            $map->put($key,$value);
        }
        // 公共参数处理
        $map->put("service","confirm_pay_shortcut");

        $reqDataPost = MerToPlat::makeRequestDataByPost($map);
        $post_url = $reqDataPost->getUrl();
        $mapfield = $reqDataPost->getField();

        // response
        $http = new UmfHttp();
        $response = $http->httpPost($post_url,$mapfield);
        $res_quickPayConfirmMap = array();
        $res_quickPayConfirmMap['mer_id'] = $response['mer_id'];
        $res_quickPayConfirmMap['order_id'] = $response['order_id'];
        $res_quickPayConfirmMap['mer_date'] = $response['mer_date'];
        $res_quickPayConfirmMap['amount'] = $response['amount'];
        $res_quickPayConfirmMap['amt_type'] = $response['amt_type'];
        $res_quickPayConfirmMap['ret_code'] = $response['ret_code'];
        $res_quickPayConfirmMap['settle_date'] = $response['settle_date'];
        $res_quickPayConfirmMap['mer_priv'] = $response['mer_priv'];
        $res_quickPayConfirmMap['trade_state'] = $response['trade_state'];
        $res_quickPayConfirmMap['pay_date'] = $response['pay_date'];
        $res_quickPayConfirmMap['ret_msg'] = $response['ret_msg'];
        $res_quickPayConfirmMap['split_data_result'] = $response['split_data_result'];
        $res_quickPayConfirmMap['usr_busi_agreement_id'] = $response['usr_busi_agreement_id'];
        $res_quickPayConfirmMap['usr_pay_agreement_id'] = $response['usr_pay_agreement_id'];
        $res_quickPayConfirmMap['gate_id'] = $response['gate_id'];
        $res_quickPayConfirmMap['last_four_cardid'] = $response['last_four_cardid'];

        return $res_quickPayConfirmMap;
    }



    /**
     * 收款---快捷支付获取银行卡列表
     */
    public function quickQuerybankSupportMap($paramsArr=array()){
        $map = self::publicParams();
        // 业务参数处理
        foreach($paramsArr as $key => $value){
            $map->put($key,$value);
        }
        // 公共参数处理
        $map->put("service","query_mer_bank_shortcut");

        $reqDataPost = MerToPlat::makeRequestDataByPost($map);
        $post_url = $reqDataPost->getUrl();
        $mapfield = $reqDataPost->getField();

        // response
        $http = new UmfHttp();
        $response = $http->httpPost($post_url,$mapfield);
        $res_quickQuerybankSupportMap = array();
        $res_quickQuerybankSupportMap['mer_id'] = $response['mer_id'];
        $res_quickQuerybankSupportMap['ret_code'] = $response['ret_code'];
        $res_quickQuerybankSupportMap['ret_msg'] = $response['ret_msg'];
        $res_quickQuerybankSupportMap['mer_bank_list'] = $response['mer_bank_list'];
        return $res_quickQuerybankSupportMap;
    }



    /**
     * 收款---快捷支付解约
     */
    public function quickCancelSurrenderMap($paramsArr=array()){
        $map = self::publicParams();
        // 业务参数处理
        foreach($paramsArr as $key => $value){
            $map->put($key,$value);
        }
        // 公共参数处理
        $map->put("service","unbind_mercust_protocol_shortcut");

        $reqDataPost = MerToPlat::makeRequestDataByPost($map);
        $post_url = $reqDataPost->getUrl();
        $mapfield = $reqDataPost->getField();

        // response
        $http = new UmfHttp();
        $response = $http->httpPost($post_url,$mapfield);
        $res_quickCancelSurrenderMap = array();
        $res_quickCancelSurrenderMap['mer_id'] = $response['mer_id'];
        $res_quickCancelSurrenderMap['ret_code'] = $response['ret_code'];
        $res_quickCancelSurrenderMap['ret_msg'] = $response['ret_msg'];
        return $res_quickCancelSurrenderMap;
    }



    //------------------------------------------------------------
    // 订单查询
    //------------------------------------------------------------
    /**
     * 订单查询---查询历史订单方法
     */
    public function queryhistoryOrderMap($paramsArr=array()){
        $map = self::publicParams();
        // 业务参数处理
        foreach($paramsArr as $key => $value){
            $map->put($key,$value);
        }
        // 公共参数处理
        $map->put("service","mer_order_info_query");

        $reqDataPost = MerToPlat::makeRequestDataByPost($map);
        $post_url = $reqDataPost->getUrl();
        $mapfield = $reqDataPost->getField();

        // response
        $http = new UmfHttp();
        $response = $http->httpPost($post_url,$mapfield);
        $res_queryhistoryOrderMap = array();
        $res_queryhistoryOrderMap['mer_id'] = $response['mer_id'];
        $res_queryhistoryOrderMap['ret_code'] = $response['ret_code'];
        $res_queryhistoryOrderMap['ret_msg'] = $response['ret_msg'];
        $res_queryhistoryOrderMap['order_id'] = $response['order_id'];
        $res_queryhistoryOrderMap['mer_date'] = $response['mer_date'];
        $res_queryhistoryOrderMap['trade_no'] = $response['trade_no'];
        $res_queryhistoryOrderMap['pay_date'] = $response['pay_date'];
        $res_queryhistoryOrderMap['amount'] = $response['amount'];
        $res_queryhistoryOrderMap['amt_type'] = $response['amt_type'];
        $res_queryhistoryOrderMap['pay_type'] = $response['pay_type'];
        $res_queryhistoryOrderMap['media_id'] = $response['media_id'];
        $res_queryhistoryOrderMap['media_type'] = $response['media_type'];
        $res_queryhistoryOrderMap['gate_id'] = $response['gate_id'];
        $res_queryhistoryOrderMap['trade_state'] = $response['trade_state'];
        $res_queryhistoryOrderMap['settle_date'] = $response['settle_date'];
        $res_queryhistoryOrderMap['bank_check_state'] = $response['bank_check_state'];
        $res_queryhistoryOrderMap['mer_priv'] = $response['mer_priv'];
        $res_queryhistoryOrderMap['user_ip'] = $response['user_ip'];
        $res_queryhistoryOrderMap['expire_time'] = $response['expire_time'];
        $res_queryhistoryOrderMap['pay_seq'] = $response['pay_seq'];
        $res_queryhistoryOrderMap['error_code'] = $response['error_code'];
        $res_queryhistoryOrderMap['product_id'] = $response['product_id'];
        $res_queryhistoryOrderMap['refund_amt'] = $response['refund_amt'];
        $res_queryhistoryOrderMap['split_query_result'] = $response['split_query_result'];

        return $res_queryhistoryOrderMap;
    }



    /**
     * 订单查询---查询当天订单状态
     */
    public function querytodayOrderMap($paramsArr=array()){
        $map = self::publicParams();
        // 业务参数处理
        foreach($paramsArr as $key => $value){
            $map->put($key,$value);
        }
        // 公共参数处理
        $map->put("service","query_order");

        $reqDataPost = MerToPlat::makeRequestDataByPost($map);
        $post_url = $reqDataPost->getUrl();
        $mapfield = $reqDataPost->getField();

        // response
        $http = new UmfHttp();
        $response = $http->httpPost($post_url,$mapfield);
        $res_querytodayOrderMap = array();
        $res_querytodayOrderMap['mer_id'] = $response['mer_id'];
        $res_querytodayOrderMap['ret_code'] = $response['ret_code'];
        $res_querytodayOrderMap['ret_msg'] = $response['ret_msg'];
        $res_querytodayOrderMap['order_id'] = $response['order_id'];
        $res_querytodayOrderMap['mer_date'] = $response['mer_date'];
        $res_querytodayOrderMap['trade_no'] = $response['trade_no'];
        $res_querytodayOrderMap['pay_date'] = $response['pay_date'];
        $res_querytodayOrderMap['amount'] = $response['amount'];
        $res_querytodayOrderMap['amt_type'] = $response['amt_type'];
        $res_querytodayOrderMap['pay_type'] = $response['pay_type'];
        $res_querytodayOrderMap['media_id'] = $response['media_id'];
        $res_querytodayOrderMap['media_type'] = $response['media_type'];
        $res_querytodayOrderMap['gate_id'] = $response['gate_id'];
        $res_querytodayOrderMap['trade_state'] = $response['trade_state'];
        $res_querytodayOrderMap['settle_date'] = $response['settle_date'];
        $res_querytodayOrderMap['bank_check_state'] = $response['bank_check_state'];
        $res_querytodayOrderMap['mer_priv'] = $response['mer_priv'];
        $res_querytodayOrderMap['user_ip'] = $response['user_ip'];
        $res_querytodayOrderMap['expire_time'] = $response['expire_time'];
        $res_querytodayOrderMap['pay_seq'] = $response['pay_seq'];
        $res_querytodayOrderMap['error_code'] = $response['error_code'];
        $res_querytodayOrderMap['product_id'] = $response['product_id'];
        $res_querytodayOrderMap['refund_amt'] = $response['refund_amt'];
        $res_querytodayOrderMap['split_query_result'] = $response['split_query_result'];
        return $res_querytodayOrderMap;
    }



    //------------------------------------------------------------
    // 撤销
    //------------------------------------------------------------
    /**
     * 撤销---撤销方法
     */
    public function cancelTradeMap($paramsArr=array()){
        $map = self::publicParams();
        // 业务参数处理
        foreach($paramsArr as $key => $value){
            $map->put($key,$value);
        }
        // 公共参数处理
        $map->put("service","mer_cancel");

        $reqDataPost = MerToPlat::makeRequestDataByPost($map);
        $post_url = $reqDataPost->getUrl();
        $mapfield = $reqDataPost->getField();

        // response
        $http = new UmfHttp();
        $response = $http->httpPost($post_url,$mapfield);
        $res_cancelTradeMap = array();
        $res_cancelTradeMap['mer_id'] = $response['mer_id'];
        $res_cancelTradeMap['order_id'] = $response['order_id'];
        $res_cancelTradeMap['mer_date'] = $response['mer_date'];
        $res_cancelTradeMap['trade_no'] = $response['trade_no'];
        $res_cancelTradeMap['trade_state'] = $response['trade_state'];
        $res_cancelTradeMap['ret_code'] = $response['ret_code'];
        $res_cancelTradeMap['ret_msg'] = $response['ret_msg'];

        return $res_cancelTradeMap;
    }




    //------------------------------------------------------------
    // 退款
    //------------------------------------------------------------
    /**
     * 退款---普通退款方法
     */
    public function generalRefundMap($paramsArr=array()){
        $map = self::publicParams();
        // 业务参数处理
        foreach($paramsArr as $key => $value){
            $map->put($key,$value);
        }
        // 公共参数处理
        $map->put("service","mer_refund");

        $reqDataPost = MerToPlat::makeRequestDataByPost($map);
        $post_url = $reqDataPost->getUrl();
        $mapfield = $reqDataPost->getField();

        // response
        $http = new UmfHttp();
        $response = $http->httpPost($post_url,$mapfield);
        $res_generalRefundMap = array();
        $res_generalRefundMap['mer_id'] = $response['mer_id'];
        $res_generalRefundMap['refund_no'] = $response['refund_no'];
        $res_generalRefundMap['order_id'] = $response['order_id'];
        $res_generalRefundMap['mer_date'] = $response['mer_date'];
        $res_generalRefundMap['amount'] = $response['amount'];
        $res_generalRefundMap['refund_state'] = $response['refund_state'];
        $res_generalRefundMap['refund_amt'] = $response['refund_amt'];
        $res_generalRefundMap['ret_code'] = $response['ret_code'];
        $res_generalRefundMap['ret_msg'] = $response['ret_msg'];

        return $res_generalRefundMap;
    }



    /**
     * 退款---退款状态查询方法
     */
    public function queryRefundStateMap($paramsArr=array()){
        $map = self::publicParams();
        // 业务参数处理
        foreach($paramsArr as $key => $value){
            $map->put($key,$value);
        }
        // 公共参数处理
        $map->put("service","mer_refund_query");

        $reqDataPost = MerToPlat::makeRequestDataByPost($map);
        $post_url = $reqDataPost->getUrl();
        $mapfield = $reqDataPost->getField();

        // response
        $http = new UmfHttp();
        $response = $http->httpPost($post_url,$mapfield);
        $res_queryRefundStateMap = array();
        $res_queryRefundStateMap['mer_id'] = $response['mer_id'];
        $res_queryRefundStateMap['refund_state'] = $response['refund_state'];
        $res_queryRefundStateMap['refund_amt'] = $response['refund_amt'];
        $res_queryRefundStateMap['split_query_result'] = $response['split_query_result'];
        $res_queryRefundStateMap['ret_code'] = $response['ret_code'];
        $res_queryRefundStateMap['ret_msg'] = $response['ret_msg'];
        $res_queryRefundStateMap['currency'] = $response['currency'];

        return $res_queryRefundStateMap;
    }



    /**
     * 退款---退费信息补录方法
     */
    public function remedyRefundInformationMap($paramsArr=array()){
        $map = self::publicParams();
        // 业务参数处理
        foreach($paramsArr as $key => $value){
            $map->put($key,$value);
        }
        // 公共参数处理
        $map->put("service","refund_info_replenish");
        $reqDataPost = MerToPlat::makeRequestDataByPost($map);
        $post_url = $reqDataPost->getUrl();
        $mapfield = $reqDataPost->getField();

        // response
        $http = new UmfHttp();
        $response = $http->httpPost($post_url,$mapfield);
        $res_remedyRefundInformationMap = array();
        $res_remedyRefundInformationMap['mer_id'] = $response['mer_id'];
        $res_remedyRefundInformationMap['refund_no'] = $response['refund_no'];
        $res_remedyRefundInformationMap['order_id'] = $response['order_id'];
        $res_remedyRefundInformationMap['mer_date'] = $response['mer_date'];
        $res_remedyRefundInformationMap['ret_code'] = $response['ret_code'];
        $res_remedyRefundInformationMap['ret_msg'] = $response['ret_msg'];

        return $res_remedyRefundInformationMap;
    }




    //------------------------------------------------------------
    // 付款
    //------------------------------------------------------------
    /**
     * 付款---下单
     */
    public function paymentOrderMap($paramsArr=array()){
        $map = self::publicParams();
        // 业务参数处理
        foreach($paramsArr as $key => $value){
            $map->put($key,$value);
        }
        // 公共参数处理
        $map->put("service","transfer_direct_req");

        //$reqDataPost = MerToPlat::makeRequestDataByPost($map);
        //$post_url = $reqDataPost->getUrl();
        //$mapfield = $reqDataPost->getField();
        //$response = UmfHttp::httpPost($post_url,$mapfield);

        $reqDataGet = MerToPlat::makeRequestDataByGet($map);
        $get_url = $reqDataGet->getUrl();
        $http = new UmfHttp();
        $response = $http->httpGet($get_url);


        $res_paymentOrderMap = array();
        $res_paymentOrderMap['mer_id'] = $response['mer_id'];
        $res_paymentOrderMap['ret_code'] = $response['ret_code'];
        $res_paymentOrderMap['ret_msg'] = $response['ret_msg'];
        $res_paymentOrderMap['trade_no'] = $response['trade_no'];
        $res_paymentOrderMap['trade_state'] = $response['trade_state'];
        $res_paymentOrderMap['order_id'] = $response['order_id'];
        $res_paymentOrderMap['mer_date'] = $response['mer_date'];
        $res_paymentOrderMap['amount'] = $response['amount'];
        $res_paymentOrderMap['transfer_settle_date'] = $response['transfer_settle_date'];
        $res_paymentOrderMap['fee'] = $response['fee'];
        $res_paymentOrderMap['fee_type'] = $response['fee_type'];

        return $res_paymentOrderMap;
    }



    /**
     * 付款---付款状态查询
     */
    public function queryPaymentStatusMap($paramsArr=array()){
        $map = self::publicParams();
        // 业务参数处理
        foreach($paramsArr as $key => $value){
            $map->put($key,$value);
        }
        // 公共参数处理
        $map->put("service","transfer_query");

        $reqDataPost = MerToPlat::makeRequestDataByPost($map);
        $post_url = $reqDataPost->getUrl();
        $mapfield = $reqDataPost->getField();

        // response
        $http = new UmfHttp();
        $response = $http->httpPost($post_url,$mapfield);
        $res_queryPaymentStatusMap = array();
        $res_queryPaymentStatusMap['mer_id'] = $response['mer_id'];
        $res_queryPaymentStatusMap['ret_code'] = $response['ret_code'];
        $res_queryPaymentStatusMap['ret_msg'] = $response['ret_msg'];
        $res_queryPaymentStatusMap['trade_state'] = $response['trade_state'];
        $res_queryPaymentStatusMap['trade_no'] = $response['trade_no'];
        $res_queryPaymentStatusMap['transfer_date'] = $response['transfer_date'];
        $res_queryPaymentStatusMap['transfer_settle_date'] = $response['transfer_settle_date'];
        $res_queryPaymentStatusMap['amount'] = $response['amount'];
        $res_queryPaymentStatusMap['order_id'] = $response['order_id'];
        $res_queryPaymentStatusMap['mer_date'] = $response['mer_date'];
        $res_queryPaymentStatusMap['fee'] = $response['fee'];
        $res_queryPaymentStatusMap['purpose'] = $response['purpose'];

        return $res_queryPaymentStatusMap;
    }



    /**
     * 付款---余额查询
     */
    public function queryAccountBalanceMap($paramsArr=array()){
        $map = self::publicParams();
        // 业务参数处理
        foreach($paramsArr as $key => $value){
            $map->put($key,$value);
        }
        // 公共参数处理
        $map->put("service","query_account_balance");

        $reqDataPost = MerToPlat::makeRequestDataByPost($map);
        $post_url = $reqDataPost->getUrl();
        $mapfield = $reqDataPost->getField();

        // response
        $http = new UmfHttp();
        $response = $http->httpPost($post_url,$mapfield);
        $res_queryAccountBalanceMap = array();
        $res_queryAccountBalanceMap['mer_id'] = $response['mer_id'];
        $res_queryAccountBalanceMap['ret_code'] = $response['ret_code'];
        $res_queryAccountBalanceMap['ret_msg'] = $response['ret_msg'];
        $res_queryAccountBalanceMap['bal_sign'] = $response['bal_sign'];

        return $res_queryAccountBalanceMap;
    }




    //------------------------------------------------------------
    // 鉴权
    //------------------------------------------------------------
    /**
     * 鉴权---借记卡实名认证
     */
    public function debitCardAuthenticationMap($paramsArr=array()){
        $map = self::publicParams();
        // 业务参数处理
        foreach($paramsArr as $key => $value){
            $map->put($key,$value);
        }
        // 公共参数处理
        $map->put("service","comm_auth");
        $map->put("version","1.0");

        $reqDataPost = MerToPlat::makeRequestDataByPost($map);
        $post_url = $reqDataPost->getUrl();
        $mapfield = $reqDataPost->getField();

        // response
        $http = new UmfHttp();
        $response = $http->httpPost($post_url,$mapfield);
        $res_debitCardAuthenticationMap = array();
        $res_debitCardAuthenticationMap['mer_id'] = $response['mer_id'];
        $res_debitCardAuthenticationMap['ret_code'] = $response['ret_code'];
        $res_debitCardAuthenticationMap['ret_msg'] = $response['ret_msg'];
        $res_debitCardAuthenticationMap['isCharge'] = $response['isCharge'];

        return $res_debitCardAuthenticationMap;
    }


    /**
     * 鉴权---借记卡实名认证
     */
    public function creditCardAuthenticationMap($paramsArr=array()){
        $map = self::publicParams();
        // 业务参数处理
        foreach($paramsArr as $key => $value){
            $map->put($key,$value);
        }
        // 公共参数处理
        $map->put("service","comm_auth");
        $map->put("version","1.0");

        $reqDataPost = MerToPlat::makeRequestDataByPost($map);
        $post_url = $reqDataPost->getUrl();
        $mapfield = $reqDataPost->getField();

        // response
        $http = new UmfHttp();
        $response = $http->httpPost($post_url,$mapfield);
        $res_creditCardAuthenticationMap = array();
        $res_creditCardAuthenticationMap['mer_id'] = $response['mer_id'];
        $res_creditCardAuthenticationMap['ret_code'] = $response['ret_code'];
        $res_creditCardAuthenticationMap['ret_msg'] = $response['ret_msg'];
        $res_creditCardAuthenticationMap['isCharge'] = $response['isCharge'];

        return $res_creditCardAuthenticationMap;
    }



    /**
     * 鉴权---身份认证
     */
    public function identityAuthenticationMap($paramsArr=array()){
        $map = self::publicParams();
        // 业务参数处理
        foreach($paramsArr as $key => $value){
            $map->put($key,$value);
        }
        // 公共参数处理
        $map->put("service","comm_auth");
        $map->put("version","1.0");

        $reqDataPost = MerToPlat::makeRequestDataByPost($map);
        $post_url = $reqDataPost->getUrl();
        $mapfield = $reqDataPost->getField();

        // response
        $http = new UmfHttp();
        $response = $http->httpPost($post_url,$mapfield);
        $res_identityAuthenticationMap = array();
        $res_identityAuthenticationMap['mer_id'] = $response['mer_id'];
        $res_identityAuthenticationMap['ret_code'] = $response['ret_code'];
        $res_identityAuthenticationMap['ret_msg'] = $response['ret_msg'];
        $res_identityAuthenticationMap['isCharge'] = $response['isCharge'];

        return $res_identityAuthenticationMap;
    }




    //------------------------------------------------------------
    // 对账
    //------------------------------------------------------------
    /**
     * 对账
     */
    public function reconciliationDownloadMap($paramsArr=array()){
        $map = self::publicParams();
        // 业务参数处理
        foreach($paramsArr as $key => $value){
            $map->put($key,$value);
        }
        // 公共参数处理
        $map->put("service","download_settle_file");

        $reqDataPost = MerToPlat::makeRequestDataByPost($map);
        $post_url = $reqDataPost->getUrl();
        $mapfield = $reqDataPost->getField();

        // response
        $http = new UmfHttp();
        $output = $http->httpPost($post_url,$mapfield);
        $res = self::writeSettle($paramsArr["mer_id"],$paramsArr["settle_date"],$paramsArr["settle_path"],$output);
        if($res == true){
            echo "对账文件写入本地成功";
            return true;
        } else {
            echo "对账文件写入本地失败";
            return false;
        }
    }



    //------------------------------------------------------------
    // 异步通知
    //------------------------------------------------------------
    public function notifyDataParserMap($notifyParamsStr){
        if(!is_null($notifyParamsStr) && strlen($notifyParamsStr)>0){
            //联动发送异步通知URL地址后的参数
            $notifyParamsArr = explode('&',$notifyParamsStr);
            //var_dump($notifyParamsArr);
            $map = new HashMap();
            $notify_map = new HashMap();
            for($i=0;$i<count($notifyParamsArr);$i++){
                $str = $notifyParamsArr[$i];
                $arr = explode('=',$str,2);
                //$map->put($arr[0],urldecode($arr[1]));
                $map->put($arr[0],iconv( "utf-8","gbk", urldecode($arr[1])));

            }
            $sign = $map->get("sign");
            //遍历key放到一个新的map中，
            $keys = $map->keys();
            foreach ($keys as $key) {
                //sign_type 不需要参与RAS签名
                if ("sign_type" != $key && "sign" != $key ) {
                    $notify_map->put($key, $map->get($key));
                }
            }
            /*
            $map->remove("sign");
            $map->remove("sign_type");*/
            //打印map
            //var_dump($notify_map);
            $plain = StringUtil::getPlainSortByAndWithoutSignType($notify_map);
            $verifyRet = SignUtil::verify($plain,$sign);
            if($verifyRet == true){
               // echo "[UMF SDK] 平台通知数据验签成功";
                //$map->remove("version");
                //var_dump($map);
                return $map->getInnerArr();
            }else{
               return false;
            }
        }
    }

    public function responseUMFMap($array){
        $merMap = new HashMap();
        $merMap->put("mer_date",$array["mer_date"]);
        $merMap->put("mer_id",$array["mer_id"]);
        $merMap->put("order_id",$array["order_id"]);
        $merMap->put("ret_code","0000");
        $merMap->put("ret_msg","success");
        $merMap->put("version","4.0");
        $merPlain = StringUtil::getPlainSortByAndWithoutSignType($merMap);
        $merSign = SignUtil::sign($merPlain,$array["mer_id"]);
        $mer2UmfPlatStr = "mer_date=" . $merMap->get("mer_date") . "&" .
            "mer_id=" . $merMap->get("mer_id") . "&" .
            "order_id=" . $merMap->get("order_id") . "&" .
            "ret_code=0000&ret_msg=success&sign_type=RSA&version=4.0&sign=" .
            $merSign ;
        return $mer2UmfPlatStr;
    }




    //------------------------------------------------------------
    // APP支付
    //------------------------------------------------------------
    /**
     * APP下单
     * @param array $paramsArr
     * @return array
     */
    public function mobileOrderMap($paramsArr=array()){
        $map = self::publicParams();
        // 业务参数处理
        foreach($paramsArr as $key => $value){
            $map->put($key,$value);
        }
        // 公共参数处理
        $map->put("service","pay_req");

        $reqDataPost = MerToPlat::makeRequestDataByPost($map);
        $post_url = $reqDataPost->getUrl();
        $mapfield = $reqDataPost->getField();

        // response
        $http = new UmfHttp();
        $response = $http->httpPost($post_url,$mapfield);
        $res_mobileOrderMap = array();
        $res_mobileOrderMap['mer_id'] = $response['mer_id'];
        $res_mobileOrderMap['token'] = $response['token'];
        $res_mobileOrderMap['order_id'] = $response['order_id'];
        $res_mobileOrderMap['mer_date'] = $response['mer_date'];
        $res_mobileOrderMap['trade_state'] = $response['trade_state'];
        $res_mobileOrderMap['trade_no'] = $response['trade_no'];
        $res_mobileOrderMap['ret_code'] = $response['ret_code'];
        $res_mobileOrderMap['ret_msg'] = $response['ret_msg'];

        return $res_mobileOrderMap;
    }


    /**
     * APP生成签名
     * @param $params
     * @return bool|string
     */
    public function mobileGenerateSignMap($params){
        if(empty($params)){
            die("[UMF SDK]待签名参数为空,请按要求传入参数");
        }
        if(empty($params["merId"]) || $params["merId"]==""){
            die("[UMF SDK]签名参数商户号为空,请传入商户号");
        }
        if(empty($params["orderId"]) || $params["orderId"]==""){
            die("[UMF SDK]签名参数订单号为空,请传入订单号");
        }
        if(empty($params["orderDate"]) || $params["orderDate"]==""){
            die("[UMF SDK]签名参数订单日期为空,请传入订单日期");
        }
        if(empty($params["amount"]) || $params["amount"]==""){
            die("[UMF SDK]签名参数订单金额为空,请传入订单金额");
        }
        if(!preg_match("/^[0-9]{1,8}$/", $params["merId"])){
            die("[UMF SDK]签名参数商户号未通过正则校验");
        }
        if(!preg_match("/^\S{1,32}$/", $params["orderId"])){
            die("[UMF SDK]签名参数订单号未通过正则校验");
        }
        if(!preg_match("/^[1-2][0-9]{7}$/", $params["orderDate"])){
            die("[UMF SDK]签名参数订单日期未通过正则校验");
        }
        if(!preg_match("/^[1-9][0-9]*$/", $params["amount"])){
            die("[UMF SDK]签名参数订单金额未通过正则校验");
        }
        $merId = $params["merId"];
        $orderId = $params["orderId"];
        $orderDate = $params["orderDate"];
        $amount = $params["amount"];
        $plain = $merId . $orderId . $amount . $orderDate;
        $plain = iconv("UTF-8", "GBK", $plain);

        $log = new Logger();
        try{
            $priv_key_file = self::$privateKeyPath[$params["merId"]];
            $log->logInfo("[UMF SDK]私钥文件绝对路径 ".$priv_key_file);
            if(!file_exists($priv_key_file)){
                $log->logInfo("[UMF SDK]传入的私钥文件不存在,私钥文件绝对路径 ".$priv_key_file);
                die("[UMF SDK]传入的私钥文件不存在,私钥文件绝对路径 ".$priv_key_file);
            }
            $fp = fopen($priv_key_file, "rb");
            $priv_key = fread($fp, 8192);
            @fclose($fp);
            $pkeyid = openssl_get_privatekey($priv_key);
            if(!is_resource($pkeyid)){ return FALSE;}
            @openssl_sign($plain, $signature, $pkeyid);
            @openssl_free_key($pkeyid);
            $log->logInfo("[UMF SDK]签名后密文串 ".strtoupper(bin2hex($signature)));
            return strtoupper(bin2hex($signature));
        } catch(Exception $e) {
            $log->logInfo("[UMF SDK]签名失败".$e->getMessage());
        }
    }


    /**
     * 传入请求参数,成sign字段
     * @param array $paramsArr
     * @return bool|string
     */
    public function generateSign($paramsArr=array()){
        $map = new HashMap();
        foreach($paramsArr as $key => $value){
            $map->put($key,$value);
        }
        $plain = StringUtil::getPlainSortByAndWithoutSignType($map);
        $sign = SignUtil::sign($plain,$paramsArr["mer_id"]);
        return $sign;
    }


    /**
     * 验签方法
     * @param string $plain 联动返回明文字符串按序拼接(明文串去除sign_type字段)和
     * @param string $sign 密文sign字段
     * @return bool
     */
    public function verifySign($plain="",$sign=""){
        if(empty($plain) || $plain==""){
            die("明文参数为空,请传入明文字符串");
        }
        if(empty($sign) || $sign==""){
            die("密文参数为空,请传入密文字符串");
        }
        $verifyRet = SignUtil::verify($plain,$sign);
        return $verifyRet;
    }






    //------------------------------------------------------------
    // private method
    //------------------------------------------------------------
    private function publicParams(){
        $map = new HashMap();
        $map->put("charset","UTF-8");
        $map->put("sign_type","RSA");
        $map->put("res_format","HTML");
        $map->put("version","4.0");
        $map->put("amt_type","RMB");
        return $map;
    }

    /**
     * @param $settle_path
     * @param $content
     * @return int
     */
    private function  writeSettle($mer_id,$settle_date,$settle_path,$content){
        $log = new Logger();
        if(empty($mer_id)){
            $log->logInfo("[UMF SDK]下载对账文件的商户号为空");
            return false;
        }
        if(empty($settle_date)){
            $log->logInfo("[UMF SDK]下载对账文件的对账日期为空");
            return false;
        }
        if(empty($settle_path)){
            $log->logInfo("[UMF SDK]下载对账文件的路径为空");
            return false;
        }
        if(empty($content)){
            $log->logInfo("[UMF SDK]对账文件的内容为空");
            return false;
        }

        $lastChar = substr($settle_path,strlen($settle_path)-1,1);
        if($lastChar == "/"){
            $filepath = $settle_path . $mer_id . "_" . $settle_date . ".txt";
        }else{
            $filepath = $settle_path . "/" . $mer_id . "_" .$settle_date . ".txt";
        }
        $settle_content = iconv("GBK","UTF-8", $content);
        $bool = file_put_contents($filepath,$settle_content,FILE_APPEND);
        if($bool == true){
            $log->logInfo("[UMF SDK]对账文件写入成功,路径 ".$filepath);
        }else{
            $log->logInfo("[UMF SDK]对账文件写入失败,路径 ".$filepath);
        }
        $log->logInfo("--------------------log end---------------------\n");
        return $bool;
    }


    //----------------------------------------
    // 子商户入网部分
    //----------------------------------------
    /**
     * 子商户入网--获取token方法
     */
    public function getTokenMap($paramsArr=array()){
        $log = new Logger();
        $log->logInfo("--------------------log start---------------------");
        $log->logInfo("[UMF SDK 子商户入网]本次请求原始参数 " .$log->logArrToStr($paramsArr));
        $jsonStr = JsonUtil::arrayToJson($paramsArr);
        $headers = array(
            "Content-type: application/json;charset=utf-8",
        );
        $http = new UmfHttp();
        $respObj = $http->httpRequestWithJson(UMF_RESTPAY_AUTHORIZE,true,$jsonStr,$headers);
        $res_getToken = array();
        $res_getToken["access_token"] = $respObj->access_token;
        $res_getToken["expires_in"] = $respObj->expires_in;
        return $res_getToken;
    }

    /**
     * 子商户入网--添加子商户方法
     */
    public function addChildMerInfoMap($paramsArr=array()){
        $log = new Logger();
        $log->logInfo("--------------------log start---------------------");
        $log->logInfo("[UMF SDK 子商户入网]本次请求原始参数 " .$log->logArrToStr($paramsArr));
        $params = array(
            "merId"=>$paramsArr["merId"],
            "merName"=>RSACryptUtil::encrypt($paramsArr["merName"]),
            "merType"=>$paramsArr["merType"],
            "contActsName"=>RSACryptUtil::encrypt($paramsArr["contActsName"]),
            "mobileNo"=>$paramsArr["mobileNo"],
            "licenseType"=>$paramsArr["licenseType"],
            "licenseNo"=>$paramsArr["licenseNo"],
            "organizationId"=>$paramsArr["organizationId"],
            "taxPayerNum"=>$paramsArr["taxPayerNum"],
            "lawyer"=>RSACryptUtil::encrypt($paramsArr["lawyer"]),
            "cardNo"=>RSACryptUtil::encrypt($paramsArr["cardNo"]),
            "bankName"=>RSACryptUtil::encrypt($paramsArr["bankName"]),
            "bankAccount"=>RSACryptUtil::encrypt($paramsArr["bankAccount"]),
            "province"=>$paramsArr["province"],
            "areaCode"=>$paramsArr["areaCode"],
            "merNotifyUrl"=>$paramsArr["merNotifyUrl"],
            "pubPriFlag"=>$paramsArr["pubPriFlag"],
            "bankBrhName"=>RSACryptUtil::encrypt($paramsArr["bankBrhName"]),
        );
        $log->logInfo("[UMF SDK 子商户入网]本次请求处理后参数 " .$log->logArrToStr($params));
        $jsonStr = JsonUtil::arrayToJson($params);
        $signature = SignUtil::sign256($jsonStr,$paramsArr["merId"]);
        $headers = array(
            "Content-type: application/json;charset=utf-8",
            "Signature: ".$signature,
            "Authorization: Bearer".$paramsArr["token"]
        );
        $http = new UmfHttp();
        $respObj = $http->httpRequestWithJson(UMF_RESTPAY_ADDCHILDMERINFO,true,$jsonStr,$headers);
        $res_addChildMerInfo = array();
        $res_addChildMerInfo["merId"] = $respObj->merId;
        $res_addChildMerInfo["licenseNo"] = $respObj->licenseNo;
        $res_addChildMerInfo["ret_msg"] = $respObj->meta->ret_msg["0"];
        $res_addChildMerInfo["ret_code"] = $respObj->meta->ret_code;
        return $res_addChildMerInfo;
    }


    /**
     * 子商户入网--上传商户资质方法
     */
    /**
     * http://php.net/manual/zh/curlfile.construct.php
     *
     * Solution for PHP 5.4 or earlier:
     * - Build up multipart content body by youself.
     * - Change "Content-Type" header by yourself.
     */
    public function uploadMerFileMap($paramsArr=array(),$files=array()){
        $log = new Logger();
        $log->logInfo("--------------------log start---------------------");
        $log->logInfo("[UMF SDK 子商户入网]本次请求原始参数 " .$log->logArrToStr($paramsArr));
        $log->logInfo("[UMF SDK 子商户入网]本次请求上传文件 " .$log->logArrToStr($files));
        $jsonStr = JsonUtil::arrayToJson($paramsArr);
        $signature = SignUtil::sign256($jsonStr,$paramsArr["merId"]);
        $url = UMF_RESTPAY_UPLOADCHILDFILE."?data=" . urlencode($jsonStr);
        $body = array();
        $boundary = "---------------------" . md5(date("Ymd"));
        foreach ($files as $v) {
            switch (true) {
                case false === $v = realpath(filter_var($v)):
                case !is_file($v):
                case !is_readable($v):
                    continue 2;
            }
            $body[] = "\r\n--" . $boundary;
            $data = file_get_contents($v);
            $v = call_user_func("end", explode(DIRECTORY_SEPARATOR, $v));
            $body[] = implode("\r\n", array(
                "Content-Disposition: form-data; name=\"file\"; filename=\"{$v}\"",
                "Content-Type: application/octet-stream",
                "",
                $data,
            ));
        }
        $body[] = "\r\n--{$boundary}--\r\n";
        $headers = array(
            "Content-Type: multipart/form-data; boundary={$boundary}",
            "Signature: ".$signature,
            "Authorization: Bearer".$paramsArr["token"]
        );
        $http = new UmfHttp();
        $respObj = $http->httpRequestWithJson($url,true,implode("\r\n", $body),$headers);
        $res_uploadChildFile = array();
        $res_uploadChildFile["merId"] = $respObj->merId;
        $res_uploadChildFile["licenseNo"] = $respObj->licenseNo;
        $res_uploadChildFile["ret_msg"] = $respObj->meta->ret_msg["0"];
        $res_uploadChildFile["ret_code"] = $respObj->meta->ret_code;
        return $res_uploadChildFile;
    }

    /**
     * 子商户入网--修改子商户信息方法
     */
    public function changeChildMerInfoMap($paramsArr=array()){
        $log = new Logger();
        $log->logInfo("--------------------log start---------------------");
        $log->logInfo("[UMF SDK 子商户入网]本次请求原始参数 " .$log->logArrToStr($paramsArr));
        $params = array(
            "merId"=>$paramsArr["merId"],
            "merName"=>RSACryptUtil::encrypt($paramsArr["merName"]),
            "merType"=>$paramsArr["merType"],
            "contActsName"=>RSACryptUtil::encrypt($paramsArr["contActsName"]),
            "mobileNo"=>$paramsArr["mobileNo"],
            "licenseType"=>$paramsArr["licenseType"],
            "licenseNo"=>$paramsArr["licenseNo"],
            "organizationId"=>$paramsArr["organizationId"],
            "taxPayerNum"=>$paramsArr["taxPayerNum"],
            "lawyer"=>RSACryptUtil::encrypt($paramsArr["lawyer"]),
            "cardNo"=>RSACryptUtil::encrypt($paramsArr["cardNo"]),
            "bankName"=>RSACryptUtil::encrypt($paramsArr["bankName"]),
            "bankAccount"=>RSACryptUtil::encrypt($paramsArr["bankAccount"]),
            "province"=>$paramsArr["province"],
            "areaCode"=>$paramsArr["areaCode"],
            "merNotifyUrl"=>$paramsArr["merNotifyUrl"],
            "pubPriFlag"=>$paramsArr["pubPriFlag"],
            "bankBrhName"=>RSACryptUtil::encrypt($paramsArr["bankBrhName"]),
        );
        $log->logInfo("[UMF SDK 子商户入网]本次请求处理后参数 " .$log->logArrToStr($params));
        $jsonStr = JsonUtil::arrayToJson($params);
        $signature = SignUtil::sign256($jsonStr,$paramsArr["merId"]);

        $headers = array(
            "Content-type: application/json;charset=utf-8",
            "Signature: ".$signature,
            "Authorization: Bearer".$paramsArr["token"]
        );
        $http = new UmfHttp();
        $respObj = $http->httpRequestWithJson(UMF_RESTPAY_CHANGECHILDREBUT,true,$jsonStr,$headers);
        $res_changeChildRebut = array();
        $res_changeChildRebut["merId"] = $respObj->merId;
        $res_changeChildRebut["licenseNo"] = $respObj->licenseNo;
        $res_changeChildRebut["ret_msg"] = $respObj->meta->ret_msg["0"];
        $res_changeChildRebut["ret_code"] = $respObj->meta->ret_code;
        return $res_changeChildRebut;
    }

    /**
     * 子商户入网--查询审核状态方法
     */
    public function queryMerStateMap($paramsArr=array()){
        $log = new Logger();
        $log->logInfo("--------------------log start---------------------");
        $log->logInfo("[UMF SDK 子商户入网]本次请求原始参数 " .$log->logArrToStr($paramsArr));
        $merId = $paramsArr["merId"];
        $licenseNo = $paramsArr["licenseNo"];
        $url = UMF_RESTPAY_SELECTCHILDMERSTATE ."?merId=".$merId."&licenseNo=".$licenseNo;
        $headers = array(
            "Content-type: application/json;charset=utf-8",
            "Authorization: Bearer".$paramsArr["token"]
        );
        $http = new UmfHttp();
        $respObj = $http->httpRequestWithJson($url,false,"",$headers);
        $res_selectChildMerState = array();
        $res_selectChildMerState["licenseNo"] = $respObj->licenseNo;
        $res_selectChildMerState["subMerId"] = $respObj->subMerId;
        $res_selectChildMerState["ret_msg"] = $respObj->meta->ret_msg["0"];
        $res_selectChildMerState["ret_code"] = $respObj->meta->ret_code;
        $res_selectChildMerState["checkState"] = $respObj->checkState;
        $res_selectChildMerState["rejectReason"] = $respObj->rejectReason;
        $res_selectChildMerState["merId"] = $respObj->merId;
        return $res_selectChildMerState;
    }

    /**
     * 子商户入网异步通知---联动通知商户数据，SDK解析结果
     */
    public function notifyChildParserMap($notifyChildParamsStr){
        if(!is_null($notifyChildParamsStr) && strlen($notifyChildParamsStr)>0){
            $notifyParamsArr = explode('&',$notifyChildParamsStr);
            $map = new HashMap();
            for($i=0;$i<count($notifyParamsArr);$i++){
                $str = $notifyParamsArr[$i];
                $arr = explode('=',$str,2);
                //$map->put($arr[0],urldecode($arr[1]));
                $map->put($arr[0],iconv( "utf-8","gbk", urldecode($arr[1])));
            }
            $sign = $map->get("sign");
            $map->remove("sign");
            $plain = StringUtil::getPlainSortByAndWithoutSignType($map);
            $verifyRet = SignUtil::verify($plain,$sign);
            if($verifyRet == true){
                echo "[UMF SDK 子商户入网] 平台通知数据验签成功";
                return $map->getInnerArr();
            }
        }
    }

    /**
     * 账户通---子商户提现
     */
    public function withdrawalsMap($array){
        $map = self::publicParams();
        // 业务参数处理
        foreach($array as $key => $value){
            $map->put($key,$value);
        }
        // 公共参数处理
        $map->put("service","edraw_trans_by_main");

        $reqDataPost = MerToPlat::makeRequestDataByPost($map);
        $post_url = $reqDataPost->getUrl();
        $mapfield = $reqDataPost->getField();

        // response
        $http = new UmfHttp();
        $response = $http->httpPost($post_url,$mapfield);
        $res_withdrawals = array();
        $res_withdrawals['mer_id'] = $response['mer_id'];
        $res_withdrawals['sub_mer_id'] = $response['sub_mer_id'];
        $res_withdrawals['order_id'] = $response['order_id'];
        $res_withdrawals['mer_date'] = $response['mer_date'];
        $res_withdrawals['trade_state'] = $response['trade_state'];
        $res_withdrawals['trade_no'] = $response['trade_no'];
        $res_withdrawals['ret_code'] = $response['ret_code'];
        $res_withdrawals['ret_msg'] = $response['ret_msg'];
        $res_withdrawals['amount'] = $response['amount'];
        $res_withdrawals['fee'] = $response['fee'];
        $res_withdrawals['com_amt_type'] = $response['com_amt_type'];
        $res_withdrawals['purpose'] = $response['purpose'];
        $res_withdrawals['transfer_settle_date'] = $response['transfer_settle_date'];
        return $res_withdrawals;
    }
    /**
     * 账户通---子商户提现查询
     */
    public function queryWithdrawalsMap($array){
        $map = self::publicParams();
        // 业务参数处理
        foreach($array as $key => $value){
            $map->put($key,$value);
        }
        // 公共参数处理
        $map->put("service","query_edraw_trans_by_main");

        $reqDataPost = MerToPlat::makeRequestDataByPost($map);
        $post_url = $reqDataPost->getUrl();
        $mapfield = $reqDataPost->getField();

        // response
        $http = new UmfHttp();
        $response = $http->httpPost($post_url,$mapfield);
        $res_withdrawals = array();
        $res_withdrawals['mer_id'] = $response['mer_id'];
        //$res_withdrawals['sub_mer_id'] = $response['sub_mer_id'];
        $res_withdrawals['order_id'] = $response['order_id'];
        $res_withdrawals['mer_date'] = $response['mer_date'];
        $res_withdrawals['trade_state'] = $response['trade_state'];
        $res_withdrawals['trade_no'] = $response['trade_no'];
        $res_withdrawals['ret_code'] = $response['ret_code'];
        $res_withdrawals['ret_msg'] = $response['ret_msg'];
        $res_withdrawals['amount'] = $response['amount'];
        $res_withdrawals['fee'] = $response['fee'];
        $res_withdrawals['com_amt_type'] = $response['com_amt_type'];
        $res_withdrawals['purpose'] = $response['purpose'];
        $res_withdrawals['order_state'] = $response['order_state'];
        return $res_withdrawals;
    }
    /**
     * 账户通---子商户余额查询
     */
    public function querySubBalanceMap($array){
        $map = self::publicParams();
        // 业务参数处理
        foreach($array as $key => $value){
            $map->put($key,$value);
        }
        // 公共参数处理
        $map->put("service","query_account_balance_by_main");

        $reqDataPost = MerToPlat::makeRequestDataByPost($map);
        $post_url = $reqDataPost->getUrl();
        $mapfield = $reqDataPost->getField();

        // response
        $http = new UmfHttp();
        $response = $http->httpPost($post_url,$mapfield);
        $res_withdrawals = array();
        $res_withdrawals['ret_code'] = $response['ret_code'];
        $res_withdrawals['ret_msg'] = $response['ret_msg'];
        $res_withdrawals['acc_msg'] = $response['acc_msg'];
        return $res_withdrawals;
    }

    /**
     * 子商户入网异步通知---商户响应联动通知SDK生成字符串
     * 该字符串拼装在HTML响应中的head中的meta标签CONTENT中
     */
    public function responseChildNotifyMap($array){
        $merMap = new HashMap();
        $merMap->put("merId",$array["merId"]);
        $merMap->put("licenseNo",$array["licenseNo"]);
        $merMap->put("ret_code","0000");
        $merPlain = StringUtil::getPlainSortByAndWithoutSignType($merMap);
        $merSign = SignUtil::sign($merPlain,$array["merId"]);
        $mer2UmfPlatStr = $merPlain . "&sign=" . $merSign;
        return $mer2UmfPlatStr;
    }


    //----------------------------------------
    // 分账部分
    //----------------------------------------
    /**
     * 分账—分账请求针对标准分账的延时分账
     */
    public function splitReqMap($paramsArr=array()){
        $map = self::publicParams();
        // 业务参数处理
        foreach($paramsArr as $key => $value){
            $map->put($key,$value);
        }
        // 公共参数处理
        $map->put("service","split_req");

        $reqDataPost = MerToPlat::makeRequestDataByPost($map);
        $post_url = $reqDataPost->getUrl();
        $mapfield = $reqDataPost->getField();

        // response
        $http = new UmfHttp();
        $response = $http->httpPost($post_url,$mapfield);
        $res_splitStateMap = array();
        $res_splitStateMap['mer_id'] = $response['mer_id'];
        $res_splitStateMap['ret_code'] = $response['ret_code'];
        $res_splitStateMap['ret_msg'] = $response['ret_msg'];
        $res_splitStateMap['split_result'] = $response['split_result'];
        return $res_splitStateMap;
    }



    /**
     * 分账---分账状态查询
     */
    public function splitStateMap($paramsArr=array()){
        $map = self::publicParams();
        // 业务参数处理
        foreach($paramsArr as $key => $value){
            $map->put($key,$value);
        }
        // 公共参数处理
        $map->put("service","query_split_order");

        $reqDataPost = MerToPlat::makeRequestDataByPost($map);
        $post_url = $reqDataPost->getUrl();
        $mapfield = $reqDataPost->getField();

        // response
        $http = new UmfHttp();
        $response = $http->httpPost($post_url,$mapfield);
        $res_splitStateMap = array();
        $res_splitStateMap['mer_id'] = $response['mer_id'];
        $res_splitStateMap['ret_code'] = $response['ret_code'];
        $res_splitStateMap['ret_msg'] = $response['ret_msg'];
        $res_splitStateMap['split_query_result'] = $response['split_query_result'];
        return $res_splitStateMap;
    }


    /**
     * 分账---分账退费
     */
    public function splitRefundMap($paramsArr=array()){
        $map = self::publicParams();
        // 业务参数处理
        foreach($paramsArr as $key => $value){
            $map->put($key,$value);
        }
        // 公共参数处理
        $map->put("service","mer_refund");

        $reqDataPost = MerToPlat::makeRequestDataByPost($map);
        $post_url = $reqDataPost->getUrl();
        $mapfield = $reqDataPost->getField();

        // response
        $http = new UmfHttp();
        $response = $http->httpPost($post_url,$mapfield);
        $res_splitRefundMap = array();
        $res_splitRefundMap['mer_id'] = $response['mer_id'];
        $res_splitRefundMap['refund_no'] = $response['refund_no'];
        $res_splitRefundMap['order_id'] = $response['order_id'];
        $res_splitRefundMap['mer_date'] = $response['mer_date'];
        $res_splitRefundMap['amount'] = $response['amount'];
        $res_splitRefundMap['refund_amt'] = $response['refund_amt'];
        $res_splitRefundMap['ret_code'] = $response['ret_code'];
        $res_splitRefundMap['ret_msg'] = $response['ret_msg'];
        $res_splitRefundMap['split_refund_result'] = $response['split_refund_result'];
        return $res_splitRefundMap;
    }



    /**
     * 分账---分账子订单退费请求
     */
    public function splitSubRefundMap($paramsArr=array()){
        $map = self::publicParams();
        // 业务参数处理
        foreach($paramsArr as $key => $value){
            $map->put($key,$value);
        }
        // 公共参数处理
        $map->put("service","split_refund_req");

        $reqDataPost = MerToPlat::makeRequestDataByPost($map);
        $post_url = $reqDataPost->getUrl();
        $mapfield = $reqDataPost->getField();

        // response
        $http = new UmfHttp();
        $response = $http->httpPost($post_url,$mapfield);
        $res_massTransferRefundMap = array();
        $res_massTransferRefundMap['mer_id'] = $response['mer_id'];
        $res_massTransferRefundMap['refund_no'] = $response['refund_no'];
        $res_massTransferRefundMap['order_id'] = $response['order_id'];
        $res_massTransferRefundMap['mer_date'] = $response['mer_date'];
        $res_massTransferRefundMap['ret_code'] = $response['ret_code'];
        $res_massTransferRefundMap['ret_msg'] = $response['ret_msg'];

        return $res_massTransferRefundMap;
    }

    /**
     * 分账---分账文件下载
     */
    public function splitFileDownloadMap($paramsArr=array()){
        $map = self::publicParams();
        // 业务参数处理
        foreach($paramsArr as $key => $value){
            $map->put($key,$value);
        }
        // 公共参数处理
        $map->put("service","download_settle_file");

        $reqDataPost = MerToPlat::makeRequestDataByPost($map);
        $post_url = $reqDataPost->getUrl();
        $mapfield = $reqDataPost->getField();

        // response
        $http = new UmfHttp();
        $output = $http->httpPost($post_url,$mapfield);
        $res = self::writeSettle($paramsArr["mer_id"],$paramsArr["settle_date"],$paramsArr["settle_path"],$output);
        if($res == true){
            echo "对账文件写入本地成功";
            return true;
        } else {
            echo "对账文件写入本地失败";
            return false;
        }
    }


    //----------------------------------------
    // E秒付
    //----------------------------------------
    /**
     * E秒付
     */
    public function epaymentOrderMap($paramsArr=array()){
        $map = self::publicParams();
        // 业务参数处理
        foreach($paramsArr as $key => $value){
            $map->put($key,$value);
        }
        // 公共参数处理
        $map->put("service","epay_direct_req");

        $reqDataPost = MerToPlat::makeRequestDataByPost($map);
        $post_url = $reqDataPost->getUrl();
        $mapfield = $reqDataPost->getField();

        // response
        $http = new UmfHttp();
        $response = $http->httpPost($post_url,$mapfield);
        $res_epaymentOrderMap = array();
        $res_epaymentOrderMap['mer_id'] = $response['mer_id'];
        $res_epaymentOrderMap['ret_code'] = $response['ret_code'];
        $res_epaymentOrderMap['ret_msg'] = $response['ret_msg'];
        $res_epaymentOrderMap['trade_no'] = $response['trade_no'];
        $res_epaymentOrderMap['trade_state'] = $response['trade_state'];
        $res_epaymentOrderMap['order_id'] = $response['order_id'];
        $res_epaymentOrderMap['mer_date'] = $response['mer_date'];
        $res_epaymentOrderMap['amount'] = $response['amount'];
        $res_epaymentOrderMap['transfer_settle_date'] = $response['transfer_settle_date'];
        $res_epaymentOrderMap['fee'] = $response['fee'];
        $res_epaymentOrderMap['cut_fee_type'] = $response['cut_fee_type'];
        return $res_epaymentOrderMap;
    }


    /**
     * 立码付API
     */
    public function publicVerticalMap($paramsArr=array()){
        $map = self::publicParams();
        // 业务参数处理
        foreach ($paramsArr as $key => $value) {
            $map->put($key, $value);
        }
        // 公共参数处理
        $map->put("service", "verticalcode_order");
        $reqDataPost = MerToPlat::makeRequestDataByPost($map);
        $post_url = $reqDataPost->getUrl();
        $mapfield = $reqDataPost->getField();
        // response
        $http = new UmfHttp();
        return $http->httpPost($post_url, $mapfield);
    }

    /**
     * 小程序
     */
    public function miniProgramPayMap($paramsArr=array()){
        $map = self::publicParams();
        // 业务参数处理
        foreach ($paramsArr as $key => $value) {
            $map->put($key, $value);
        }
        // 公共参数处理
        $map->put("service", "wechatminiprogram_order");
        $reqDataPost = MerToPlat::makeRequestDataByPost($map);
        $post_url = $reqDataPost->getUrl();
        $mapfield = $reqDataPost->getField();
        // response
        $http = new UmfHttp();
        return $http->httpPost($post_url, $mapfield);
    }

    /**
     * 微信主扫线上
     */
    public function activeOnlineScanPayMap($paramsArr=array()){
        $map = self::publicParams();
        // 业务参数处理
        foreach ($paramsArr as $key => $value) {
            $map->put($key, $value);
        }
        // 公共参数处理
        $map->put("service", "active_scancode_order_on");
        $reqDataPost = MerToPlat::makeRequestDataByPost($map);
        $post_url = $reqDataPost->getUrl();
        $mapfield = $reqDataPost->getField();
        // response
        $http = new UmfHttp();
        return $http->httpPost($post_url, $mapfield);
    }

    /**
     * 公众号线上
     */
    public function publicOnlinePayMap($paramsArr=array()){
        $map = self::publicParams();
        // 业务参数处理
        foreach ($paramsArr as $key => $value) {
            $map->put($key, $value);
        }
        // 公共参数处理
        $map->put("service", "public_payment_order_on");
        $reqDataPost = MerToPlat::makeRequestDataByPost($map);
        $post_url = $reqDataPost->getUrl();
        $mapfield = $reqDataPost->getField();
        // response
        $http = new UmfHttp();
        return $http->httpPost($post_url, $mapfield);
    }

    /**
     * 公众号线下
     */
    public function publicUnderlinePayMap($paramsArr=array()){
        $map = self::publicParams();
        // 业务参数处理
        foreach ($paramsArr as $key => $value) {
            $map->put($key, $value);
        }
        // 公共参数处理
        $map->put("service", "public_payment_order_off");
        $reqDataPost = MerToPlat::makeRequestDataByPost($map);
        $post_url = $reqDataPost->getUrl();
        $mapfield = $reqDataPost->getField();
        // response
        $http = new UmfHttp();
        return $http->httpPost($post_url, $mapfield);
    }

    /**
     * 收款---一键快捷支付下单方法
     */
    public function quickOrderMap($paramsArr=array()) {
        $map = self::publicParams();
        // 业务参数处理
        foreach ($paramsArr as $key => $value) {
            $map->put($key, $value);
        }
        // 公共参数处理
        $map->put("service", "pay_req_shortcut");
        $reqDataPost = MerToPlat::makeRequestDataByPost($map);
        $post_url = $reqDataPost->getUrl();
        $mapfield = $reqDataPost->getField();
        // response
        $http = new UmfHttp();
        return $http->httpPost($post_url, $mapfield);
    }

    /**
     * 收款---一键快捷支付获取短信方法
     */
    public function getSMSMap($paramsArr=array()) {
        $map = self::publicParams();
        // 业务参数处理
        foreach ($paramsArr as $key => $value) {
            $map->put($key, $value);
        }
        // 公共参数处理
        $map->put("service", "req_smsverify_shortcut");
        //$map->put("media_type","MOBILE");
        $reqDataPost = MerToPlat::makeRequestDataByPost($map);
        $post_url = $reqDataPost->getUrl();
        $mapfield = $reqDataPost->getField();
        // response
        $http = new UmfHttp();
        return $http->httpPost($post_url, $mapfield);
    }
    /**
     * 收款---一键快捷支付首次支付方法
     */
    public function FirstPaymentMap($paramsArr=array()) {
        $map = self::publicParams();
        // 业务参数处理
        foreach ($paramsArr as $key => $value) {
            $map->put($key, $value);
        }
        // 公共参数处理
        $map->put("service", "first_pay_confirm_shortcut");
        $map->put("identity_type","IDENTITY_CARD");
        $reqDataPost = MerToPlat::makeRequestDataByPost($map);
        $post_url = $reqDataPost->getUrl();
        $mapfield = $reqDataPost->getField();
        // response
        $http = new UmfHttp();
        return $http->httpPost($post_url, $mapfield);
    }
    /**
     * 收款---一键快捷支付协议支付方法
     */
    public function agreementPaymentMap($paramsArr=array()) {
        $map = self::publicParams();
        // 业务参数处理
        foreach ($paramsArr as $key => $value) {
            $map->put($key, $value);
        }
        // 公共参数处理
        $map->put("service", "agreement_pay_confirm_shortcut");
        $reqDataPost = MerToPlat::makeRequestDataByPost($map);
        $post_url = $reqDataPost->getUrl();
        $mapfield = $reqDataPost->getField();
        // response
        $http = new UmfHttp();
        return $http->httpPost($post_url, $mapfield);
    }
    /**
     * 收款---一键快捷支付查询商户支持的银行卡列表方法
     */
    public function supportBankMap($paramsArr=array()) {
        $map = self::publicParams();
        // 业务参数处理
        foreach ($paramsArr as $key => $value) {
            $map->put($key, $value);
        }
        // 公共参数处理
        $map->put("service", "query_mer_bank_shortcut");
        $reqDataPost = MerToPlat::makeRequestDataByPost($map);
        $post_url = $reqDataPost->getUrl();
        $mapfield = $reqDataPost->getField();
        // response
        $http = new UmfHttp();
        return $http->httpPost($post_url, $mapfield);
    }
    /**
     * 收款---一键快捷支付查询商户用户已签约的银行列表方法
     */
    public function signedBankMap($paramsArr=array()) {
        $map = self::publicParams();
        // 业务参数处理
        foreach ($paramsArr as $key => $value) {
            $map->put($key, $value);
        }
        // 公共参数处理
        $map->put("service", "query_mercust_bank_shortcut");
        $reqDataPost = MerToPlat::makeRequestDataByPost($map);
        $post_url = $reqDataPost->getUrl();
        $mapfield = $reqDataPost->getField();
        // response
        $http = new UmfHttp();
        return $http->httpPost($post_url, $mapfield);
    }
    /**
     * 收款---一键快捷支付用户签约下短信方法
     */
    public function bindingApplicationMap($paramsArr=array()) {
        $map = self::publicParams();
        // 业务参数处理
        foreach ($paramsArr as $key => $value) {
            $map->put($key, $value);
        }
        // 公共参数处理
        $map->put("service", "req_bind_verify_shortcut");
        $map->put("media_type", "MOBILE");
        $reqDataPost = MerToPlat::makeRequestDataByPost($map);
        $post_url = $reqDataPost->getUrl();
        $mapfield = $reqDataPost->getField();
        // response
        $http = new UmfHttp();
        return $http->httpPost($post_url, $mapfield);
    }
    /**
     * 收款---一键快捷支付签约确认方法
     */
    public function bindingConfirmationMap($paramsArr=array()) {
        $map = self::publicParams();
        // 业务参数处理
        foreach ($paramsArr as $key => $value) {
            $map->put($key, $value);
        }
        // 公共参数处理
        $map->put("service", "req_bind_confirm_shortcut");
        $map->put("media_type", "MOBILE");
        $reqDataPost = MerToPlat::makeRequestDataByPost($map);
        $post_url = $reqDataPost->getUrl();
        $mapfield = $reqDataPost->getField();
        // response
        $http = new UmfHttp();
        return $http->httpPost($post_url, $mapfield);
    }
    /**
     * 收款---一键快捷支付API解除绑定方法
     */
    public function unbindMap($paramsArr=array()) {
        $map = self::publicParams();
        // 业务参数处理
        foreach ($paramsArr as $key => $value) {
            $map->put($key, $value);
        }
        // 公共参数处理
        $map->put("service", "unbind_mercust_protocol_shortcut");
        $reqDataPost = MerToPlat::makeRequestDataByPost($map);
        $post_url = $reqDataPost->getUrl();
        $mapfield = $reqDataPost->getField();
        // response
        $http = new UmfHttp();
        return $http->httpPost($post_url, $mapfield);
    }

    /**
     * 商业委托---签约下单及获取短信验证码
     */
    public function CommercialSignOrderMap($paramsArr=array()) {
        $map = self::publicParams();
        // 业务参数处理
        foreach ($paramsArr as $key => $value) {
            $map->put($key, $value);
        }
        // 公共参数处理
        $map->put("service", "apply_sms_bind_shortcut");
        $map->put("media_type","MOBILE");
        $reqDataPost = MerToPlat::makeRequestDataByPost($map);
        $post_url = $reqDataPost->getUrl();
        $mapfield = $reqDataPost->getField();
        // response
        $http = new UmfHttp();
        return $http->httpPost($post_url, $mapfield);
    }

    /**
     * 商业委托---查询商户用户已签约的银行列表方法
     */
    public function querySignedBankMapNew($paramsArr=array()) {
        $map = self::publicParams();
        // 业务参数处理
        foreach ($paramsArr as $key => $value) {
            $map->put($key, $value);
        }
        // 公共参数处理
        $map->put("service", "query_mercust_bank_shortcut_new");
        $reqDataPost = MerToPlat::makeRequestDataByPost($map);
        $post_url = $reqDataPost->getUrl();
        $mapfield = $reqDataPost->getField();
        // response
        $http = new UmfHttp();
        return $http->httpPost($post_url, $mapfield);
    }


    /**
     * 商业委托---签约确认
     */
    public function CommercialSignConfirmMap($paramsArr=array()) {
        $map = self::publicParams();
        // 业务参数处理
        foreach ($paramsArr as $key => $value) {
            $map->put($key, $value);
        }
        // 公共参数处理
        $map->put("service", "confirm_bind_shortcut");
        $reqDataPost = MerToPlat::makeRequestDataByPost($map);
        $post_url = $reqDataPost->getUrl();
        $mapfield = $reqDataPost->getField();
        // response
        $http = new UmfHttp();
        return $http->httpPost($post_url, $mapfield);
    }
    /**
     *收款---分期查询用户手续费
     */
    public function queryUserStagingComamtMap($paramsArr=array()){
        $map = self::publicParams();
        // 业务参数处理
        foreach ($paramsArr as $key => $value) {
            $map->put($key, $value);
        }
        // 公共参数处理
        $map->put("service", "query_user_staging_comamt");
        $reqDataPost = MerToPlat::makeRequestDataByPost($map);
        $post_url = $reqDataPost->getUrl();
        $mapfield = $reqDataPost->getField();
        // response
        $http = new UmfHttp();
        return $http->httpPost($post_url, $mapfield);
    }

    /**
     *收款---立码付获取用户标识
     */
    public function getUseridUnionMap($paramsArr=array()){
        $map = self::publicParams();
        // 业务参数处理
        foreach ($paramsArr as $key => $value) {
            $map->put($key, $value);
        }
        // 公共参数处理
        $map->put("service", "get_userid_union");
        $reqDataPost = MerToPlat::makeRequestDataByPost($map);
        $post_url = $reqDataPost->getUrl();
        $mapfield = $reqDataPost->getField();
        // response
        $http = new UmfHttp();
        return $http->httpPost($post_url, $mapfield);
    }

    /**
     * 收款---借记卡认证支付下单获取交易号方法
     */
    public function debitAuthenPayOrderMap($paramsArr=array()) {
        $map = self::publicParams();
        // 业务参数处理
        foreach ($paramsArr as $key => $value) {
            $map->put($key, $value);
        }
        // 公共参数处理
        $map->put("service", "pay_req");
        $map->put("pay_type", "DEBITCARD");
        $reqDataPost = MerToPlat::makeRequestDataByPost($map);
        $post_url = $reqDataPost->getUrl();
        $mapfield = $reqDataPost->getField();
        // response
        $http = new UmfHttp();
        return $http->httpPost($post_url, $mapfield);
    }
    /**
     * 收款---借记卡认证支付获取短信验证码方法
     */
    public function debitAuthenPayGetVerifyCodeMap($paramsArr=array()) {
        $map = self::publicParams();
        // 业务参数处理
        foreach ($paramsArr as $key => $value) {
            $map->put($key, $value);
        }
        // 公共参数处理
        $map->put("service", "req_sms_verifycode");
        $map->put("media_type", "MOBILE");
        $reqDataPost = MerToPlat::makeRequestDataByPost($map);
        $post_url = $reqDataPost->getUrl();
        $mapfield = $reqDataPost->getField();
        // response
        $http = new UmfHttp();
        return $http->httpPost($post_url, $mapfield);
    }
    /**
     * 收款---借记卡认证支付确认支付方法
     */
    public function debitAuthenPayConfirmMap($paramsArr=array()) {
        $map = self::publicParams();
        // 业务参数处理
        foreach ($paramsArr as $key => $value) {
            $map->put($key, $value);
        }
        // 公共参数处理
        $map->put("service", "pay_confirm");
        $map->put("media_type", "MOBILE");
        $map->put("identity_type","IDENTITY_CARD");
        $map->put("pay_category","02");
        $reqDataPost = MerToPlat::makeRequestDataByPost($map);
        $post_url = $reqDataPost->getUrl();
        $mapfield = $reqDataPost->getField();
        // response
        $http = new UmfHttp();
        return $http->httpPost($post_url, $mapfield);
    }
}





