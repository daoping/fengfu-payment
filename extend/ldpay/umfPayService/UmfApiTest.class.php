<?php

// cli 模式执行 目录改为文件所在目录
chdir(dirname(__FILE__));

#require_once("../api/common.php");
#require_once("../api/mer2Plat.php");
require_once("./UmfService.class.php");

class UmfApiTest {

    //------------------------------------------------------------
    // Web收银台
    //------------------------------------------------------------
    /**
     * Web收银台—生成get后的请求参数，商户只需要拼接URL进行get请求即可
     */
    public function test_WebFrontPagePayMap(){
        $params = array(
            "mer_id"=>"60016102",
            "ret_url"=>"http://www.xxx.com/xxxx.php",
            "notify_url"=>"http://www.xxx.com/xxxx.php",
            "goods_inf"=>"商品描述",
            "order_id"=>rand(0,999999999),
            "mer_date"=>date ( "Ymd" ),
            "amount"=>"1",
            "interface_type"=>"01",
        );
        $service = new UmfService("60016102","D:/60016102_.key.pem");
        $web_pay_get_url = $service->WebFrontPagePayMap($params);
        var_dump($web_pay_get_url);
    }


    //------------------------------------------------------------
    // H5收银台
    //------------------------------------------------------------
    /**
     * H5收银台—生成get后的请求参数，商户只需要拼接URL进行get请求即可
     */
    public function test_H5FrontPageMap(){
        $params = array(
            "mer_id"=>"60000100",
            "ret_url"=>"recharge://return",
            "notify_url"=>"http://www.xxx.com/xxxx.php",
            "goods_inf"=>"商品描述",
            "order_id"=>rand(0,999999999),
            "mer_date"=>date ( "Ymd" ),
            "amount"=>"1",
        );
        $service = new UmfService("60000100","D:/60000100_.key.pem");
        $h5_pay_get_url = $service->H5FrontPageMap($params);
        var_dump($h5_pay_get_url);
    }



    //------------------------------------------------------------
    // 公众号支付
    //------------------------------------------------------------
    /**
     * 公众号支付–生成get后的请求参数，商户只需要拼接URL进行get请求即可
     */
    public function test_PublicPaymentMap(){
        $params = array(
            "mer_id"=>"60000100",
            "notify_url"=>"http://www.xxx.com/xxxx.php",
            "goods_inf"=>"商品描述",
            "order_id"=>rand(0,999999999),
            "mer_date"=>date ( "Ymd" ),
            "amount"=>"1",
            "is_public_number"=>"Y",
        );
        $service = new UmfService("60000100","D:/60000100_.key.pem");
        $wechat_pay_get_url = $service->PublicPaymentMap($params);
        var_dump($wechat_pay_get_url);
    }



    //------------------------------------------------------------
    // 收款 : 网银直连
    //------------------------------------------------------------
    public function test_pBankDirectMap(){
        $params = array(
            "mer_id"=>"60000100",
            "mer_date"=>date ( "Ymd" ),
            "order_id"=>rand(0,999999999),
            "amount"=>"1",
            "pay_type"=>"B2BBANK",
            "gate_id"=>"CCB",
            "interface_type"=>"02"
        );
        $service = new UmfService("60000100","D:/60000100_.key.pem");
        $pbank_direct_get_url = $service->pBankDirectMap($params);
        var_dump($pbank_direct_get_url);
    }



    //------------------------------------------------------------
    // 收款 : 借记卡直连
    //------------------------------------------------------------
    public function test_debitDirectMap(){
        $params = array(
            "mer_id"=>"60000100",
            "mer_date"=>date ( "Ymd" ),
            "order_id"=>rand(0,999999999),
            "amount"=>"1",
            "card_id"=>"xxxxxxxxxxx",
            "identity_type"=>"1",
            "identity_code"=>"xxxxxxxxxxx",
            "card_holder"=>"张三",
        );
        $service = new UmfService("60000100","D:/60000100_.key.pem");
        $res = $service->debitDirectMap($params);
        var_dump($res);
    }



    //------------------------------------------------------------
    // 收款 : 信用卡直连
    //------------------------------------------------------------
    public function test_creditDirectMap(){
        $params = array(
            "mer_id"=>"60000100",
            "mer_date"=>date ( "Ymd" ),
            "order_id"=>rand(0,999999999),
            "amount"=>"1",
            "valid_date"=>"2504",
            "card_id"=>"6253360104124044",
            "cvv2"=>"188",
        );
        $service = new UmfService("60000100","D:/60000100_.key.pem");
        $res = $service->creditDirectMap($params);
        var_dump($res);
    }


    //------------------------------------------------------------
    // 收款 : 扫码支付
    //------------------------------------------------------------
    /**
     * 主扫支付
     */
    public function test_activeScanPaymentMap(){
        $params = array(
            "mer_id"=>"60038402",
            "ret_url"=>"http://www.xxx.com/xxxx.php",
            "notify_url"=>"http://www.xxx.com/xxxx.php",
            "goods_id"=>"",
            "goods_inf"=>"商品描述",
            "order_id"=>rand(0,999999999),
            "mer_date"=>date ( "Ymd" ),
            "amount"=>"1",
            "scancode_type"=>"WECHAT",
            "mer_priv"=>"",
            "expand"=>"",
            "user_ip"=>"",
            "expire_time"=>"",
        );
        $service = new UmfService("60038402","D:/60038402_.key.pem");
       // $service = new UmfService("60000100","D:/60000100_.key.pem");
        $res = $service->activeScanPaymentMap($params);
        var_dump($res);
    }


    /**
     * 主扫支付（新）
     */
    public function test_activeNewScanPaymentMap(){
        $params = array(
            "mer_id"=>"60038402",
            "ret_url"=>"http://www.xxx.com/xxxx.php",
            "notify_url"=>"http://www.xxx.com/xxxx.php",
            "goods_id"=>"",
            "goods_inf"=>"商品描述",
            "order_id"=>rand(0,999999999),
            "mer_date"=>date ( "Ymd" ),
            "amount"=>"1",
            "scancode_type"=>"WECHAT",
            "mer_priv"=>"",
            "expand"=>"",
            "user_ip"=>"",
            "expire_time"=>"",
        );
        //$service = new UmfService("60016102","D:/60016102_.key.pem");
        $service = new UmfService("60000100","D:/60000100_.key.pem");
        $res = $service->activeNewScanPaymentMap($params);
        var_dump($res);
    }


    /**
     * H5直连-范围URL地址
     */
    public function test_H5AutoConnectMap(){
        $params = array(
            "mer_id"=>"60000100",
            "ret_url"=>"http://www.xxx.com/xxxx.php",
            "notify_url"=>"http://www.xxx.com/xxxx.php",
            "goods_inf"=>"商品描述",
            "order_id"=>"123456789",
            "mer_date"=>date ("Ymd"),
            "amount"=>"1",
            "scancode_type"=>"ALIPAY",
        );
        $service = new UmfService("60000100","D:/60000100_.key.pem");
        $h5_auto_connect_url = $service->H5AutoConnectMap($params);
        var_dump($h5_auto_connect_url);
    }



    /**
     * H5直连-返回请求参数
     */
    public function test_H5ConnectMap(){
        $params = array(
            "mer_id"=>"60000100",
            "ret_url"=>"http://www.xxx.com/xxxx.php",
            "notify_url"=>"http://www.xxx.com/xxxx.php",
            "goods_inf"=>"商品描述",
            "order_id"=>"123456789",
            "mer_date"=>date ("Ymd"),
            "amount"=>"1",
            "scancode_type"=>"ALIPAY",
        );
        $service = new UmfService("60000100","D:/60000100_.key.pem");
        $res = $service->H5ConnectMap($params);
        var_dump($res);
    }


    /**
     * 被扫支付
     */
    public function test_passiveScanPaymentMap(){
        $params = array(
            "mer_id"=>"60000100",
            "ret_url"=>"http://www.xxx.com/xxxx.php",
            "notify_url"=>"http://www.xxx.com/xxxx.php",
            "goods_id"=>"",
            "goods_inf"=>"商品描述",
            "order_id"=>rand(0,999999999),
            "mer_date"=>date ( "Ymd" ),
            "amount"=>"1",
            "scancode_type"=>"WECHAT",
            "auth_code"=>'135036529140563496',//微信付款二维码
            //"scancode_type"=>"ALIPAY",
            //"auth_code"=>'288747129231022246',//支付宝付款二维码,目前(20170913)支付宝支持银行卡支付,花呗的付款二维码不能付款
            "use_desc"=>"测试",
            "mer_priv"=>"",
            "expand"=>"",
            "user_ip"=>"",
            "expire_time"=>"",
        );
        $service = new UmfService("60000100","D:/60000100_.key.pem");
        $res = $service->passiveScanPaymentMap($params);
        var_dump($res);
    }


    //------------------------------------------------------------
    // 收款 : 快捷支付
    //------------------------------------------------------------
    /**
     * 收款---快捷支付下单方法
     */
    public function test_quickPayOrderMap(){
        $params = array(
            "mer_id"=>"60000100",
            "ret_url"=>"http://www.xxx.com/xxxx.php",
            "notify_url"=>"http://www.xxx.com/xxxx.php",
            "order_id"=>rand(0,999999999), // 707794189
            "mer_date"=>date ( "Ymd" ),
            "amount"=>"1",
            "mer_cust_id"=> "",//"200000000020",
            "pay_type"=>"CREDITCARD", //信用卡  //"DEBITCARD",//借记卡
            "gate_id"=>"ABC", //"CITIC",//中信银行
            "goods_id"=>"",
            "goods_inf"=>"",
            "media_id"=>"",
            "media_type"=>"",
            "user_ip"=>"",
            "expand"=>"",
            "expire_time"=>"",
            "risk_expand"=>"",
            "media_type"=>"",//"MOBILE",
        );
        $service = new UmfService("60000100","D:/60000100_.key.pem");
        $res = $service->quickPayOrderMap($params);
        var_dump($res);
    }


    /**
     * 收款---快捷支付向平台获取短信验证码方法
     */
    public function test_quickGetMessageMap(){
        $params = array(
            "mer_id"=>"60000100",
            "trade_no"=>"3709141629214534",
            "media_id"=>"xxxxxxx",
            "media_type"=>"MOBILE",
            "card_id"=>"xxxxxxxx",
            "identity_type"=>"IDENTITY_CARD",
            "identity_code"=>"xxxxxxxx",
            "card_holder"=>"张三",
            "valid_date"=>"xxxxx",
            "cvv2"=>"xxxx",
        );
        $service = new UmfService("60000100","D:/60000100_.key.pem");
        $res = $service->quickGetMessageMap($params);
        var_dump($res);
    }



    /**
     * 收款---快捷支付确认支付
     */
    public function test_quickPayConfirmMap(){
        $params = array(
            "verify_code"=>"660412",
            "mer_id"=>"60000100",
            "trade_no"=>"3709141629214534",
            "media_id"=>"xxxxxxxxxx",
            "media_type"=>"MOBILE",
            "card_id"=>"xxxxxxxxxxxxx",
            "identity_type"=>"IDENTITY_CARD",
            "identity_code"=>"xxxxxxxxxx",
            "card_holder"=>"张三",
            "valid_date"=>"xxx",
            "cvv2"=>"xxx",
        );
        $service = new UmfService("60000100","D:/60000100_.key.pem");
        $res = $service->quickPayConfirmMap($params);
        var_dump($res);
    }


    /**
     * 收款---快捷支付获取银行卡列表
     */
    public function test_quickQuerybankSupportMap(){
        $params = array(
            "mer_id"=>"60000100",
            "pay_type"=>"CREDITCARD"
        );
        $service = new UmfService("60000100","D:/60000100_.key.pem");
        $res = $service->quickQuerybankSupportMap($params);
        var_dump($res);
    }



    /**
     * 收款---快捷支付解约
     */
    public function test_quickCancelSurrenderMap(){
        $params = array(
            "mer_id"=>"60000100",
            "mer_cust_id"=>"7836578",
            "usr_busi_agreement_id"=>"",
            "usr_pay_agreement_id"=>"",
        );
        $service = new UmfService("60000100","D:/60000100_.key.pem");
        $res = $service->quickCancelSurrenderMap($params);
        var_dump($res);
    }



    //------------------------------------------------------------
    // 订单查询
    //------------------------------------------------------------
    /**
     * 订单查询---查询历史订单方法
     */
    public function test_queryhistoryOrderMap(){
        $params = array(
            "mer_id"=>"60000100",
            "mer_date"=>date ( "Ymd" ),
            "order_id"=>"707794189",
            "order_type"=>"",
            "trade_no"=>"3709141130507194",
        );
        $service = new UmfService("60000100","D:/60000100_.key.pem");
        $res = $service->queryhistoryOrderMap($params);
        var_dump($res);
    }


    /**
     * 订单查询---查询当天订单状态
     */
    public function test_querytodayOrderMap(){
        $params = array(
            "mer_id"=>"60000100",
            "mer_date"=>date ( "Ymd" ),
            "order_id"=>"707794189",
            "amount"=>"1",
            "order_type"=>"",
            "trade_no"=>"3709141130507194",
            "goods_id"=>"",
            "goods_inf"=>"",
            "media_id"=>"17600537566",
            "media_type"=>"MOBILE",
        );
        $service = new UmfService("60000100","D:/60000100_.key.pem");
        $res = $service->querytodayOrderMap($params);
        var_dump($res);
    }




    //------------------------------------------------------------
    // 撤销
    //------------------------------------------------------------
    /**
     * 撤销---撤销方法
     */
    public function test_cancelTradeMap(){
        $params = array(
            "mer_id"=>"60000100",
            "mer_date"=>date ( "Ymd" ),
            "order_id"=>"707794189",
            "amount"=>"1"
         );
        $service = new UmfService("60000100","D:/60000100_.key.pem");
        $res = $service->cancelTradeMap($params);
        var_dump($res);
    }



    //------------------------------------------------------------
    // 退款
    //------------------------------------------------------------
    /**
     * 退款---普通退款方法
     */
    public function test_generalRefundMap(){
        $params = array(
            "mer_id"=>"60000100",
            "refund_no"=>"1709151040280000",
            "order_id"=>"546813964",
            "mer_date"=>"20170914",//date ( "Ymd" ),
            "org_amount"=>"1",
            "refund_amount"=>"1",
            "notify_url"=>"http://www.xxx.com/xxxx.php",
        );
        $service = new UmfService("60000100","D:/60000100_.key.pem");
        $res = $service->generalRefundMap($params);
        var_dump($res);
    }


    /**
     * 退款---退款状态查询方法
     */
    public function test_queryRefundStateMap(){
        $params = array(
            "mer_id"=>"60000100",
            "refund_no"=>"1709151040280000",
        );
        $service = new UmfService("60000100","D:/60000100_.key.pem");
        $res = $service->queryRefundStateMap($params);
        var_dump($res);
    }


    /**
     * 退款---退费信息补录方法
     */
    public function test_remedyRefundInformationMap(){
        $params = array(
            "mer_id"=>"60000100",
            "refund_no"=>"1709121725281236",
            "card_holder"=>"张三",
            "card_id"=>"xxxxxxxxxxxx",
            "gate_id"=>"CITIC",
            "card_branch_name"=>"中信银行",
        );
        $service = new UmfService("60000100","D:/60000100_.key.pem");
        $res = $service->remedyRefundInformationMap($params);
        var_dump($res);
    }



    //------------------------------------------------------------
    // 付款
    //------------------------------------------------------------
    /**
     * 付款---下单
     */
    public function test_paymentOrderMap(){
        $params = array(
            "mer_id"=>"60000100",
            "notify_url"=>"http://www.xxx.com/xxxx.php",
            "order_id"=>rand(0,999999999), // 430480957
            "mer_date"=>date ( "Ymd" ),
            "amount"=>"1",
            "recv_account_type"=>"00",
            "recv_bank_acc_pro"=>"0",
            "recv_account"=>"xxxxxxxxxx",
            "recv_user_name"=>"张三",
            "purpose"=>"测试",
           // "bank_brhname"=>"中信银行",
        );
        $service = new UmfService("60000100","D:/60000100_.key.pem");
        $res = $service->paymentOrderMap($params);
        var_dump($res);
    }


    /**
     * 付款---付款状态查询
     */
    public function test_queryPaymentStatusMap(){
        $params = array(
            "mer_id"=>"60000100",
            "order_id"=>"409545898",
            "mer_date"=>date ( "Ymd" ),
        );
        $service = new UmfService("60000100","D:/60000100_.key.pem");
        $res = $service->queryPaymentStatusMap($params);
        var_dump($res);
    }




    /**
     * 付款---余额查询
     */
    public function test_queryAccountBalanceMap(){
        $params = array(
            "mer_id"=>"60000100",
            "acc_type"=>"2",
        );
        $service = new UmfService("60000100","D:/60000100_.key.pem");
        $res = $service->queryAccountBalanceMap($params);
        var_dump($res);
    }


    //------------------------------------------------------------
    // 鉴权
    //------------------------------------------------------------
    /**
     * 鉴权---借记卡实名认证
     */
    public function test_debitCardAuthenticationMap(){
        $params = array(
            "mer_id"=>"60000100",
            "auth_type"=>"1",
            "auth_mode"=>"0",
            "order_id"=>rand(0,999999999),
            "bank_account"=>"xxxxxxxxx",
            "account_name"=>"张三",
            "identity_type"=>"1",
            "identity_code"=>"xxxxxxxxxx",
            "mobile_id"=>"xxxxxxxxxx",
        );
        $service = new UmfService("60000100","D:/60000100_.key.pem");
        $res = $service->debitCardAuthenticationMap($params);
        var_dump($res);
    }


    /**
     * 鉴权---借记卡实名认证
     */
    public function test_creditCardAuthenticationMap(){
        $params = array(
            "mer_id"=>"60000100",
            "auth_type"=>"1",
            "auth_mode"=>"0",
            "order_id"=>rand(0,999999999),
            "bank_account"=>"xxxxxxxxxxx",
            "account_name"=>"张三",
            "identity_type"=>"1",
            "identity_code"=>"xxxxxxxxxxxxx",
            "mobile_id"=>"xxxxxxxxxxxx",
            "cvv2"=>"188",
            "endDate"=>"2504"
        );
        $service = new UmfService("60000100","D:/60000100_.key.pem");
        $res = $service->creditCardAuthenticationMap($params);
        var_dump($res);
    }



    /**
     * 鉴权---身份认证
     */
    public function test_identityAuthenticationMap(){
        $params = array(
            "mer_id"=>"60000100",
            "auth_type"=>"1",
            "auth_mode"=>"3",
            "order_id"=>rand(0,999999999),
            "account_name"=>"张三",
            "identity_code"=>"xxxxxxxxxxxx",
        );
        $service = new UmfService("60000100","D:/60000100_.key.pem");
        $res = $service->identityAuthenticationMap($params);
        var_dump($res);
    }



    //------------------------------------------------------------
    // 对账
    //------------------------------------------------------------
    /**
     * 对账
     */
    public function test_reconciliationDownloadMap(){
        $params = array(
            "mer_id"=>"60000100",
            "settle_date"=>"20180803",//date ( "Ymd" ),
            "settle_path"=>"D:/umf_settle"
        );
        $service = new UmfService("60000100","D:/60000100_.key.pem");
        $res = $service->reconciliationDownloadMap($params);
        var_dump($res);
    }



    //------------------------------------------------------------
    // 异步通知
    //------------------------------------------------------------
    public function test_doGet(){
        //联动通知商户GET请求URL所带的参数
        $notifyParamsStr = "amount=1&amt_type=RMB&charset=UTF-8&error_code=0000&gate_id=BOC&goods_id=123&last_four_cardid=9112&media_id=18519105446&media_type=MOBILE&mer_date=20170321&mer_id=60000100&order_id=20170321333781&pay_date=20170321&pay_seq=D170321044815412782&pay_type=DEBITCARD&service=pay_result_notify&settle_date=20170321&trade_no=3703211703772982&trade_state=TRADE_SUCCESS&usr_busi_agreement_id=UB201703211650520000000013656539&usr_pay_agreement_id=P2017011014175200000000023372710&version=4.0&sign=YgZqiVdBALgIFcu6bXQRfSSew4Pg2t1AK25fwMfb%2FukBeRlklTdrPNw3RGbdFGSzbp7WxnV7TD20PuVs1FQbrtFZMvo2DTgB1uxJN1HwciBO30EfmpEuFSfx4m%2BoaxFNwezHdf35EHqhptRR%2BOmEe7uaGpV1EOa%2Fe%2BQPSrYRRP4%3D&sign_type=RSA";
         //$notifyParamsStr = "amount=1&amt_type=RMB&charset=UTF-8&error_code=0000&media_type=MOBILE&mer_date=20180504&mer_id=60275202&order_id=201805041438217192&pay_date=20180504&pay_seq=ZXWC0000000000000000041671351885&pay_type=WECHATWEB&service=pay_result_notify&settle_date=20180504&trade_no=3805041438501905&trade_state=TRADE_SUCCESS&version=4.0&sign=VDU1kPMhf0d10h1t5FYbzhYz0Wn7LXtpBTZy3MpWrTfJ0d64jAe7sV%2FmeFf%2FQ7IuyGS9HNpzrilfzR6xwMXHAEYCuvqd07hoSxfAnfuikarVFkwSXdm53LZGz2m1rCt9Ink43uqy0nRTWfIatug17cGDbvA2Kf6D1%2BT6KLZxXLY%3D&sign_type=RSA";
        //$notifyParamsStr = "amount=500000&com_amt=80&com_amt_type=-99&mer_date=20180305&mer_id=9602&order_id=719540&ret_code=0000&ret_msg=%E4%BA%A4%E6%98%93%E6%88%90%E5%8A%9F&trade_no=1803051026644972&trade_state=4&transfer_date=20180305&transfer_settle_date=20180305&version=4.0&sign=JxyCw7CmvfsfyB1UkHLNyNrdql23O3sz09CvzKgNb00tATSxGoUFUBUMWAyoH3Kz21M%2Br35lCd2u6Zi6dY2nHnAD0skwiIN9H%2Fr4uHRPPMzhzTbvQtB3hopI8hHB7XjS31hDGKnh4bbxLX2SrZ%2BEzVgKow6O17clFpALYzKZ1Ro%3D&sign_type=RSA";
        //$notifyParamsStr = "amount=11&com_amt=100&com_amt_type=-99&extra_com_amt=0&mer_date=20190128&mer_id=60010902&order_id=Y119834965582103745_1&ret_code=0000&ret_msg=%E4%BA%A4%E6%98%93%E6%88%90%E5%8A%9F&trade_no=1901281551974287&trade_state=4&transfer_date=20190128&transfer_settle_date=20190128&version=4.0&sign=LJJSeluyHjKoxzBDhlLkiiY59JR8z1JduFEV3elHUEXmXu3AWJjZd%2BAYDJJrfFesLJDV%2F2sbaOp0B5O6ym0aw3NzGCP9X%2FxkDI%2BhSnGFmQERYxJc%2F6KaULqyrO6sheFu%2BcTr%2BcPCP59VnvFFI4IRxPNVS30aKRHGmn%2BWrmk9xZM%3D&sign_type=RSA";
        //$notifyParamsStr = "amount=11&com_amt=100&com_amt_type=-99&extra_com_amt=0&mer_date=20190128&mer_id=60010902&order_id=Y119834965582103745_1&ret_code=0000&ret_msg=%E4%BA%A4%E6%98%93%E6%88%90%E5%8A%9F&trade_no=1901281551974287&trade_state=4&transfer_date=20190128&transfer_settle_date=20190128&version=4.0&sign=LJJSeluyHjKoxzBDhlLkiiY59JR8z1JduFEV3elHUEXmXu3AWJjZd%2BAYDJJrfFesLJDV%2F2sbaOp0B5O6ym0aw3NzGCP9X%2FxkDI%2BhSnGFmQERYxJc%2F6KaULqyrO6sheFu%2BcTr%2BcPCP59VnvFFI4IRxPNVS30aKRHGmn%2BWrmk9xZM%3D&sign_type=RSA";
       // $notifyParamsStr = "amount=1000&com_amt=70&com_amt_type=2&extra_com_amt=0&mer_date=20190314&mer_id=50751&order_id=787761688&ret_code=0000&ret_msg=%E4%BA%A4%E6%98%93%E6%88%90%E5%8A%9F&trade_no=1903141204699317&trade_state=4&transfer_date=20190314&transfer_settle_date=20190314&version=4.0&sign=MLVriuhYPQ1sWeGz5vH4eGmKQDmiXrha8bVoEBl8L7dAaTukXiXES%2FwLLQxDCYcLuvJLUCPAE0BRUaiEVQPVpEg73VHV0hxQ0R3v0Iw4LANl0mAnEFtnDZcxRQhuSOQzrbnXCoxQ7xVVstxDV%2B%2FYDGzTxW0Yx%2FMnhL2JXfM0Hqo%3D&sign_type=RSA";

        //$notifyParamsStr ="amount=1&amt_type=RMB&charset=UTF-8&error_code=0000&media_type=MOBILE&mer_date=20190409&mer_id=60439102&open_id=oEufkwCVGcvptjaU5mUsNO3LhgMs&order_id=190409024430119926&pay_date=20190409&pay_seq=ECZ00130201904091444323474258482&pay_type=WECHATWEB&service=pay_result_notify&settle_date=20190409&trade_no=3904091444123362&trade_state=TRADE_SUCCESS&version=4.0&sign=AfxDTtNb7BSku6licNl3qYQ9pqAwANzGtPvrKURcfouKak%2B07sTb%2FRLZqZHWrmAdqH4CbvAoeiZGyFn5nHVX5kImqfyQo%2F%2BvC26g8pOoyj2DHVhEt2WPdkR4JUiGUXFMUbqJVgd1f%2Fr9vJPGSz5Q4tcaSlUK0Cd8Qf8LlvhCrOs%3D&sign_type=RSA";
        $service = new UmfService("60000100","D:/60000100_.key.pem");
        $array = $service->notifyDataParserMap($notifyParamsStr);
        $mer2UmfPlatStr = $service->responseUMFMap($array);
        //var_dump($array);
        echo("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">");
        echo("<HTML>");
        echo(" <HEAD><META NAME=\"MobilePayPlatform\" CONTENT=\"" . $mer2UmfPlatStr . "\"/></HEAD>");
        echo(" <BODY>");
        echo("</BODY>");
        echo("</HTML>");
    }



    //------------------------------------------------------------
    // APP支付
    //------------------------------------------------------------
    /**
     * APP支付下单
     */
    public function test_mobileOrderMap(){
        $params = array(
            "mer_id"=>"60000100",
            "notify_url"=>"http://www.xxx.com/xxxx.php",
            "ret_url"=>"http://www.xxx.com/xxxx.php",
            "goods_id"=>"",
            "goods_inf"=>"商品描述",
            "media_id"=>"",
            "media_type"=>"",
            "order_id"=>rand(0,999999999),
            "mer_date"=>date ( "Ymd" ),
            "amount"=>"1",
            "mer_priv"=>"",
            "expand"=>"",
            "user_ip"=>"",
            "expire_time"=>"",
        );
        $service = new UmfService("60000100","D:/60000100_.key.pem");
        $res = $service->mobileOrderMap($params);
        var_dump($res);
    }

    /**
     * APP生成签名
     */
    public function test_mobileGenerateSignMap(){
        $params = array(
            "merId"=>"60000100",
            "orderId"=>"745239257",
            "orderDate"=> "20170929",
            "amount"=>"1",
        );
        $service = new UmfService("60000100","D:/60000100_.key.pem");
        $res = $service->mobileGenerateSignMap($params);
        var_dump($res);
    }


    /**
     * 传入请求参数,成sign字段
     * 生成 "a=1&b=2&c=3&d=4&mer_id=60000100" 的签名
     */
    public function test_generateSign(){
        $params = array(
            "mer_id"=>"60000100",// mer_id 是必传参数
            "c"=>"3",
            "d"=>"4",
            "b"=>"2",
            "a"=>"1",
        );
        $service = new UmfService("60000100","D:/60000100_.key.pem");
        $res = $service->generateSign($params);
        var_dump($res);
    }


    /**
     * 验签方法
     * @param string $plain 联动返回明文字符串按序拼接(明文串去除sign_type字段)和
     * @param string $sign 密文sign字段
     * @return bool
     */
    public function test_verifySign(){
        $plain =
            "amount=1&amt_type=RMB&bank_payurl=&mer_date=20180829&mer_id=60000100&mer_priv=&order_id=15319824&platDate=20180829&ret_code=00080502&ret_msg=根据gateId无法找到支付银行,PSP170451a51a482&trade_no=3808291704813123&trade_state=WAIT_BUYER_PAY&version=4.0";
    $sign =  "ExKMJsC0djEgQrqgbgtf/mp7oPrwt5imNDW81fU14hxLphSfiG2RQvUzd75VSmonNzgzs3GtP6UTuoebIHzvb43ceglE0wgwpoDAwrvOPHpaqtymEX7GXeOzbew9XjSEH3FgdBGuxD6FaMfuMCIyNBM8LQ+BK4S3YkiFlKc62Sw=";
       // $plain =
            //"amount=1&amt_type=RMB&bank_payurl=&mer_date=20180829&mer_id=52079&mer_priv=&order_id=1234567789088777&platDate=20180829&ret_code=00252020&ret_msg=系统忙！请稍后再试！,PSP163919bca9cfe&trade_no=2808291224125993&trade_state=WAIT_BUYER_PAY&version=4.0";
        //$sign =  "qMkQX352Gp6QSiuXCUC8c8NIR7bHRnGgO+D6r0oPqzqEemyC3k5MLHexPrhifaVLp7YT3BoIyBKPCN3FSznBz5CXOXw2L/j6ByqJorAJemYk8Xn+BWVbjQetPfqaiLB8PQ9K/pNiqvIN4s/9N2T8BsDo20B13DxTfPVmgCqrNuk=";

        $service = new UmfService("60000100","D:/60000100_.key.pem");
        $res = $service->verifySign($plain,$sign);
        var_dump($res);
    }


    /**
     * 添加新私钥
     */
    public function test_addMerPrivateCertMap(){
        UmfService::addMerPrivateCertMap("60000100","D:/60000100_.key.pem");
        UmfService::addMerPrivateCertMap("60000101","D:/60000100_.key.pem");
        var_dump(UmfService::$privateKeyPath);
    }


    //----------------------------------------
    // 子商户入网部分
    //----------------------------------------
    /**
     * 子商户入网--获取token方法
     */
    public function test_getTokenMap(){
        $params = array(
            "grant_type"=>"client_credentials",
            "client_id"=>"79756098a6d291260523f6199ddfc4bc0875ccf6",
            "client_secret"=>"f642ab9bd929ab0d7f802e12dc689e416d4b8e6a"
        );
        $service = new UmfService("60038402","D:/60038402_.key.pem");;
        $res = $service->getTokenMap($params);
        var_dump($res);
    }

    /**
     * 子商户入网--添加子商户方法
     */
    public function test_addChildMerInfoMap(){
        $params = array(
            "token" => "839836d8848f267ffee93d3005388bc18cba67b57e64b5c15eaf814a06a8ea1a",
            "merId"=>"60038402",
            "merName"=>"十九",
            "merType"=>"1",
            "contActsName"=>"白居易",
            "mobileNo"=>"13911698741",
            "licenseType"=>"1",
            "licenseNo"=>"91140100743546791A",
            "organizationId"=>"",
            "taxPayerNum"=>"",
            "lawyer"=>"李白",
            "cardNo"=>"143624199210050537",
            "bankName"=>"招商银行",
            "bankAccount"=>"4638792345247569",
            "province"=>"010",
            "areaCode"=>"010",
            "merNotifyUrl"=>"http://www.baidu.com",
            "pubPriFlag"=>"1",
            "bankBrhName"=>"招商银行"
        );
        $service = new UmfService("60038402","D:/60000100_.key.pem");
        $res = $service->addChildMerInfoMap($params);
        var_dump($res);
    }

    /**
     * 子商户入网--上传商户资质方法
     */
    public function test_uploadMerFileMap(){
        $params = array(
            "token" => "839836d8848f267ffee93d3005388bc18cba67b57e64b5c15eaf814a06a8ea1a",
            "licenseNo"=>"91140100743546791A",
            "merId"=>"60038402"
        );
        $files = array(
            "C:/APPLY_TABLE.jpg",
            "C:/BANK_ACCOUNT_LICENCE.jpg",
            "C:/BUSSINESS_LICENSE.jpg",
            "C:/ID_CARD_BACK.jpg",
            "C:/ID_CARD_FRONT.jpg"
        );
        $service = new UmfService("60038402","D:/60000100_.key.pem");
        $res = $service->uploadMerFileMap($params,$files);
        var_dump($res);
    }

    /**
     * 子商户入网--修改子商户信息方法
     */
    public function test_changeChildMerInfoMap(){
        $params = array(
            "token" => "839836d8848f267ffee93d3005388bc18cba67b57e64b5c15eaf814a06a8ea1a",
            "merId"=>"60038402",
            "merName"=>"十九",
            "merType"=>"1",
            "contActsName"=>"白居易",
            "mobileNo"=>"13911698741",
            "licenseType"=>"1",
            "licenseNo"=>"91140100743546791A",
            "organizationId"=>"",
            "taxPayerNum"=>"",
            "lawyer"=>"李白",
            "cardNo"=>"143624199210050537",
            "bankName"=>"招商银行",
            "bankAccount"=>"4638792345247569",
            "province"=>"010",
            "areaCode"=>"010",
            "merNotifyUrl"=>"http://www.baidu.com",
            "pubPriFlag"=>"1",
            "bankBrhName"=>"招商银行"
        );
        $service = new UmfService("60038402","D:/60000100_.key.pem");
        $res = $service->changeChildMerInfoMap($params);
        var_dump($res);
    }

    /**
     * 子商户入网--查询审核状态方法
     */
    public function test_queryMerStateMap(){
        $params = array(
            "token" => "839836d8848f267ffee93d3005388bc18cba67b57e64b5c15eaf814a06a8ea1a",
            "merId"=>"60038402",
            "licenseNo"=>"91140100743546791A"
        );
        $service = new UmfService("60038402","D:/60038402_.key.pem");;
        $res = $service->queryMerStateMap($params);
        var_dump($res);
    }

    /**
     * 子商户入网--异步通知
     */
    public function test_childDoGet(){
        //$notifyChildParamsStr = "checkState=2&licenseNo=98761131653694400B&merId=60038402&noPassInfo=%E5%AD%90%E5%95%86%E6%88%B7%EF%BC%9AFills944%E5%AE%A1%E6%A0%B8%E6%88%90%E5%8A%9F%EF%BC%81&subMerId=20000560&sign=VNVeO%2B%2FxPNEmwh9eXP4Aup0gJRQ4e6DqMY1PSv4sOF25skRgm23H2Mmy7b7sWu%2BAKGBLoOTlzdGpw%2Bmv5PwQgOfBgo8ykciIYIlwTt0yhtvAAzzvsOsLqx6Lr0A0p%2B5ubAWisk%2FB2KOMWWScHo8uZgnPIc%2FEeU2p0W%2BaYK2Pzzw%3D";
        //$notifyChildParamsStr = "amount=500000&com_amt=80&com_amt_type=-99&mer_date=20180305&mer_id=9602&order_id=719540&ret_code=0000&ret_msg=%E4%BA%A4%E6%98%93%E6%88%90%E5%8A%9F&trade_no=1803051026644972&trade_state=4&transfer_date=20180305&transfer_settle_date=20180305&version=4.0&sign=JxyCw7CmvfsfyB1UkHLNyNrdql23O3sz09CvzKgNb00tATSxGoUFUBUMWAyoH3Kz21M%2Br35lCd2u6Zi6dY2nHnAD0skwiIN9H%2Fr4uHRPPMzhzTbvQtB3hopI8hHB7XjS31hDGKnh4bbxLX2SrZ%2BEzVgKow6O17clFpALYzKZ1Ro%3D&sign_type=RSA";
        $notifyChildParamsStr = "amount=200000&amt_type=RMB&charset=UTF-8&error_code=0000&gate_id=CMB&last_four_cardid=9800&media_id=18358258638&media_type=MOBILE&mer_date=20180305&mer_id=8180&order_id=201803059127121&pay_date=20180305&pay_type=CREDITCARD&service=pay_result_notify&settle_date=20180305&trade_no=3803051650163801&trade_state=TRADE_SUCCESS&version=4.0&sign=p%2Fonvrt6%2FAD%2FfyWMdw7JRo3o%2BvQWQuyvxcJgXBANvp4vt%2BkNNkYs6Mayo1rFMdRGQfZnGB1XZuC3ffybSwntFz4wfSIahe277oirgNqbkBZ3i9kse9wQPV1E3bNzOywQb7cNBYoQIzfRxkQf7jmKdvUB%2BKLINR6K0PI6tAVokDo%3D&sign_type=RSA";
        //$service = new UmfService("60038402","D:/60038402_.key.pem");
        $service = new UmfService("60000100","D:/60000100_.key.pem");
        $array = $service->notifyChildParserMap($notifyChildParamsStr);
        $mer2UmfPlatStr = $service->responseChildNotifyMap($array);
        //调用SDK生成返回联动平台字符串，加到CONTENT中
        echo("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">");
        echo("<HTML>");
        echo(" <HEAD><META NAME=\"MobilePayPlatform\" CONTENT=\"" . $mer2UmfPlatStr . "\"/></HEAD>");
        echo(" <BODY>");
        echo("</BODY>");
        echo("</HTML>");
    }
    /**
     * 账户通---子商户提现
     */
    public function test_ChildWithdrawalsMap(){
        $params = array(
            "mer_id" => "60016102",
            "order_id"=>"201804030726",
            "mer_date"=>"20180403",
            "amount"=>"1",
            "sub_mer_id"=>"20000585",
            "set_com_type"=>"1"
        );
        $service = new UmfService("60016102","D:/60016102_.key.pem");
        $res = $service->withdrawalsMap($params);
        var_dump($res);
    }
    /**
     * 账户通---子商户提现查询
     */
    public function test_QueryChildWithdrawalsMap(){
        $params = array(
            "mer_id" => "60016102",
            "order_id"=>"201804030726",
            "mer_date"=>"20180403",
            "sub_mer_id"=>"20000583"
        );

        //$service = new UmfService("60000100","D:/60000100_.key.pem");
        $service = new UmfService("60016102","D:/60016102_.key.pem");
        $res = $service->queryWithdrawalsMap($params);
        var_dump($res);
    }
    /**
     * 账户通---子商户余额查询
     */
    public function test_QueryChildBalancesMap(){
        $params = array(
            "mer_id" => "60016102",
            "sub_mer_id"=>"20000583"
        );
        $service = new UmfService("60016102","D:/60016102_.key.pem");
        $res = $service->querySubBalanceMap($params);
        var_dump($res);
    }
    //----------------------------------------
    // 分账部分
    //----------------------------------------
    /**
     * 分账—分账请求针对标准分账的延时分账
     */
    public function test_splitReqMap(){
        $params = array(
            "mer_id"=>"60038402",
            "order_id"=>"6262051263",
            "mer_date"=>"20171106"
        );
        $service = new UmfService("60038402","D:/60038402_.key.pem");
        $res = $service->splitReqMap($params);
        var_dump($res);
    }


    /**
     * 分账---分账状态查询
     */
    public function test_splitStateMap(){
        $params = array(
            "mer_id"=>"60038402",
            "order_id"=>"20171025156977045",//rand(0,999999999),
            "mer_date"=>"20171025",//date ( "Ymd" )
        );
        $service = new UmfService("60038402","D:/60000100_.key.pem");
        $res = $service->splitStateMap($params);
        var_dump($res);
    }


    /**
     * 分账---分账退费
     */
    public function test_splitRefundMap(){
        $params = array(
            "mer_id"=>"60038402",
            "refund_no"=>"17102711311234",
            "order_id"=>"20171025156977045",
            "mer_date"=> "20171025",
            "org_amount"=>"2",
        );
        $service = new UmfService("60038402","D:/60000100_.key.pem");
        $res = $service->splitRefundMap($params);
        var_dump($res);
    }

    /**
     * 分账---分账退费子订单请求
     */
    public function test_massTransferRefundMap(){
        $params = array(
            "mer_id"=>"60000100",
            "mer_date"=>"20170913",
            "order_id"=>"8155547220",
            "refund_no"=>"1709131750281234",
            "refund_amount"=>"1",
            "org_amount"=>"1",
            "notify_url"=>"http://www.xxx.com/xxxx.php",
            "sub_mer_id"=>"2000",
            "sub_order_id"=>"20144546131213",
            "refund_desc"=>"测试",
        );
        $service = new UmfService("60000100","D:/60000100_.key.pem");
        $res = $service->splitSubRefundMap($params);
        var_dump($res);
    }


    /**
     * 分账---分账文件下载
     */
    public function test_splitFileDownloadMap(){
        $params = array(
            "mer_id"=>"60038402",
            "settle_date"=>"20171024",
            "settle_path"=>"C:/umf_settle",
            "settle_type"=>"SPLITDETAIL"
        );
        $service = new UmfService("60038402","D:/60000100_.key.pem");
        $res = $service->splitFileDownloadMap($params);
        var_dump($res);
    }


    //----------------------------------------
    // E秒付
    //----------------------------------------
    /**
     * E秒付
     */
    public function test_epaymentOrderMap(){
        $params = array(
            "mer_id"=>"60000100",
            "notify_url"=>"www.xxx.com",
            "order_id"=>rand(0,999999999),
            "mer_date"=> date ("Ymd"),
            "amount"=>"10",
            "recv_account_type"=>"00",
            "recv_bank_acc_pro"=>"0",
            "recv_user_name"=>"张三",
            "recv_account"=>"xxxxxxxxxxxxxxxx",
            "cut_fee_type"=>"2",
        );
        $service = new UmfService("60000100","D:/60000100_.key.pem");
        $res = $service->epaymentOrderMap($params);
        var_dump($res);
    }


    /**
     * 立码付API
     */
    public function test_publicVerticalMap(){
        $params = array(
            "mer_id"=>"60000100",
            "notify_url"=>"http://xxx.xxx.com",
            "goods_inf"=>"测试商品",
            "order_id"=>rand(0,999999999),
            "mer_date"=>date("Ymd"),
            "amount"=>"1",
            "app_id"=>"xxxxxxxx",
            "open_id"=>"xxxxxxxx",
            "scancode_type"=>"WECHAT",
        );
        $service = new UmfService("60000100","D:/60000100_.key.pem");
        $res = $service->publicVerticalMap($params);
        var_dump($res);
    }

    /**
     * 小程序API
     */
    public function test_miniProgramPayMap(){
        $params = array(
            "mer_id"=>"60000100",
            "notify_url"=>"http://xxx.xxx.com",
            "goods_inf"=>"测试商品",
            "order_id"=>rand(0,999999999),
            "mer_date"=>date("Ymd"),
            "amount"=>"1",
            "app_id"=>"xxxxxxxx",
            "open_id"=>"xxxxxxxx",
            "scancode_type"=>"WECHAT",
        );
        $service = new UmfService("60000100","D:/60000100_.key.pem");
        $res = $service->miniProgramPayMap($params);
        var_dump($res);
    }

    /**
     * 微信主扫线上
     */
    public function test_activeOnlineScanPayMap(){
        $params = array(
            "mer_id"=>"60000100",
            "notify_url"=>"http://xxx.xxx.com",
            "goods_inf"=>"测试商品",
            "order_id"=>rand(0,999999999),
            "mer_date"=>date("Ymd"),
            "amount"=>"1",
            "app_id"=>"xxxxxxxx",
            "open_id"=>"xxxxxxxx",
            "scancode_type"=>"WECHAT",
        );
        $service = new UmfService("60000100","D:/60000100_.key.pem");
        $res = $service->activeOnlineScanPayMap($params);
        var_dump($res);
    }

    /**
     * 公众号线上
     */
    public function test_publicOnlinePayMap(){
        $params = array(
            "mer_id"=>"60000100",
            "notify_url"=>"http://xxx.xxx.com",
            "goods_inf"=>"测试商品",
            "order_id"=>rand(0,999999999),
            "mer_date"=>date("Ymd"),
            "amount"=>"1",
            "app_id"=>"xxxxxxxx",
            "open_id"=>"xxxxxxxx",
            "scancode_type"=>"WECHAT",
        );
        $service = new UmfService("60000100","D:/60000100_.key.pem");
        $res = $service->publicOnlinePayMap($params);
        var_dump($res);
    }

    /**
     * 公众号线下
     */
    public function test_publicUnderlinePayMap(){
        $params = array(
            "mer_id"=>"60000100",
            "notify_url"=>"http://xxx.xxx.com",
            "goods_inf"=>"测试商品",
            "order_id"=>rand(0,999999999),
            "mer_date"=>date("Ymd"),
            "amount"=>"1",
            "app_id"=>"xxxxxxxx",
            "open_id"=>"xxxxxxxx",
            "scancode_type"=>"WECHAT",
        );
        $service = new UmfService("60000100","D:/60000100_.key.pem");
        $res = $service->publicUnderlinePayMap($params);
        var_dump($res);
    }

    /**
     * 立马付下单获取用户标识
     */
    public function test_prepareGetUserUnionMap(){
        $params = array(
            "mer_id"=>"60000100",
            "order_id"=>rand(0,999999999),
            "mer_date"=>date("Ymd"),
            "amount"=>"1",
            "userAuthCode"=>"11111111111111111111",
            "respCode"=>"00",
        );
        $service = new UmfService("60000100","D:/60000100_.key.pem");
        $res = $service->getUseridUnionMap($params);
        var_dump($res);
    }

    /**
     * 分期用户手续费查询
     */
    public function test_prepareQuerystageUserStageNumMap(){
        $params = array(
            "mer_id"=>"60000100",
            "order_id"=>rand(0,999999999),
            "amount"=>"1",
            "staging_num"=>"06",
            "discount_mode"=>"01",
        );
        $service = new UmfService("60000100","D:/60000100_.key.pem");
        $res = $service->queryUserStagingComamtMap($params);
        var_dump($res);
    }

    /**
     * 一键支付下单
     */
    public function test_quickOrderMap(){
        $params = array(
            "mer_id"=>"60000100",
            "goods_inf"=>"测试商品",
            "order_id"=>rand(0,999999999),
            "mer_date"=>date("Ymd"),
            "amount"=>"1"
        );
        $service = new UmfService("60000100","D:/60000100_.key.pem");
        $res = $service->quickOrderMap($params);
        var_dump($res);
    }

    /**
     * 一键支付下单获取短信
     */
    public function test_getSMSMap(){
        $params = array(
            "mer_id"=>"60000100",
            "trade_no"=>"3801261003248915",
            "media_id"=>rand(0,999999999),
            "amount"=>"1"
        );
        $service = new UmfService("60000100","D:/60000100_.key.pem");
        $res = $service->getSMSMap($params);
        var_dump($res);
    }

    /**
     * 一键支付协议支付、商业委托下单获取短信
     */
    public function test_getAgreementSMSMap(){
        $params = array(
            "mer_id"=>"60000100",
            "trade_no"=>"3801261003248915",
            "mer_cust_id"=>"13333333333",
            "usr_pay_agreement_id"=>"P2017111208553000000000033565625"
        );
        $service = new UmfService("60000100","D:/60000100_.key.pem");
        $res = $service->getSMSMap($params);
        var_dump($res);
    }
    /**
     * 一键支付首次支付
     */
    public function test_FirstPaymentMap(){
        $params = array(
            "mer_id"=>"60000100",
            "trade_no"=>"3801261003248915",
            "identity_code"=>"111111111111111",
            "card_id"=>"11111111111111",
            "card_holder"=>"张三",
            "media_type"=>"MOBILE",
            "media_id"=>"13333333333",
            "verify_code"=>"1111",
        );
        $service = new UmfService("60000100","D:/60000100_.key.pem");
        $res = $service->FirstPaymentMap($params);
        var_dump($res);
    }

    /**
     * 一键支付协议支付
     */
    public function test_agreementPaymentMap(){
        $params = array(
            "mer_id"=>"60000100",
            "trade_no"=>"3801261003248915",
            "usr_pay_agreement_id"=>"111111111111111",
            "mer_cust_id"=>"11111111111111",
            "verify_code"=>"1111",
        );
        $service = new UmfService("60000100","D:/60000100_.key.pem");
        $res = $service->agreementPaymentMap($params);
        var_dump($res);
    }

    /**
     * 一键支付查询商户支持的银行卡列表
     */
    public function test_supportBankMap(){
        $params = array(
            "mer_id"=>"60000100",
            "pay_type"=>"DEBITCARD"
        );
        $service = new UmfService("60000100","D:/60000100_.key.pem");
        $res = $service->supportBankMap($params);
        var_dump($res);
    }

    /**
     * 一键支付查询商户用户已签约的银行列表
     */
    public function test_signedBankMap(){
        $params = array(
            "mer_id"=>"60000100",
            "pay_type"=>"DEBITCARD",
            "mer_cust_id"=>"11111111111111"
        );
        $service = new UmfService("60000100","D:/60000100_.key.pem");
        $res = $service->querySignedBankMapNew($params);
        var_dump($res);
    }

    /**
     * 一键支付用户签约下短信
     */
    public function test_bindingApplicationMap(){
        $params = array(
            "mer_id"=>"60000100",
            "card_id"=>"11111111111111",
            "media_id"=>rand(0,999999999),
        );
        $service = new UmfService("60000100","D:/60000100_.key.pem");
        $res = $service->bindingApplicationMap($params);
        var_dump($res);
    }

    /**
     * 一键支付签约确认
     */
    public function test_bindingConfirmationMap(){
        $params = array(
            "mer_id"=>"60000100",
            "card_id"=>"11111111111111",
            "media_id"=>rand(0,999999999),
            "mer_cust_id"=>"11111111111111",
            "verify_code"=>"1111",
            "bind_no"=>"1111"
    );
        $service = new UmfService("60000100","D:/60000100_.key.pem");
        $res = $service->bindingConfirmationMap($params);
        var_dump($res);
    }

    /**
     * 一键支付API解除绑定
     */
    public function test_unbindMap(){
        $params = array(
            "mer_id"=>"60000100",
            "mer_cust_id"=>"11111111111111",
        );
        $service = new UmfService("60000100","D:/60000100_.key.pem");
        $res = $service->unbindMap($params);
        var_dump($res);
    }

    /**
     * 商业委托---签约下单及获取短信验证码
     */
    public function test_CommercialSignOrderMap(){
        $params = array(
            "mer_id"=>"60000100",
            "mer_cust_id"=>"11111111111111",
            "media_id"=>"133333333333",
            "card_id"=>"60000100",
            "identity_type"=>"1",
            "identity_code"=>"1000000000000",
            "card_holder"=>"张三",
        );
        $service = new UmfService("60000100","D:/60000100_.key.pem");
        $res = $service->CommercialSignOrderMap($params);
        var_dump($res);
    }

    /**
     * 商业委托---签约确认
     */
    public function test_CommercialSignConfirmMap(){
        $params = array(
            "mer_id"=>"60000100",
            "bind_id"=>"11111111111111",
            "verify_code"=>"1234",
        );
        $service = new UmfService("60000100","D:/60000100_.key.pem");
        $res = $service->CommercialSignConfirmMap($params);
        var_dump($res);
    }



    /**
     * 借记卡认证支付下单
     */
    public function test_debitAuthenPayOrderMap(){
        $params = array(
            "mer_id"=>"60000100",
            "notify_url"=>"",
            "order_id"=>rand(0,999999999),
            "mer_date"=>date("Ymd"),
            "amount"=>"1"
        );
        $service = new UmfService("60000100","D:/60000100_.key.pem");
        $res = $service->debitAuthenPayOrderMap($params);
        var_dump($res);
    }

    /**
     * 借记卡认证支付获取短信验证码
     */
    public function test_debitAuthenPayGetVerifyCodeMap(){
        $params = array(
            "mer_id"=>"60000100",
            "trade_no"=>"3801261003248915",
            "media_id"=>"1333333333",
        );
        $service = new UmfService("60000100","D:/60000100_.key.pem");
        $res = $service->debitAuthenPayGetVerifyCodeMap($params);
        var_dump($res);
    }

    /**
     * 借记卡认证支付确认支付
     */
    public function test_debitAuthenPayConfirmMap(){
        $params = array(
            "mer_id"=>"60000100",
            "card_id"=>"11111111111111",
            "media_id"=>"1333333333",
            "trade_no"=>"3801261003248915",
            "identity_code"=>"11111111111111",
            "card_holder"=>"张三",
        );
        $service = new UmfService("60000100","D:/60000100_.key.pem");
        $res = $service->debitAuthenPayConfirmMap($params);
        var_dump($res);
    }



}

?>

