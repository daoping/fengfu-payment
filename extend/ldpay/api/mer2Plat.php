<?php
require_once 'mer2PlatUtil.php';

Class MerToPlat{
	/**
	 * @param $map HashMap
	 * @return ReqData
	 */
	public static function makeRequestDataByGet($map){
		$util = new ReqDataUtil();
		$reqData = $util->makeRequestData($map,UMF_METHOD_GET);
		return $reqData;
	}
	/**
	 * @param $map HashMap
	 * @return ReqData
	 */
	public static function makeRequestDataByPost($map){
		$util = new ReqDataUtil();
		$reqData = $util->makeRequestData($map,UMF_METHOD_POST);
		return $reqData;
	}

}


?>