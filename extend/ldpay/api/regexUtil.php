<?php

/**订单查询数据字段*/
//define("PLATTOMER_QUERYTRANS_FIELD", "merId,goodsId,orderId,merDate,payDate,amount,amtType,bankType,mobileId,gateId,transType,transState,settleDate,bankCheck,merPriv,retCode,version,sign");
/**商户撤销交易数据字段*/
define("PLATTOMER_REVOKE_FIELD", "merId,amount,retCode,retMsg,version,sign");
/**商户退费交易数据字段*/
define("PLATTOMER_REFUND_FIELD", "merId,refundNo,amount,retCode,retMsg,version,sign");
/**后台直连数据字段*/
define("PLATTOMER_DIRECTREQPAY_FIELD", "merId,goodsId,orderId,merDate,retCode,retMsg,version,sign");
/**2011-10-14 add by xiajiajia*/
/** 一般支付请求*/
define("PAY_REQ_RULE", "service,charset,mer_id,sign_type,version,order_id,mer_date,amount,amt_type");
/** IVR支付方式下单*/
define("PAY_REQ_IVR_CALL_RULE", "service,charset,mer_id,sign_type,version,order_id,mer_date,amount,amt_type");
/** IVR转呼方式下单*/
define("PAY_REQ_IVR_TCALL_RULE", "service,charset,mer_id,sign_type,version,order_id,mer_date,amount,amt_type");
/** 商户查询订单状态*/
define("QUERY_ORDER_RULE","service,charset,sign_type,mer_id,version,mer_date");
/** 商户撤销交易*/
define("MER_CANCEL_RULE","service,charset,sign_type,mer_id,version,order_id,mer_date,amount");
/** 商户退费*/
define("MER_REFUND_RULE", "service,charset,sign_type,mer_id,version,refund_no,order_id,mer_date,org_amount");//,refund_amount
/** 下载对账文件*/
define("DOWNLOAD_SETTLE_FILE_RULE", "service,sign_type,mer_id,version,settle_date");
/** 分账前端支付请求*/
define("PAY_REQ_SPLIT_FRONT_RULE", "service,charset,mer_id,sign_type,version,order_id,mer_date,amount,amt_type");
/** 分账后端支付请求*/
define("PAY_REQ_SPLIT_BACK_RULE", "service,charset,mer_id,sign_type,version,order_id,mer_date,amount,amt_type");
/** 分账退费*/
define("SPLIT_REFUND_REQ_RULE", "service,charset,mer_id,sign_type,version,refund_no,order_id,mer_date,refund_amount,org_amount,sub_mer_id,sub_order_id");
/** 直连网银*/
define("PAY_REQ_SPLIT_DIRECT_RULE", "service,charset,mer_id,sign_type,version,order_id,mer_date,amount,amt_type");
/** 交易结果通知*/
define("PAY_RESULT_NOTIFY_RULE", "service,charset,mer_id,sign_type,version,trade_no,order_id,mer_date,pay_date,amount,amt_type,pay_type,settle_date,trade_state");
/** 分账结果通知*/
define("SPLIT_REQ_RESULT_RULE", "service,charset,mer_id,sign_type,version,order_id,mer_date,is_success");
/** 分账退费结果通知*/
define("SPLIT_REFUND_RESULT_RULE", "service,charset,sign_type,mer_id,version,refund_no,order_id,mer_date");//,refund_amount,org_amount,refund_amt,sub_mer_id,sub_order_id,sub_refund_amt,is_success
/** 信用卡直连*/
define("CREDIT_DIRECT_PAY_RULE", "service,charset,mer_id,sign_type,version,order_id,mer_date,amount,amt_type,pay_type,card_id,valid_date,cvv2");
/** 借记卡直连*/
define("DEBIT_DIRECT_PAY_RULE", "service,charset,mer_id,sign_type,version,order_id,mer_date,amount,amt_type,pay_type,card_id");
/**预授权直连申请*/
define("PRE_AUTH_DIRECT_REQ", "service,charset,mer_id,sign_type,version,order_id,mer_date,media_id,media_type,amount,amt_type,pay_type,card_id,valid_date,cvv2");
/**预授权完成*/
define("PRE_AUTH_DIRECT_PAY", "service,charset,mer_id,sign_type,version,order_id,trade_no,mer_date,amount,amt_type,pay_type");
/**预授权撤销*/
define("PRE_AUTH_DIRECT_CANCEL", "service,charset,mer_id,sign_type,version,order_id,trade_no,mer_date");
/**银行卡转账注册*/
define("PAY_TRANSFER_REGISTER ", "service,charset,mer_id,res_format,version,sign_type,req_date,req_time,media_type,media_id,identity_type,identity_code,cust_name");
/**银行卡转账申请*/
define("PAY_TRANSFER_REQ", "service,charset,mer_id,ret_url,res_format,version,sign_type,order_id,mer_date,req_time,media_id,media_type,amount,fee_amount,recv_account_type,recv_bank_acc_pro,recv_account,recv_user_name,recv_gate_id,recv_type,purpose");
/**银行卡转账订单查询*/
define("PAY_TRANSFER_ORDER_QUERY", "service,charset,mer_id,res_format,version,sign_type,order_id,mer_date");
/**银行卡转账退费*/
define("PAY_TRANSFER_MER_REFUND", "service,charset,mer_id,res_format,version,sign_type,refund_no,order_id,mer_date");
/**预授权查询*/
define("PRE_AUTH_DIRECT_QUERY", "service,charset,mer_id,sign_type,version,order_id,mer_date");
/**预授权退费*/
define("PRE_AUTH_DIRECT_REFUND", "service,charset,sign_type,mer_id,version,order_id,mer_date,refund_no,refund_amount,org_amount");
/**预授权下载对账文件*/
define("PRE_AUTH_DIRECT_SETTLE", "service,sign_type,mer_id,version,settle_date");
/**实名认证*/
define("CARD_AUTH", "service,charset,mer_id,sign_type,version,mer_date,card_id");
/**信用卡API快捷---获取短信验证码*/
define("REQ_SMS_VERIFYCODE", "service,mer_id,charset,sign_type,version,trade_no,media_id,media_type");
/**信用卡API快捷---确认支付*/
define("PAY_CONFIRM", "service,mer_id,charset,sign_type,version,trade_no,pay_category,card_id");
/**一键快捷--前端请求*/
define("PAY_REQ_SHORTCUT_FRONT", "service,charset,mer_id,sign_type,version,order_id,mer_date,amount,amt_type,pay_type,gate_id");
/**一键快捷--API下单*/
define("PAY_REQ_SHORTCUT", "service,charset,mer_id,sign_type,version,order_id,mer_date,amount,amt_type");
/**一键快捷--(首次支付)确认支付*/
define("FIRST_PAY_CONFIRM_SHORTCUT", "service,mer_id,charset,sign_type,version,trade_no,media_id,media_type,card_id");
/**一键快捷--（协议支付)确认支付*/
define("AGREEMENT_PAY_CONFIRM_SHORTCUT", "service,mer_id,charset,sign_type,version,trade_no,usr_pay_agreement_id");
/**一键快捷--获取短信验证码*/
define("REQ_SMSVERIFY_SHORTCUT", "service,mer_id,sign_type,version,trade_no");
/**一键快捷--查询商户支持的银行列表*/
define("QUERY_MER_BANK_SHORTCUT", "service,sign_type,charset,mer_id,version,pay_type");
/**一键快捷--查询用户签约的银行列表*/
define("QUERY_MERCUST_BANK_SHORTCUT", "service,sign_type,charset,mer_id,version,pay_type");
/**一键快捷--商户解除用户关联*/
define("UNBIND_MERCUST_PROTOCOL_SHORTCUT", "service,sign_type,charset,mer_id,version");
/**分账项目--分账指令*/
define("SPLIT_REQ_RULE", "service,charset,mer_id,sign_type,version,order_id,mer_date");
/**分账项目--分账状态查询*/
define("QUERY_SPLIT_ORDER_RULE", "service,sign_type,charset,mer_id,version,order_id,mer_date");
/**付款API直连--付款请求*/
define("TRANSFER_DIRECT_REQ_RULE", "service,charset,mer_id,version,sign_type,order_id,mer_date,amount,recv_account_type,recv_bank_acc_pro,recv_account,recv_user_name");
/**付款API直连--付款查询*/
define("TRANSFER_QUERY_RULE", "service,charset,mer_id,version,sign_type,order_id,mer_date");
/**历史订单查询*/
define("MER_ORDER_INFO_QUERY", "service,sign_type,charset,mer_id,version,mer_date");//order_id,trade_no
/**退费订单状态查询*/
define("MER_REFUND_QUERY", "service,sign_type,charset,mer_id,version,refund_no");
/**聚合支付--微信、支付宝扫码支付*/
define("ACTIVE_SCANCODE_ORDER", "service,charset,mer_id,sign_type,version,goods_inf,order_id,mer_date,amount,amt_type,scancode_type");
define("ACTIVE_SCANCODE_ORDER_NEW", "service,charset,mer_id,sign_type,version,goods_inf,order_id,mer_date,amount,amt_type,scancode_type");

/**聚合支付--微信、支付宝被扫*/
define("PASSIVE_SCANCODE_PAY", "service,charset,mer_id,sign_type,version,goods_inf,order_id,mer_date,amount,amt_type,auth_code,use_desc,scancode_type");
/**付款--查询结算账户余额*/
define("QUERY_ACCOUNT_BALANCE", "service,charset,mer_id,version,sign_type");
/**公共验证产品*/
define("COMM_AUTH", "service,charset,mer_id,sign_type,version,auth_type,order_id");

/**快捷支付2.2产品-下单*/
define("QUICK_PAY", "service,charset,mer_id,sign_type,version,order_id,order_id,mer_date,amount,amt_type,pay_type,gate_id");
/**商户向平台请求获取短信验证码*/
define("GET_MESSAGE", "service,charset,mer_id,sign_type,version,trade_no,media_id,media_type");
/**快捷支付中的确认支付*/
define("QUICK_PAY_FIRST", "service,charset,mer_id,sign_type,version,trade_no,trade_no,verify_code,media_type,media_id");
/**获取商户支持的银行列表*/
define("GET_BANK_MER", "service,charset,mer_id,sign_type,version,pay_type");
/**快捷支付解约列表*/
define("CANCEL_SURRENDER", "service,charset,mer_id,sign_type,version");

/**退费消息补录*/
define("REFUND_INFO_REPLENISH", "service,charset,mer_id,sign_type,version,refund_no,card_holder,card_id");
/**一键快捷API绑定申请*/
define("REQ_BIND_VERIFY_SHORCUT", "service,mer_id,sign_type,version,media_type,media_id,card_id");
/**一键快捷API绑定确认*/
define("REQ_BIND_CONFIRM_SHORCUT", "service,mer_id,sign_type,charset,version,media_type,media_id,card_id");
/**一键快捷绑定结果通知*/
define("BIND_AGREEMENT_NOTIFY_SHORCUT", "service,mer_id,sign_type,version,mer_cust_id,media_type,media_id,usr_busi_agreement_id,usr_pay_agreement_id,gate_id,last_four_cardid,bank_card_type");
/**一键快捷前台绑定请求*/
define("BIND_REQ_SHORTCUT_FRONT", "service,mer_id,sign_type,charset,version,pay_type,gate_id,mer_cust_id");


/**商业委托签约下短信*/
define("COM_SIGN_ORDER","service,mer_id,sign_type,charset,media_id,media_type,card_id");

/**商业委托签约确认*/
define("COM_BIND_CONFIRM","service,mer_id,sign_type,charset,bind_id,verify_code");

/**子商户提现*/
define("EDRAW_TRANS_MAIN ", "service,charset,mer_id,sign_type,version");

/**子商户提现查询*/
define("QUERY_EDRAW_TRANS_MAIN", "service,charset,mer_id,sign_type,version");

/**子商户余额查询*/
define("QUERY_ACCOUNT_BALANCE_MAIN", "service,charset,mer_id,sign_type,version");



/**
 * 正则表达式校验工具类
 */
Class RegexUtil{

public static $serviceRule = array(
    "pay_req"=>PAY_REQ_RULE,
    "query_order"=>QUERY_ORDER_RULE,
    "mer_cancel"=>MER_CANCEL_RULE,
    "mer_refund"=>MER_REFUND_RULE,
    "download_settle_file"=>DOWNLOAD_SETTLE_FILE_RULE,
    "split_refund_req"=>SPLIT_REFUND_REQ_RULE,
    "query_mer_bank_shortcut"=>QUERY_MER_BANK_SHORTCUT,
    "unbind_mercust_protocol_shortcut"=>UNBIND_MERCUST_PROTOCOL_SHORTCUT,
    "transfer_direct_req"=>TRANSFER_DIRECT_REQ_RULE,
    "transfer_query"=>TRANSFER_QUERY_RULE,
    "mer_order_info_query"=>MER_ORDER_INFO_QUERY,
    "mer_refund_query"=>MER_REFUND_QUERY,
    "active_scancode_order"=>ACTIVE_SCANCODE_ORDER,
    "active_scancode_order_new"=>ACTIVE_SCANCODE_ORDER_NEW,
    "passive_scancode_pay"=>PASSIVE_SCANCODE_PAY,
    "query_account_balance"=>QUERY_ACCOUNT_BALANCE,
    "comm_auth"=>COMM_AUTH,
    "apply_pay_shortcut"=>QUICK_PAY,
    "sms_req_shortcut"=>GET_MESSAGE,
    "confirm_pay_shortcut"=>QUICK_PAY_FIRST,
    "refund_info_replenish"=>REFUND_INFO_REPLENISH,
    "apply_sms_bind_shortcut"=>COM_SIGN_ORDER,
    "confirm_bind_shortcut"=>COM_BIND_CONFIRM,
    "pay_req_shortcut"=>PAY_REQ_SHORTCUT,
    "agreement_pay_confirm_shortcut"=>AGREEMENT_PAY_CONFIRM_SHORTCUT,
);


public static $reqRule = array(
    "service"=>"/^[a-zA-Z0-9_]{1,32}$/",
    "charset"=>"/^(UTF-8|GBK|GB2312|GB18030)$/",
    "mer_id"=>"/^[0-9]{1,8}$/",
    "sign_type"=>"/^RSA$/",
    "version"=>"/^(4.0|1.0)$/",
    "media_type"=>"/^(MOBILE|EMAIL|MERUSERID)$/",
    "order_id"=>"/^\S{1,32}$/",
    "mer_date"=>"/^[1-2][0-9]{7}$/",
    "amount"=>"/^[1-9][0-9]*$/",
    "amt_type"=>"/^RMB$/",
    "expire_time"=>"/^[0-9]{1,32}$/",
    "ret_code"=>"/^[0-9]+$/",
    "trade_no"=>"/^[0-9]{1,16}$/",
    "pay_date"=>"/^[1-2][0-9]{7}$/",
    "settle_date"=>"/^[1-2][0-9]{7}$/",
    "error_code"=>"/^[0-9]*$/",
    "mer_check_date"=>"/^[1-2][0-9]{7}$/",
    "refund_amt"=>"/^[1-9][0-9]*$/",
    "refund_amount"=>"/^[1-9][0-9]*$/",
    "org_amount"=>"/^[1-9][0-9]*$/",
    "split_type"=>"/^[1-2]{0,2}$/",
    "is_success"=>"/^(Y|N)$/",
    "sub_mer_id"=>"/^[0-9]*$/",
    "req_date"=>"/^[1-2][0-9]{7}$/",
    "req_time"=>"/^[0-9]{6}$/",
    "birthday"=>"/^[1-2][0-9]{7}$/",
    "sex"=>"/^(M|F)$/",
    "contact_mobile"=>"^[0-9]{11}$/",
    "fee_amount"=>"/^(0|[1-9][0-9]*)$/",
    "recv_account_type"=>"/^[0-1]{2}$/",
    "recv_bank_acc_pro"=>"/^[0-1]{1}$/",
    "recv_type"=>"/^[0-1]$/",
    "debit_pay_type"=>"^(1|2)$",
    "pay_category"=>"/^(01|02)$/",
    "split_category"=>"/^(1|2|3)$/",
    "push_type"=>"/^(0|1|2|3)$/",
    "order_type"=>"/^(1|2)$/",
    "sign"=>"/^\S+$/",
    "res_format"=>"/^\S+$/",
    "goods_inf"=>"/^\S+$/",
    "token"=>"/^\S+$/",
    "trade_state"=>"/^\S{1,32}$/",
    "refund_no"=>"/^\S{1,16}$/",
    "refund_state"=>"/^\S+$/",
    "sub_order_id"=>"/^\S{1,32}$/",
    "refund_desc"=>"/^\S{1,128}$/",
    "valid_date"=>"/^\S{1,256}$/",
    "cvv2"=>"/^\S{1,256}$/",
    "mail_addr"=>"/^\S{1,64}$/",
    "contact_phone"=>"/^\S+$/",
    "finance_vou_no"=>"/^\S{1,32}$/",
    "purpose"=>"/^\S+$/",
    "prov_name"=>"/^\S+$/",
    "city_name"=>"/^\S+$/",
    "bank_brhname"=>"/^\S+$/",
    "ret_url"=>"/^\S*$/",
    "notify_url"=>"/^\S*$/",
    "goods_id"=>"/^\S*$/",
    "media_id"=>"/^\S+$/",
    "mobile_id"=>"/^\S{0,11}$/",
    "pay_type"=>"/^\S*$/",
    "gate_id"=>"/^\S*$/",
    "mer_priv"=>"/^\S*$/",
    "user_ip"=>"/^\S*$/",
    "expand"=>"/^\S*$/",
    "ret_msg"=>"/^\S*$/",
    "pay_seq"=>"/^\S*$/",
    "bank_check_state"=>"/^\S*$/",
    "product_id"=>"/^\S*$/",
    "mer_trace"=>"/^\S*$/",
    "split_data"=>"/^\S*$/",
    "card_id"=>"/^\S{0,256}$/",
    "pass_wd"=>"/^\S{0,256}$/",
    "identity_type"=>"/^\S{0,256}$/",
    "identity_code"=>"/^\S{0,256}$/",
    "card_holder"=>"/^\S{0,256}$/",
    "cust_name"=>"/^\S{0,32}$/",
    "recv_account"=>"/^\S*$/",
    "recv_user_name"=>"/^\S*$/",
    "recv_gate_id"=>"/^\S*$/",
    "verify_code"=>"/^\S{0,8}$/",
    "mer_cust_id"=>"/^\S{0,32}$/",
    "usr_busi_agreement_id"=>"/^\S{0,64}$/",
    "usr_pay_agreement_id"=>"/^\S{0,64}$/",
    "identity_holder"=>"/^\S{0,256}$/",
    "split_refund_list"=>"/^\S*$/",
    "split_cmd"=>"/^\S*$/",
    "settle_type"=>"/^\S*$/",
    //新增
    //"scancode_type"=>"/WECHAT|ALIPAY|QQ/",
    "auth_code"=>"",
    "use_desc"=>"",
    // "last_four_cardid"=>"[0-9]{4}",4,false),  
    "bank_account"=>"",
    "account_name"=>"",
    // "bank_card_type"=>"",16,false),
);

    



}