<?php
require_once 'common.php';

/**
 * 商户请求平台参数处理
 */
Class ReqDataUtil{
	/**
	 * @param $map      HashMap 参数Map
	 * @param $method   string  http请求方式 get / post
	 * @return ReqData  校验加密后的请求参数
	 */
	public function makeRequestData($map,$method){
		$mp = clone $map;
		$log = new Logger();
		$log->logInfo("--------------------log start---------------------");
		$funcode = StringUtil::trim($mp->get("service"));
		$log->logInfo("[UMF SDK]本次请求 service = " . $funcode );
		$log->logInfo("[UMF SDK]本次请求原始参数 " .$log->logArrToStr($mp->getInnerArr()));
		//对请求数据进行有效性验证
		CheckReqDataAndEncrypt::doCheck ( $mp );
		//敏感字段加密
		$mp=CheckReqDataAndEncrypt::doEncrypt( $mp );
		//获取请求数据签名密文串
		$sign = $this->getSignData($mp);
		$log->logInfo ("[UMF SDK]本次请求方式 " . $method );
		//获取平台URL
		$url = UMF_PAY_REQ_URL;
		$reqData = new ReqData ();
		//获取GET方式请求数据对象
		if ($method == UMF_METHOD_GET) {
			//获取请求参数
			$param = StringUtil::getPlainSortByAndWithSignType($mp);
			$reqData->setUrl($url . "?" . $param . '&sign=' . urlencode($sign));
		} //获取POST方式请求数据对象
		else if ($method == UMF_METHOD_POST) {
			$reqData->setUrl ( $url );
			$mp->put ( "sign", $sign );
			$reqData->setField ( $mp );
			$log->logInfo("[UMF SDK]本次请求原始参数处理后参数 " .$log->logArrToStr($mp->getInnerArr()));
		}
		return $reqData;
	}

	/**
	 * 获取签名密文串
	 */
	private function getSignData($map) {
		$log = new Logger ();
		$plain = StringUtil::getPlainSortByAndWithoutSignType($map);
		$merId = $map->get('mer_id');
		$sign = SignUtil::sign($plain,$merId);
		return $sign;
	}
}


/**
 * API请求数据包装类
 */
Class ReqData{
	private $url;
	private $field;

	function setUrl($url){
		$this->url = $url;
	}
	
	function setField($field){
		$this->field = $field;
	}

	function getUrl(){
		return $this->url;
	}
	
	function getField(){
		return $this->field;
	}
}

?>