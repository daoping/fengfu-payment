PHP开发指南
1.本PHP SDK采用PHP5.1.6标准，请确认已安装PHP5.1.6及以上运行环境,类Linux系统需要按照openssl扩展、curl扩展
2.下载服务端PHP SDK,将SDK连同目录结构导入工程中
3.实例化UmfService，初始化传入商户号、私钥文件路径
4.如果商户需要增加私钥，可以调用addMerPrivateCertMap，传入商户号和新的私钥路径
5.在config.php中配置log输出路径和是否打印调试错误信息
6.查看API使用方法，参见 UmfApiTest ，在终端执行如下命令
[php的可执行命令] [cli2cgi.php路径] UmfApiTest 测试方法名
如
C:\xampp\php\php.exe C:\UMF_Server_SDK_PHP\umfPayService\cli2cgi.php UmfApiTest test_activeScanPaymentMap


