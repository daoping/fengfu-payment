<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

// +----------------------------------------------------------------------
// | 应用设置
// +----------------------------------------------------------------------
error_reporting(E_ERROR | E_WARNING | E_PARSE);
define("MAX_MENU_LENGTH",'3');
define("MAX_SUB_MENU_LENGTH","5");
define('DS', DIRECTORY_SEPARATOR);
defined('APP_PATH') or define('APP_PATH', dirname($_SERVER['SCRIPT_FILENAME']) . DS);
defined('ROOT_PATH') or define('ROOT_PATH', dirname(realpath(APP_PATH)) . DS);
return [
    // 应用名称
    'app_name'               => '',
    // 应用地址
    'app_host'               => '',
    // 应用调试模式
    'app_debug'              => true,
    // 应用Trace
    'app_trace'              => false,
    // 是否支持多模块
    'app_multi_module'       => true,
    // 入口自动绑定模块
    'auto_bind_module'       => false,
    // 注册的根命名空间
    'root_namespace'         => [],
    // 默认输出类型
    'default_return_type'    => 'html',
    // 默认AJAX 数据返回格式,可选json xml ...
    'default_ajax_return'    => 'json',
    // 默认JSONP格式返回的处理方法
    'default_jsonp_handler'  => 'jsonpReturn',
    // 默认JSONP处理方法
    'var_jsonp_handler'      => 'callback',
    // 默认时区
    'default_timezone'       => 'Asia/Shanghai',
    // 是否开启多语言
    'lang_switch_on'         => false,
    // 默认全局过滤方法 用逗号分隔多个
    'default_filter'         => 'filter_default',
    // 默认语言
    'default_lang'           => 'zh-cn',
    // 应用类库后缀
    'class_suffix'           => false,
    // 控制器类后缀
    'controller_suffix'      => false,

    // +----------------------------------------------------------------------
    // | 模块设置
    // +----------------------------------------------------------------------

    // 默认模块名
    'default_module'         => 'Home',
    // 禁止访问模块
    'deny_module_list'       => ['common'],
    // 默认控制器名
    'default_controller'     => 'Index',
    // 默认操作名
    'default_action'         => 'index',
    // 默认验证器
    'default_validate'       => '',
    // 默认的空模块名
    'empty_module'           => 'home',
    // 默认的空控制器名
    'empty_controller'       => 'EmptyController',
    // 操作方法前缀
    'use_action_prefix'      => false,
    // 操作方法后缀
    'action_suffix'          => '',
    // 自动搜索控制器
    'controller_auto_search' => false,

    // +----------------------------------------------------------------------
    // | URL设置
    // +----------------------------------------------------------------------

    // PATHINFO变量名 用于兼容模式
    'var_pathinfo'           => 's',
    // 兼容PATH_INFO获取
    'pathinfo_fetch'         => ['ORIG_PATH_INFO', 'REDIRECT_PATH_INFO', 'REDIRECT_URL'],
    // pathinfo分隔符
    'pathinfo_depr'          => '/',
    // HTTPS代理标识
    'https_agent_name'       => '',
    // IP代理获取标识
    'http_agent_ip'          => 'X-REAL-IP',
    // URL伪静态后缀
    'url_html_suffix'        => 'html',
    // URL普通方式参数 用于自动生成
    'url_common_param'       => false,
    // URL参数方式 0 按名称成对解析 1 按顺序解析
    'url_param_type'         => 0,
    // 是否开启路由延迟解析
    'url_lazy_route'         => false,
    // 是否强制使用路由
    'url_route_must'         => false,
    // 合并路由规则
    'route_rule_merge'       => false,
    // 路由是否完全匹配
    'route_complete_match'   => false,
    // 使用注解路由
    'route_annotation'       => false,
    // 域名根，如thinkphp.cn
    'url_domain_root'        => '',
    // 是否自动转换URL中的控制器和操作名
    'url_convert'            => true,
    // 默认的访问控制器层
    'url_controller_layer'   => 'controller',
    // 表单请求类型伪装变量
    'var_method'             => '_method',
    // 表单ajax伪装变量
    'var_ajax'               => '_ajax',
    // 表单pjax伪装变量
    'var_pjax'               => '_pjax',
    // 是否开启请求缓存 true自动缓存 支持设置请求缓存规则
    'request_cache'          => false,
    // 请求缓存有效期
    'request_cache_expire'   => null,
    // 全局请求缓存排除规则
    'request_cache_except'   => [],
    // 是否开启路由缓存
    'route_check_cache'      => false,
    // 路由缓存的Key自定义设置（闭包），默认为当前URL和请求类型的md5
    'route_check_cache_key'  => '',
    // 路由缓存类型及参数
    'route_cache_option'     => '',

    // 默认跳转页面对应的模板文件
    'dispatch_success_tmpl'  => Env::get('think_path') . 'tpl/dispatch_jump.tpl',
    'dispatch_error_tmpl'    => Env::get('think_path') . 'tpl/dispatch_jump.tpl',

    // 异常页面的模板文件
    'exception_tmpl'         => Env::get('think_path') . 'tpl/think_exception.tpl',

    // 错误显示信息,非调试模式有效
    'error_message'          => '页面错误！请稍后再试～',
    // 显示错误信息
    'show_error_msg'         => false,
    // 异常处理handle类 留空使用 \think\exception\Handle
    'exception_handle'       => '',

    //分页配置
    'paginate'               => [
        'type'      => 'clt\Bootstrap',
        'var_page'  => 'page',
        'list_rows' => 15,
        'page_size'=>5, //页码数量
        'page_button'=>[
            'total_rows'=>true, //是否显示总条数
            'turn_page'=>true, //上下页按钮
            'turn_group'=>true, //上下组按钮
            'first_page'=>true, //首页
            'last_page'=>true  //尾页
        ]
    ],
    'pageSize'=>15,
    //自定义配置
    'sys_name' =>'支付',
    //文件上传
    'addwater'=>false,
    'watertext'=>'支付',
    'version'=>'6.0',
    //加密盐
    'salt' => 'rqabeE43YtkjKUh7hI6Zzh*!OA@xg2B%!',
    "adapay"=>[
        "rsa_public_key"=>"MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDeuWeoJP9bs+Gxmek5yelzrxpMQ5Ka97p75nANbJTd2Y2wC+HLe8PWDWUhZUcrAtPsJXk7Tq/rZoadJiByB3DcEplt+QVL9OPJz7W7895cNlUzZ4Cz/BrZV8L3G82CLsrTQqcty1kUVyx/XbyhIM8TSlDB7SgG5IQhawzegGAybwIDAQAB",
        "rsa_private_key"=>"MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAN65Z6gk/1uz4bGZ6TnJ6XOvGkxDkpr3unvmcA1slN3ZjbAL4ct7w9YNZSFlRysC0+wleTtOr+tmhp0mIHIHcNwSmW35BUv048nPtbvz3lw2VTNngLP8GtlXwvcbzYIuytNCpy3LWRRXLH9dvKEgzxNKUMHtKAbkhCFrDN6AYDJvAgMBAAECgYEAoDP0X3ndQ/TrjngKzkpx5Yg5/klMlrNCLATg7IcBvWHTDKI/U8I+ZyVTbOYQftuV2/JIdWRwHX6trTPTerHnds1KU5sGt26L2SsVrr5l4QWv2c0DSJohqTHBkJslD6TmTRNe0FcBhq7rCYhz5MuxyX31QE0i26lfITYHqJL68IECQQDy053uZ3PlT2rOWJ+5827gsqbnRpPlsf27/xH/8qnfX6pxepaDVVP1NQqpOYJqAF+k7tbIIMCPwYAWnMbXRLdxAkEA6s6a/NqF/H1IGAbL9ggJ1jpDAkYhDq+Rfo4ebkLWWDSlmpuYOirSNuc6CQxr2l/5vvp8PPihIUjRZT8Sf31X3wJAY+Fxi9eXz1G8YAYKJUWhbOFvT6AzQ37UHJkNgVcCUrQZ+rVu5bSaZTkdGoBiSVHABaqMRCNcsYTpN+Pi0jccsQJBAOJKayP0ldxYAPrm0foylQhY7etrGsDEgzusS7eHwnsPgeoWugaU7jLrdQrzZZ33kQF09fbsxp0FSIiC2Jj1e5ECQGsLhSSUsgcj0rNcIM9hR9Y2pi9eX7h4ind5XEJnoDZXxLusofmNEfj5zOrGApNA3g/c2xONzp4fXD3YSEzoDQM="
    ],
    //汇付密钥配置
    "adapays"=>[
        "rsa_public_key"=>"MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCwN6xgd6Ad8v2hIIsQVnbt8a3JituR8o4Tc3B5WlcFR55bz4OMqrG/356Ur3cPbc2Fe8ArNd/0gZbC9q56Eb16JTkVNA/fye4SXznWxdyBPR7+guuJZHc/VW2fKH2lfZ2P3Tt0QkKZZoawYOGSMdIvO+WqK44updyax0ikK6JlNQIDAQAB",
        "rsa_private_key"=>"MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAOcZ23rotGZJxPgQqYici7AHoQtYlDpOcGaSuj83qJc4hxlFFL8TxhQG80jVmy+6HifkGqUECclNfxFJnTBDQN0YRq7NPRKrdXf7sVf3NsGrUZ42JBr1Zj4CGUUq7GSdTNBv0N5XyLysH8z94WtEiAHQ/trP5rCivH3lNolSyXCbAgMBAAECgYEAmeAwsL0DaVxTJ21IxOqfZB0gUQKw58XUt6eZFYDE47PC2MOTlcrfx/P8x5FbHENL0vnh9+AheAT+x48Bd+I/6/NMwxOBsaizOiI5os5LxbfU8fyZgEtpZV5RMIiYWXuOf4yECJ5eANEo2SwYumDmvWVxT9z9Ulh1VCeAJvZkQ0kCQQD223wClFZSByGCtDuf0+zXJSAHpuoiaqI03OrciY2xj/lDaEcC3ArPf82av1OOdERlYSSC2m6QOUEa+X/BbrN9AkEA76j7p+sn/K/YUiQ9mXI8pupEIOqw6mRvGBuPN9jtb/VyUKwe3XEwv8ft8js9Z/yBqw2ZBmPDFelLdpvHqlC/9wJAKXa0XbqMxTptZobffPkbzwdtWduveBhxJm/1AxYXtrt4ItphExnitvfAkT2Ry1u8IUsv1srZx1/Wj7FYxCTUAQJARWomab58uH2eJDyG8vjZhGb6EKapU1p/qvXgh4Z3TyRCVgdkcTMlsYaQ7sYlxQan8AliNy2SBbiquNHqBPSahQJAN/8/AgDYbCBmALbGyO0PI7mvlf86uqkI9w4fypY9hBAWjAj77HBei9qAlvOENHZlFwpSyV6dDqLalPsO3AgOBg=="
    ],
];
